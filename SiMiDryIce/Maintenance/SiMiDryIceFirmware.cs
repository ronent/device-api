﻿using System;
using System.Runtime.Serialization;

namespace SiMiDryIce.Maintenance
{
    [Serializable]
    internal class SiMiDryIceTFirmware : SiMiBase.Maintenance.SiMiBaseFirmware
    {
        public override string Name { get { return Devices.V1.SiMiDryIceLogger.firmwareDeviceName; } }

        public SiMiDryIceTFirmware(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public SiMiDryIceTFirmware()
        {

        }
    }
}
