﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;

namespace SiMiDryIce.Sensors.V1
{
    [Serializable]
    public sealed class InternalNTC : Base.Sensors.Types.Temperature.PT100//   SiMiAPI.Sensors.V1.PT1K
    {
        #region Properties
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(100); }
        }

        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-100); }
        }

        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
       
        internal InternalNTC(SensorManagerV2 parent)
            : base(parent)
        {
        }

        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.InternalNTC();
        }
        #endregion
    }
}
