﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SiMiDryIce.Sensors.V1.Calibrations
{
    [Serializable]
    sealed class InternalNTC : Base.Sensors.Calibrations.GenericNTC
    {
        internal InternalNTC()
        {

        }

        #region Fields
        public override decimal DefaultCoeffA { get { return 0.0008832357m; } }

        public override decimal DefaultCoeffB { get { return 0.0002524509m; } }
        
        public override decimal DefaultCoeffC { get { return 0.0000001867763m; } }

        #endregion
        #region Not Supported
        internal override decimal Calibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        internal override decimal Decalibrate(decimal value)
        {
            throw new NotSupportedException();
        }
        #endregion
        #region Serialization & Deserialization
        protected InternalNTC(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        #endregion
    }
}
