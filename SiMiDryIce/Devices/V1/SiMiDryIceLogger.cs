﻿using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace SiMiDryIce.Devices.V1
{
    [Serializable]
    public sealed class SiMiDryIceLogger : Devices.SiMiDryIceLogger
    {
        #region Members
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

       // internal static readonly string firmwareDeviceName = Devices.SiMiDryIceLogger.firmwareDeviceName + " V1";
        #endregion
        #region Properties
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        internal SiMiDryIceLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiDryIceLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
