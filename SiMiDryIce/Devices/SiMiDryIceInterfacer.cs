﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using SiMiAPI.Modules;
using SiMiBase.Devices.Management;

namespace SiMiDryIce.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "SiMi DryIce")]
    internal class SiMiDryIceInterfacer : GenericSubDeviceInterfacer<SiMiDryIceLogger>
    {
        public const byte TYPE = 0x04;//0xA0
        public const byte BITS = 0x02;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public SiMiDryIceInterfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(SiMiBaseLoggerManager.VID, 0x0007) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.SiMiDryIceLogger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
