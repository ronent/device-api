﻿using System;
using SiMiAPI.DataStructures;
using SiMiBase.DataStructures.TSetup;
using SiMiBase.Devices.Types;

namespace SiMiDryIce.DataStructures
{
    [Serializable]
    public sealed class SiMiDryIceSetupConfiguration : SiMiLogSetupConfiguration, ITSensorSetup
    {
        #region Constructor
        public SiMiDryIceSetupConfiguration()
            : base()
        {
            TemperatureSensor = new TSensorSetup();
        }

        #endregion
        #region Properties
        public TSensorSetup TemperatureSensor { get; set; }

        #endregion
        #region Validation
       
        internal override void ThrowIfInvalidFor(SiMiBaseLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void checkSensors(SiMiBaseLogger device)
        {
            ThrowIfFalse(TemperatureSensor.TemperatureEnabled, "Temperature sensor must be enabled");
            TemperatureSensor.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}
