﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class DynamicSetup: SiMiBase.OpCodes.Common.DynamicSetup
    {
        public DynamicSetup(SiMiDryIceLogger device)
            : base(device)
        {

        }

        protected override void PopulateSetupBooleans()
        {
            
        }

        protected override void PopulateTime()
        {
            
        }

        protected override void PopulateInterval()
        {
           
        }

        protected override void PopulateDisplayConfiguration()
        {
            
        }

        protected override void PopulateNumberOfSamples()
        {
            
        }

        protected override void PopulatePdfOptions()
        {
            
        }

        protected override void PopulateRunDelay()
        {
            
        }

        protected override void PopulateTimerRun()
        {
           
        }

        public override byte[] SendOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override byte[] ReceiveOpCode
        {
            get { throw new NotImplementedException(); }
        }

        protected override void Parse()
        {
          
        }
    }
}
