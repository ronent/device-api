﻿using System;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : SiMiBase.OpCodes.Common.GetBoomerang
    {
        public GetBoomerang(SiMiDryIceLogger device)
            : base(device)
        {

        }
    }
}
