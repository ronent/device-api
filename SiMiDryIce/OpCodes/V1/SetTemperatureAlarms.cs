﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class SetTemperatureAlarms: SiMiBase.OpCodes.Common.SetTemperatureAlarms
    {
        public SetTemperatureAlarms(SiMiDryIceLogger device)
            : base(device)
        {

        }

        protected override void PopulateHighAlarms()
        {
            throw new NotImplementedException();
        }

        protected override void PopulateLowAlarms()
        {
            throw new NotImplementedException();
        }

        public override byte[] SendOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override byte[] ReceiveOpCode
        {
            get { throw new NotImplementedException(); }
        }

        protected override void Parse()
        {
            throw new NotImplementedException();
        }

        protected override void Populate()
        {
            throw new NotImplementedException();
        }
    }
}
