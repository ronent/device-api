﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : SiMiBase.OpCodes.Common.SetCalibration
    {
        public SetCalibration(SiMiDryIceLogger device)
            : base(device)
        {

        }

        protected override void PopulateGainOffset()
        {
            throw new NotImplementedException();
        }
    }
}
