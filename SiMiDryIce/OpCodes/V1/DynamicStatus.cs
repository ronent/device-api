﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class DynamicStatus: SiMiBase.OpCodes.Common.DynamicStatus
    {
        public DynamicStatus(SiMiDryIceLogger device)
            : base(device)
        {

        }

        public override byte[] SendOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override byte[] ReceiveOpCode
        {
            get { throw new NotImplementedException(); }
        }

        protected override void Parse()
        {
            throw new NotImplementedException();
        }
    }
}
