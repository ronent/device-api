﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiDryIce.Devices;

namespace SiMiDryIce.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang: SiMiBase.OpCodes.Common.SetBoomerang
    {
        public SetBoomerang(SiMiDryIceLogger device)
            : base(device)
        {

        }
    }
}
