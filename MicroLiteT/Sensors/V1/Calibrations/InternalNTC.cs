﻿using System;
using System.Runtime.Serialization;

namespace MicroLite2T.Sensors.V1.Calibrations
{
    /// <summary>
    /// Internal NTC Sensor Calibration.
    /// </summary>
    [Serializable]    
    sealed class InternalNTC : Base.Sensors.Calibrations.GenericNTC
    {
        internal InternalNTC()
        {

        }

        #region Fields
        public override decimal DefaultCoeffA { get { return 0.000883235736449016m; } }

        public override decimal DefaultCoeffB { get { return 0.000252450876683081m; } }

        public override decimal DefaultCoeffC { get { return 0.00000018677625301m; } }

        #endregion
        #region Not Supported
        /// <summary>
        /// Calibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Calibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Decalibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Decalibrate(decimal value)
        {
            throw new NotSupportedException();
        }
        #endregion
        #region Serialization & Deserialization
        protected InternalNTC(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
