﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroLogAPI.OpCodes.V1.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
