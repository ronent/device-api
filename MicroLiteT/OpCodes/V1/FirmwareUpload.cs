﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUpload : MicroLogAPI.OpCodes.V1.FirmwareUpload
    {
        #region Constructors
        public FirmwareUpload(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
