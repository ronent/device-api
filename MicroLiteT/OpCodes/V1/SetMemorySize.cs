﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class SetMemorySize : MicroLogAPI.OpCodes.V1.SetMemorySize
    {
        #region Constructors
        public SetMemorySize(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
