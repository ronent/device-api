﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroLogAPI.OpCodes.V1.SetSN
    {
        #region Constructors
        public SetSN(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
