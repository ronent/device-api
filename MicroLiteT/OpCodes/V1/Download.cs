﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroLogAPI.OpCodes.V1.Download
    {
        #region Constructors
        public Download(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
