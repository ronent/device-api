﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroLogAPI.OpCodes.V1.SetCalibration
    {
        #region Constructors
        public SetCalibration(MicroLite2TLogger device)
            : base(device)
        { }
        #endregion
    }
}
