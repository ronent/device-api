﻿using MicroLite2T.Devices;
using System;


namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroLogAPI.OpCodes.V1.Status
    {
        #region Constructors
        public Status(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
