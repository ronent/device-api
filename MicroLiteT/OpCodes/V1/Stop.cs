﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class Stop : MicroLogAPI.OpCodes.V1.Stop
    {
        #region Constructors
        public Stop(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
