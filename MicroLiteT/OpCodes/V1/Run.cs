﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class Run : MicroLogAPI.OpCodes.V1.Run
    {
        #region Constructors
        public Run(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
