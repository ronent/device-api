﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroLogAPI.OpCodes.V1.FirmwareUploadManager
    {
        #region Constructors
        public FirmwareUploadManager(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
