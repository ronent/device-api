﻿using Base.OpCodes;
using MicroLite2T.DataStructures;
using MicroLite2T.Devices;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroLogAPI.OpCodes.V1.Setup
    {
        #region Properties
        new public MicroLite2TSetupConfiguration Configuration
        {
            get { return base.Configuration as MicroLite2TSetupConfiguration; }
            set { base.Configuration = value; }
        }
        #endregion
        #region Configuration Properties
        protected override bool TemperatureEnabled { get { return Configuration.TemperatureSensor.TemperatureEnabled; } }
        protected override Alarm TemperatureAlarm { get { return Configuration.TemperatureSensor.TemperatureAlarm; } }
        protected override bool StopOnDisconnect { get { return false; } }
        #endregion
        #region Constructors
        public Setup(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
