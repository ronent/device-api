﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : MicroLogAPI.OpCodes.V1.DefaultCalibration
    {
        #region Constructors
        public DefaultCalibration(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
