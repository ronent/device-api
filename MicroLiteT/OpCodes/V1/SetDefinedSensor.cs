﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class SetDefinedSensor : MicroLogAPI.OpCodes.V1.SetDefinedSensor
    {
        #region Constructors
        public SetDefinedSensor(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
