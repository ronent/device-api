﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : MicroLogAPI.OpCodes.V1.GetCalibration
    {
        #region Constructors
        public GetCalibration(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
