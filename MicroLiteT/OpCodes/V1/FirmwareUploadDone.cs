﻿using System;
using MicroLite2T.Devices.V1;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    public class FirmwareUploadDone : MicroLogAPI.OpCodes.V1.FirmwareUploadDone
    {
        #region Constructors
        public FirmwareUploadDone(MicroLite2TDevice device)
            : base(device)
        {
        }
        #endregion
    }
}
