﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroLogAPI.OpCodes.V1.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }

}
