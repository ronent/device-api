﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : MicroLogAPI.OpCodes.V1.OnlineDataPacket
    {
        #region Constructors
        public OnlineDataPacket(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
