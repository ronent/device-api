﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : MicroLogAPI.OpCodes.V1.OnlineTimeStamp
    {
        #region Constructors
        public OnlineTimeStamp(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
