﻿using MicroLite2T.Devices.V1;
using System;

namespace MicroLite2T.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : MicroLogAPI.OpCodes.V1.DataPacketWithTimeStamp
    {
        #region Constructors
        public DataPacketWithTimeStamp(MicroLite2TLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
