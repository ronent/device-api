﻿using System;
using System.Runtime.Serialization;

namespace MicroLite2T.Log
{
    /// <summary>
    /// MicroLite II Temperature Device Firmware.
    /// </summary>
    [Serializable]
    internal class MicroLite2TFirmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return "MicroLite II"; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2TFirmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public MicroLite2TFirmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2TFirmware"/> class.
        /// </summary>
        public MicroLite2TFirmware()
        {

        }
    }
}
