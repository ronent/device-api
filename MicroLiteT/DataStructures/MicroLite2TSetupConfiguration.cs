﻿using Base.Devices;
using MicroLogAPI.DataStructures;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;

namespace MicroLite2T.DataStructures
{
    /// <summary>
    /// Setup configuration for MicroLite II Temperature.
    /// </summary>
    [Serializable]
    public sealed class MicroLite2TSetupConfiguration : MicroLogSetupConfiguration, ITSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2TSetupConfiguration"/> class.
        /// </summary>
        public MicroLite2TSetupConfiguration()
            : base()
        {
            TemperatureSensor = new TSensorSetup();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        public TSensorSetup TemperatureSensor { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Validates the configuration and throws exception for error.
        /// </summary>
        /// <param name="device">The device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(TemperatureSensor.TemperatureEnabled, "Temperature sensor must be enabled");
            TemperatureSensor.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}