﻿using Base.OpCodes;
using Base.Sensors.Management;
using Maintenance;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;

using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MicroLite2T.DataStructures
{
    [Serializable]
    public class MicroLite2TSetupConfiguration : SetupConfiguration, ITSensorSetup
    {
        public TSensorSetup TemperatureSensor { get; set; }

        public MicroLite2TSetupConfiguration()
            :base()
        {
            TemperatureSensor = new TSensorSetup();
        }
    }
}