﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Features;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace MicroLite2T.Devices
{
    /// <summary>
    /// MicroLite II Temperature Device.
    /// </summary>
    [Serializable]
    public class MicroLite2TLogger : MicroLiteLogger
    {
        #region Members
        private static readonly string deviceTypeName = "MicroLite II Temperature";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return MicroLite2TInterfacer.TYPE; } }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return MicroLite2TInterfacer.BITS; } }
        
        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[]
                {
                    MicroXFeature.STOP_ON_KEY_PRESS,
                    MicroXFeature.SHOW_MINMAX,
                    MicroXFeature.SET_MEMORY_SIZE,
                    MicroXFeature.LED_ON_ALARM,                    
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new eSensorType[] { eSensorType.InternalNTC };
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2TLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal MicroLite2TLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2TLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLite2TLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
