﻿using Base.Devices.Management;
using MicroLogAPI.Modules;
using MicroXBase.Modules;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using System;
using Base.Devices.Management.DeviceManager.HID;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLite2T.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "MicroLite II Temperature")]
    internal class MicroLite2TInterfacer : GenericSubDeviceInterfacer<MicroLite2TLogger>
    {
        public const byte TYPE = 0xA0;
        public const byte BITS = 0x02;
                         
        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public MicroLite2TInterfacer()
            :base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x10C4, 0xEA70), new DeviceID(MicroXLoggerManager.VID, 0x0002) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.MicroLite2TLogger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
