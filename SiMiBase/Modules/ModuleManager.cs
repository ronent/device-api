﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices.Management.DeviceManager.HID;
using Base.Modules.Interfacer;
using Log4Tech;
using SiMiBase.Devices.Management;

namespace SiMiBase.Modules
{
    public class ModuleManager : IDisposable
    {
        #region Static
        private static ModuleManager instance = new ModuleManager();

        internal static List<IDeviceInterfacer> Modules
        {
            get
            {
                return (from module in instance.modules
                        select module.Value).ToList();
            }
        }

        #endregion
        #region Fields/Properties
        private CompositionContainer _container;

#pragma warning disable
        [ImportMany]
        IEnumerable<Lazy<IDeviceInterfacer, IDeviceInterfacerMetadata>> modules;
#pragma warning enable

        #endregion
        #region Initializers
        internal static void Start(SiMiBaseLoggerManager parent)
        {
            instance.loadModuleDLLs(parent);
        }

        private void loadModuleDLLs(SiMiBaseLoggerManager parent)
        {
            var catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new DirectoryCatalog(".", "ModuleSiMi*.dll"));
            
            _container = new CompositionContainer(catalog);

            try
            {
                _container.ComposeExportedValue(parent as IHIDDeviceManager);
                _container.ComposeParts(this);
                goOverModules();
            }
            catch (Exception e)
            {
                Log.Instance.WriteError("[API] ==> Fail in loadModuleDLLs", this, e);
            }
        }

        private void goOverModules()
        {
            foreach (var module in modules)
                Log.Instance.Write("[API] ==> SiMi module [" + module.Metadata.Name + "] loaded successfully", this);
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        #endregion
    }
}
