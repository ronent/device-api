﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Modules.Interfacer;
using Infrastructure.Communication;

namespace SiMiBase.Modules.Interfacer
{
    internal interface IHIDLoggerInterfacer : IDeviceInterfacer
    {
        List<DeviceID> DeviceIDs { get; }
    }
}
