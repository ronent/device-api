﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Infrastructure.Communication;
using SiMiBase.Devices.Types;
using Base.Modules.Interfacer;

namespace SiMiBase.Modules.Interfacer
{
    internal abstract class SiMiBaseLoggerInterfacer<T> : GenericDeviceInterfacer<T>, IHIDLoggerInterfacer where T : SiMiBaseLogger
    {
        #region Properties
        public abstract List<DeviceID> DeviceIDs { get; }
        protected abstract byte Bits { get; }

        [Import]
        private IHIDDeviceManager parentDeviceManager;

        #endregion
        #region Constructor
        public SiMiBaseLoggerInterfacer()
            : base()
        {

        }

        #endregion
        #region IDeviceInterfacer
        protected override IDeviceManager ParentDeviceManager
        {
            get
            {
                return parentDeviceManager;
            }
        }

        public override bool CanCreate(ConnectionEventArgs e)
        {
            if (!(e is HIDConnectionEventArgs))
                return false;

            return DeviceIDs.Any(x => x.Equals((e as HIDConnectionEventArgs).DeviceID));
        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            return checkType(device as SiMiBaseLogger);
        }

        protected override Type[] LookUpCandidateTypes(GenericDevice device)
        {
            Assembly asm = this.GetType().Assembly;
            try
            {
                return asm.GetTypes()
                    .Where((t) => isOfType(t) && isOfSubtype(t, (device as SiMiBaseLogger).Status.DeviceType))
                    .ToArray();
            }
            catch { return null; }
        }

        #endregion
        #region Methods
        protected virtual Type GetSubType(byte subtype)
        {
            return typeof(T);
        }

        #endregion
        #region Private Methods
        private bool checkPrerequisites(SiMiBaseLogger genericDevice)
        {
            return genericDevice.Status.PCBAssembly == Bits
                && genericDevice.Status.DeviceType == Type;
        }

        private bool checkType(SiMiBaseLogger device)
        {
            return device.Status.DeviceType == Type;
            //return true;
        }

        private bool isOfSubtype(Type type, byte subtype)
        {
            return type.IsSubclassOf(GetSubType(subtype));
        }

        #endregion
    }
}
