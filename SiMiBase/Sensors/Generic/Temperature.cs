﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;
using Base.Sensors.Types;

namespace SiMiBase.Sensors.Generic
{
    [Serializable]
    public abstract class Temperature : GenericSensorV2
    {
        #region Constructors

        internal Temperature(SensorManagerV2 parent)
            : base(parent)
        {

        }

        #endregion
       
    }
}
