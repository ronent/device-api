﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Units;

namespace SiMiBase.Sensors.Generic
{
    [Serializable]
    public abstract class Humidity :  GenericSensorV2
    {
        #region Constructors
        internal Humidity(SensorManagerV2 parent)
            : base(parent)
        {
        }

        #endregion

        public override string Name
        {
            get { return "Relative Humidity"; }
        }

        public override IUnit Unit
        {
            get
            {
                return new Base.Sensors.Units.Humidity();
            }

            internal set { throw new NotSupportedException(); }
        }
        
    }
}
