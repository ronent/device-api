﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using Base.Devices.Types;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Types;
using SiMiBase.Devices.Types;

namespace SiMiBase.Sensors.Management
{
    [Serializable]
    public abstract class SiMiBaseSensorManager: SensorManagerV2
    {
         #region Properties
        new internal SiMiBaseLogger ParentLogger
        {
            get { return base.ParentLogger as SiMiBaseLogger; }
            set { base.ParentLogger = value; }
        }

        public CalibrationConfigurationV2 Calibration
        {
            get;
            protected set;
        }

        #endregion
        #region Constructors
        
        internal SiMiBaseSensorManager(GenericLoggerV2 parent)
            : base(parent)
        {

        }

        internal override void Initialize(GenericLoggerV2 parent)
        {
            base.Initialize(parent);

            Calibration = new CalibrationConfigurationV2(this);
            InitializeSensors();
            AssignCalibration();
        }

        #endregion
        #region Methods
        
        internal override void ParentLogger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            initializeSensors();
            AssignConfiguration();
        }

        internal void EnableDetachable(eSensorType type)
        {
            var sensor = GetDetachableByType(type);
            if (sensor != null)
            {
                DisableAllDetachables();
                sensor.Enabled = true;
            }
        }

        internal void DisableAllDetachables()
        {
            var all = Items.FindAll(x => x.Index == eSensorIndex.External1).ToArray();

            foreach (var sensor in all)
                sensor.Enabled = false;
        }
      
        private void AssignCalibration()
        {
            foreach (var entry in Calibration)
                if (Contains(entry.Key))
                    this[entry.Key].Calibration.Set(entry.Value);
                    //foreach (var sensor in this[entry.Key])
                    //    sensor.Calibration.Set(entry.Value);
        }

        private void initializeSensors()
        {
            foreach (var sensor in Items)
                sensor.Initialize();
        }

        internal abstract void AssignConfiguration();

        internal void AssignFixedSensor(eSensorIndex sensorIndex, bool enabled, AlarmV2 alarm)
        {
            var sensor = GetFixedByIndex(sensorIndex);
            AssignFixedSensor(sensor, enabled, alarm);
        }

        internal void AssignFixedSensor(GenericSensorV2 sensor, bool enabled, AlarmV2 alarm)
        {
            if (sensor != null)
            {
                sensor.Enabled = enabled;
                if (sensor.Enabled)
                    sensor.Alarm.Assign(alarm);
            }
        }

        private bool Contains(eSensorIndex sensorIndex)
        {
            return this[sensorIndex] != null;
        }

        internal CalibrationConfigurationV2 GetResetCalibration()
        {
            var config = new CalibrationConfigurationV2(this);
            addResetConfigurationFor(config, eSensorIndex.Temperature);
            addResetConfigurationFor(config, eSensorIndex.Humidity);
            addResetConfigurationFor(config, eSensorIndex.External1);

            return config;
        }

        private void addResetConfigurationFor(CalibrationConfigurationV2 config, eSensorIndex index)
        {
            var sensor = index < eSensorIndex.External1 ? GetFixedByIndex(index) : GetEnabledDetachable();
            var coeff = sensor == null ? new CalibrationCoefficients() : sensor.GetDefaultCalibration();
            config.Add(index, coeff);
        }

        #endregion
    }
}
