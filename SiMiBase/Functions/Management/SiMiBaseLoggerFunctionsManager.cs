﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures.Device;
using Base.Devices.Features;
using Base.Functions.Management;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Calibrations;
using Infrastructure.FunctionQueue;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Common;

namespace SiMiBase.Functions.Management
{
    [Serializable]
    public abstract class SiMiBaseLoggerFunctionsManager : LoggerFunctionsManagerV2
    {
     
        #region Constructor

        protected SiMiBaseLoggerFunctionsManager(SiMiBaseLogger logger)
            : base(logger)
        {
            NotifyNextStatus = true;
        }

        protected override void SubscribeOpCodesEvents()
        {
//            if (Device.OpCodes.FirmwareUpload != null)
//                Device.OpCodes.FirmwareUpload.OnFirmwareUpdateProgressReported += (sender, e) =>
//                {
//                    InvokeFWUpdateProgressReport(sender, e);
//                };

            if (Device.OpCodes.FunctionQueue != null)
                Device.OpCodes.FunctionQueue.FunctionAddedToQueue += (sender, e) =>
                {
                    if (e.SerialNumber != Device.Status.SerialNumber)
                        return;

                    switch (e.FunctionType)
                    {
                        case eFunctionType.Download:
                            InvokeOnDownloadProgressReported(this, new ProgressReportEventArgs(e.SerialNumber, eStage.Queued));
                            break;
                        case eFunctionType.FirmwareUpdate:
                            InvokeFWUpdateProgressReport(this, new ProgressReportEventArgs(e.SerialNumber, eStage.Queued));
                            break;
                    }
                };
        }

        #endregion
        #region Properties
        new protected internal SiMiBaseLogger Device { get { return base.Device as SiMiBaseLogger; } set { base.Device = value; } }

        public override bool IsBusy
        {
            get
            {
                return base.IsBusy || Device.OpCodes.FunctionQueue.IsBusy(Device.Status.SerialNumber);
            }
        }

        public override bool IsUpdatingFirmware
        {
            get
            {
//                if (Device.OpCodes.FirmwareUpload != null)
//                    return Device.OpCodes.FirmwareUpload.IsRunning;
                return false;
            }
        }

        public override bool IsRunning { get { return Device.Status.IsRunning; } }

        public override bool PushToRunMode { get { return Device.Status.PushToRunMode; } }

        internal bool NotifyNextStatus { get; set; }

        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get {
                return LoggerFunctionsManager.AvailableFunctions;
            }
        }

        #endregion
        #region Override Functions

        public override Task<Result> Run()
        {
            var result = CheckRunningAndBusy().Result;
            return !result.IsOK
                ? Task.FromResult(result)
                : Device.OpCodes.Run.Invoke();
        }

        public override Task<Result> Stop()
        {
            if (!(IsRunning))
                return Task.FromResult(new Result(eResult.ILLEGAL_CALL) {MessageLog = NOT_RUNNING_ERROR});
            return IsBusy
                ? Task.FromResult(new Result(eResult.BUSY) {MessageLog = BusyErrorMessage})
                : Device.OpCodes.Stop.Invoke();
        }

        public override Task< Result> Download()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        public override Task<Result> SendSetup(BaseDeviceSetupConfiguration configuration)
        {
            if (IsRunning)
                return Task.FromResult(new Result(eResult.ILLEGAL_CALL) {MessageLog = RUNNING_ERROR});
            if (IsBusy)
                return Task.FromResult(new Result(eResult.BUSY) {MessageLog = BusyErrorMessage});

            try
            {
//                var result = Device.OpCodes.StaticSetup.Invoke(configuration as SiMiStaticSetupConfiguration);
//                
//                if(!result.IsOK)
//                    return Task.FromResult(result);
                
                var result = Device.OpCodes.DynamicSetup.Invoke(configuration as SiMiBaseSetupConfiguration);
                if (!result.IsOK)
                    return Task.FromResult(result);

                result.SynthesizeResultAndThrowOnError(DoAfterSetup());

                result.SynthesizeResultAndThrowOnError(Device.OpCodes.DynamicSetup.RunSetBoomerang());
                result.SynthesizeResultAndThrowOnError(Device.OpCodes.DynamicSetup.CheckAgainstStatus());

                return Task.FromResult(result);
            }
            catch (Exception ex)
            {
                return Task.FromResult(new Result(ex));
            }
        }

        protected abstract Result DoAfterSetup();

        internal override Task<Result> GetDeviceType()
        {
            return GetStatus();
        }

        public override Task<Result> UploadFirmware()
        {
            var result = CheckRunningAndBusy().Result;
            if (!result.IsOK)
                return Task.FromResult(result);

            try
            {
                Device.OpCodes.FunctionQueue.Add(eFunctionType.FirmwareUpdate, Device.Status.SerialNumber);

                InvokeFWUpdateProgressReport(this,
                    new ProgressReportEventArgs(Device.Status.SerialNumber, eStage.Started));

//                    result = Device.OpCodes.FirmwareUploadManager.Start();

                if (!result.IsOK)
                    Device.OpCodes.FunctionQueue.Remove(Device.Status.SerialNumber);

                return Task.FromResult(result);
            }
            catch (AddException ae)
            {
                return Task.FromResult(new Result(eResult.BUSY) {MessageLog = BusyErrorMessage});
            }
            catch (Exception ex)
            {
                return Task.FromResult(new Result(ex));
            }
        }

        public override Task<Result> ResetCalibration()
        {
            var result = CheckRunningAndBusy().Result;
            if (!result.IsOK)
                return Task.FromResult(result);

            var calibration = Device.Sensors.GetResetCalibration();
            return SendCalibration(calibration);
        }

        public override Task<Result> SendCalibration(CalibrationConfigurationV2 calibration)
        {
            var result = CheckRunningAndBusy().Result;
            return !result.IsOK
                ? Task.FromResult(result)
                : Device.OpCodes.SetCalibration.Invoke(calibration);
        }

        #endregion
        #region New Functions

        internal Task<Result> GetBoomerang()
        {
            if (!Device.Status.BoomerangEnabled) return Task.FromResult(new Result(eResult.OK));
            var result = Device.OpCodes.GetBoomerang.Invoke().Result;

            // fixFor0xFFBug
            if (result.LastException != null && result.LastException.Message.Equals("Boomerang is disabled."))
                return Task.FromResult(Result.OK);
            return Task.FromResult(result);
        }

        #endregion
        #region Methods
        protected override void InvokeOnStatus()
        {
            if (NotifyNextStatus)
                base.InvokeOnStatus();
            else
                NotifyNextStatus = true;
        }

        internal override void InvokeOnDownloadCompleted(object sender, DownloadCompletedArgs e)
        {
            Device.OpCodes.FunctionQueue.Remove(Device.Status.SerialNumber);
            base.InvokeOnDownloadCompleted(sender, e);
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {
                Device.OpCodes.FunctionQueue.Remove(Device.Status.SerialNumber);

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public SiMiBaseLoggerFunctionsManager(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion


        public override bool IsDownloading
        {
            get { throw new NotImplementedException(); }
        }

        
    }
}
