﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiBase.DataStructures
{
    [Serializable]
    public class PdfSetup
    {
        public bool GeneratePdf { get; internal set; }
        public bool AddDataPages { get; internal set; }
        public bool AddLightSensorInfo { get; internal set; }
    }
}
