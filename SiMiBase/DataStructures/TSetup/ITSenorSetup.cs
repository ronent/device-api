﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiBase.DataStructures.TSetup
{
    public interface ITSensorSetup
    {
      
        TSensorSetup TemperatureSensor { get; }
    }
}
