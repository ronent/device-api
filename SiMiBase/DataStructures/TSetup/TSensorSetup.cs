﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Management;
using SiMiBase.Devices.Types;

namespace SiMiBase.DataStructures.TSetup
{
    [Serializable]
    public class TSensorSetup : ISensorSetup
    {
        public TSensorSetup()
        {
            TemperatureAlarm = new AlarmV2();
            TemperatureEnabled = true;
        }

        #region Properties
        public AlarmV2 TemperatureAlarm { get; set; }

        public virtual bool TemperatureEnabled { get; set; }

        #endregion
        #region Methods
        public virtual void ThrowIfInvalidFor(SiMiBaseLogger dummyDevice)
        {
            ThrowIfInvalidDevice(dummyDevice);
            ThrowIfInvalidAlarms(dummyDevice);
        }

        public virtual void ThrowIfInvalidDevice(SiMiBaseLogger dummyDevice)
        {
            if (!(dummyDevice.Status is ITSensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support temperature sensor setup");
        }

        public virtual void ThrowIfInvalidAlarms(SiMiBaseLogger dummyDevice)
        {
            if (TemperatureEnabled && TemperatureAlarm.Enabled)
                TemperatureAlarm.ThrowIfNotValid(dummyDevice.Sensors.GetFixedByIndex(eSensorIndex.Temperature));
        }

        #endregion
        #region Equals
        public override bool Equals(object obj)
        {
            TSensorSetup other = obj as TSensorSetup;
            if (other == null) return false;

            return TemperatureAlarm.Equals(other.TemperatureAlarm)
                && TemperatureEnabled == other.TemperatureEnabled;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Temperature Enabled: " + TemperatureEnabled + " " + TemperatureAlarm;
        }
        #endregion
    }
}
