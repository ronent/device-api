﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.Devices.Types;

namespace SiMiBase.DataStructures
{
    public interface ISensorSetup
    {
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidFor(SiMiBaseLogger dummyDevice);

        /// <summary>
        /// Throws if invalid device.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidDevice(SiMiBaseLogger dummyDevice);

        /// <summary>
        /// Throws if invalid alarms.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidAlarms(SiMiBaseLogger dummyDevice);
    }
}
