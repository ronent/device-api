﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiBase.DataStructures
{
    [Serializable]
    public abstract class SiMiStaticSetupConfiguration
    {
        #region Fields
        public static readonly int AllowedCommentLength = 15;
        #endregion

        #region Properties
        
        public uint SerialNumber { get; set; }

        public string Comment { get; set; }

        #endregion

        #region Validation

        internal virtual void ThrowIfInvalidFor(SiMiBaseLogger device)
        {
            checkCommentLength(device);
            checkComment();
        }

        private void checkCommentLength(SiMiBaseLogger dummyDevice)
        {
            Comment = Comment.TrimStart(' ').TrimEnd(' ');
            ThrowIfFalse(Comment.Length <= AllowedCommentLength && Comment.Length > 0, "Comment length is empty or exceeds device limit");
        }

        private bool validateComment(string comment)
        {
            return comment.All(character => character >= 32 && character <= 125 && character != 34 && character != 38 && character != 39 && character != 92);
        }

        private void checkComment()
        {
            ThrowIfFalse(validateComment(Comment), "Invalid ASCII character");
        }

        internal void ThrowIfFalse(bool result, string msg)
        {
            if (!result)
                throw new Exception(msg);
        }
        #endregion


     
    }
}
