﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiBase.DataStructures
{
    interface ISiMiBaseSetupConfiguration
    {
        BoomerangConfiguration Boomerang { get; }

        bool BoomerangEnabled { get; }

        bool CyclicMode { get; }

        bool PushToRunMode { get; }

        int TimerRun { get; }

        sbyte UtcOffset { get; }

        bool TestMode { get; }
    }
}
