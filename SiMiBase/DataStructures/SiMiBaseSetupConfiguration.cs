﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using Base.DataStructures.Logger;
using Base.Devices;
using Log4Tech;
using SiMiBase.Devices.Types;

namespace SiMiBase.DataStructures
{
    public enum LCDConfigurationEnum : byte
    {
        AlwaysOn = 0x00,
        OnFor30Sec = 0x01,
        OnFor90Sec = 0x02,
        OnFor120Sec = 0x03,
    }

    [Serializable]
    public abstract class SiMiBaseSetupConfiguration : BaseLoggerSetupConfiguration, ISiMiBaseSetupConfiguration, IBoomerangable
    {
        #region Fields

        private bool testMode = false;

        #endregion
        #region Constructor
        public SiMiBaseSetupConfiguration()
            : base()
        {
            Initialize();
        }

       
        protected virtual void Initialize()
        {
            Boomerang = new BoomerangConfiguration();
        }

        #endregion
        #region Properties
        
        public bool BoomerangEnabled { get; set; }
       
        public BoomerangConfiguration Boomerang { get; set; }
      
        public int TimerRun { get; set; }
      
        public bool CyclicMode { get; set; }
       
        public bool PushToRunMode { get; set; }
        
        public sbyte UtcOffset { get; set; }
      
        internal virtual DateTime DeviceTime { get; set; }
    
        public bool TestMode
        {
            get { return testMode; }
            set
            {
                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                {
                    testMode = value;
                }
            }
        }

        #endregion
        #region Validation
      
        internal virtual void ThrowIfInvalidFor(SiMiBaseLogger device)
        {
           
            checkTimerRun();
            checkInterval();
            checkSensors(device);
            if (BoomerangEnabled)
                Boomerang.ThrowIfInvalid();

        }

        internal abstract void checkSensors(SiMiBaseLogger device);

        internal SiMiBaseLogger GetDummyDevice(SiMiBaseLogger device)
        {
            var dummy = GenericDevice.DeepCopy<SiMiBaseLogger>(device);
            adjustDummyStatus(dummy);

            return dummy;
        }

        internal virtual void adjustDummyStatus(SiMiBaseLogger device)
        {
            device.Status.FahrenheitMode = FahrenheitMode;
        }

        protected virtual void checkInterval()
        {
            ThrowIfFalse(Interval.TotalSeconds > 0, "Interval must by greater than 1");
            ThrowIfFalse(Interval.TotalSeconds < ushort.MaxValue, "Interval must by smaller than " + ushort.MaxValue);
        }

        private void checkTimerRun()
        {
            ThrowIfFalse(!(TimerRun>0 && PushToRunMode), "Can't set 'timer run' and 'push to run' together.");
            ThrowIfFalse((TimerRun>=0), "Timer run has invalid value.");

        }

        internal void ThrowIfFalse(bool result, string msg)
        {
            if (!result)
                throw new Exception(msg);
        }

        #endregion
        #region Equals
        internal void ThrowIfNotEquals(GenericConfiguration statusConfiguration)
        {
            var statusMembers = statusConfiguration.GetMembers();

            foreach (var setup in GetMembers())
            {
                try
                {
                    //Log.Instance.Write("Try to match {0} with status item",this,Log.LogSeverity.DEBUG, setup.Item1);

                    var match = statusMembers.FirstOrDefault(status => status.Item1 == setup.Item1);
                    //if found any
                    if (match != null)
                    {

                        if (match.Item2 == null)
                        {
                            if (match.Item2 != null)
                                throw new KeyNotFoundException(setup.Item1);
                        }
                        else if (match.Item2 is BoomerangConfiguration && !BoomerangEnabled)
                        {
                            continue;
                        }
                        else if (match.Item2 is uint)
                        {
                            if (match.Item1 == "TimerRun" && !(TimerRun > 0)
                                || match.Item1 == "DeviceTime")
                                continue;
                        }
                        else if (match.Item1 == "BatteryLevel" ||
                                 (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User &&
                                  match.Item1 == "SerialNumber"))
                            continue;
                        else if (!match.Item2.Equals(setup.Item2))
                            throwNotEqualException(setup, match);
                    }
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in throwNotEqual", this, ex);
                }
            }
        }

        private static void throwNotEqualException(Tuple<string, object> setup, Tuple<string, object> status)
        {
            //throw new KeyNotFoundException(setup.Item1, new Exception(string.Format("Setup: {0}{1}Status: {2}", setup.Item2, Environment.NewLine, status.Item2)));
            Log.Instance.Write("Property:{0} Setup value: {1}, Status value: {2}", "throwNotEqualException", Log.LogSeverity.WARNING,setup.Item1, setup.Item2, status.Item2);
        }

        public override bool Equals(object obj)
        {
            try
            {
                ThrowIfNotEquals(obj as SiMiBaseStatusConfiguration);
                return true;
            }
            catch { return false; }
        }
        
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
