﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiBase.DataStructures
{
    [Serializable]
    public class SiMiBaseVersion : GenericVersion
    {
        #region Constructors
        internal SiMiBaseVersion(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new internal SiMiBaseLogger ParentDevice
        {
            get { return base.ParentDevice as SiMiBaseLogger; }
            set { base.ParentDevice = value; }
        }

        public override byte PCBAssembly
        {
            get { return ParentDevice.Status.PCBAssembly; }
        }

        public override byte PCBVersion
        {
            get { return ParentDevice.Status.PCBVersion; }
        }

        public override byte FirmwareRevision
        {
            get { return ParentDevice.Status.FWRevision; }
        }

        public override Version Firmware
        {
            get { return ParentDevice.Status.FirmwareVersion; }
        }

        #endregion
    }
}
