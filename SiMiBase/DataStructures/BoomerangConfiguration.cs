﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.Misc;

namespace SiMiBase.DataStructures
{
    [Serializable]
    public sealed class BoomerangConfiguration : ICloneable
    {
        #region Consts
        internal const byte CONTACT_BREAK_BYTE = 0xff;

        internal const char CONTACT_BREAK_CHAR = '?';

        internal const byte INFO_BREAK_BYTE = (byte)INFO_BREAK_CHAR;

        internal const char INFO_BREAK_CHAR = ';';

        internal const byte NO_TIMEZONE = 0x1A;

        internal const int MAX_BANK0_SIZE = 61;

        const byte CELSIUS = 0x10;

        const byte FAHRENHEIT = 0x11;

        const int OPCODE_SIZE = 1;

        const int GETSET_SIZE = 1;

        const int BREAK_SIZE = 1;

        const int BANK_COUNT = 2;

        const int INFO_BYTE_FIELD_COUNT = 3;

        const string DEFAULT_AUTHOR = "USER";

        #endregion
        #region Constructors
        internal BoomerangConfiguration()
        {
            initialize();
        }

        private void initialize()
        {
            Contacts = new List<BasicContact>();
            Comment = string.Empty;
            CelsiusMode = true;

        }
        #endregion
        #region Properties
       
        public static int AllowedCommentAuthorLength
        {
            get
            {
                return SiMiBase.OpCodes.Management.SiMiBaseOpCodeManager.GENERIC_REPORT_SIZE - 1 -
                    OPCODE_SIZE - GETSET_SIZE - 4 * BREAK_SIZE - INFO_BYTE_FIELD_COUNT;
            }
        }

      
        public static int AllowedContactLength
        {
            //2 Banks contain up to 62 bytes
            get { return AllowedCommentAuthorLength * BANK_COUNT; }
        }

        
        public List<BasicContact> Contacts { get; set; }

       
        public string Comment { get; set; }

       
        public string Author { get; set; }

        
        public bool CelsiusMode { get; set; }

       
        public bool DisplayAlarmLevels { get; set; }

        internal byte CelsiusModeByte
        {
            get { return Convert.ToByte(CelsiusMode ? CELSIUS : FAHRENHEIT); }
            set { CelsiusMode = (value == CELSIUS); }
        }

       
        internal byte DisplayAlarmLevelsByte
        {
            get { return Convert.ToByte(DisplayAlarmLevels ? '1' : '0'); }
            set { DisplayAlarmLevels = (value == '1'); }
        }

        
        public bool Valid
        {
            get
            {
                try
                {
                    if (Author == string.Empty)
                        Author = DEFAULT_AUTHOR;

                    ThrowIfInvalid();
                    return true;
                }
                catch { return false; }
            }
        }

        #endregion
        #region Methods
      
        internal byte[] GetContactBytes()
        {
            List<byte> Bytes = new List<byte>();
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            if (Contacts != null)
                foreach (BasicContact Contact in Contacts)
                {
                    byte[] bytes = enc.GetBytes(Contact.EMail);
                    if (bytes.Length + Bytes.Count > AllowedContactLength)
                        break;
                    else
                    {
                        Bytes.AddRange(bytes);
                        Bytes.Add(CONTACT_BREAK_BYTE);
                    }
                }
            return Bytes.ToArray();
        }
        #endregion
        #region Validation
        
        internal void ThrowIfInvalid()
        {
            if (Comment.Length + Author.Length > AllowedCommentAuthorLength)
                throw new Exception("Comment + Author string exceed allowed maximum: " + AllowedCommentAuthorLength);
            else if (GetContactBytes().Count() < 1)
                throw new Exception("Boomerang contacts cannot be empty");
            else
            {
                foreach (var item in Contacts)
                {
                    if (!item.EMail.IsEmailValid())
                        throw new Exception("Boomerang email is not valid");
                }
            }
        }

        #endregion
        #region Object Overrides
       
        public override bool Equals(object obj)
        {
            BoomerangConfiguration boomerang = obj as BoomerangConfiguration;
            if (boomerang != null)
                return this.GetHashCode() == boomerang.GetHashCode();

            return false;
        }

      
        public override int GetHashCode()
        {
            return (int)(
                GetContactsHashCode() +
                Comment.GetHashCode() +
                Author.GetHashCode() +
                CelsiusMode.GetHashCode() +
                DisplayAlarmLevels.GetHashCode());
        }

       
        private int GetContactsHashCode()
        {
            return (from contact in Contacts
                    select contact.GetHashCode()).Sum();
        }

        
        public override string ToString()
        {
            StringBuilder str = new StringBuilder(Environment.NewLine);
            str.AppendLine("\tAuthor: " + Author);
            str.AppendLine("\tComment: " + Comment);
            str.AppendLine("\tCelsius: " + CelsiusMode);
            str.AppendLine("\tDisplay alarm levels: " + DisplayAlarmLevels);
            int index = 1;
            foreach (var contact in Contacts)
                str.AppendLine("\tContact #" + index++ + ": " + contact.EMail);

            return str.ToString();
        }

        #endregion
        #region IClonable
       
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
