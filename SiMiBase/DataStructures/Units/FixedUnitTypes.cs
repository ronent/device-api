﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiBase.DataStructures.Units
{
    /// <summary>
    /// Fixed Unit Types Supported By The Device.
    /// </summary>
    public enum ExternalSensorUnitEnum : byte { Custom = 0x00, mS, V, mA, pH, RH, C, F };

    /// <summary>
    /// Fixed Unit Types Class.
    /// </summary>
    public static class FixedUnitTypes
    {
        private static readonly string[] unitNames = { "", "mS", "V", "mA", "pH", "RH", "°C", "°F" };

        public static UserDefinedUnit[] DefaultUnits
        {
            get
            {
                var array = Enum.GetValues(typeof(ExternalSensorUnitEnum)).Cast<ExternalSensorUnitEnum>();
                return (from unitEnum in array
                        select new UserDefinedUnit(unitEnum)).ToArray();
            }
        }

        public static ExternalSensorUnitEnum Get(string typeName)
        {
            var index = Array.FindIndex(unitNames, x => x == typeName);
            return (ExternalSensorUnitEnum)index;
        }

        public static string Get(ExternalSensorUnitEnum type)
        {
            var index = (int)type;
            return unitNames[index];
        }
    }
}
