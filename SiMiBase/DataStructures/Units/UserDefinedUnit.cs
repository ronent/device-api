﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Units;

namespace SiMiBase.DataStructures.Units
{
    [Serializable]
    public class UserDefinedUnit : IUnit
    {
        private ExternalSensorUnitEnum type;

        public string Name { get; set; }

        public ExternalSensorUnitEnum Type
        {
            get { return type; }
            set
            {
                type = value;
                if (type != ExternalSensorUnitEnum.Custom)
                    Name = FixedUnitTypes.Get(type);
            }
        }

        internal UserDefinedUnit()
        {
        }
        
        internal UserDefinedUnit(ExternalSensorUnitEnum type)
        {
            Type = type;
        }

        #region Methods
        public void Set(byte unitByte)
        {
            if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), unitByte))
                Type = (ExternalSensorUnitEnum)unitByte;
            else
                Type = ExternalSensorUnitEnum.Custom;
        }

        #endregion
        #region Object Overrides
        public override bool Equals(object obj)
        {
            var other = obj as IUnit;

            if (other != null)
                return GetHashCode() == other.GetHashCode();

            return false;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode() + Type.GetHashCode();
        }

        public override string ToString()
        {
            return Type == ExternalSensorUnitEnum.Custom ? Type.ToString() : Name;
        }

        #endregion
    }
}
