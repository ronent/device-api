﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures.Logger;

namespace SiMiBase.DataStructures
{
    [Serializable]
    public class SiMiBaseStatusConfiguration : BaseLoggerStatusConfiguration, ISiMiBaseSetupConfiguration
    {
        #region Constructor
        
        internal SiMiBaseStatusConfiguration()
            : base()
        {
            Initialize();
        }

        private void Initialize()
        {
            Boomerang = new BoomerangConfiguration();
        }

        #endregion
        #region Properties
        public byte DeviceType { get; internal set; }

        #endregion
        #region SetupConfiguration
        public BoomerangConfiguration Boomerang { get; internal set; }

        public bool BoomerangEnabled { get; internal set; }

        public bool CyclicMode { get; internal set; }

        public bool PushToRunMode { get; internal set; }

        public int TimerRun { get; internal set; }

        public sbyte UtcOffset { get; internal set; }

        public bool TestMode { get; internal set; }

        public bool DeepSleepMode { get; internal set; }

        public bool UnitRequiresSetup { get; internal set; }

        public Version MinimumRequiredSWVersion { get; internal set; }

        internal int FirstPacketTime { get; set; }

        public LCDConfigurationEnum LCDConfiguration { get; internal set; }

        public bool StopOnKeyPress { get; internal set; }

        public byte TripNumber { get; internal set; }

        public byte NumberOfActivations { get; internal set; }

        public short NumberOfSamples { get; internal set; }

        public PdfSetup PdfSetupConfiguration { get; internal set; }

        public uint RunDelay { get; internal set; }
        #endregion
    }
}
