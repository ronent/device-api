﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiBase.DataStructures
{
    public interface IBoomerangable
    {
        BoomerangConfiguration Boomerang { get; set; }

        bool BoomerangEnabled { get; set; }
    }
}
