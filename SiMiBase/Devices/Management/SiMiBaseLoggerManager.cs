﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Infrastructure.Communication;
using Log4Tech;
using SiMiBase.Devices.Types;
using System.ComponentModel.Composition;
using Base.Modules.Interfacer;
using SiMiBase.Modules;
using SiMiBase.Modules.Interfacer;

namespace SiMiBase.Devices.Management
{
    [Export(typeof(IDeviceManager))]
    [ExportMetadata("Name", "SiMi Loggers Manager")]
    [Serializable]
    public class SiMiBaseLoggerManager : GenericDeviceManager<SiMiBaseLogger>, IHIDDeviceManager
    {
        #region Fields
        public const uint VID = 0x2999;

        #endregion
        #region Constructor
        internal SiMiBaseLoggerManager()
        {
            ModuleManager.Start(this);
        }

        #endregion
        #region IDeviceManager Methods
        public override bool OnDeviceFound(object sender, ConnectionEventArgs e)
        {
            bool toReturn = false;
            try
            {
                IDeviceInterfacer module = ModuleManager.Modules.FirstOrDefault(x => x.CanCreate(e));
                if (module != null)
                {
                    module.CreateFromIdAndReport(e);
                    toReturn = true;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On OnDeviceFound()", this, ex);
            }

            return toReturn;
        }

        public override bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod)
        {
            //Debug.Assert(typeof(E).IsSubclassOf(typeof(GenericDevice)));

            foreach (var module in ModuleManager.Modules)
                if (module.GetDeviceClassType() == typeof(E))
                {
                    module.Subscribe<E>(reportMethod);
                    return true;
                }

            return false;
        }

        #endregion
        #region IHIDDeviceManager
        public List<DeviceID> DeviceIDs
        {
            get
            {
                return (from module in ModuleManager.Modules
                        where module is IHIDLoggerInterfacer
                        from deviceid in (module as IHIDLoggerInterfacer).DeviceIDs
                        select deviceid).Distinct().ToList();
            }
        }

        #endregion
        #region Serialization & Deserialization
        protected SiMiBaseLoggerManager(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
