﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices.Features;

namespace SiMiBase.Devices.Features
{
    public enum SiMiFeatureEnum : byte
    {
        CYCLIC = 0x00,
        PUSH_TO_RUN,
        TIMER_RUN,
        STOP_ON_KEY_PRESS,
       // STOP_ON_DISCONNECT,
        TEST_MODE,
      //  SET_MEMORY_SIZE,
       // SHOW_MINMAX,
        DEEP_SLEEP,
      //  LED_ON_ALARM,
    }

   
    [Serializable]
    public sealed class SiMiBaseFeature : DeviceFeature
    {
        #region Ready-To-Use DeviceFeatures
        public static SiMiBaseFeature CYCLIC { get { return new SiMiBaseFeature(SiMiFeatureEnum.CYCLIC); } }

        public static SiMiBaseFeature PUSH_TO_RUN { get { return new SiMiBaseFeature(SiMiFeatureEnum.PUSH_TO_RUN); } }

        public static SiMiBaseFeature TIMER_RUN { get { return new SiMiBaseFeature(SiMiFeatureEnum.TIMER_RUN); } }

        public static SiMiBaseFeature STOP_ON_KEY_PRESS { get { return new SiMiBaseFeature(SiMiFeatureEnum.STOP_ON_KEY_PRESS); } }


        public static SiMiBaseFeature TEST_MODE { get { return new SiMiBaseFeature(SiMiFeatureEnum.TEST_MODE); } }



        public static SiMiBaseFeature DEEP_SLEEP { get { return new SiMiBaseFeature(SiMiFeatureEnum.DEEP_SLEEP); } }


        #endregion
        public SiMiFeatureEnum CurrentFeature
        {
            get;
            protected set;
        }

        public override byte Code
        {
            get { return (byte)CurrentFeature; }
        }

        public override string Text
        {
            get { return CurrentFeature.ToString(); }
        }

        internal SiMiBaseFeature(SiMiFeatureEnum code)
        {
            CurrentFeature = code;
        }

    }
}
