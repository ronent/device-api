﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Base.Devices.Types;
using Log4Tech;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Components;
using SiMiBase.Devices.Features;
using SiMiBase.Functions.Management;
using SiMiBase.OpCodes.Management;
using SiMiBase.Sensors.Management;

namespace SiMiBase.Devices.Types
{
    [Serializable]
    public abstract class SiMiBaseLogger : GenericLoggerV2
    {
        #region Properties
        protected abstract byte Type { get; }

        protected abstract byte Bits { get; }

        public virtual SiMiBaseFeature[] Features
        {
            get
            {
                return new[]
                { 
                    SiMiBaseFeature.CYCLIC,
                    SiMiBaseFeature.PUSH_TO_RUN,
                    SiMiBaseFeature.TIMER_RUN,
                };
            }
        }

        new public SiMiBaseSensorManager Sensors
        {
            get { return base.Sensors as SiMiBaseSensorManager; }
            protected set { base.Sensors = value; }
        }

        new internal SiMiBaseOpCodeManager OpCodes
        {
            get { return base.OpCodes as SiMiBaseOpCodeManager; }
            set { base.OpCodes = value; }
        }

        new public SiMiBaseStatusConfiguration Status
        {
            get { return base.Status as SiMiBaseStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public SiMiBaseLoggerFunctionsManager Functions
        {
            get { return base.Functions as SiMiBaseLoggerFunctionsManager; }
            set { base.Functions = value; }
        }

        #endregion
        #region Constructors & Cleanup
        internal SiMiBaseLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }

        internal override void Initialize()
        {
            base.Initialize();

            try
            {
                Version = new SiMiBaseVersion(this);
                Battery = new SiMiBaseBattery(this);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Initialize SiMiBaseLogger", this, ex);
            }
        }

        #endregion
        #region Properties

        #endregion
        #region GenericDevice Implementation
        public override bool IsOfSameType(GenericDevice other)
        {
            return base.IsOfSameType(other)
                     && Memory == (other as SiMiBaseLogger).Memory;
        }

        #endregion
        #region Serialization & Deserialization
        protected SiMiBaseLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
