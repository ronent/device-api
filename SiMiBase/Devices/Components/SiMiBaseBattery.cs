﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiBase.Devices.Components
{
    [Serializable]
    public class SiMiBaseBattery : GenericBattery
    {
        #region Properties
        new internal SiMiBaseLogger ParentDevice
        {
            get { return base.ParentDevice as SiMiBaseLogger; }
            set { base.ParentDevice = value; }
        }

        public override byte BatteryLevel
        {
            get { return ParentDevice.Status.BatteryLevel; }
        }

        public override bool HasAlarm
        {
            get { return false; }
        }

        #endregion
        #region Constructors
        internal SiMiBaseBattery(SiMiBaseLogger device)
            : base(device)
        {

        }
        #endregion
    }
}
