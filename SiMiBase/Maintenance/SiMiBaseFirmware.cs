﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Firmware;

namespace SiMiBase.Maintenance
{
    [Serializable]
    public abstract class SiMiBaseFirmware : DeviceFirmware
    {
        internal override UInt32 FullStartAddress { get { return Convert.ToUInt32("100800", 16); } }

        internal override UInt16 StartAddress { get { return Convert.ToUInt16("800", 16); } }

        internal override int BlockSize { get { return BLOCK_LENGTH; } }

        public SiMiBaseFirmware(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        internal SiMiBaseFirmware()
        {

        }

    }
}
