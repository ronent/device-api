﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.FunctionQueue;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Common;

namespace SiMiBase.OpCodes.Management
{
    [Serializable]
    internal abstract class SiMiBaseOpCodeManager : OpCodeManager, IDisposable
    {
        #region Fields
        public const int GENERIC_REPORT_SIZE = 65;

        private FunctionQueue functionQueue;
        private object sendSyncLock = new object();

        #endregion
        #region Properties
        

        new public IHIDDeviceIO DeviceIO
        {
            get
            {
                return base.DeviceIO as IHIDDeviceIO;
            }
        }

        public FunctionQueue FunctionQueue
        {
            get
            {
                if (DeviceIO != null)
                    return DeviceIO.FunctionQueue;

                return null;
            }
        }

        public override int MaximumArraySize
        {
            get { return GENERIC_REPORT_SIZE; }
        }

        public override int OpCodeStartByte
        {
            get { return 1; }
        }

        #endregion
        #region Constructors
        public SiMiBaseOpCodeManager(SiMiBaseLogger parent)
            : base(parent)
        {

        }

        #endregion
        #region OpCodes
        internal DynamicStatus Run;
        internal DynamicStatus Stop;
        internal DynamicSetup DynamicSetup;
        internal DynamicStatus DynamicStatus;
        internal StaticSetup StaticSetup;
        internal StaticStatus StaticStatus;
        internal BaseDataPacket OnlineDataPacket;
       // internal BaseDataPacket OnlineTimeStamp;
        internal Boomerang GetBoomerang;
        internal Boomerang SetBoomerang;
        internal SetCalibration SetCalibration;
        internal SetTemperatureAlarms SetTemperatureAlarms;

        #endregion
    }
}
