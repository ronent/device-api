﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using SiMiBase.Devices.Types;

namespace SiMiBase.OpCodes.Management
{
    [Serializable]
    internal abstract class OpCodeNoParseAck : SiMiBaseOpCode
    {
        #region Properties
        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        #endregion
        #region Constructors

        protected OpCodeNoParseAck(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Methods
        protected override void AckReceived()
        {
            Finish(Result.OK);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        protected override void DoBeforeParse()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
