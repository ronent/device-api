﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.Devices.Types;
using Base.OpCodes.Types;

namespace SiMiBase.OpCodes.Management
{
    [Serializable]
    internal abstract class SiMiBaseOpCode : OpCode
    {
        #region Fields
        private static readonly byte[] ackHeader = { 0x28 };

        #endregion
        #region Constructor

        protected SiMiBaseOpCode(SiMiBaseLogger device) : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] AckHeader
        {
            get { return ackHeader; }
        }

        public override byte[] OpCodeSpecificAckHeader
        {
            get
            {
                return SendOpCode != null
                    ? AckHeader.Concat(SendOpCode).ToArray()
                    : null;
            }
        }

        #endregion
        #region Override
        new protected SiMiBaseLogger ParentDevice
        {
            get { return base.ParentDevice as SiMiBaseLogger; }
            set { base.ParentDevice = value; }
        }

        new protected SiMiBaseOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiBaseOpCodeManager; }
        }

        protected override byte[] GetOutReportContent()
        {
            return OutReport.GetContent();
        }

        #endregion
        #region Virtual
        public virtual List<byte[]> GetReturnOpCodesToProcess() { return null; }

        protected List<byte[]> GetReturnOpCodesToIgnore()
        {
            return new List<byte[]> { AckHeader };
        }

        #endregion
        #region Methods
        protected void PopulateSendOpCode()
        {
            OutReport.Insert(0xFD, SendOpCode);
        }

        #endregion

        
    }
}
