﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class StaticStatus : SiMiBaseOpCode
    {
        #region Constructors

        protected StaticStatus(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Parse Methods

        protected virtual void ParseUtcOffset()
        {
            ParentDevice.Status.UtcOffset = (sbyte)InReport.Next;
//            ParentDevice.Status.UtcOffset -= 12; //For legacy purposes with minus
        }
        protected virtual void ParseNumberOfActivations()
        {
            ParentDevice.Status.NumberOfActivations = InReport.Next;
        }

        protected virtual void ParseTripNumber()
        {
            ParentDevice.Status.TripNumber = InReport.Next;
        }

        protected virtual void ParseComment()
        {
            ParentDevice.Status.Comment = InReport.GetString(SiMiStaticSetupConfiguration.AllowedCommentLength);
        }

        protected virtual void ParseSerialNumber()
        {
            ParentDevice.Status.SerialNumber = InReport.Get<uint>().ToString();//BitConverter.ToUInt32(arraySN, 0).ToString();
        }

        protected virtual void ParseSwVersion()
        {
            ParentDevice.Status.MinimumRequiredSWVersion = new Version(InReport.Next, InReport.Next, InReport.Next);
        }

        protected virtual void ParseVersion()
        {
            ParentDevice.Status.PCBVersion = InReport.Next;
            ParentDevice.Status.FirmwareVersion = new Version(InReport.Next, InReport.Next, InReport.Next);
        }

        protected virtual void ParseDeviceType()
        {
            ParentDevice.Status.DeviceType = InReport.Next;
        }
        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Static Methods
        protected static bool IsValidStartTime(byte[] timerRun)
        {
            return BitConverter.ToInt32(timerRun, 0) != 0;
        }

        #endregion
    }
}
