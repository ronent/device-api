﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class DynamicStatus : SiMiBaseOpCode
    {
        #region Constructors

        protected DynamicStatus(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        protected virtual void ParseBatteryLevel()
        {
            ParentDevice.Status.BatteryLevel = InReport.Next;
        }

        protected virtual void ParseTimerRun()
        {
            ParentDevice.Status.TimerRun = InReport.Get<int>();
        }

        protected virtual void ParseRunDelay()
        {
            ParentDevice.Status.RunDelay = InReport.Get<uint>();
        }

        protected virtual void ParsePdfOptions()
        {
            var setupPdf = InReport.Next;
            ParentDevice.Status.PdfSetupConfiguration = new PdfSetup
            {
                GeneratePdf = Convert.ToBoolean((byte)(setupPdf & 0x01)),
                AddDataPages = Convert.ToBoolean((byte)(setupPdf & 0x02)),
                AddLightSensorInfo = Convert.ToBoolean((byte)(setupPdf & 0x04))
            };
        }

        protected virtual void ParseNumberOfSamples()
        {
            ParentDevice.Status.NumberOfSamples = InReport.Get<short>();
        }

        protected virtual void ParseInterval()
        {
            ParentDevice.Status.Interval = new TimeSpan(0, 0, InReport.Get<ushort>());
        }

        protected virtual void ParseDisplayConfiguration()
        {
            ParentDevice.Status.LCDConfiguration = (LCDConfigurationEnum)InReport.Next;
        }

        protected virtual void ParseUtcOffset()
        {
            ParentDevice.Status.UtcOffset = (sbyte)InReport.Next;
        }

        protected virtual void ParseTime()
        {
            ParentDevice.Status.FirstPacketTime = InReport.Get<int>();
        }

        protected virtual void ParseSetupBooleans()
        {
            var setupBits = InReport.Get<ushort>();
            ParentDevice.Status.IsRunning = Convert.ToBoolean(setupBits & 0x0001);
            ParentDevice.Status.CyclicMode = Convert.ToBoolean(setupBits & 0x0002);
            ParentDevice.Status.PushToRunMode = Convert.ToBoolean(setupBits & 0x0004);
            ParentDevice.Status.FahrenheitMode = Convert.ToBoolean(setupBits & 0x0008);
            ParentDevice.Status.StopOnKeyPress = Convert.ToBoolean(setupBits & 0x0010);
            ParentDevice.Status.TestMode = Convert.ToBoolean(setupBits & 0x0020);
            ParentDevice.Status.DeepSleepMode = Convert.ToBoolean(setupBits & 0x0040);
            ParentDevice.Status.UnitRequiresSetup = Convert.ToBoolean(setupBits & 0x8000);
        }

        #endregion
    }
}
