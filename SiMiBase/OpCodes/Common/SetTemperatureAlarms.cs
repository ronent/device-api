﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class SetTemperatureAlarms : SiMiBaseOpCode
    {
        #region Constructors

        protected SetTemperatureAlarms(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion

        #region Properties

        public SiMiBaseSetupConfiguration Configuration;

        #endregion

        #region Invoke Methods

        public Result Invoke(SiMiBaseSetupConfiguration configuration)
        {
            Configuration = configuration;
            return Invoke().Result;
        }

        protected override void DoBeforePopulate()
        {
            Configuration.ThrowIfInvalidFor(ParentDevice);
        }

        #endregion

        #region Populate Methods

        protected abstract void PopulateHighAlarms();
        protected abstract void PopulateLowAlarms();

        #endregion
    }
}
