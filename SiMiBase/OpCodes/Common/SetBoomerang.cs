﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class SetBoomerang : Boomerang
    {
        #region Consts
        const byte SET_METHOD = 0x00;

        #endregion
        #region Constructors
        public SetBoomerang(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        protected override byte Method
        {
            get { return SET_METHOD; }
        }

        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x44 }; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        public BoomerangConfiguration Configuration
        {
            get { return ParentOpCodeManager.DynamicSetup.Configuration.Boomerang; }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            if (!Configuration.Valid)
                throw new Exception("Invalid Boomerang Report");

            ResetActionType();
        }

        protected override void AckReceived()
        {
            NextStep();
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            populateActionTypeAndMethod();

            switch (ActionType)
            {
                case ActionTypeEnum.Info:
                    populateInfo();
                    break;
                case ActionTypeEnum.Bank0:
                    populateBank0();
                    break;
                case ActionTypeEnum.Bank1:
                    populateBank1();
                    break;
            }
        }

        private void populateActionTypeAndMethod()
        {
            OutReport.Next = (byte)(((byte)ActionType) << 4 | (byte)Method);
        }

        private void populateInfo()
        {
            try
            {
                OutReport.Insert(Configuration.Comment);
                OutReport.Next = BoomerangConfiguration.INFO_BREAK_BYTE;
                OutReport.Insert(Configuration.Author);
                OutReport.Next = BoomerangConfiguration.INFO_BREAK_BYTE;
                OutReport.Next = BoomerangConfiguration.NO_TIMEZONE;
                OutReport.Next = BoomerangConfiguration.INFO_BREAK_BYTE;
                OutReport.Next = Configuration.CelsiusModeByte;
                OutReport.Next = BoomerangConfiguration.INFO_BREAK_BYTE;
                OutReport.Next = Configuration.DisplayAlarmLevelsByte;
            }
            catch (Exception ex)
            {
                throw new Exception("SetBoomerang failed to process info", ex);
            }
        }

        private void populateBank0()
        {
            try
            {
                byte[] contactBytes = Configuration.GetContactBytes();
                OutReport.Insert(contactBytes);
            }
            catch (Exception ex)
            {
                throw new Exception("SetBoomerang failed to process bank0", ex);
            }
        }

        private void populateBank1()
        {
            try
            {
                byte[] contactBytes = Configuration.GetContactBytes();

                if (contactBytes.Length > BoomerangConfiguration.MAX_BANK0_SIZE)
                {
                    byte[] bank1 = new byte[contactBytes.Length - BoomerangConfiguration.MAX_BANK0_SIZE];
                    Array.Copy(contactBytes, BoomerangConfiguration.MAX_BANK0_SIZE + 1, bank1, 0, bank1.Length);

                    OutReport.Insert(bank1);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SetBoomerang failed to process bank1", ex);
            }
        }

        #endregion
    }
}
