﻿using System;
using Base.OpCodes;
using Log4Tech;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class DynamicSetup : DynamicStatus
    {
        #region Constructors

        protected DynamicSetup(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public SiMiBaseSetupConfiguration Configuration;

        #endregion
        #region Invoke Methods
        public Result Invoke(SiMiBaseSetupConfiguration configuration)
        {
            Configuration = configuration;
            return Invoke().Result;
        }

        protected override void DoBeforePopulate()
        {
            checkVersion(ParentDevice);
            Configuration.ThrowIfInvalidFor(ParentDevice);
        }

        #endregion
        #region Private Methods
        private void checkVersion(SiMiBaseLogger ParentDevice)
        {
            string config = Configuration.GetType().Namespace;
            string device = ParentDevice.GetType().Namespace;

            int configVersion;
            if (!int.TryParse(config.Substring(config.LastIndexOf('.') + 2), out configVersion))
                return;

            if (!configVersion.ToString().Equals(device.Substring(device.LastIndexOf('.') + 2)))
                throw new Exception("Setup Configuration version unmatched the device.");
        }

        #endregion
        #region Populate Methods
        protected abstract void PopulateSetupBooleans();
        protected abstract void PopulateTime();
        protected abstract void PopulateInterval();
        protected abstract void PopulateDisplayConfiguration();
        protected abstract void PopulateNumberOfSamples();
        protected abstract void PopulatePdfOptions();
        protected abstract void PopulateRunDelay();
        protected abstract void PopulateTimerRun();

        protected virtual void PopulateUtcOffset()
        {
//            Configuration.UtcOffset = TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now) ? Convert.ToSByte(TimeZoneInfo.Local.BaseUtcOffset.Hours + 1) : Convert.ToSByte(TimeZoneInfo.Local.BaseUtcOffset.Hours);
//            var utcOffset = (byte)(Configuration.UtcOffset + 12); //For legacy purposes with minus
//            OutReport.Next = utcOffset;
            OutReport.Next = (byte) Configuration.UtcOffset;
        }

        #endregion
        #region Methods
//        internal Result RunSetSN()
//        {
//            if (Configuration.ValidSerialNumber)
//                return ParentOpCodeManager.SetSN.Invoke();
//            else
//                return Result.ERROR;
//        }

        internal Result RunSetBoomerang()
        {
            return Configuration.BoomerangEnabled ? ParentOpCodeManager.SetBoomerang.Invoke().Result : Result.OK;
        }

        internal Result CheckAgainstStatus()
        {
            //ParentDevice.Functions.NotifyNextStatus = false;
            var result = ParentDevice.Functions.GetStatusNoBusy().Result;

            if (!result.IsOK) return result;
            try
            {
                Configuration.ThrowIfNotEquals(ParentDevice.Status);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Status FAILED to check out against sent setup. \n", this, ex);
                return new Result(ex);
            }

            return result;
        }

        #endregion
    }
}
