﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Log4Tech;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class StaticSetup : StaticStatus
    {
        #region Constructors

        protected StaticSetup(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public SiMiStaticSetupConfiguration Configuration;

        #endregion
        #region Invoke Methods
        public Result Invoke(SiMiStaticSetupConfiguration configuration)
        {
            Configuration = configuration;
            return Invoke().Result;
        }

        protected override void DoBeforePopulate()
        {
            Configuration.ThrowIfInvalidFor(ParentDevice);
        }

        #endregion
        #region Populate Methods
        protected abstract void PopulateSerialNumber();
        protected abstract void PopulateComment();

        
        #endregion
        #region Methods

        internal Result CheckAgainstStatus()
        {
            //ParentDevice.Functions.NotifyNextStatus = false;
            var result = ParentDevice.Functions.GetStatusNoBusy().Result;

            if (!result.IsOK) return result;
            try
            {
                // Configuration.ThrowIfNotEquals(ParentDevice.Status);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Status FAILED to check out against sent setup. \n", this, ex);
                return new Result(ex);
            }

            return result;
        }

        #endregion
    }
}
