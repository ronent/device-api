﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using Base.Sensors.Alarms;
using Base.Sensors.Management;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using Log4Tech;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiBase.OpCodes.Common
{
    [Serializable]
    internal abstract class BaseDataPacket : SiMiBaseOpCode
    {
        #region Properties
        protected virtual uint PacketTime { get; set; }
        protected virtual float TemperatureValue { get; set; }
        protected virtual float HumidityValue { get; set; }
        protected virtual eAlarmStatus TemperatureAlarmStatus { get; set; }
        protected virtual eAlarmStatus HumidityAlarmStatus { get; set; }
        protected virtual bool LightStatus { get; set; }
        protected virtual SampleV2.eErrorCodes ErrorCode { get; set; }


        protected SiMiBaseLogger ParentLogger { get { return ParentDevice; } }

        #endregion
        #region Constructors

        protected BaseDataPacket(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Parse Methods

        protected abstract void AddSampleToSensor(GenericSensorV2 sensor, float value, uint dateTime,
            eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isTimeStamp= false);

        protected void ExtractAndAdd()
        {
            try
            {
                foreach (var sensor in ParentLogger.Sensors.EnabledSensors)
                {
                    switch (sensor.Type)
                    {
                        case eSensorType.PT100:
                            AddSampleToSensor(sensor, TemperatureValue, PacketTime, TemperatureAlarmStatus, LightStatus, ErrorCode);
                            break;
                        case eSensorType.Humidity:
                            AddSampleToSensor(sensor, HumidityValue, PacketTime, HumidityAlarmStatus, LightStatus, ErrorCode);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in ExtractAndAdd", this, ex);
            }
        }

//        protected void ExtractAndAdd(uint dateTime)
//        {
//            try
//            {
//                foreach (var sensor in ParentLogger.Sensors.EnabledSensors)
//                {
//                    AddSampleToSensor(sensor, SampleValue, dateTime, AlarmStatus, LightStatus, ErrorCode, true);
//                }
//            }
//            catch (Exception ex)
//            {
//                Log.Instance.WriteError("Fail in ExtractAndAdd TimeStamp", this, ex);
//            }
//        }

       // protected abstract string GetTimestampNoneComment();

//        protected virtual uint GetPacketTime()
//        {
//            return PacketTime;
//        }

//        protected abstract long GetRawValue();

//        protected long GetRawValueFor(GenericSensorV2 sensor)
//        {
//            return sensor.SWGenerated ? 0 : GetRawValue();
//        }

        #endregion
    }
}
