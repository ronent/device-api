﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("PicoLite")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Fourtec")]
[assembly: AssemblyProduct("PicoLite")]
[assembly: AssemblyCopyright("Copyright ©  2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("4af687f2-04f3-4da6-a5e3-c4f60ec2f244")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.1.0.0")]

[assembly: InternalsVisibleTo("FirmwareManager, PublicKey=002400000480000094000000060200000024000052534131000400000100010077aeb169b5610e" +
"df362632733fb00c5cdf5b8544051ad0e927ddd54b5b365ed7b1c6d5c58a321d8d62748d39e49c" +
"d7a5ff5651328d4d1d9eea7edbd3a4421cd1e539b8b64d6844b3bbd4c3ea1030201651cb69afd9" +
"95615f6d8a74bf884275307681ca5bc2fd00491f8a9e6627d5ad36a44f28cf18597fbc8b3003ff" +
"92770ed2")]