﻿using Base.Devices.Features;
using Base.OpCodes;
using Log4Tech;
using MicroXBase.Devices.Features;
using PicoLite.Devices.V2;
using PicoLite.Functions.Management.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.Functions.Management.V2
{
    [Serializable]
    public class PicoLiteLoggerV2FunctionsManager : PicoLiteLoggerV1FunctionsManager
    {
        #region Constructor
        public PicoLiteLoggerV2FunctionsManager(PicoLiteLogger parent)
            :base(parent)
        {

        }

        #endregion
        #region Properties
        new protected internal PicoLiteLogger Device { get { return base.Device as PicoLiteLogger; } set { base.Device = value; } }

        public override bool IsDownloading { get { return Device.OpCodes.Download.IsRunning || Device.OpCodes.NewDownloadTimeStamps.IsRunning; } }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    if (item.Function != eDeviceFunction.MarkTimeStamp)
                        yield return item;
                }
            }
        }

        /// <summary>
        /// Gets the available functions.
        /// </summary>
        /// <value>
        /// The available functions.
        /// </value>
        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                foreach (var item in PicoLiteLoggerV1FunctionsManager.AvailableFunctions)
                {
                    if (item != eDeviceFunction.MarkTimeStamp)
                        yield return item;
                }
            }
        }

        #endregion
        #region Override Functions

        public override Task<Result> GetStatus()
        {
            var result = new Result(eResult.BUSY) {MessageLog = BusyErrorMessage};
            if (!IsBusy)
            {
                var now = DateTime.Now.ToUniversalTime();

                result = Device.OpCodes.Status.Invoke().Result;

                if (result.IsOK)
                    result.SynthesizeResult(Device.OpCodes.Status2.Invoke().Result);

                fixTimeDifference(now);

                if (result.IsOK)
                    result.SynthesizeResult(GetFastClockMode());

                if (result.IsOK)
                    result.SynthesizeResult(GetBoomerang().Result);

                if (result.IsOK)
                    InvokeOnStatus();
            }
            return Task.FromResult(result);
        }

        internal override Task<Result> DownloadTimeStamps()
        {
            return Device.OpCodes.NewDownloadTimeStamps.Invoke();
        }

        [Obsolete("Unsupported in PicoLite2", true)]
        public override Task<Result> MarkTimeStamp()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        #endregion
        #region Methods
        /// <summary>
        /// Fixes the time difference caused by deep sleep feature.
        /// </summary>
        /// <param name="universalTime">The now.</param>
        private void fixTimeDifference(DateTime universalTime)
        {
            if (Device.Status.TimeDifference && !Device.Status.IsFlashErased)
            {
                int localUTC = TimeZoneInfo.Local.BaseUtcOffset.Hours;
                if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
                    localUTC++;

                Device.Status.TimeDifferenceSeconds = (int)(universalTime.AddMinutes(Device.Status.UtcOffset * 60) - Device.Status.DeviceTime).TotalSeconds;
                Device.OpCodes.Status2.FixDeviceTimes();

                Log.Instance.Write("[API] ==> Fixing time difference of {0} seconds.", this, Log.LogSeverity.DEBUG, Device.Status.TimeDifferenceSeconds);

                Device.OpCodes.TimeDifference.Invoke();
            }
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public PicoLiteLoggerV2FunctionsManager(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
          base.GetObjectData(info, context);
        }

        #endregion
    }
}
