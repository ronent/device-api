﻿using Base.Devices.Features;
using Base.Functions.Management;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Calibrations;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Features;
using MicroXBase.Functions.Management;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.Functions.Management.V1
{
    [Serializable]
    public class PicoLiteLoggerV1FunctionsManager : MicroXLoggerFunctionsManager
    {
        #region Constructor
        public PicoLiteLoggerV1FunctionsManager(PicoLiteLogger parent)
            :base(parent)
        {

        }

        #endregion
        #region Properties
        new protected internal PicoLiteLogger Device { get { return base.Device as PicoLiteLogger; } set { base.Device = value; } }
        public override bool IsDownloading { get { return Device.OpCodes.Download.IsRunning || Device.OpCodes.DownloadTimeStamps.IsRunning; } }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    yield return item;
                }

                yield return new AvailableFunction(eDeviceFunction.MarkTimeStamp) { Visible = Device.IsOnline, Enabled = IsRunning && !IsDownloading };
                            
                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                    yield return new AvailableFunction(eDeviceFunction.DeepSleep) { Visible = Device.IsOnline, Enabled = !IsBusy && !IsRunning };
            }
        }

        /// <summary>
        /// Gets the available functions.
        /// </summary>
        /// <value>
        /// The available functions.
        /// </value>
        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                foreach (var item in MicroXLoggerFunctionsManager.AvailableFunctions)
                {
                    yield return item;
                }

                yield return eDeviceFunction.MarkTimeStamp;

                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                    yield return eDeviceFunction.DeepSleep;
            }
        }

        #endregion
        #region Override Functions

        public override Task<Result> SendCalibration(CalibrationConfiguration calibration)
        {
            var result = CheckRunningAndBusy().Result;
            if (!result.IsOK)
                return Task.FromResult(result);

            result = GetStatus().Result;
            if (!result.IsOK)
                return Task.FromResult(result);

            var setup = Device.Status.GenerateFromStatus();

            setup.CalibrationCoeffs.A = calibration[0].A;
            setup.CalibrationCoeffs.B = calibration[0].B;

            return Device.OpCodes.Setup.Invoke(setup);
        }

        public override Task<Result> Download()
        {
            var downloadResult = base.Download().Result;
            {
                try
                {
//                        if (downloadResult.IsFaulted)
//                            return Result.ERROR;

                    if (downloadResult.IsOK)
                        downloadResult.SynthesizeResult(DownloadTimeStamps().Result);

//                    if (downloadResult.IsOK)
//                        Device.Functions.InvokeOnDownloadCompleted(this, new DownloadCompletedArgs(Device));

                    return Task.FromResult(downloadResult);
                }
                catch (Exception ex)
                {
                    return Task.FromResult(new Result(ex));
                }
            }
        }

        
        protected override void SubscribeOpCodesEvents()
        {
            base.SubscribeOpCodesEvents();
            if (Device.OpCodes.Download != null)
                Device.OpCodes.Download.OnFinished += (sender, e) =>
                {
                    if (e.IsOK)
                        Device.Functions.InvokeOnDownloadCompleted(sender, new DownloadCompletedArgs(Device));
                };
        }

        

        public override Task<Result> GetStatus()
        {
            var result = new Result(eResult.BUSY) {MessageLog = BusyErrorMessage};
            if (!IsBusy)
                result = GetStatusNoBusy().Result;
            
            return Task.FromResult(result);
        }

        internal override Task<Result> GetStatusNoBusy()
        {
            var result = Device.OpCodes.Status.Invoke().Result;

            result.SynthesizeResultAndThrowOnError(GetFastClockMode());
            result.SynthesizeResultAndThrowOnError(GetBoomerang().Result);

            if (result.IsOK)
                InvokeOnStatus();

            return Task.FromResult(result);
        }

        protected override Task<Result> DoAfterSetup()
        {
            var result = Result.OK;

            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                result.SynthesizeResultAndThrowOnError(Device.OpCodes.Setup.RunSetFastClock().Result);

            return Task.FromResult(result);
        }

        #endregion
        #region New Functions

        public Task<Result> DeepSleep()
        {
            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User)
                return Task.FromResult(Result.UNSUPPORTED);

            if (IsRunning)
                return Task.FromResult(new Result(eResult.ILLEGAL_CALL) {MessageLog = RUNNING_ERROR});

            return TimerRunEnabled
                ? Task.FromResult(new Result(eResult.ILLEGAL_CALL) {MessageLog = TIMER_RUN_ERROR})
                :  Device.OpCodes.DeepSleep.Invoke();
        }

        public virtual Task<Result> MarkTimeStamp()
        {
            return !IsRunning ? Task.FromResult(new Result(eResult.ILLEGAL_CALL) {MessageLog = NOT_RUNNING_ERROR}) : Device.OpCodes.MarkTimeStamp.Invoke();
        }

        internal Result GetFastClockMode()
        {

            return Device.OpCodes.GetFastClockMode.Invoke().Result;

        }

        internal virtual Task<Result> DownloadTimeStamps()
        {
             return Device.OpCodes.DownloadTimeStamps.Invoke();
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public PicoLiteLoggerV1FunctionsManager(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
          base.GetObjectData(info, context);
        }

        #endregion
    }
}
