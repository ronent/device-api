﻿using Auxiliary.Tools;
using Base.OpCodes;
using PicoLite.DataStructures.V2;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class Setup : V1.Setup
    {
        #region Fields
        SpareBytes LED = new SpareBytes(1);

        #endregion
        #region Constructors
        public Setup(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new public PicoLiteV2SetupConfiguration Configuration
        {
            get { return base.Configuration as PicoLiteV2SetupConfiguration; }
            set { base.Configuration = value; }
        }

        #endregion
        #region Populate Methods
        // Unused in V2
        protected override void PopulateDisplayConfiguration()
        {
            OutReport.Skip(LED);
        }

        // changed to Run Delay
        protected override void PopulateAlarmInfo()
        {
            ushort runDelay = 0;
            if (Configuration.PushToRunMode)
                runDelay = Configuration.RunDelay;

            OutReport.Insert(runDelay, Endianness.LITTLE);
        }

        protected override void PopulateSetupBooleans()
        {
            OutReport.InsertBits(false,
                Configuration.CyclicMode,
                Configuration.PushToRunMode,
                Configuration.FahrenheitMode,
                Configuration.BoomerangEnabled,
                Configuration.StopOnKeyPress);
        }

        #endregion
    }
}
