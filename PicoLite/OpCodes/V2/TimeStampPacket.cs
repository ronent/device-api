﻿using Base.OpCodes;
using Base.Sensors.Types;
using Log4Tech;
using PicoLite.DataStructures.V2;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class TimeStampPacket : V1.TimeStampPacket
    {
        #region Constructors
        public TimeStampPacket(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Override Methods
        protected override DownloadAddresses Addresses
        {
            get { return ParentDevice.OpCodes.NewDownloadTimeStamps.Addresses; }
        }

        public override ushort CurrentAddress
        {
            get
            {
                return ParentDevice.OpCodes.NewDownloadTimeStamps.CurrentAddress;
            }
            set
            {
                ParentDevice.OpCodes.NewDownloadTimeStamps.CurrentAddress = value;
            }
        }

        protected override bool CancelationPending { get { return ParentDevice.OpCodes.NewDownloadTimeStamps.CancelationPending; } }

        protected override long GetRawValue()
        {
            byte rawValue = InReport.Next;

            if (rawValue.Equals(Consts.V2Readings.SAMPLE_DUMMY))
                return long.MaxValue;
            else
                return Convert.ToInt64(rawValue);
        }

        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            bool isDummy = value == long.MaxValue;
            sensor.samples.AddOffline(value, dateTime, (PacketTime as PicoLiteV2Time).TimestampType.ToString(), isDummy);
        }

        protected override void WriteFinishToLog(Result result)
        {
            if (result.IsOK && !NameString.Contains("Online"))
            {
                Log4Tech.Log.Instance.Write("[API]  Device: '{0}' finished Opcode: '{1}' with result: {2} {3}/{4}", this, Log.LogSeverity.DEBUG, ParentDevice,
                      NameString, result, ParentDevice.OpCodes.NewDownloadTimeStamps.PacketIndex + 1, ParentDevice.OpCodes.NewDownloadTimeStamps.PacketCount);
            }
            else
                base.WriteFinishToLog(result);
        }

        #endregion
        #region Parse Methods
        protected override void ParseTimestamp()
        {
            PacketTime = PicoLiteV2Time.ParseTimestamp(InReport.GetBlock(Consts.Time.TIMESTAMP_SIZE), ParentDevice.Status.DeviceTime);
            PacketTime.Increase((ParentDevice.Status.TimeDifferenceSeconds));
        }

        #endregion
    }
}
