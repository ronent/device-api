﻿using Base.OpCodes;
using PicoLite.Devices.V2;
using PicoLite.OpCodes.Helpers.V1;
using PicoLite.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class TimeDifference : PicoLiteOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x19 };

        #endregion
        #region Constructors
        public TimeDifference(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {

        }

        protected override void AckReceived()
        {
            Finish(Result.OK);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateTimeDifference();
        }

        private void PopulateTimeDifference()
        {
            OutReport.Insert(ParentDevice.Status.TimeDifferenceSeconds);
        }

        #endregion
    }
}
