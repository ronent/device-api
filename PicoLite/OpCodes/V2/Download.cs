﻿using Base.OpCodes;
using Base.Sensors.Samples;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V2;
using PicoLite.OpCodes.Management.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class Download : V1.Download
    {
        #region Constructors
        public Download(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new public PicoLiteV2Time StartTimeDate { get; protected set; }

        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Override Methods
        protected override void parseDownloadTimeDate()
        {
            StartTimeDate = PicoLiteV2Time.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
            StartTimeDate.Increase((ParentDevice.Status.TimeDifferenceSeconds));
        }

        #endregion
    }
}
