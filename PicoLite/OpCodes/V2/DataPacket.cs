﻿using Base.Sensors.Samples;
using Base.Sensors.Types;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Management.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class DataPacket : V1.DataPacket
    {
        #region Constructors
        public DataPacket(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        protected override byte Flush
        {
            get { return Consts.V2Readings.FLUSH; }
        }

        protected override ushort MaximumDataBlockSize
        {
            get { return Consts.V2Readings.SAMPLE_SIZE; }
        }

        protected override string GetTimestampNoneComment()
        {
            return PicoLite.OpCodes.Helpers.Consts.eV2TimestampType.None.ToString();
        }

        #endregion
        #region Override Methods
        protected override Base.DataStructures.Time PacketTime
        {
            get
            {
                return ((ParentOpCodeManager as PicoLiteV2OpCodeManager).Download as V2.Download).StartTimeDate;
            }
            set
            {
            }
        }

        protected override long GetRawValue()
        {
            return GetRawValue(InReport.Next);
        }

        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            bool isDummy = value == long.MaxValue;
            sensor.samples.AddOffline(value, dateTime, isDummy);
        }

        #endregion
        #region Static Methods
        public static long GetRawValue(byte value)
        {
            if (value.Equals(Consts.V2Readings.SAMPLE_DUMMY))
                return long.MaxValue;
            else
                return Convert.ToInt64(value);
        }

        #endregion
    }
}
