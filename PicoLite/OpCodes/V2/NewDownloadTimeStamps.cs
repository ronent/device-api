﻿using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class NewDownloadTimeStamps : V2.Download
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x18 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x20 };

        private bool isSubscribed = false;

        #endregion
        #region Constructors
        public NewDownloadTimeStamps(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Methods
        protected override void SubscribeToOnDataPacketFinished()
        {
            if (ParentOpCodeManager.TimeStampPacket != null)
            {
                isSubscribed = true;
                ParentOpCodeManager.TimeStampPacket.OnFinished += DataPacket_OnFinished;
            }
        }

        protected override void UnsubscribeToOnDataPacketFinished()
        {
            try
            {
                ParentOpCodeManager.TimeStampPacket.OnFinished -= DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On UnsubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        protected override bool ReachedEndOfData()
        {
            return Addresses.IsAtEnd;
        }

        protected override void DoBeforePopulate()
        {
            if (!isSubscribed)
                SubscribeToOnDataPacketFinished();

            base.DoBeforePopulate();

            PacketIndex = 0;
            PacketCount = 0;

            ParentOpCodeManager.TimeStampPacket.Initialize();
            Command = DownloadCommandEnum.Info;
        }

        protected override void DoAfterFinished()
        {
            ParentOpCodeManager.TimeStampPacket.WaitingForInReport = false;
        }

        public override List<byte[]> GetReturnOpCodesToProcess()
        {
            return new List<byte[]>() { ParentOpCodeManager.TimeStampPacket.ReceiveOpCode };
        }

        protected override void ResetDownloadTimeout()
        {
            ResetProcessTimer();
        }

        #endregion
    }
}