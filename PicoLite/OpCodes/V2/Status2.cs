﻿using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using PicoLite.OpCodes.Helpers.V2;
using PicoLite.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class Status2 : PicoLiteOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x1A };
        private static readonly byte[] receiveOpCode = new byte[] { 0x22 };

        #endregion
        #region Constructors
        public Status2(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            parserSetupTime();
            parseRunTime();
            parseTimeDifference();
        }

        private void parserSetupTime()
        {
            ParentDevice.Status.SetupTime = parseTime();
        }

        private void parseRunTime()
        {
            ParentDevice.Status.RunTime = parseTime();
        }

        private void parseTimeDifference()
        {
            ParentDevice.Status.TimeDifferenceSeconds = InReport.Get<Int32>();
            FixDeviceTimes();
        }

        private DateTime parseTime()
        {
            try
            {
                byte[] block = InReport.GetBlock(Consts.Time.TIME_SIZE);
                return PicoLiteV2Time.Parse(block).ToDateTime();
            }
            catch { return DateTime.MinValue; }
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Methods
        internal void FixDeviceTimes()
        {
            int diff = ParentDevice.Status.TimeDifferenceSeconds;
            if (diff == 0)
                return;

            //Log.Log.WriteLine(string.Format("[API] ==> Fixing time difference from logger of {0} seconds.", diff));

            if (ParentDevice.Status.RunTime != default(DateTime))
            {
                ParentDevice.Status.RunTime = ParentDevice.Status.RunTime.AddSeconds(diff);
            }

            if (ParentDevice.Status.DeviceTime != default(DateTime))
            {
                ParentDevice.Status.DeviceTime = ParentDevice.Status.DeviceTime.AddSeconds(diff);                
            }
        }

        #endregion
    }
}
