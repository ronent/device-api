﻿using Auxiliary.Tools;
using Base.OpCodes;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class Status : V1.Status
    {
        #region Fields
        private byte firmwareBuild;

        #endregion
        #region Constructors
        public Status(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Methods
        protected override void DoAfterParse()
        {
            ParentDevice.Status.FirmwareVersion = new Version(ParentDevice.Status.FirmwareVersion.Major, ParentDevice.Status.FirmwareVersion.Minor, firmwareBuild);
            base.DoAfterParse();
        }

        #endregion
        #region Parse
        protected override void ParseDisplayConfiguration()
        {
            firmwareBuild = InReport.Next;
        }

        protected override void ParseAlarmInfo()
        {
            ParentDevice.Status.RunDelay = InReport.Get<UInt16>(Endianness.LITTLE);
        }

        protected override void ParseSetupBooleans()
        {
            int index = 0;
            bool[] bitArray = InReport.Get8Bits().ToArray();

            ParentDevice.Status.IsRunning = bitArray[index++];
            ParentDevice.Status.CyclicMode = bitArray[index++];
            ParentDevice.Status.PushToRunMode = bitArray[index++];
            ParentDevice.Status.FahrenheitMode = bitArray[index++];
            ParentDevice.Status.BoomerangEnabled = bitArray[index++];
            ParentDevice.Status.StopOnKeyPress = bitArray[index++];
            ParentDevice.Status.TimeDifference = bitArray[index++];
        }

        #endregion
    }
}
