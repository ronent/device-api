﻿using Base.Sensors.Types;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class OnlineDataPacket : V1.OnlineDataPacket
    {
        #region Constructors
        public OnlineDataPacket(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        new protected Devices.V2.PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as Devices.V2.PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Override Methods
        protected override void parsePacketTime()
        {
            PacketTime = PicoLiteV2Time.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
            PacketTime.Increase((ParentDevice.Status.TimeDifferenceSeconds));
        }

        protected override long GetRawValue()
        {
            return DataPacket.GetRawValue(InReport.Next);
        }

        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            bool isDummy = value == long.MaxValue;
            sensor.samples.AddOnline(value, dateTime, isDummy);
        }

        #endregion
    }
}
