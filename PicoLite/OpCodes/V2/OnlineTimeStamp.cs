﻿using Base.Sensors.Types;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V2
{
    [Serializable]
    internal class OnlineTimeStamp : OnlineDataPacket
    {
        #region Constructors
        public OnlineTimeStamp(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] ReceiveOpCode
        {
            get { return V1.OnlineTimeStamp.receiveOpCode; }
        }

        #endregion
        #region Override Methods
        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOnline(value, dateTime, PicoLite.OpCodes.Helpers.Consts.eV1TimestampType.None.ToString(), true);
        }

        #endregion
    }
}
