﻿using Base.OpCodes;
using PicoLite.Devices;
using System;
using System.Collections.Generic;


namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class DownloadTimeStamps : Download
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x18 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x20 };

        #endregion
        #region Constructors
        public DownloadTimeStamps(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Methods
        protected override void SubscribeToOnDataPacketFinished()
        {
            try
            {
                ParentOpCodeManager.TimeStampPacket.OnFinished += DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On SubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        protected override void UnsubscribeToOnDataPacketFinished()
        {
            try
            {
                ParentOpCodeManager.TimeStampPacket.OnFinished -= DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On UnsubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        protected override void DoBeforePopulate()
        {
            base.DoBeforePopulate();

            PacketIndex = 0;
            PacketCount = 0;

            ParentOpCodeManager.TimeStampPacket.Initialize();
            Command = DownloadCommandEnum.Info;
        }

        protected override void DoAfterFinished()
        {
            ParentOpCodeManager.TimeStampPacket.WaitingForInReport = false;
        }

        public override List<byte[]> GetReturnOpCodesToProcess()
        {
            return new List<byte[]>() { ParentOpCodeManager.TimeStampPacket.ReceiveOpCode };
        }

        protected override void ResetDownloadTimeout()
        {
            ResetProcessTimer();
        }

        #endregion
    }
}
