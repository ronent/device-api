﻿using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroXBase.OpCodes.Common.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
    }

}
