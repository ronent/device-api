﻿using Base.OpCodes;
using Log4Tech;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using PicoLite.OpCodes.Management;
using PicoLite.OpCodes.Management.V1;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroXBase.OpCodes.Common.Download
    {
        #region Fields
        public PicoLiteTime StartTimeDate { get; protected set; }
        public DownloadAddresses Addresses;

        #endregion
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x15 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x23 };

        #endregion
        #region Constructors
        public Download(PicoLiteLogger device)
            : base(device)
        {
            
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new protected PicoLiteV1OpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as PicoLiteV1OpCodeManager; }
        }

        public ushort CurrentAddress { get { return Addresses.Current; } set { Addresses.Current = value; } }

        public bool IsMemoryFull { get { return MessageType == MessageTypeEnum.MemoryFull; } }

        new protected PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            base.DoBeforePopulate();
            Command = DownloadCommandEnum.Info;
        }

        protected override void DoAfterFinished()
        {
            ParentOpCodeManager.DataPacket.WaitingForInReport = false;
        }

        protected override uint? GetExpectedPacketsCount()
        {
            return PacketCount - 1;
        }

        protected override bool ReachedEndOfData()
        {
            return Addresses.IsAtEnd;
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            try
            {
                parseAddresses();
                parseDownloadTimeDate();
            }
            catch (Exception ex)
            {
                if (ex.Message != "Device contains no data")
                {
                    //Log.Instance.WriteError("On Parse()", this, ex);
                    throw ex;
                }
            }
        }

        private void parseAddresses()
        {
            Addresses.Current = 0;

            Addresses.Start = InReport.Get<UInt16>();
            Addresses.End = InReport.Get<UInt16>();
            Addresses.AbsoluteEnd = InReport.Get<UInt16>();
            Addresses.IsMemoryFull = IsMemoryFull;
            Addresses.Validate();
            Addresses.ThrowIfInvalidAddress();

            PacketCount = Addresses.PacketCount;

            //Log.Instance.Write("[API] ==> Device: '{0}' Download addresses: {1}", this, Log.LogSeverity.DEBUG, ParentDevice, Addresses);
        }

        protected virtual void parseDownloadTimeDate()
        {
            StartTimeDate = PicoLiteTime.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateCommand();
        }

        protected override void PopulateCommand()
        {
            OutReport.Next = Convert.ToByte(Command);
        }

        #endregion
    }
}
