﻿using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.OpCodes;
using Log4Tech;
using PicoLite.DataStructures.V1;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using PicoLite.OpCodes.Management;
using PicoLite.OpCodes.Management.V1;
using System;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroXBase.OpCodes.Common.Setup
    {
        #region Fields
        SpareBytes timerRunSpare = new SpareBytes(4);

        SpareBytes spare1 = new SpareBytes(22,24);
        SpareBytes spare2 = new SpareBytes(29,48);

        #endregion
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x11 };

        #endregion
        #region Constructors
        public Setup(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        new public PicoLiteV1SetupConfiguration Configuration
        {
            get { return base.Configuration as PicoLiteV1SetupConfiguration; }
            set { base.Configuration = value; }
        }

        new public PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        new public PicoLiteV1OpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as PicoLiteV1OpCodeManager; }
        }

        #endregion
        #region Configuration Properties
        protected virtual bool TemperatureEnabled { get { return true; } }
        protected virtual Alarm TemperatureAlarm { get { return Configuration.TemperatureSensor.TemperatureAlarm; } }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateSetupBooleans();
            PopulateTime();
            PopulateDisplayConfiguration();
            PopulateUtcOffset();
            PopulateInterval();
            PopulateAlarms();
            populateTimerConfiguration();
            PopulateAlarmInfo();
            OutReport.Skip(spare1);
            populateCalibration();
            OutReport.Skip(spare2);
            OutReport.Insert(Configuration.Comment, PicoLiteV1SetupConfiguration.AllowedCommentLength); 
        }
        
        protected override void PopulateSetupBooleans()
        {
            OutReport.InsertBits(false,
                Configuration.CyclicMode,
                Configuration.PushToRunMode,
                Configuration.FahrenheitMode,
                Configuration.BoomerangEnabled);
        }

        protected override void PopulateTime()
        {
            Configuration.DeviceTime = DateTime.UtcNow;
            var time = PicoLiteTime.CreateFrom(Configuration.DeviceTime);
            OutReport.Insert(time.GetBytes());
        }

        protected override void PopulateDisplayConfiguration()
        {
            OutReport.Next = (byte)Configuration.LEDConfiguration;
        }

        protected override void PopulateInterval()
        {
            if (Configuration.TestMode)
                OutReport.Insert((ushort)Configuration.Interval.TotalSeconds);
            else
                OutReport.Insert((ushort)Configuration.Interval.TotalMinutes);

        }

        protected override void PopulateAlarms()
        {
            PopulateAlarm(TemperatureAlarm);
        }

        protected override void PopulateAlarm(Alarm alarmValue)
        {
            alarmValue.DetermineIfAlarmEnabled(ParentDevice.Sensors.Temperature);

            if (!alarmValue.Enabled)
                alarmValue.Invalidate(ParentDevice.Sensors.Temperature);

            if (Configuration.FahrenheitMode)
            {
                populateAlarmEnd(Transformation.ConvertToCelsius(alarmValue.High));
                populateAlarmEnd(Transformation.ConvertToCelsius(alarmValue.Low));
            }
            else
            {
                populateAlarmEnd(alarmValue.High);
                populateAlarmEnd(alarmValue.Low);
            }
        }

        private void populateAlarmEnd(decimal alarmEnd)
        {
            try
            {
                byte alarmEndDigital = Convert.ToByte(ParentDevice.Sensors.Temperature.GetDigitalValue(alarmEnd));
                OutReport.Next = alarmEndDigital;
            }
            catch
            { 
                OutReport.Next = 0xff; 
            }
        }

        private void populateTimerConfiguration()
        {
            if (Configuration.TimerRunEnabled)
            {
                UInt32 totalSeconds = 0;

                Utilities.TryCatch(() =>
                   totalSeconds = Convert.ToUInt32(Math.Ceiling((Configuration.TimerStart - DateTime.Now).TotalSeconds))
                   ,null);

                OutReport.Insert(totalSeconds);
            }
            else
                OutReport.Skip(timerRunSpare);
            OutReport.Insert(Configuration.TimerRunEnabled);
        }

        protected override void PopulateAlarmInfo()
        {
            OutReport.Insert(Configuration.AlarmDelay, Endianness.BIG);
        }

        private void populateCalibration()
        {
            var calibration = Configuration.CalibrationCoeffs;
            OutReport.Insert(Convert.ToUInt16(calibration.IntGain * 1000M), endianness:Endianness.BIG);
            OutReport.Insert(Convert.ToInt16(calibration.IntOffset * 2M), endianness: Endianness.BIG);
        }

        #endregion
        #region Methods
        internal Task<Result> RunSetFastClock()
        {
            return ParentOpCodeManager.SetFastClockMode.Invoke();
        }

        #endregion
    }
}
