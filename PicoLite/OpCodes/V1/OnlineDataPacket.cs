﻿using Base.OpCodes;
using Base.Sensors.Types;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : DataPacket
    {
        #region Fields
        protected object processingPacket = new object();
        #endregion
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x29 };

        #endregion
        #region Constructors
        public OnlineDataPacket(PicoLiteLogger device)
            : base(device)
        {
            
        }

        protected override void initialize()
        {
            base.initialize();

            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override Base.DataStructures.Time PacketTime { get; set; }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            lock (processingPacket)
            {
                parsePacketTime();
                ExtractAndAdd();
            }
        }

        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOnline(value, dateTime);
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Virtual Methods
        protected virtual void parsePacketTime()
        {
            PacketTime = PicoLiteTime.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
        }

        #endregion
        #region NotSupported
        protected override void DoBeforePopulate()
        {
            throw new NotSupportedException();
        }

        protected override void Populate()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
