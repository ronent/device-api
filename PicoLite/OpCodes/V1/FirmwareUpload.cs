﻿using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUpload : MicroXBase.OpCodes.Common.FirmwareUpload
    {
        #region Members
        private static readonly byte[] ackHeader = new byte[] { 0x32 };

        #endregion
        #region Constructors
        public FirmwareUpload(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override TimeSpan InvokeTimeOutDuration
        {
            get { return new TimeSpan(0, 2, 30); }
        }

        public override byte[] AckHeader
        {
            get { return ackHeader; }
        }

        #endregion
        #region Methods
        protected override void Parse()
        {
        }

        #endregion
    }
}
