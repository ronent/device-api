﻿using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class SetFastClockMode : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x90 };

        #endregion
        #region Constructors
        public SetFastClockMode(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            OutReport.Insert(ParentOpCodeManager.Setup.Configuration.TestMode);
        }
        #endregion
    }

}
