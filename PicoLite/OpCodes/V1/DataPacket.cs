﻿using Log4Tech;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Management;
using PicoLite.OpCodes.Management.V1;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class DataPacket : MicroXBase.OpCodes.Common.DataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x24 };
        private int numberOfFlushedBytes = 0;
        
        #endregion
        #region Constructors
        public DataPacket(PicoLiteLogger device)
            : base(device)
        {
            
        }

        protected override void initialize()
        {
            base.initialize();

            numberOfFlushedBytes = 0;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new public PicoLiteV1OpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as PicoLiteV1OpCodeManager; }
        }

        protected virtual DownloadAddresses Addresses
        {
            get { return ParentOpCodeManager.Download.Addresses; }
        }

        protected virtual byte Flush
        {
            get { return Consts.V1Readings.FLUSH; }
        }

        public override uint PacketIndex
        {
            get
            {
                return Addresses.PacketIndex;
            }
        }

        protected override uint TotalPackets
        {
            get
            {
                return Addresses.PacketCount;
            }
        }

        public override ushort CurrentAddress
        {
            get
            {
                return ParentOpCodeManager.Download.CurrentAddress;
            }
            set
            {
                ParentOpCodeManager.Download.CurrentAddress = value;
            }
        }

        protected override Base.DataStructures.Time PacketTime
        {
            get
            {
                return ParentOpCodeManager.Download.StartTimeDate;
            }
            set
            {
            }
        }

        protected override ushort MaximumDataBlockSize
        {
            get { return Consts.V1Readings.SAMPLE_SIZE; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            ParseAddress();
            ParseDataEntries();
            ReportDoneIfLastPacket();
        }

        protected override string GetTimestampNoneComment()
        {
            return PicoLite.OpCodes.Helpers.Consts.eV1TimestampType.None.ToString();
        }

        private void adjustTimeForCyclicMode()
        {
            if (Addresses.IsMemoryFull && CurrentAddress == Addresses.End)
            {
                double span = (double)(ParentDevice.Status.Interval.TotalSeconds * (Addresses.StartToAbsEndSpan - numberOfFlushedBytes));
                PacketTime.Decrease((long)span);
            }
        }

        protected virtual void ParseAddress()
        {
            CurrentAddress = InReport.Get<UInt16>();
            Addresses.ThrowIfInvalidAddress(CurrentAddress);
        }

        protected void ParseDataEntries()
        {
            while (!CancelationPending && WithinAddressBounds() && ReportHasAnyDataLeft())
            {
                adjustTimeForCyclicMode();

                if (!ReportHasMeaningfulData())
                {
                    setAddressToEnd();
                    return;
                }

                //Flush byte is an empty byte created when download is invoked before the 2-byte array had been flushed. 
                //Therefore the second byte is empty.
                if (InReport.PeekNext() != Flush)
                {
                    CustomExtractAndAdd();
                    reportProgressBasedOnAddress();
                    IncreasePacketTime();
                    IncreaseAddress();
                }
                else
                {
                    InReport.Skip(MaximumDataBlockSize);
                    numberOfFlushedBytes++;
                    IncreaseAddress();
                }
            }
        }

        private void setAddressToEnd()
        {
            CurrentAddress = Addresses.RealEnd;
        }

        protected virtual void CustomExtractAndAdd()
        {
            ExtractAndAdd();
        }

        protected override long GetRawValue()
        {
            return Convert.ToInt64(InReport.Next);
        }

        protected virtual void IncreaseAddress()
        {
            CurrentAddress += MaximumDataBlockSize;
        }

        protected virtual bool WithinAddressBounds()
        {
            return CurrentAddress < Addresses.RealEnd;
        }

        protected virtual void reportProgressBasedOnAddress()
        {
            ReportProgress(Addresses.GetPercentage());
        }

        new protected virtual void ReportDoneIfLastPacket()
        {
            if (PacketIndex >= TotalPackets)
                ReportProgress(100);
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
    }
}