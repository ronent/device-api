﻿using Base.Sensors.Samples;
using Base.Sensors.Types;
using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : OnlineDataPacket
    {
        #region Members
        internal static readonly byte[] receiveOpCode = new byte[] { 0x30 };

        #endregion
        #region Constructors
        public OnlineTimeStamp(PicoLiteLogger device)
            : base(device)
        {
          
        }

        protected override void initialize()
        {
            base.initialize();

            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Methods
        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOnline(value, dateTime, PicoLite.OpCodes.Helpers.Consts.eV1TimestampType.None.ToString());
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
    }
}
