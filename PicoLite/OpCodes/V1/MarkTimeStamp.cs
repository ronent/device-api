﻿using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class MarkTimeStamp : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x50 };

        #endregion
        #region Constructors
        public MarkTimeStamp(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
    }
}
