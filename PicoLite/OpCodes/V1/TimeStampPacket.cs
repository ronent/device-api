﻿using Base.OpCodes;
using Base.Sensors.Types;
using Log4Tech;
using PicoLite.DataStructures.V1;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class TimeStampPacket : DataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x25 };

        #endregion
        #region Constructors
        public TimeStampPacket(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override bool CancelationPending { get { return ParentOpCodeManager.DownloadTimeStamps.CancelationPending; } }
        protected override ushort MaximumDataBlockSize { get { return Consts.V1Readings.TIMESTAMP_SIZE; } }


        protected override DownloadAddresses Addresses
        {
            get { return ParentOpCodeManager.DownloadTimeStamps.Addresses; }
        }

        public override ushort CurrentAddress
        {
            get
            {
                return ParentOpCodeManager.DownloadTimeStamps.CurrentAddress;
            }
            set
            {
                ParentOpCodeManager.DownloadTimeStamps.CurrentAddress = value;
            }
        }

        protected override Base.DataStructures.Time PacketTime
        {
            get;
            set;
        }

        #endregion
        #region Parse Methods
        protected override DateTime GetPacketTime()
        {
            ParseTimestamp();
            return base.GetPacketTime();
        }

        protected virtual void ParseTimestamp()
        {
            PacketTime = PicoLiteTime.ParseTimestamp(InReport.GetBlock(Consts.Time.TIMESTAMP_SIZE), (ParentDevice.Status as PicoLiteV1StatusConfiguration).DeviceTime);
        }
        #endregion
        #region Methods
        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOffline(value, dateTime, (PacketTime as PicoLiteTime).TimestampType.ToString());
        }

        protected override void WriteFinishToLog(Result result)
        {
            if (result.IsOK && !NameString.Contains("Online"))
            {
                Log4Tech.Log.Instance.Write("[API]  Device: '{0}' finished Opcode: '{1}' with result: {2} {3}/{4}", this, Log.LogSeverity.DEBUG, ParentDevice,
                      NameString, result, ParentOpCodeManager.DownloadTimeStamps.PacketIndex + 1, ParentOpCodeManager.DownloadTimeStamps.PacketCount);
            }
            else
                base.WriteFinishToLog(result);
        }

        #endregion
    }
}
