﻿using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.OpCodes;
using Log4Tech;
using PicoLite.DataStructures.V1;
using PicoLite.Devices;
using PicoLite.OpCodes.Helpers;
using PicoLite.OpCodes.Helpers.V1;
using PicoLite.OpCodes.Management;
using PicoLite.OpCodes.Management.V1;
using PicoLite.Sensors.V1;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroXBase.OpCodes.Common.Status
    {
        #region Fields
        SpareBytes spare1 = new SpareBytes(15, 17);
        SpareBytes spare2 = new SpareBytes(30, 32);

        #endregion
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x12 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x21 };
        private byte[] alarmValues = new byte[2];

        #endregion
        #region Constructors
        public Status(PicoLiteLogger device)
            : base(device)
        {

        }

        protected override void InitializeReports(Base.Devices.GenericDevice device)
        {
            base.InitializeReports(device);
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new protected PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        new protected PicoLiteV1OpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as PicoLiteV1OpCodeManager; }
        }

        #endregion
        #region Parse Methods
        /// <summary>
        /// Parse status according to the protocol
        /// </summary>
        protected override void Parse()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseDisplayConfiguration();
            ParseUtcOffset();
            ParseInterval();
            ParseAlarms();
            InReport.Skip(spare1);
            ParseBatteryLevel();
            parseCyclicStatus();
            ParseTimerStatus();
            ParseAlarmInfo();
            InReport.Skip(spare2);
            ParseDeviceType();
            ParseFirmwareVersion();
            ParseAssembly();
            parseSN();
            parseCalibration();
            parseComment();

            this.ParentDevice.Sensors.Temperature.Enabled = true;
        }

        protected override void ParseSetupBooleans()
        {
            int index = 0;
            bool[] bitArray = InReport.Get8Bits().ToArray();

            ParentDevice.Status.IsRunning = bitArray[index++];
            ParentDevice.Status.CyclicMode = bitArray[index++];
            ParentDevice.Status.PushToRunMode = bitArray[index++];
            ParentDevice.Status.FahrenheitMode = bitArray[index++];
            ParentDevice.Status.BoomerangEnabled = bitArray[index++];
        }

        protected override void ParseTime()
        {
            try
            {
                byte[] block = InReport.GetBlock(Consts.Time.TIME_SIZE);
                ParentDevice.Status.DeviceTime = PicoLiteTime.Parse(block).ToDateTime();
            }
            catch { ParentDevice.Status.DeviceTime = DateTime.Now; }
        }

        protected override void ParseDisplayConfiguration()
        {
            ParentDevice.Status.LEDConfiguration = (eLEDConfiguration)InReport.Next;
        }

        protected override void ParseInterval()
        {
            ParentDevice.Status.Interval = new TimeSpan(0,InReport.Get<UInt16>(), 0);
        }

        protected override void ParseAlarms()
        {
            alarmValues[0] = InReport.Next;
            alarmValues[1] = InReport.Next;

            assignAlarms();
        }

        private void assignAlarms()
        {
            Alarm alarm = new Alarm()
            {
                High = ParentDevice.Sensors.Temperature.GetSensorValue(alarmValues[0]),
                Low = ParentDevice.Sensors.Temperature.GetSensorValue(alarmValues[1]),
            };

            ParentDevice.Status.TemperatureSensor.TemperatureAlarm.Enabled = determineIfAlarmEnabled(alarm);

            if (ParentDevice.Status.TemperatureSensor.TemperatureAlarm.Enabled)
            {
                ParentDevice.Status.TemperatureSensor.TemperatureAlarm.High = alarm.High;
                ParentDevice.Status.TemperatureSensor.TemperatureAlarm.Low = alarm.Low;
            }
        }

        private bool determineIfAlarmEnabled(Alarm alarm)
        {
            try
            {
                alarm.ThrowIfNotValid(ParentDevice.Sensors.Temperature);
                return alarm.Enabled;
            }
            catch { return false; }
        }

        protected override void ParseBatteryLevel()
        {
            decimal ADCbg = (decimal)InReport.Get<UInt16>(Endianness.BIG);
            decimal ADCDiv = (decimal)InReport.Get<UInt16>(Endianness.BIG);

            int batteryLevel = Convert.ToInt16(ADCDiv / ADCbg * Consts.Battery.VOLTAGE_SCALAR);
            ParentDevice.Status.BatteryLevel = Convert.ToByte(Utilities.GetBoundedValue(0, batteryLevel, 100));
        }

        private void parseCyclicStatus()
        {
            ParentOpCodeManager.Download.MessageType = InReport.Next == Consts.Status.CYCLE_COMPLETED
                ? MicroXBase.OpCodes.Common.Download.MessageTypeEnum.MemoryFull
                : MicroXBase.OpCodes.Common.Download.MessageTypeEnum.PacketCount;
        }

        protected override void ParseTimerStatus()
        {
            var totalSeconds = InReport.Get<UInt32>();
            ParentDevice.Status.TimerRunEnabled = InReport.GetBool();

            if (ParentDevice.Status.TimerRunEnabled)
                ParentDevice.Status.TimerStart = ParentDevice.Status.DeviceTime.AddSeconds(totalSeconds);
        }

        protected override decimal ParseAlarmEnd()
        {
            byte value = InReport.Next;
            return ParentDevice.Sensors.Temperature.GetSensorValue(value);
        }

        protected override void ParseAlarmInfo()
        {
            ParentDevice.Status.AlarmDelay = InReport.Get<UInt16>(Endianness.BIG);
        }

        protected override void ParseDeviceType()
        {
            ParentDevice.Status.DeviceType = InReport.Next;
        }

        protected override void ParseFirmwareVersion()
        {
            byte integer = InReport.Next;
            byte fraction = InReport.Next;

            ParentDevice.Status.FirmwareVersion = new Version(string.Format("{0}.{1:D2}", integer, fraction));
        }

        protected override void ParseAssembly()
        {
            ParentDevice.Status.PCBAssembly = InReport.Next;
        }

        private void parseSN()
        {
            ParentDevice.Status.SerialNumber = InReport.GetString(PicoLiteV1SetupConfiguration.AllowedSNLength);
        }

        private void parseCalibration()
        {
            parseGain();
            parseOffset();
        }

        private void parseGain()
        {
            UInt16 gain = InReport.Get<UInt16>(endianness:Endianness.BIG);
            ParentDevice.Status.CalibrationCoeffs.A = gain / 1000M;
        }

        private void parseOffset()
        {
            Int16 offset = InReport.Get<Int16>(endianness: Endianness.BIG);
            ParentDevice.Status.CalibrationCoeffs.B = offset / 2M;
        }

        private void parseComment()
        {
            ParentDevice.Status.Comment = InReport.GetString(PicoLiteV1SetupConfiguration.AllowedCommentLength);
        }

        #endregion
    }
}