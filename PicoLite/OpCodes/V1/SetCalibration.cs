﻿using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroXBase.OpCodes.Common.SetCalibration
    {
        #region Constructors
        public SetCalibration(PicoLiteLogger device)
            : base(device)
        { 

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateGainOffset();
        }
        protected override void PopulateGainOffset()
        {
            PopulateGainOffsetAux(Calibration[eSensorIndex.Temperature]);
        }

        protected override void PopulateGainOffsetAux(CalibrationCoefficients coefficients)
        {
            ValidateCoefficients(coefficients);

            populateGain(coefficients.Af);
            populateOffset(coefficients.Bf);
        }

        private void populateGain(float p)
        {
            UInt16 gain = 0;
            try { gain = Convert.ToUInt16(p); }
            catch { }
            OutReport.Insert(gain);
        }

        private void populateOffset(float p)
        {
            Int16 offset = 0;
            try { offset = Convert.ToInt16(p); }
            catch { }
            OutReport.Insert(offset);
        }

        #endregion
    }
}
