﻿using PicoLite.Devices;
using PicoLite.OpCodes.Management;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class GetFastClockMode : PicoLiteOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x91 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x91 };

        #endregion
        #region Constructors
        public GetFastClockMode(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            ParentDevice.Status.TestMode = InReport.GetBool();

            if (ParentDevice.Status.TestMode)
                ParentDevice.Status.Interval = new TimeSpan(0, 0, Convert.ToInt32(ParentDevice.Status.Interval.TotalSeconds / 60));
        }
        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }
        #endregion
    }

}
