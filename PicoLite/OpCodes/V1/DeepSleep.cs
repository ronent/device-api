﻿using Base.OpCodes;
using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class DeepSleep : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x09 };

        #endregion
        #region Constructors
        public DeepSleep(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        #endregion
        #region Methods
        protected override void Parse()
        {
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        protected override void AckReceived()
        {
            Finish(Result.OK);
        }

        #endregion
    }
}
