﻿using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroXBase.OpCodes.Common.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
    }

}
