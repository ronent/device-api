﻿using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using System;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class Stop : OpCodeNoParseAck
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x14 };

        #endregion
        #region Constructors
        public Stop(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
        #region Methods
        protected override void DoAfterFinished()
        {
            ParentDevice.Status.IsRunning = false;
            ParentDevice.Functions.InvokeOnStatusReceived();
        }

        #endregion
    }
}
