﻿using Base.OpCodes;
using PicoLite.Devices;
using System;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroXBase.OpCodes.Common.FirmwareUploadManager
    {
        #region Fields
        public enum CommandEnum { START = 1, FINISH };

        public CommandEnum Command { get; protected set; }


        #endregion
        #region Constructors
        public FirmwareUploadManager(PicoLiteLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override bool SupportsAck { get { return false; } }

        #endregion
        #region Methods
        public override Result Start()
        {
            if (verifyFirmwareFile())
            {
                Command = CommandEnum.START;
                var result = Invoke().Result;

                if (result.IsOK)
                    return base.Start();
                else
                    return Result.ERROR;
            }
            else
                return new Result(eResult.ERROR) { MessageLog = "Firmware file is missing" };
        }

        private bool verifyFirmwareFile()
        {
            return ParentOpCodeManager.FirmwareUpload.IsFirmwareFileExists;
        }

        public override Result Finish()
        {
            Command = CommandEnum.FINISH;
            return Invoke().Result;
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            populateCommand();
        }

        private void populateCommand()
        {
            OutReport.Next = (byte)Command;
            OutReport.Next = (byte)Command;
        }

        #endregion
    }
}
