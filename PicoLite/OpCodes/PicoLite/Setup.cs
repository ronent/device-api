﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.OpCodes.Generic;
using Base;
using MicroLabAPI.Sensors;
using Base.Sensors;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using AuxiliaryLibrary;
using AuxiliaryLibrary.Tools;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class Setup : Base.OpCodes.Generic.Setup, ICloneableEx
    {
        #region Constructors
        public Setup(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        {
            CurrentTime = new Time();
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.Setup;
        }
        protected override void InitializeOpCodeAck()
        {
        }
        public override bool ParseAck(ref byte[] data)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
//            throw new Exception("Cannot parse acknowledgment packet.");
            return PacketSizeOverflow;
        }
        protected override bool VerifyAck()
        {
            return base.VerifyAck();
//            throw new NotSupportedException();
        }
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
            PopulateSetup();
            PopulateTime();
            PopulateLCDnLEDConfiguration();
            OutReport.Next = UTC;
            PopulateInterval();
            PopulateAlarms();
            PopulateTimerStatus();
            PopulateAlarmInfo();
            Index += 3; //Custom application data
            PopulateGainOffset();
            Index += 20; // Blank (cannot be used by app since an appropriate memory section is not available in status)
            PopulateStringOut(Comment, MicroLogDevice.CommentLength); //9 is the endbyte as Comment spans bytes 49 through 63
        }
        #endregion
        #region Setup OpCode Methods
        protected override void PopulateStatus()
        {
            throw new NotSupportedException();
        }
        protected override void PopulateSetup()
        {
            int ShiftIndex = 0;
            OutReport.Next = (byte)((Convert.ToByte(RunMode) << ShiftIndex) |
                        (Convert.ToByte(CyclicMode) << ShiftIndex + 1) |
                        (Convert.ToByte(PushRun) << ShiftIndex + 2) |
                        (Convert.ToByte(Fahrenheit) << ShiftIndex + 3) |
                        (Convert.ToByte(BoomerangActive) << ShiftIndex + 4));
        }
        protected override void PopulateTime()
        {
            CurrentTime = new Time(DateTime.Now);
            CurrentTime.Populate(ref OutReport);
            try
            {
                CurrentTime.ToDateTime();
            }
            catch (System.Exception ex)
            {
                throw ex;            	
            }
        }

        protected virtual void PopulateGainOffset()
        {
            UInt16 gain;
            Int16 offset;
            
            gain = deviceInfo.OpCodes.SetCalibration.InternalTemperatureGain;
            offset = deviceInfo.OpCodes.SetCalibration.InternalTemperatureOffset;

            OutReport.Next = (byte)((gain & 0xff00) >> 8);
            OutReport.Next = (byte)(gain & 0x00ff);

            OutReport.Next = (byte)((offset & 0xff00) >> 8);
            OutReport.Next = (byte)(offset & 0x00ff);
        }

        protected override void PopulateLCDnLEDConfiguration()
        {
            byte ShiftIndex = 3;
            OutReport.Next = (byte)((byte)RedLEDConfiguration |
                        (Convert.ToByte(RedLEDBlinkOnNormalized) << ShiftIndex)
                        | ((byte)(GreenLEDConfiguration) << ShiftIndex + 1));
        }

        protected override void PopulateInterval()
        {
            int Interval = this.Interval / (deviceInfo as PicoLiteDevice).IntervalUnits;
            OutReport.Next = (byte)(Interval & 0x00ff);
            OutReport.Next = (byte)((Interval & 0xff00) >> 8);
        }

        protected override void PopulateAlarms()
        {
            PopulateAlarmsAux(ref InternalTemperatureAlarm, (int)SensorIndexEnum.InternalTemperature);
        }

        protected override void PopulateAlarmInfo()
        {
            OutReport.Next = (byte)((AlarmDelay & 0xff00) >> 8);
            OutReport.Next = (byte)(AlarmDelay & 0x00ff);
        }

        protected override void PopulateAlarmsAux(ref int index, ref Alarm alarmValue, int sensorIndex)
        {
            if (alarmValue.CorrectedHigh.FractionalPartBaseMinus2 > 255)
                throw new Exception("Fractional part of AlarmValue exceeds expected value");

            float hi = alarmValue.CorrectedHigh.ToFloat();
            float lo = alarmValue.CorrectedLow.ToFloat();

            byte[] HighTemp = BitConverter.GetBytes(deviceInfo.SensorInfoList[sensorIndex].GetDigitalValueFromSensorValue(hi, true, false));
            OutReport[index++] = HighTemp[0];
            byte[] LowTemp = BitConverter.GetBytes(deviceInfo.SensorInfoList[sensorIndex].GetDigitalValueFromSensorValue(lo, true, false));
            OutReport[index++] = LowTemp[0];
        }
        protected override void PopulateTimerStatus()
        {
            byte[] TimeSpanInSecondsByteArray = new byte[] { 0, 0, 0, 0 };
            if (TimerRunEnabled)
            {
                Int32 TotalSeconds = -1;
                try
                {
                    TimeSpan TimerRunTimeSpan = TimerRunTime.ToDateTime() - CurrentTime.ToDateTime();
                    TotalSeconds = Convert.ToInt32(TimerRunTimeSpan.TotalSeconds);
                    TimeSpanInSecondsByteArray = BitConverter.GetBytes(TotalSeconds);
                }
                catch (System.Exception)
                {
                    TimerRunEnabled = false;
                }
                Array.Copy(TimeSpanInSecondsByteArray, 0, OutReport, Index, sizeof(UInt32));
                Index += sizeof(UInt32);
                OutReport.Next = 0x01; //This byte determines if it's a timer run
                deviceInfo.Log("[Setup] Timer run set for: " + TotalSeconds + " seconds [" + TimeSpanInSecondsByteArray[0].ToString("X") + ", "
                            + TimeSpanInSecondsByteArray[1].ToString("X") + ", "
                            + TimeSpanInSecondsByteArray[2].ToString("X") + ", "
                            + TimeSpanInSecondsByteArray[3].ToString("X") + "]");
            }
            else
            {
                Array.Copy(TimeSpanInSecondsByteArray, 0, OutReport, Index, sizeof(UInt32));
                Index += sizeof(UInt32);
                OutReport.Next = 0x00; //This byte determines if it's a timer run
            }
        }
        #endregion
        #region Methods
        protected override void PopulateExternalSensorStatus()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region ICloneableEx Members

        public bool Cloning
        {
            get;
            set;
        }

        #endregion
    }
}
