﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using AuxiliaryLibrary.Tools;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class Time : MicroLabAPI.OpCodes.Time
    {
        #region Properties
        public static Time Now
        {
            get { return new Time(DateTime.Now); }
        }
        #endregion
        #region Constructors
        public Time()
            : base()
        {
        }
        public Time(MicroLabAPI.OpCodes.Time TimeObject)
            : base(TimeObject)
        {
        }
        public Time(DateTime TimeObject)
            : base(TimeObject)
        {
        }
        #endregion
        #region Time Methods
        public override void Parse(byte[] Report, ref long Index)
        {
            Year = Transformation.BCD2Byte(Report[Index++]);
            Month = Transformation.BCD2Byte(Report[Index++]);
            Day = Transformation.BCD2Byte(Report[Index++]);
            Hour = Transformation.BCD2Byte(Report[Index++]);
            Minute = Transformation.BCD2Byte(Report[Index++]);
            if (Minute >= 60)
            {
                Console.Write("Incorrect Minute format");
                Minute = 59;
            }
            Second = Transformation.BCD2Byte(Report[Index++]);
            if (Second >= 60)
            {
                Console.Write("Incorrect Second format");
                Second = 59;
            }
        }
        public void ParseAux(byte[] Report, ref long Index)
        {
//             Month += (byte)((Report[Index] & 0xC0) >> 6);
//             Day = (byte)((Report[Index] & 0x3E) >> 1);
//             Hour = (byte)((Report[Index++] & 0x01) << 4);
//             Hour += (byte)((Report[Index] & 0xF0) >> 4);
//             Minute = (byte)((Report[Index++] & 0x0F) << 2);
//             Minute += (byte)((Report[Index] & 0xC0) >> 6);
//             Second = (byte)((Report[Index++] & 0x3F));
        }
        public override void Set(DateTime dateTime)
        {
            Year = Convert.ToByte(dateTime.Year % 100);
            Month = Convert.ToByte(dateTime.Month);
            Day = Convert.ToByte(dateTime.Day);
            Hour = Convert.ToByte(dateTime.Hour);
            Minute = Convert.ToByte(dateTime.Minute);
            Second = (dateTime.Second);
        }
        #endregion
        #region Methods
        public override bool ParseDataPacketTime(byte[] Report, ref long Index, MicroLabAPI.OpCodes.Time StartingTime)
        {
            this.Year = StartingTime.Year;
            this.Month = StartingTime.Month;
            this.Day = StartingTime.Day;
            this.Hour = Report[Index++];
            this.Minute = Report[Index++];
            this.Second = Report[Index++];
            return true;
        }
        public override void Populate(ref byte[] Report, ref long Index)
        {
            Report[Index++] = Transformation.IntToBCD(Year);
            Report[Index++] = Transformation.IntToBCD(Month);
            Report[Index++] = Transformation.IntToBCD(Day);
            Report[Index++] = Transformation.IntToBCD(Hour);
            Report[Index++] = Transformation.IntToBCD(Minute);
            Report[Index++] = Transformation.IntToBCD(Second);
        }

        public void ParseTimeStamp(byte[] Report, MicroLabAPI.OpCodes.Time StartingTime)
        {
            /* Byte 1:
             * 4 upper bits store last 4 bits of year
             * 4 lower bits store month
             * Byte 2:
             * 6 lower bits store day
             * Byte 3: Hour
             * Byte 4: Minute
             * Byte 5: Seconds
             */

            Year = Convert.ToByte(Report[Index] >> 4);
            Month = Convert.ToByte(Report[Index++] & 0x0f);
            FindCorrectYear(StartingTime);
            Day = Transformation.BCD2Byte(Convert.ToByte(Report[Index++] & 0x3F));
            Hour = Transformation.BCD2Byte(Report[Index++]);
            Minute = Transformation.BCD2Byte(Report[Index++]);
            Second = Transformation.BCD2Byte(Report[Index++]);
        }

        private void FindCorrectYear(MicroLabAPI.OpCodes.Time StartingTime)
        {
            //StartingTime.Year consists of the last 2 digits of the starting year
            //for our purposes we require just the last digit
            int year = StartingTime.Year % 10;
            if(year > Year) 
                Year = Convert.ToByte(StartingTime.Year - 1);
            else if (year < Year)
                Year = Convert.ToByte(StartingTime.Year + 1);
            else
                Year = StartingTime.Year;
        }
        #endregion
        #region ICloneable Members
        public override object Clone()
        {
            return new Time(this);
        }
        #endregion

    }
}
