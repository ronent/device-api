﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class Download : MicroXBase.OpCodes.Common.Download
    {

        #region OpCode Methods
        public override bool ParseAck(ref byte[] data)
        {
                PopulateAddresses();
                PopulateDownloadTimeDate();
                MessageType = MessageTypeEnum.NumberOfPackets;
        }
       
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
            OutReport.Next = Convert.ToByte(Command);
        }
        #endregion
        #region Download OpCode Methods

        private void PopulateDownloadTimeDate()
        {
            StartTimeDate.Parse(InReport);
        }
        #endregion

    }
}
