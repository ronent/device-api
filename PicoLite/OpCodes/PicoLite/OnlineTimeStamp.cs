﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Definitions.EC8xx;
using Base;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using Base.Defintions;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class OnlineTimeStamp : OnlineDataPacket
    {
        #region Constructors
        public OnlineTimeStamp(GenericDevice deviceInfo)
            : base(deviceInfo)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.OnlineTimeStamp;
        }
        public override bool ParseAck(ref byte[] data)
        {
            throw new NotSupportedException("Not Implemented. Use ParseAck(data, Status) instead.");
        }
        protected override bool VerifyAck()
        {
            return AckOpCode[0] == InReport[Index++];
        }
        #endregion
        #region DataPacket OpCode Methods
        public override ResultEnum ParseAck(byte[] data, ref DateTime FirstPacketDateTime)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
            int Index = OpCodes.StartByte;
            if (VerifyAck())
                ParseData(ref FirstPacketDateTime, true);
            
            return ResultEnum.EndOfPackets;
        }
        protected override void PopulateReport()
        {
            throw new Exception("Illegal function call PopulateReport");
        }
        #endregion
    }
}
