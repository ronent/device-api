﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroLabAPI.OpCodes.PicoLite
{
    public enum OpCodeSendEnum : byte
    {
        Reset = 0x09,
        Setup = 0x11,
        Status = 0x12,
        Run = 0x13,
        Stop = 0x14,
        Download = 0x15,
        DownloadTimeStamp = 0x18,
        SetSN = 0x16,
        TimerRun = 0x19,
        TimeStamp = 0x1A,
        SetCalibration = 0x17,
        FirmwareUpload = 0x31,
        FirmwareUploadManager = 0x33,
        TestHardware = 0x42,
        GetBoomerang = 0x43,
        SetBoomerang = 0x44,
        SetDebugMode = 0x90,
        GetDebugMode = 0x91,
        MarkTimeStamp = 0x50,
    };

    public enum OpCodeAckEnum : byte
    {
        Status = 0x21,
        Calibration = 0x22,
        DownloadInfo = 0x23,
        DownloadTimeStampInfo = 0x20,
        DataPacket = 0x24,
        TimeStampPacket = 0x25,
        AckOpCode = 0x28,
        OnlineDataPacket = 0x29,
        OnlineTimeStamp = 0x30,
        FirmwareSegmentUploaded = 0x32,
        TestHardware = 0x41,
        GetBoomerang = 0x43,
        GetDebugMode = 0x91,
        MarkTimeStamp = 0x50,
    };

    public struct OpCodes
    {
        public static readonly int ReportSize = 65;
        public static readonly int StartByte = 1;

        public const float RefVoltage = 1.2f;
        public const float AtoDPrecision = 10f;
        public const float MaxVoltage = 3.3f;

        public struct Readings
        {
            public const byte Maximum = 0xA0;
            public const byte Minimum = 0x00;
            public const byte ExceedsMaximum = 0xEE;
            public const byte ExceedsMinimum = 0xDD;
            public const byte FlushByte = 0xE0;
        }

        public struct Send
        {
            public static readonly byte[] Reset = { 0x09 };
            public static readonly byte[] SetSN = { 0x16 };
            public static readonly byte[] TimerRun = { 0x19 };
            public static readonly byte[] TimeStamp = { 0x1A };
            public static readonly byte[] TestHardware = { 0x42 };
            public static readonly byte[] SetDebugMode = { 0x90 };
            public static readonly byte[] GetDebugMode = { 0x91 };
            public static readonly byte[] MarkTimeStamp = { 0x50 };
        }

        public struct Ack
        {
            public static readonly byte[] AckOpCode = { 0x28 };
            public static readonly byte[] TestHardware = { 0x41 };
            public static readonly byte[] GetDebugMode = { 0x91 };
            public static readonly byte[] MarkTimeStamp = { 0x50 };
        }

        public const int OpCodeAcknowledgedIndex = 2;
    }
}
