﻿using Base.Devices;
using Base.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    public abstract class DebugMode : OpCode
    {
        #region Enums, Consts, etc.
        public enum StateEnum { Off = 0x00, On = 0x01 };
        #endregion
        #region Properties
        public StateEnum State
        {
            get;
            protected set;
        }
        protected StateEnum outgoingState
        {
            get;
            set;
        }
        #endregion
        #region Constructors
        public DebugMode(GenericDevice deviceInfo)
            : base(deviceInfo)
        { }
        #endregion
        #region Methods
        public void Toggle()
        {
            outgoingState = State == StateEnum.On ? StateEnum.Off : StateEnum.On;
        }
        #endregion
    }
}