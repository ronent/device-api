﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class FirmwareUploadDone : EC8xx.FirmwareUploadDone
    {
        #region Consts
        public enum StateEnum : byte { Start = 0x01, Finish = 0x02 };
        #endregion
        #region Properties
        public StateEnum State
        {
            get;
            set;
        }
        #endregion
        #region Constructors
        public FirmwareUploadDone(GenericDevice deviceInfo)
            : base(deviceInfo)
        {
        }
        #endregion
        #region Populate Methods
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
            PopulateState();
        }

        protected void PopulateState()
        {
            OutReport.Next = 
                OutReport.Next = (byte)State;
            deviceInfo.Log("FirmwareUploadDone state: " + State.ToString());
        }
        #endregion

    }
}
