﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class TestHardware : Base.OpCodes.Generic.Test
    {
        #region Constructors
        public TestHardware(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.TestHardware;
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.TestHardware;
        }
        public override bool ParseAck(ref byte[] data)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
            int Index = OpCodes.StartByte;
            if (VerifyAck())
            {
                Passed = Convert.ToBoolean(data[Index++]);
            }

            return PacketSizeOverflow;
        }
        #endregion
        #region Populate data Methods
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
        }
        #endregion
    }

}
