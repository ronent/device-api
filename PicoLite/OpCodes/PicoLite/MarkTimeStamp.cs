﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using MicroLabAPI.OpCodes.Generic;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class MarkTimeStamp : Base.OpCodes.Generic.MarkTimeStamp
    {
        #region Properties
        #endregion
        #region Constructors
        public MarkTimeStamp(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        { }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.MarkTimeStamp;
        }
        protected override void InitializeOpCodeAck()
        {
        }
        public override bool ParseAck(ref byte[] data)
        {
            throw new NotSupportedException();
        }

        protected override bool VerifyAck()
        {
            throw new NotSupportedException();
        }
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
        }
        #endregion
    }
}
