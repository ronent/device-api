﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class Reset : OpCode
    {
        #region Variables

        #endregion
        #region Constructors
        public Reset(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        {

        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.Reset;
        }
        protected override void InitializeOpCodeAck()
        {
        }
        public override bool ParseAck(ref byte[] data)
        {
            throw new NotSupportedException();
        }
       
        protected override void PopulateReport()
        {
            base.PopulateReport();

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
        }
        #endregion
    }
}
