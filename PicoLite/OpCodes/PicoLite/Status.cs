﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.OpCodes.Generic;
using MicroLabAPI.Sensors;
using MicroLabAPI.Devices;
using AuxiliaryLibrary;
using Base.Sensors;
using MicroLabAPI.Definitions.EC8xx;
using AuxiliaryLibrary.Tools;
using AuxiliaryLibrary.MathLib;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class Status : Setup
    {
        #region OpCode Methods
        public override bool ParseAck(ref byte[] data)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
            int Index = deviceInfo.StartByte;
            if (VerifyAck())
            {
                PopulateLCDnLEDConfiguration();
                PopulateUTC();
                PopulateInterval();
                PopulateAlarms();
                Index += 3; //Blank
                PopulateBatteryLevel();
                PopulateCycleCompleted();
                PopulateTimerStatus();
                PopulateAlarmInfo();
                Index += 3; //Custom application data
                DeviceType = (DeviceTypes.Enums)InReport[Index++];              
                PopulateFirmwareVersion();
                if (FirmwareVersion < 0.59f)
                    DeviceType = DeviceTypes.Enums.PicoLite;
                PCBVersion = InReport[Index++];
                PopulateSerialNumber();
                PopulateGainOffset();
                PopulateStringIn(MicroLogDevice.CommentLength, ref _Comment);

                //The only way to go for this logger
                InternalTemperatureSensorEnabled = true;
                OnStatusArrived(this);
            }
            return PacketSizeOverflow;
        }

        protected override void PopulateReport()
        {
            OpCode.InitializeArray(ref OutReport);

            int Index = OpCodes.StartByte;
            PopulateOpCodeSend();
        }
        #endregion
        #region Setup OpCode Methods

        protected override void PopulateLCDnLEDConfiguration()
        {
            byte ShiftIndex = 3;
            RedLEDConfiguration = (RedLEDConfigurationEnum)((byte)(InReport[Index] & 0x07));
            RedLEDBlinkOnNormalized = Convert.ToBoolean((byte)((InReport[Index] & 0x08) >> ShiftIndex));
            GreenLEDConfiguration = (GreenLEDConfigurationEnum)((byte)(InReport[Index] & 0x10));
            Index++;
        }
        protected override void PopulateExternalSensorStatus()
        {
            ExternalSensorType = (eSensorType)(InReport[Index] & 0x0F);
            deviceInfo.Log("Status on ExternalSensorType: " + ExternalSensorType);
            ExternalSensorUnitCode = (ExternalSensorUnitEnum)(InReport[Index] & 0xF0);
            ExternalSensorEnabled = (ExternalSensorType != eSensorType.None);
            Index++;
        }

        protected override void PopulateInterval()
        {
            Interval = (UInt16)(InReport[Index++]);
            Interval += (UInt16)(InReport[Index++] << 8);
            Interval *= (deviceInfo as PicoLiteDevice).IntervalUnits; //Since Interval is measured in minutes
        }


        protected void PopulateUTC()
        {
            UTC = InReport[Index++];
        }

        protected override void PopulateAlarms()
        {
            PopulateAlarmsAux(ref InternalTemperatureAlarm, (int)SensorIndexEnum.InternalTemperature, true);
        }

        protected void PopulateAlarmsAux(ref int index, ref Alarm alarmValue, int sensorIndex, bool isTemperatureSensor)
        {
            //PicoLite requires values in celsius
            alarmValue.Fahrenheit = false;
            alarmValue.TemperatureSensor = isTemperatureSensor;
            alarmValue.High = deviceInfo.SensorInfoList[sensorIndex].GetSensorValueFromDigitalValue(InReport[index++], true, false);
            alarmValue.Low = deviceInfo.SensorInfoList[sensorIndex].GetSensorValueFromDigitalValue(InReport[index++], true, false);

            float sensorMin = deviceInfo.SensorInfoList[sensorIndex].SensorMinimum;
            float sensorMax = deviceInfo.SensorInfoList[sensorIndex].SensorMaximum;
            float low = alarmValue.CorrectedLow.ToFloat();
            float high = alarmValue.CorrectedHigh.ToFloat();
            #region Active
            alarmValue.Active = low >= sensorMin
                && high <= sensorMax
                && high - low != sensorMax - sensorMin
                && low < high;
            #endregion
        }
        #endregion
        #region Status OpCode Methods
        protected void PopulateSerialNumber()
        {
            PopulateStringIn(MicroLogDevice.SNLength, ref _SN);
            try
            {
                Convert.ToUInt32(_SN);
            }
            catch
            {
                _SN = "0";
            }
        }

        protected override void PopulateFirmwareVersion()
        {
            IntegerFraction decimalValue = new IntegerFraction();
            decimalValue.IntegerPart = Convert.ToInt16(InReport[Index++]);
            decimalValue.FractionalPartBaseMinus2 = Convert.ToUInt16(InReport[Index++]);

            FirmwareVersion = decimalValue.ToFloat();
        }
        protected override void PopulateAlarmInfo()
        {
            if (InReport[Index] == 0xFF
                 && InReport[Index + 1] == 0xFF)
            {
                AlarmDelay = 0;
                Index += 2;
            }
            else
            {
                AlarmDelay = (UInt16)(InReport[Index++] << 8);
                AlarmDelay += (UInt16)(InReport[Index++]);
            }
        }
        protected override void PopulateTimerStatus()
        {
            TimerRunEnabled = InReport[Index + 4] == 0x01;
            try
            {
                if (TimerRunEnabled)
                {
                    Int32 TotalSeconds = BitConverter.ToInt32(InReport, Index);
                    deviceInfo.Log("[Status] Timer run set for: " + TotalSeconds + " seconds [" + InReport[Index].ToString("X") + ", "
                        + InReport[Index + 1].ToString("X") + ", "
                        + InReport[Index + 2].ToString("X") + ", "
                        + InReport[Index + 3].ToString("X") + "]");

                    TimeSpan TimerSpan = TimeSpan.FromSeconds(TotalSeconds);
                    TimerRunTime = new Time(CurrentTime.ToDateTime().AddSeconds(TimerSpan.TotalSeconds));

                    if (TimerRunTime.ToDateTime() < DateTime.Now)
                        throw new Exception("Timer expired");
                }
                else
                    throw new Exception("Timer run disabled");
            }
            catch (System.Exception)
            {
                TimerRunTime = new Time();
                TimerRunEnabled = false;
            }
            Index += 5;
        }

        protected override void PopulateBatteryLevel()
        {
            int Vbg = (InReport[Index] << 8) + InReport[Index + 1];
            Index += 2;
            int AtoD = (InReport[Index] << 8) + InReport[Index + 1];
            Index += 2;

            BatteryLevel = (byte)(OpCodes.RefVoltage * (float)AtoD * OpCodes.AtoDPrecision * 100 / (float)Vbg / OpCodes.MaxVoltage);
            if (BatteryLevel > 100)
                BatteryLevel = 100;
            else if(BatteryLevel < 0)
                BatteryLevel = 0;
        }

        protected override void PopulateGainOffset()
        {
            deviceInfo.OpCodes.Setup.IncludeCalibration = false;
            deviceInfo.OpCodes.GetCalibration.InternalTemperatureGain = (UInt16)(InReport[Index++] << 8);
            deviceInfo.OpCodes.GetCalibration.InternalTemperatureGain += InReport[Index++];

            deviceInfo.OpCodes.GetCalibration.InternalTemperatureOffset = (Int16)(InReport[Index++] << 8);
            deviceInfo.OpCodes.GetCalibration.InternalTemperatureOffset += InReport[Index++];
        }

        private void PopulateCycleCompleted()
        {
            MemoryFull = InReport[Index++] == 0xAA;

//            Index += 2; //Picolite writes in blocks of 2 and in this case the next byte simply contains the same value.    
        }
        #endregion
    }
}
