﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Definitions.EC8xx;
using Base;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using Base.Defintions;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class OnlineDataPacket : DataPacket
    {
        #region Constructors
        public OnlineDataPacket(GenericDevice deviceInfo)
            : base(deviceInfo)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.OnlineDataPacket;
        }
        public override bool ParseAck(ref byte[] data)
        {
            return base.ParseAck(ref data);
        }
        protected override bool VerifyAck()
        {
            return AckOpCode[0] == InReport[Index++];
        }
        #endregion
        #region DataPacket OpCode Methods
        public override ResultEnum ParseAck(byte[] data, ref DateTime FirstPacketDateTime)
        {
            bool PacketSizeOverflow = ParseAck(ref data);
            int Index = OpCodes.StartByte;
            if (VerifyAck())
                ParseData(ref FirstPacketDateTime, false);
            
            return ResultEnum.EndOfPackets;
        }
        protected ResultEnum ParseData(ref DateTime FirstPacketDateTime, bool IncludeTimeStamp)
        {
            Time StampTime = new Time();
            StampTime.Parse(InReport);
//             if (FirstPacketDateTime == DateTime.MinValue)
//                 FirstPacketDateTime = StampTime.ToDateTime();

            if (InReport[Index] == OpCodes.Readings.ExceedsMaximum)
                InReport[Index] = OpCodes.Readings.Maximum;
            else if (InReport[Index] == OpCodes.Readings.ExceedsMinimum)
                InReport[Index] = OpCodes.Readings.Minimum;
            else if (InReport[Index] > MAXIMUM_DIGITAL_VALUE)
                return ResultEnum.EndOfPackets;
            AddReading(GetDigitalReading(), StampTime, IncludeTimeStamp, TimeStampTypeEnum.NORMAL);

            return ResultEnum.EndOfPackets;
        }
        protected override void PopulateReport()
        {
            throw new Exception("Illegal function call PopulateReport");
        }
        #endregion
    }
}
