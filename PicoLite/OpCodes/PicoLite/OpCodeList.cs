﻿using System.Collections.Generic;
using System;
using System.Windows.Forms;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using MicroLabAPI.OpCodes.PicoLite;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class OpCodeList : MicroLabAPI.OpCodes.EC8xx.OpCodeList
    {
        #region Constructor
        public OpCodeList(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        {
            try
            {
                this.Test = new TestHardware(deviceInfo);
                this.Status = new Status(deviceInfo);
                this.Setup = new Setup(deviceInfo);
                this.FirmwareUploadDone = new FirmwareUploadDone(deviceInfo);
                this.DataPacket = new DataPacket(deviceInfo);
                this.TimeStampPacket = new TimeStampPacket(deviceInfo);
                this.Download = new Download(deviceInfo);
                this.DownloadTimeStamp = new DownloadTimeStamp(deviceInfo);
                this.Reset = new Reset(deviceInfo);
                this.OnlineDataPacket = new OnlineDataPacket(deviceInfo);
                this.OnlineTimeStamp = new OnlineTimeStamp(deviceInfo);
                this.SetDebugMode = new SetDebugMode(deviceInfo);
                this.GetDebugMode = new GetDebugMode(deviceInfo);
                this.MarkTimeStamp = new MarkTimeStamp(deviceInfo);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Methods
        public override AnalysisResultEnum Analyze(byte[] data, out ErrorCodeEnum errorCode)
        {
            errorCode = ErrorCodeEnum.None;
            if (data[1] == 0xe0)
                return AnalysisResultEnum.DownloadNextPacket;
            switch ((OpCodeAckEnum)ParseAckOpCode(data))
            {
                case OpCodeAckEnum.GetDebugMode:
                    GetDebugMode.ParseAck(ref data);
                    break;
                case OpCodeAckEnum.DownloadTimeStampInfo:
                    DownloadTimeStamp.ParseAck(ref data);
                    return AnalysisResultEnum.TimeStampDownloadInfoReceived;
                case OpCodeAckEnum.FirmwareSegmentUploaded:
                    (deviceInfo as PicoLiteDevice).SendFictiveFWUploadAck();
                    return base.Analyze(data, out errorCode);
                case OpCodeAckEnum.OnlineTimeStamp:

                    if (deviceInfo.Ready)
                    {
                        try
                        {
                            OnlineTimeStamp.ParseAck(data, ref deviceInfo.DownloadStartTime);
                            if (!deviceInfo.Running)
                                return AnalysisResultEnum.DownloadPacketReceived;
                            else
                                return AnalysisResultEnum.OnlinePacketReceived;
                        }
                        catch (System.Exception ex)
                        {
                            deviceInfo.Log(ex.Message);
                        }
                    }
                    break;
                case OpCodeAckEnum.OnlineDataPacket:
                    //EC8xx and picolite have different opcodes for OnlineDataPacket.
                    //Otherwise the mechanism that handles it is generic regardless of the device
                    //Therefore i delegate this action to base.Analyze
                    return Analyze(MicroLabAPI.OpCodes.EC8xx.OpCodeAckEnum.OnlineDataPacket, data, ref errorCode);
                case OpCodeAckEnum.TimeStampPacket:
                        try
                        {
                            deviceInfo.Downloading = true;
                            if (TimeStampPacket.ParseAck(data) == MicroLabAPI.OpCodes.Generic.DataPacket.ResultEnum.NextPacketAvailable)
                                return AnalysisResultEnum.DownloadNextTimeStampPacket;
                            else
                                return AnalysisResultEnum.FinishedDownloadingTimeStamps;
                        }
                        catch (System.Exception ex)
                        {
                            deviceInfo.Log(ex.Message);
                            return AnalysisResultEnum.Error;
                        }
                        finally
                        {
                            deviceInfo.Downloading = false;
                        }
                case OpCodeAckEnum.TestHardware:
                        Test.ParseAck(ref data);
                        (deviceInfo as PicoLiteDevice).HardwareTestComplete();
                        break;
                    //This is a bug in picolite that sends opcode 0x44 instead of 0x43 for GetBoomerang
                case (OpCodeAckEnum)OpCodeSendEnum.SetBoomerang:
                        data[deviceInfo.StartByte] = (byte)OpCodeAckEnum.GetBoomerang;
                        goto default;
                default:
                    return base.Analyze(data, out errorCode);
            }
            return AnalysisResultEnum.None;           
        }

        protected override void SortOutIncomingDataStream(byte[] data)
        {
            throw new NotImplementedException();
        }
        #endregion
        #region Initializing
        public override void Initialize(SendDelegate Method, AddSensorReadingDelegate AddSensorReading, AddTimeStampDelegate AddTimeStamp)
        {
            base.Initialize(Method, AddSensorReading, AddTimeStamp);

            Reset.Send =
                DownloadTimeStamp.Send =
                TimeStampPacket.Send =
                Test.Send =
                SetDebugMode.Send =
                GetDebugMode.Send = 
                MarkTimeStamp.Send = Method;

            TimeStampPacket.AddSensorReading = AddSensorReading;
            TimeStampPacket.AddTimeStamp = AddTimeStamp;
        }

        public override void Initialize(GenericDevice deviceInfo, SendDelegate Method, AddSensorReadingDelegate AddSensorReading, AddTimeStampDelegate AddTimeStamp)
        {
            Reset.Initialize(deviceInfo);
            DownloadTimeStamp.Initialize(deviceInfo);
            TimeStampPacket.Initialize(deviceInfo);
            Test.Initialize(deviceInfo);
            SetBoomerang.Initialize(deviceInfo);
            GetBoomerang.Initialize(deviceInfo);

            base.Initialize(deviceInfo, Method, AddSensorReading, AddTimeStamp);
        }
        #endregion
    }
}