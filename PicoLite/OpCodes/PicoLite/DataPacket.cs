﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Definitions.EC8xx;
using Base;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using Base.Devices;
using Base.Sensors;
using Base.Defintions;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class DataPacket : Base.OpCodes.Generic.DataPacket
    {
        #region Consts
        #endregion
        #region Properties
        protected UInt16 PacketAddress
        {
            get;
            set;
        }
        protected virtual UInt16 PacketNumber
        {
            get
            {
                return Convert.ToUInt16((PacketAddress - deviceInfo.OpCodes.Download.StartAddress) / 64);
            }
        }
        private DateTime startTime;
        #endregion
        #region Constructors
        public DataPacket(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.DataPacket;
        }
        public override bool ParseAck(ref byte[] data)
        {
            return base.ParseAck(ref data);
            //throw new NotSupportedException("Not Implemented. Use ParseAck(data, Status) instead.");
        }
        public override ResultEnum ParseAck()
        {
            throw new NotSupportedException();
        }
        #endregion
        #region DataPacket OpCode Methods
        private void DecreaseCurrentTimeByCycleStartTime()
        {
            /*
             * I add(/subtract) 1 at the end in order to include AbsEndAddress within the spec
             * There is no need to include EndAddress since it is not included as a valid point
             */ 
            int span = deviceInfo.OpCodes.Download.AbsEndAddress - deviceInfo.OpCodes.Download.EndAddress - 1;
            deviceInfo.OpCodes.Download.StartTimeDate.Decrease(deviceInfo.TimeBetweenSamples * span);
            CurrentTime = new Time(deviceInfo.OpCodes.Download.StartTimeDate);
        }

        protected override ResultEnum ParseData(ref DateTime FirstPacketDateTime)
        {
            int i;
            int numberOfEntries = (int)Download.NUMBER_OF_SAMPLES_PER_PACKET;
            try
            {
                /* First parse packet number */
                PacketAddress = BitConverter.ToUInt16(InReport, Index);
                //                deviceInfo.Log("Packet address: " + PacketAddress.ToString("X"));
                Index += 2;

                if (FirstPacketDateTime == DateTime.MinValue)
                {
                    //Here I subtract -1 so that SaveData is run in CheckDownloadedDataForAlarms
                    FirstPacketDateTime = CurrentTime.ToDateTime().AddSeconds(-1);
                    startTime = FirstPacketDateTime;
                }
                CurrentAddress = PacketAddress;
                if (CurrentAddress < deviceInfo.OpCodes.Download.StartAddress)
                    return ResultEnum.Error;
                int RealEndAddress = deviceInfo.OpCodes.Status.MemoryFull 
                    ? deviceInfo.OpCodes.Download.AbsEndAddress 
                    : deviceInfo.OpCodes.Download.EndAddress;
                for (i = 0; i < numberOfEntries && CurrentAddress < RealEndAddress; i++)
                {
                    if (CurrentAddress == deviceInfo.OpCodes.Download.AbsEndAddress - 1)
                        Console.Write("ALRIGHT!!");

                    //Flush byte is an empty byte created when download is invoked before the 2-byte array had been flushed. Therefore the second byte is empty.
                    if (InReport[Index] != OpCodes.Readings.FlushByte)
                    {
                        if (InReport[Index] == OpCodes.Readings.ExceedsMaximum)
                            InReport[Index] = OpCodes.Readings.Maximum;
                        else if (InReport[Index] == OpCodes.Readings.ExceedsMinimum)
                            InReport[Index] = OpCodes.Readings.Minimum;
                        else if (InReport[Index] > MAXIMUM_DIGITAL_VALUE)
                            //return ResultEnum.EndOfPackets;
                            continue;

                        deviceInfo.QueueProgress =
                            Convert.ToByte(
                                100
                                * (CurrentAddress - deviceInfo.OpCodes.Download.StartAddress)
                                / (RealEndAddress - deviceInfo.OpCodes.Download.StartAddress));

                        ushort digValue = GetDigitalReading();
                        if (digValue < 0x20)
                            Console.Write("");
                        AddReading(digValue, (Time)CurrentTime, false, TimeStampTypeEnum.NORMAL);
                        CurrentTime.Increase(deviceInfo.TimeBetweenSamples);
                    }
                    else
                    {
                        Index++;
                    }

                    if ((CurrentAddress == deviceInfo.OpCodes.Download.EndAddress)
                        && deviceInfo.OpCodes.Status.MemoryFull)
                    {
                        DecreaseCurrentTimeByCycleStartTime();
                    }
                    CurrentAddress++;
                }
                return CurrentAddress < RealEndAddress ? ResultEnum.NextPacketAvailable : ResultEnum.EndOfPackets;
            }
            catch (System.Exception ex)
            {
                ExpectedPacketTime = null;
                throw ex;
            }
        }

        protected virtual UInt16 GetDigitalReading()
        {
            return Convert.ToUInt16(InReport[Index++]);
        }
        protected virtual void AddReading(UInt16 Reading, Time StampTime, bool IncludeTimeStamp, TimeStampTypeEnum timeStampType)
        {
            float value = deviceInfo.SensorInfoList[(int)SensorIndexEnum.InternalTemperature].GetSensorValueFromDigitalValue(Reading, true, false);
            (deviceInfo as PicoLiteDevice).IncorporateCalibrationIntoReading(ref value);
            AddSensorReading(SensorIndexEnum.InternalTemperature,
                value,
                StampTime.ToDateTime());

            if (IncludeTimeStamp)
            {
                AnnotatedTimeStamp ats = new AnnotatedTimeStamp(StampTime.ToDateTime(), value, timeStampType);
                addTimeStamp(ats);
            }   
        }
        #endregion
    }
}
