﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using MicroLabAPI.OpCodes.Generic;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class GetDebugMode : DebugMode
    {
        #region Properties
        #endregion
        #region Constructors
        public GetDebugMode(GenericDevice deviceInfo)
            : base(deviceInfo, ProtocolTypeEnum.PicoLite)
        { }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.GetDebugMode;
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.GetDebugMode;
        }
        public override bool ParseAck(ref byte[] data)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
            int Index = OpCodes.StartByte;
            if (VerifyAck())
            {
                this.State = (StateEnum)InReport[Index++];
            }
            return PacketSizeOverflow;
        }

        protected override void PopulateReport()
        {
            int index = OpCodes.StartByte;
            PopulateReport(ref index);
        }

        protected void PopulateReport(ref int index)
        {
            base.PopulateReport();
            PopulateOpCodeSend(ref index);
        }
        #endregion
    }
}
