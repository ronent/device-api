﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroLabAPI.Definitions.EC8xx;
using Base;
using MicroLabAPI.Devices;
using MicroLabAPI.Settings;
using Base.Devices;
using Base.Sensors;
using Base.Defintions;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class TimeStampPacket : DataPacket
    {
        #region Consts
        protected const int PACKET_DATETIME_LENGTH = 5;
        protected const int MAXIMUM_NUMBER_OF_ENTRIES = 10; //This is because each entry is 1 byte long and the first 3 bytes are taken by opcode and packet address
        #endregion
        #region Variables
        List<DateTime> TimeStampHistory = new List<DateTime>();
        #endregion
        #region Properties
        protected override UInt16 PacketNumber
        {
            get
            {
                return Convert.ToUInt16((PacketAddress - deviceInfo.OpCodes.DownloadTimeStamp.StartAddress) / 64);
            }
        }
        public override bool Valid
        {
            get { return CurrentAddress != 0; }
        }
        #endregion
        #region Constructors
        public TimeStampPacket(GenericDevice deviceInfo)
            : base(deviceInfo)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.TimeStampPacket;
        }
        public override bool ParseAck(ref byte[] data)
        {
            throw new NotSupportedException("Not Implemented. Use ParseAck(data, Status) instead.");
        }
        #endregion

        #region DataPacket OpCode Methods
        public override ResultEnum ParseAck(byte[] data)
        {
            bool PacketSizeOverflow = base.ParseAck(ref data);
            int Index = OpCodes.StartByte;
            if (VerifyAck())
            {
                /*
                 * The section below was removed due to inability to run download while in online mode. 
                 * The fix stops online streaming all samples taken between the time of the download request and the end of packets
                 * are not recorded in the ExpectedNumberOfPackets therefore the discrepancy. Download actually finishes only when an appropriate msg is received from device
                 */
                if (CurrentAddress > deviceInfo.OpCodes.DownloadTimeStamp.EndAddress)
                        return ResultEnum.EndOfPackets;

                if (ParseData() == ResultEnum.EndOfPackets)
                {
                    return ResultEnum.EndOfPackets;
                }
            }

            return ResultEnum.NextPacketAvailable;
        }

        protected ResultEnum ParseData()
        {
            try
            {
                /* First parse packet number */
                PacketAddress = BitConverter.ToUInt16(InReport, Index);
                Index += 2;
                CurrentAddress = PacketAddress;
                int numberOfEntries = Math.Min(MAXIMUM_NUMBER_OF_ENTRIES, (int)(deviceInfo.OpCodes.DownloadTimeStamp.EndAddress - CurrentAddress));
                int i = 0;
                while (Index < InReportSize - 1)
                {
                    try
                    {
                        int tmpIndex = Index;
                        deviceInfo.QueueProgress = Convert.ToByte(100 * (PacketNumber * numberOfEntries + i) / (ExpectedNumberOfPackets * numberOfEntries));
                        if (InReport[Index] > MAXIMUM_DIGITAL_VALUE)
                            return ResultEnum.EndOfPackets;

                        UInt16 Reading = GetDigitalReading();
                        TimeStampTypeEnum timeStampType = (TimeStampTypeEnum)(InReport[Index + 1] >> 6);
                        CurrentTime = ParseTime();
                        CurrentAddress += Convert.ToUInt16(Index - tmpIndex);
                        AddReading(Reading, (Time)CurrentTime, true, timeStampType);
                        i++;
                    }
                    catch (System.Exception ex)
                    {
                        Console.Write(ex.Message);
                        return ResultEnum.EndOfPackets;
                    }
                }

                return ResultEnum.NextPacketAvailable;
            }
            catch (System.Exception ex)
            {
                ExpectedPacketTime = null;
                throw ex;
            }
        }

        private Time ParseTime()
        {
            Time NewTimeStamp = new Time();
            NewTimeStamp.ParseTimeStamp(InReport, deviceInfo.OpCodes.Download.StartTimeDate);
            return NewTimeStamp;
        }
        #endregion
    }
}
