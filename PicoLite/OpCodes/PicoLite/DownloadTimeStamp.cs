﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PicoLite.Device;

namespace MicroLabAPI.OpCodes.PicoLite
{
    [Serializable]
    public class DownloadTimeStamp : Download
    {
        #region Constructors
        public DownloadTimeStamp(PicoLiteDevice deviceInfo)
            : base(deviceInfo)
        {
        }
        #endregion
        #region OpCode Methods
        protected override void InitializeOpCodeSend()
        {
            SendOpCode = OpCodes.Send.DownloadTimeStamp;
        }
        protected override void InitializeOpCodeAck()
        {
            AckOpCode = OpCodes.Ack.DownloadTimeStampInfo;
        }
        #endregion
        #region Populate Methods
        protected override void PopulateReport()
        {
            base.PopulateReport();
        }
        private void PopulateDownloadTimeDate()
        {
            //Do nothing. N/A for time stamps.
        }
        public override void Anew()
        {
            Command = DownloadCommandEnum.Anew;
            deviceInfo.DownloadTimeStamps();
        }
        public override void Next()
        {
            //return;
            Command = DownloadCommandEnum.Next;
            deviceInfo.DownloadTimeStamps();
        }
        public override void Cancel()
        {
            throw new NotSupportedException();
        }
        public override void GetDownloadInfo()
        {
            Command = DownloadCommandEnum.GetDownloadInfo;
            deviceInfo.DownloadTimeStamps();
        }
        #endregion
        #region Override Methods
        protected override void PopulateAddresses()
        {
            PopulateAddresses(false);
        }
        #endregion
    }
}
