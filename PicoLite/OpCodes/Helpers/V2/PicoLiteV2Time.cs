﻿using Auxiliary.Tools;
using Base.DataStructures;
using PicoLite.OpCodes.Helpers.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.Helpers.V2
{
    internal class PicoLiteV2Time : PicoLiteTime
    {
        new public PicoLite.OpCodes.Helpers.Consts.eV2TimestampType TimestampType;

        new public static PicoLiteV2Time ParseTimestamp(byte[] block, Time deviceTime)
        {
            //PicoLite timestamps have a 5-byte ss:mm:HH:dd:MM format
            //The year is calculated based on the assumption that picolite devices do not run for more than a year.

            /* Byte 1:
             * 4 upper bits store last 4 bits of year
             * 4 lower bits store month
             * Byte 2:
             * 6 lower bits store day
             * 2 upper bits store T.S event:    
             *                                  00: N/A
             *                                  01: Normal TS
             *                                  10: USB connected
             *                                  11: EndOfPause
             * Byte 3: Hour
             * Byte 4: Minute
             * Byte 5: Seconds
             */
            int index = 0;
            PicoLiteV2Time time = new PicoLiteV2Time();

            time.Year = Convert.ToByte(block[index] >> 4);
            time.Month = Convert.ToByte(block[index++] & 0x0f);
            time.TimestampType = (PicoLite.OpCodes.Helpers.Consts.eV2TimestampType)Convert.ToByte(block[index] >> 6);
            time.findCorrectYear(deviceTime);
            time.Day = Transformation.BCD2Byte(Convert.ToByte(block[index++] & 0x3F));
            time.Hour = Transformation.BCD2Byte(block[index++]);
            time.Minute = Transformation.BCD2Byte(block[index++]);
            time.Second = Transformation.BCD2Byte(block[index++]);


            return time;
        }

        new internal static PicoLiteV2Time ParseTimestamp(byte[] block, DateTime deviceTime)
        {
            Time time = new PicoLiteV2Time();
            time.Set(deviceTime);
            return ParseTimestamp(block, time);
        }

        new public static PicoLiteV2Time Parse(byte[] block)
        {
            PicoLiteV2Time time = new PicoLiteV2Time();
            int index = 0;
            time.Year = Transformation.BCD2Byte(block[index++]);
            time.Month = Transformation.BCD2Byte(block[index++]);
            time.Day = Transformation.BCD2Byte(block[index++]);
            time.Hour = Transformation.BCD2Byte(block[index++]);
            time.Minute = Transformation.BCD2Byte(block[index++]);
            time.Second = Transformation.BCD2Byte(block[index++]);

            time.ThrowExceptionIfInvalid();

            return time;
        }

    }
}
