﻿namespace PicoLite.OpCodes.Helpers
{
    internal class Consts
    {
        public struct Time
        {
            public const int TIME_SIZE = 6;
            public const int TIMESTAMP_SIZE = 5;
        }

        public struct Status
        {
            public const float CYCLE_COMPLETED = 0xAA;
        }

        public struct Battery
        {
            public const decimal VOLTAGE_SCALAR = 431.2m;
        }

        public struct V1Readings
        {
            public const byte MAX = 0xA0;
            public const byte MIN = 0x00;
            public const byte EXCEEDS_MAX = 0xEE;
            public const byte EXCEEDS_MIN = 0xDD;
            public const byte FLUSH = 0xE0;
            public const int SAMPLE_SIZE = 1;
            public const int TIMESTAMP_SIZE = 6;
        }

        public struct V2Readings
        {
            public const byte MAX = 0xF0;
            public const byte MIN = 0x00;
            public const byte EXCEEDS_MAX = 0xFE;
            public const byte EXCEEDS_MIN = 0xFD;
            public const byte FLUSH = 0xF8;
            public const int SAMPLE_SIZE = 1;
            public const byte SAMPLE_DUMMY = 0xF9;
            public const int TIMESTAMP_SIZE = 6;
        }

        public enum eV1TimestampType
        {
            NA = 0x00,
            None = 0x01,
            Connected = 0x02,
            Disconnected = 0x03,
        }

        public enum eV2TimestampType
        {
            NA = 0x00,
            None = 0x01,
            Connected = 0x02,
            EndOfPause = 0x03,
        }
    }
}
