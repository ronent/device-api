﻿using Auxiliary.Tools;
using Base.DataStructures;
using Base.Sensors.Samples;
using System;

namespace PicoLite.OpCodes.Helpers.V1
{
    internal class PicoLiteTime : Time
    {
        public PicoLite.OpCodes.Helpers.Consts.eV1TimestampType TimestampType { get; set; } 

        public static PicoLiteTime Parse(byte[] block)
        {
            PicoLiteTime time = new PicoLiteTime();
            int index = 0;
            time.Year = Transformation.BCD2Byte(block[index++]);
            time.Month = Transformation.BCD2Byte(block[index++]);
            time.Day = Transformation.BCD2Byte(block[index++]);
            time.Hour = Transformation.BCD2Byte(block[index++]);
            time.Minute = Transformation.BCD2Byte(block[index++]);          
            time.Second = Transformation.BCD2Byte(block[index++]);

            time.ThrowExceptionIfInvalid();

            return time;
        }

        public override byte[] GetBytes()
        {
            byte[] block = new byte[6];
            int index = 0;
            block[index++] = Transformation.IntToBCD(Year);
            block[index++] = Transformation.IntToBCD(Month);
            block[index++] = Transformation.IntToBCD(Day);
            block[index++] = Transformation.IntToBCD(Hour);
            block[index++] = Transformation.IntToBCD(Minute);
            block[index++] = Transformation.IntToBCD(Second);

            return block;
        }

        internal static PicoLiteTime ParseTimestamp(byte[] block, DateTime deviceTime)
        {
            Time time = new PicoLiteTime();
            time.Set(deviceTime);
            return ParseTimestamp(block, time);
        }

        public static PicoLiteTime ParseTimestamp(byte[] block, Time deviceTime)
        {
            //PicoLite timestamps have a 5-byte ss:mm:HH:dd:MM format
            //The year is calculated based on the assumption that picolite devices do not run for more than a year.

            /* Byte 1:
             * 4 upper bits store last 4 bits of year
             * 4 lower bits store month
             * Byte 2:
             * 6 lower bits store day
             * 2 upper bits store T.S event:    
             *                                  00: N/A
             *                                  01: Normal TS
             *                                  10: USB connected
             *                                  11: USB disconnected
             * Byte 3: Hour
             * Byte 4: Minute
             * Byte 5: Seconds
             */
            int index = 0;
            PicoLiteTime time = new PicoLiteTime();

            time.Year = Convert.ToByte(block[index] >> 4);
            time.Month = Convert.ToByte(block[index++] & 0x0f);
            time.TimestampType = (PicoLite.OpCodes.Helpers.Consts.eV1TimestampType)Convert.ToByte(block[index] >> 6);
            time.findCorrectYear(deviceTime);
            time.Day = Transformation.BCD2Byte(Convert.ToByte(block[index++] & 0x3F));
            time.Hour = Transformation.BCD2Byte(block[index++]);
            time.Minute = Transformation.BCD2Byte(block[index++]);
            time.Second = Transformation.BCD2Byte(block[index++]);


            return time;
        }

        protected void findCorrectYear(Time time)
        {
            //time.Year consists of the last 2 digits of the starting year
            //for our purposes we require just the last digit
            int year = time.Year % 10;
            if (year > Year)
                Year = Convert.ToByte(time.Year - 1);
            else if (year < Year)
                Year = Convert.ToByte(time.Year + 1);
            else
                Year = time.Year;
        }

        public static PicoLiteTime CreateFrom(DateTime dateTime)
        {
            var time = new PicoLiteTime();
            time.Set(dateTime);
            return time;
        }
    }
}
