﻿using Auxiliary.Tools;
using System;

namespace PicoLite.OpCodes.Helpers
{
    struct DownloadAddresses
    {
        #region Consts
        public const int NUMBER_OF_SAMPLES_PER_PACKET = 61;

        #endregion
        #region Fields
        public UInt16 Current;

        public UInt16 Start;
        /// <summary>
        /// Marks the last data address to download, In case of cyclic, Marks the current logger index
        /// </summary>
        public UInt16 End;

        /// <summary>
        /// Marks the maximal address, Used only in cyclic mode
        /// </summary>
        public UInt16 AbsoluteEnd;

        #endregion
        #region Properties
        public bool IsMemoryFull { get; set; }

        public bool IsAtEnd
        {
            get { return Current >= RealEnd || PacketCount == 0; }
        }

        public ushort RealEnd
        {
            get { return IsMemoryFull ? AbsoluteEnd : End; }
        }

        /// <summary>
        /// Number of expected packets
        /// </summary>
        public uint PacketCount 
        {
            get { return (uint)Math.Ceiling((RealEnd - Start) / (double)NUMBER_OF_SAMPLES_PER_PACKET); }
        }

        public uint PacketIndex
        {
            get { return PacketCount - ((uint)((RealEnd - Current) / (double)NUMBER_OF_SAMPLES_PER_PACKET)); }
        }

        public ushort StartToAbsEndSpan
        {
            get { return Convert.ToUInt16(AbsoluteEnd - Start); }
        }

        #endregion
        #region Methods
        public void SetCurrent(ushort value)
        {
            this.Current = value;
        }

        public void Validate()
        {
            if (End < Start)
            {
                End = Start;
            }
        }

        public void ThrowIfInvalidAddress(uint currentAddress)
        {
            if (Utilities.GetBoundedValue(Start, currentAddress, RealEnd) != currentAddress)
                throw new Exception("Packet address out of bounds.");
        }

        internal void ThrowIfInvalidAddress()
        {
            if (PacketCount == 0)
                throw new Exception("Device contains no data");
        }

        public int GetPercentage()
        {
            return Convert.ToUInt16(100*(Current - Start) / (float)(RealEnd - Start));
        }

        #endregion
        #region Object Overrides
        public override string ToString()
        {
            return string.Format("Start: {0}, End: {1}, Packet Count: {2}", Start, RealEnd, PacketCount);
        }

        #endregion
    }
}
