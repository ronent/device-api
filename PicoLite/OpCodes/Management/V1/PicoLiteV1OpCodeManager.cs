﻿using Base.OpCodes;
using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using PicoLite.OpCodes.V1;
using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.Management.V1
{
    [Serializable]
    internal class PicoLiteV1OpCodeManager : MicroXOpCodeManager
    {
        public PicoLiteV1OpCodeManager(PicoLiteLogger device)
            :base(device)
        {

        }

        #region OpCode Fields
        new internal Setup Setup
        {
            get { return base.Setup as Setup; }
            set { base.Setup = value; }
        }

        new internal Download Download
        {
            get { return base.Download as Download; }
            set { base.Download = value; }
        }

        internal MarkTimeStamp MarkTimeStamp;
        internal TimeStampPacket TimeStampPacket;
        internal DownloadTimeStamps DownloadTimeStamps;
        internal GetFastClockMode GetFastClockMode;
        internal SetFastClockMode SetFastClockMode;
        internal DeepSleep DeepSleep;

        #endregion
        #region Methods
        protected override void DataReceived(byte[] data)
        {
            fixForBoomerangBug(data);
            base.DataReceived(data);
        }

        private void fixForBoomerangBug(byte[] data)
        {
            if (data[1] == 0x44 || data[1] == 0xFF)
                data[1] = 0x43;
        }

        #endregion
    }
}
