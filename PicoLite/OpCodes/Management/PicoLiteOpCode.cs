﻿using MicroXBase.OpCodes.Management;
using PicoLite.Devices;
using PicoLite.OpCodes.Management.V1;
using System;

namespace PicoLite.OpCodes.Management
{
    // Adds parent device and opcode wrapper
    [Serializable]
    internal abstract class PicoLiteOpCode : MicroXOpCode
    {
        public PicoLiteOpCode(PicoLiteLogger device)
            : base(device)
        {

        }

        new protected PicoLiteLogger ParentDevice
        {
            get { return base.ParentDevice as PicoLiteLogger; }
            set { base.ParentDevice = value; }
        }

        new protected PicoLiteV1OpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as PicoLiteV1OpCodeManager; }
        }
    }
}
