﻿using PicoLite.Devices;
using PicoLite.OpCodes.Management.V1;
using PicoLite.OpCodes.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.OpCodes.Management.V2
{
    [Serializable]
    internal class PicoLiteV2OpCodeManager : PicoLiteV1OpCodeManager
    {
        public PicoLiteV2OpCodeManager(PicoLiteLogger device)
            : base(device)
        {
            IgnoredOpCodes.Add("DownloadTimeStamps");
        }

        internal Status2 Status2;
        internal TimeDifference TimeDifference;
        internal NewDownloadTimeStamps NewDownloadTimeStamps;

    }
}
