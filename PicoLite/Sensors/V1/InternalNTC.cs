﻿using Base.Sensors.Management;
using Log4Tech;
using PicoLite.OpCodes.Helpers;
using System;

namespace PicoLite.Sensors.V1
{
    /// <summary>
    /// Internal NTC Sensor.
    /// </summary>
    [Serializable]
    public sealed class InternalNTC : Base.Sensors.Types.Temperature.InternalNTC
    {
        #region Properties
        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(40); }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit (-40); }
        }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal InternalNTC(SensorManager parent)
            : base(parent)
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            return ConvertToCorrectUnit(GetSensorValueWithoutConverting(digitalValue));
        }

        internal decimal GetSensorValueWithoutConverting(long digitalValue)
        {
            if (digitalValue == Consts.V1Readings.EXCEEDS_MAX)
                return Maximum;
            else if (digitalValue == Consts.V1Readings.EXCEEDS_MIN)
                return Minimum;
            else if (digitalValue > Consts.V1Readings.MAX)
            {
                //Log.Log.Write(new Exception("Invalid sensor value in PicoLite InternalNTC: " + digitalValue));
                return Minimum;
            }

            decimal value = digitalValue / 2m + Minimum;
            return Calibration.Calibrate(value);
        }

        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            return Convert.ToInt64((sensorValue - Minimum) * 2);
        }

        #endregion
    }
}
