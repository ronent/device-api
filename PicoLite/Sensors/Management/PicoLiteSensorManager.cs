﻿using Base.Sensors.Management;
using Base.Sensors.Types;
using MicroXBase.DataStructures;
using MicroXBase.Sensors.Management;
using PicoLite.Devices;
using System;

namespace PicoLite.Sensors.Management
{
    /// <summary>
    /// PicoLite Sensor Manager.
    /// </summary>
    [Serializable]
    public sealed class PicoLiteSensorManager : MicroXSensorManager
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent logger.
        /// </summary>
        /// <value>
        /// The parent logger.
        /// </value>
        new internal PicoLiteLogger ParentLogger
        {
            get { return base.ParentLogger as PicoLiteLogger; }
            set {base.ParentLogger = value;}
        }

        /// <summary>
        /// Gets the (fixed) temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature.
        /// </value>
        public GenericSensor Temperature
        {
            get { return GetFixedByIndex(eSensorIndex.Temperature); }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteSensorManager"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal PicoLiteSensorManager(PicoLiteLogger parent)
            :base(parent)
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Assigns the configuration.
        /// </summary>
        internal override void AssignConfiguration()
        {
            if (this.IsEmpty)
                return;

            assignTemperatureSensor();
            assignTemperatureSensorCalibration();
        }

        /// <summary>
        /// Assigns the temperature sensor calibration.
        /// </summary>
        private void assignTemperatureSensorCalibration()
        {
            ParentLogger.Sensors.Temperature.Calibration.Set(ParentLogger.Status.CalibrationCoeffs);
        }

        /// <summary>
        /// Assigns the temperature sensor.
        /// </summary>
        private void assignTemperatureSensor()
        {
            ITSensorSetup configT = ParentLogger.Status as ITSensorSetup;
            if (configT != null)
                AssignFixedSensor(eSensorIndex.Temperature, configT.TemperatureSensor.TemperatureEnabled, configT.TemperatureSensor.TemperatureAlarm);
        }

        #endregion
    }
}
