﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace PicoLite.Devices.V1
{
    /// <summary>
    /// PicoLite Logger.
    /// </summary>
    [Serializable]
    public class PicoLiteLogger : Devices.PicoLiteLogger, ISerializable
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 1);

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal PicoLiteLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected PicoLiteLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
