﻿using Base.DataStructures;
using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Management;
using Log4Tech;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Features;
using MicroXBase.Devices.Types;
using PicoLite.DataStructures.V1;
using PicoLite.Functions.Management.V1;
using PicoLite.OpCodes.Management;
using PicoLite.OpCodes.Management.V1;
using PicoLite.Sensors.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace PicoLite.Devices
{
    /// <summary>
    /// PicoLite Device.
    /// </summary>
    public abstract class PicoLiteLogger : MicroXLogger, ISerializable
    {
        #region Members
        internal static readonly string firmwareDeviceName = "PicoLite";

        private static readonly string deviceTypeName = "PicoLite";
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return PicoLiteInterfacer.TYPE; } }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return PicoLiteInterfacer.BITS; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new eSensorType[] { eSensorType.InternalNTC };
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                var list = base.Features.ToList();

                list.Add(MicroXFeature.TEST_MODE);

                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                {
                    list.Add(MicroXFeature.DEEP_SLEEP);                    
                }

                return list.ToArray();
            }
        }

        /// <summary>
        /// Gets the sensors manager.
        /// </summary>
        /// <value>
        /// The sensors.
        /// </value>
        new public PicoLiteSensorManager Sensors
        {
            get { return base.Sensors as PicoLiteSensorManager; }
            protected set { base.Sensors = value; }
        }

        new public PicoLiteLoggerV1FunctionsManager Functions
        {
            get { return base.Functions as PicoLiteLoggerV1FunctionsManager; }
            set { base.Functions = value; }
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal PicoLiteV1OpCodeManager OpCodes
        {
            get { return base.OpCodes as PicoLiteV1OpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public PicoLiteV1StatusConfiguration Status
        {
            get { return base.Status as PicoLiteV1StatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal PicoLiteLogger(IHIDDeviceManager parent)
            : base(parent)
        {
            
        }

        internal override void InitializeOpCodesManager()
        {
            OpCodes = new PicoLiteV1OpCodeManager(this);
        }

        internal override void InitializeFunctionManager()
        {
            Functions = new PicoLiteLoggerV1FunctionsManager(this);
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal override void InitializeStatus()
        {
            Status = new PicoLiteV1StatusConfiguration();
        }

        /// <summary>
        /// Initializes the sensor manager.
        /// </summary>
        internal override void InitializeSensorManager()
        {
            Sensors = new PicoLiteSensorManager(this);
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected PicoLiteLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
