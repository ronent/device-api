﻿using Base.Devices.Management;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace PicoLite.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "PicoLite")]
    internal class PicoLiteInterfacer : MicroXDeviceInterfacer<PicoLiteLogger>
    {
        public const byte TYPE = 0xC0;
        public const byte BITS = 0x00;
                         
        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public PicoLiteInterfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x1F0C, 0x2000), new DeviceID(MicroXLoggerManager.VID, 0x0001) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.PicoLiteLogger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
