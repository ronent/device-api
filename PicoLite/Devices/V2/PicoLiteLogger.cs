﻿using Base.Devices.Management.DeviceManager.HID;
using Base.OpCodes;
using Log4Tech;
using MicroXBase.Devices.Features;
using PicoLite.DataStructures.V2;
using PicoLite.Functions.Management.V2;
using PicoLite.OpCodes.Management.V2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.Devices.V2
{
    /// <summary>
    /// PicoLite V2 Logger.
    /// </summary>
    [Serializable]
    public class PicoLiteLogger : Devices.V1.PicoLiteLogger, ISerializable
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 11);

        /// <summary>
        /// The firmware device name.
        /// </summary>
        internal static readonly string firmwareDeviceName = PicoLite.Devices.PicoLiteLogger.firmwareDeviceName + " V2";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public PicoLiteV2StatusConfiguration Status
        {
            get { return base.Status as PicoLiteV2StatusConfiguration; }
            private set { base.Status = value; }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public PicoLiteLoggerV2FunctionsManager Functions
        {
            get { return base.Functions as PicoLiteLoggerV2FunctionsManager; }
            private set { base.Functions = value; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal PicoLiteLogger(IHIDDeviceManager parent)
            : base(parent)
        {
            
        }

        #endregion
        #region Override Methods
        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return base.DeviceTypeName + " V2";
            }
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal override void InitializeStatus()
        {
            Status = new PicoLiteV2StatusConfiguration();
        }

        /// <summary>
        /// Initializes the op codes.
        /// </summary>
        internal override void InitializeOpCodesManager()
        {
            OpCodes = new PicoLiteV2OpCodeManager(this);
        }

        /// <summary>
        /// Initializes the function manager.
        /// </summary>
        internal override void InitializeFunctionManager()
        {
            Functions = new PicoLiteLoggerV2FunctionsManager(this);
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal PicoLiteV2OpCodeManager OpCodes
        {
            get { return base.OpCodes as PicoLiteV2OpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[] 
                { 
                    MicroXFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected PicoLiteLogger(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}