﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.Maintenance
{
    /// <summary>
    /// PicoLite V2 Device Firmware.
    /// </summary>
    [Serializable]
    public class PicoLiteV2Firmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return PicoLite.Devices.V2.PicoLiteLogger.firmwareDeviceName; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1Firmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public PicoLiteV2Firmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1Firmware"/> class.
        /// </summary>
        public PicoLiteV2Firmware()
        {

        }
    }
}
