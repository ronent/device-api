﻿using System;
using System.Runtime.Serialization;

namespace PicoLite.Maintenance
{
    /// <summary>
    /// PicoLite Device Firmware.
    /// </summary>
    [Serializable]
    public class PicoLiteV1Firmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return PicoLite.Devices.PicoLiteLogger.firmwareDeviceName; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1Firmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public PicoLiteV1Firmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1Firmware"/> class.
        /// </summary>
        public PicoLiteV1Firmware()
        {

        }
    }
}
