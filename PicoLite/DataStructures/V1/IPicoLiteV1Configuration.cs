﻿using Base.Sensors.Calibrations;
using MicroXBase.DataStructures;

namespace PicoLite.DataStructures.V1
{
    /// <summary>
    /// PicoLite Configuration.
    /// </summary>
    internal interface IPicoLiteV1Configuration
    {
        /// <summary>
        /// Gets or sets the alarm delay.
        /// </summary>
        /// <value>
        /// The alarm delay.
        /// </value>
        ushort AlarmDelay { get; }

        /// <summary>
        /// Gets or sets the calibration coefficients.
        /// </summary>
        /// <value>
        /// The calibration coefficients.
        /// </value>
        CalibrationCoefficients CalibrationCoeffs { get; }

        /// <summary>
        /// Gets or sets the LED configuration.
        /// </summary>
        /// <value>
        /// The LED configuration.
        /// </value>
        eLEDConfiguration LEDConfiguration { get; }

        /// <summary>
        /// Gets or sets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        TSensorSetup TemperatureSensor { get; }
    }
}
