﻿using Base.Sensors.Calibrations;
using MicroXBase.DataStructures;
using System;
using Auxiliary.Extentions;

namespace PicoLite.DataStructures.V1
{
    /// <summary>
    /// PicoLite Status Configuration.
    /// </summary>
    [Serializable]
    public class PicoLiteV1StatusConfiguration : MicroXStatusConfiguration, IPicoLiteV1Configuration, ITSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1StatusConfiguration"/> class.
        /// </summary>
        internal PicoLiteV1StatusConfiguration()
            : base()
        {
            TemperatureSensor = new TSensorSetup();
            CalibrationCoeffs = new CalibrationCoefficients();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the calibration coefficients.
        /// </summary>
        /// <value>
        /// The calibration coefficients.
        /// </value>
        public CalibrationCoefficients CalibrationCoeffs { get; internal set; }

        /// <summary>
        /// Gets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        public TSensorSetup TemperatureSensor { get; internal set; }

        /// <summary>
        /// Gets the alarm delay.
        /// </summary>
        /// <value>
        /// The alarm delay.
        /// </value>
        public UInt16 AlarmDelay { get; internal set; }

        /// <summary>
        /// Gets the LED configuration.
        /// </summary>
        /// <value>
        /// The LED configuration.
        /// </value>
        public eLEDConfiguration LEDConfiguration { get; internal set; }

        /// <summary>
        /// Gets the device time.
        /// </summary>
        /// <value>
        /// The device time.
        /// </value>
        internal DateTime DeviceTime { get; set; }

        #endregion
        #region Methods
        /// <summary>
        /// Generates setup from status.
        /// </summary>
        /// <returns>Cloned status</returns>
        internal virtual PicoLiteV1SetupConfiguration GenerateFromStatus()
        {
            var setup = GenerateSetup();

            setup.AlarmDelay = this.AlarmDelay;
            setup.Boomerang = this.Boomerang;
            setup.BoomerangEnabled = this.BoomerangEnabled;
            setup.CalibrationCoeffs = this.CalibrationCoeffs;
            setup.Comment = this.Comment;
            setup.CyclicMode = this.CyclicMode;
            setup.FahrenheitMode = this.FahrenheitMode;
            setup.Interval = this.Interval;
            setup.LEDConfiguration = this.LEDConfiguration;
            setup.PushToRunMode = this.PushToRunMode;
            setup.SerialNumber = this.SerialNumber;
            setup.TemperatureSensor = this.TemperatureSensor;
            setup.TestMode = this.TestMode;
            setup.TimerRunEnabled = this.TimerRunEnabled;
            setup.TimerStart = this.TimerStart;
            setup.UtcOffset = this.UtcOffset;

            setup.SetPrivateFieldValue<bool>("testMode", this.TestMode);

            return setup;
        }

        internal virtual PicoLiteV1SetupConfiguration GenerateSetup()
        {
            return new PicoLiteV1SetupConfiguration();
        }

        #endregion
    }
}
