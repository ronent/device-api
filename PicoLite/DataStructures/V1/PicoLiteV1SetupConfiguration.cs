﻿using Auxiliary.Tools;
using Base.Devices;
using Base.Sensors.Calibrations;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;

namespace PicoLite.DataStructures.V1
{
    /// <summary>
    /// LED Configuration.
    /// </summary>
    public enum eLEDConfiguration : byte
    {
        RedAlwaysOff = 0x00,
        RedAlarmBlink10s = 0x01,
        RedAlarmBlink30s = 0x02,
        RedAlarmBlink60s = 0x03,

        RedBlinkOnNormalized = 0x08,
        GreenBlinkOnReading = 0x10,
    }

    /// <summary>
    /// PicoLite Setup Configuration.
    /// </summary>
    [Serializable]
    public class PicoLiteV1SetupConfiguration : MicroXSetupConfiguration, IPicoLiteV1Configuration, ITSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="PicoLiteV1SetupConfiguration"/> class.
        /// </summary>
        public PicoLiteV1SetupConfiguration()
            : base()
        {
            TemperatureSensor = new TSensorSetup();
            CalibrationCoeffs = new CalibrationCoefficients();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the calibration coefficients.
        /// </summary>
        /// <value>
        /// The calibration coefficients.
        /// </value>
        public CalibrationCoefficients CalibrationCoeffs { get; set; }

        /// <summary>
        /// Gets or sets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        public TSensorSetup TemperatureSensor { get; set; }

        /// <summary>
        /// Gets or sets the alarm delay.
        /// </summary>
        /// <value>
        /// The alarm delay.
        /// </value>
        public UInt16 AlarmDelay { get; set; }

        /// <summary>
        /// Gets or sets the LED configuration.
        /// </summary>
        /// <value>
        /// The LED configuration.
        /// </value>
        public eLEDConfiguration LEDConfiguration { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);

            checkAlarmDelay();
        }

        internal override void adjustDummyStatus(MicroXLogger device)
        {
            base.adjustDummyStatus(device);
            device.Status.TestMode = TestMode;
        }

        /// <summary>
        /// Checks the interval.
        /// </summary>
        protected override void checkInterval()
        {
            if (TestMode)
                base.checkInterval();
            else
                ThrowIfFalse(Interval.TotalSeconds >= 60, "Interval must by greater than 1 minute");
        }

        /// <summary>
        /// Checks the alarm delay.
        /// </summary>
        private void checkAlarmDelay()
        {
            ThrowIfFalse(AlarmDelay >= 0, "Alarm delay must be >= 0");
        }

        /// <summary>
        /// Checks the temperature sensor.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(TemperatureSensor.TemperatureEnabled, "Temperature sensor must be enabled");
            TemperatureSensor.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}