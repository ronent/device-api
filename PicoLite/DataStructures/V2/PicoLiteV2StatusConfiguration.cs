﻿using PicoLite.DataStructures.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.DataStructures.V2
{
    /// <summary>
    /// PicoLite V2 Status Configuration.
    /// </summary>
    [Serializable]
    public class PicoLiteV2StatusConfiguration : DataStructures.V1.PicoLiteV1StatusConfiguration, IPicoLiteV2Configuration
    {
        #region Constructor
        internal PicoLiteV2StatusConfiguration()
            :base()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the delay interval in minutes for start running.
        /// </summary>
        /// <value>
        /// The run delay.
        /// </value>
        public UInt16 RunDelay { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        public bool StopOnKeyPress { get; internal set; }

        /// <summary>
        /// Gets last setup time.
        /// </summary>
        /// <value>
        /// The setup time.
        /// </value>
        public DateTime SetupTime { get; internal set; }

        /// <summary>
        /// Gets the time when the logger started running.
        /// </summary>
        /// <value>
        /// The run time.
        /// </value>
        public DateTime RunTime { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether logger has a time drift caused by Deep Sleep.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [time difference]; otherwise, <c>false</c>.
        /// </value>
        internal bool TimeDifference { get; set; }

        /// <summary>
        /// Gets or sets the time difference in seconds.
        /// </summary>
        /// <value>
        /// The time difference seconds.
        /// </value>
        internal int TimeDifferenceSeconds { get; set; }

        #endregion
        #region Methods
        /// <summary>
        /// Generates setup from status.
        /// </summary>
        /// <returns>Cloned status</returns>
        internal override PicoLiteV1SetupConfiguration GenerateFromStatus()
        {
            PicoLiteV2SetupConfiguration setup = (PicoLiteV2SetupConfiguration)base.GenerateFromStatus();
            setup.RunDelay = this.RunDelay;

            return setup;
        }

        /// <summary>
        /// Generates the setup.
        /// </summary>
        /// <returns></returns>
        internal override PicoLiteV1SetupConfiguration GenerateSetup()
        {
            return new PicoLiteV2SetupConfiguration();
        }

        #endregion
    }
}
