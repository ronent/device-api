﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.DataStructures.V2
{
    /// <summary>
    /// PicoLite Configuration.
    /// </summary>
    internal interface IPicoLiteV2Configuration
    {
        /// <summary>
        /// Gets the run delay.
        /// </summary>
        /// <value>
        /// The run delay.
        /// </value>
        UInt16 RunDelay { get; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        bool StopOnKeyPress { get; }
    }
}
