﻿using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicoLite.DataStructures.V2
{
    /// <summary>
    /// PicoLite V2 Setup Configuration.
    /// </summary>
    [Serializable]
    public class PicoLiteV2SetupConfiguration : DataStructures.V1.PicoLiteV1SetupConfiguration, IPicoLiteV2Configuration
    {
        #region Constructor
        public PicoLiteV2SetupConfiguration()
            :base()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the delay interval in minutes for start running.
        /// </summary>
        /// <value>
        /// The run delay.
        /// </value>
        public UInt16 RunDelay { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        public bool StopOnKeyPress { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            base.ThrowIfInvalidFor(device);

            if (PushToRunMode)
                checkRunDelay();
        }

        /// <summary>
        /// Adjusts the dummy status.
        /// </summary>
        /// <param name="device">The device.</param>
        internal override void adjustDummyStatus(MicroXLogger device)
        {
            base.adjustDummyStatus(device);
            (device.Status as PicoLiteV2StatusConfiguration).RunDelay = RunDelay;
        }

        /// <summary>
        /// Checks the alarm delay.
        /// </summary>
        private void checkRunDelay()
        {
            ThrowIfFalse(RunDelay >= 0 && RunDelay <= 1439, "Alarm delay must be between 0 - 1439");
        }

        #endregion
    }
}
