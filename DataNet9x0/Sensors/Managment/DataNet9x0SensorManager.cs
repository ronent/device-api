﻿using DataNet9x0.Devices.V1;
using DataNetLoggersAPI.Sensors.Managment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.Sensors.Managment
{
    /// <summary>
    /// DataNet 9x0 Sensor Manager.
    /// </summary>
    [Serializable]
    public class DataNet9x0SensorManager : GenericDataNetLoggerSensorManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet9x0SensorManager"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public DataNet9x0SensorManager(DataNet9x0Logger logger)
            :base(logger)
        {

        }

        #endregion
    }
}
