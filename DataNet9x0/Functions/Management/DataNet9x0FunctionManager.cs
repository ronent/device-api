﻿using DataNet9x0.Devices.V1;
using DataNetLoggersAPI.Functions.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.Functions.Management
{
    /// <summary>
    /// Manager for DataNet 9x0 Logger.
    /// </summary>
    [Serializable]
    public class DataNet9x0FunctionManager : GenericDataNetLoggerFunctionsManager
    {
        #region Constructor
        public DataNet9x0FunctionManager(DataNet9x0Logger logger)
            :base(logger)
        {

        }

        /// <summary>
        /// Subscribes the op codes events.
        /// </summary>
        protected override void SubscribeOpCodesEvents()
        {
            
        }

        #endregion
        #region Properties
        new protected internal DataNet9x0Logger Device { get { return base.Device as DataNet9x0Logger; } set { base.Device = value; } }

        #endregion
    }
}
