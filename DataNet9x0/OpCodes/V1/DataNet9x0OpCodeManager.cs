﻿using DataNet9x0.Devices.V1;
using DataNetLoggersAPI.OpCodes.Managment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.OpCodes.Managment.V1
{
    /// <summary>
    /// DataNet 9x0 Op Code Manager.
    /// </summary>
    [Serializable]
    internal class DataNet9x0OpCodeManager : GenericDataNetLoggerOpCodeManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet9x0OpCodeManager"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public DataNet9x0OpCodeManager(DataNet9x0Logger logger)
            :base(logger)
        {

        }

        #endregion
        #region OpCodes

        #endregion
    }
}
