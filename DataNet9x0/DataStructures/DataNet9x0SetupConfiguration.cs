﻿using DataNetLoggersAPI.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.DataStructures
{
    [Serializable]
    public class DataNet9x0SetupConfiguration : GenericDataNetLoggerSetupConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet9x0SetupConfiguration"/> class.
        /// </summary>
        public DataNet9x0SetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
