﻿using DataNetLoggersAPI.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.DataStructures
{
    [Serializable]
    public class DataNet9x0StatusConfiguration : GenericDataNetLoggerStatusConfiguration
    {
        #region Constructor
        internal DataNet9x0StatusConfiguration()
            :base()
        {

        }

        #endregion
    }
}
