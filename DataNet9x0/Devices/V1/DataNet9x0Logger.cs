﻿using Base.Devices.Management.DeviceManager;
using DataNet9x0.DataStructures;
using DataNet9x0.Functions.Management;
using DataNet9x0.OpCodes.Managment.V1;
using DataNet9x0.Sensors.Managment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet9x0.Devices.V1
{
    [Serializable]
    public abstract class DataNet9x0Logger : DataNetLoggersAPI.Devices.V1.GenericDataNetLogger
    {
        #region Constructor
        public DataNet9x0Logger(IDeviceManager parent)
            :base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the sensors.
        /// </summary>
        /// <value>
        /// The sensors.
        /// </value>
        new public DataNet9x0SensorManager Sensors
        {
            get { return base.Sensors as DataNet9x0SensorManager; }
            protected set { base.Sensors = value; }
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal DataNet9x0OpCodeManager OpCodes
        {
            get { return base.OpCodes as DataNet9x0OpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public DataNet9x0StatusConfiguration Status
        {
            get { return base.Status as DataNet9x0StatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public DataNet9x0FunctionManager Functions
        {
            get { return base.Functions as DataNet9x0FunctionManager; }
            protected set { base.Functions = value; }
        }

        #endregion
    }
}
