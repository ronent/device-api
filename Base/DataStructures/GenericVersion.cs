﻿using Base.Devices;
using System;

namespace Base.DataStructures
{
    /// <summary>
    /// Generic Version.
    /// </summary>
    [Serializable]
    public abstract class GenericVersion : GenericComponent
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericVersion"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal GenericVersion(GenericDevice device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the PCB assembly version.
        /// </summary>
        /// <value>
        /// The PCB assembly version.
        /// </value>
        public abstract byte PCBAssembly { get; }

        /// <summary>
        /// Gets the PCB version.
        /// </summary>
        /// <value>
        /// The PCB version.
        /// </value>
        public abstract byte PCBVersion { get; }

        /// <summary>
        /// Gets the firmware revision.
        /// </summary>
        /// <value>
        /// The firmware revision.
        /// </value>
        public abstract byte FirmwareRevision { get; }

        /// <summary>
        /// Gets the firmware version.
        /// </summary>
        /// <value>
        /// The firmware version.
        /// </value>
        public abstract Version Firmware { get; }

        /// <summary>
        /// Gets the full version.
        /// </summary>
        /// <value>
        /// The full version.
        /// </value>
        public virtual Version FullVersion
        {
            get
            {
                return new Version(PCBVersion.ToString() + "." + Firmware + "." + FirmwareRevision);
            }
        }
        #endregion
    }
}