﻿using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace Base.DataStructures
{
    /// <summary>
    /// User Defined Sensor Configuration.
    /// </summary>
    [Serializable]
    public abstract class UDSConfiguration : ISerializable
    {
        #region Fields
        /// <summary>
        /// The transformer
        /// </summary>
        protected TwoPoint Transformer = new TwoPoint();

        #endregion
        #region Properties
        /// <summary>
        /// Gets the length of the allowed name.
        /// </summary>
        /// <value>
        /// The length of the allowed name.
        /// </value>
        public abstract int AllowedNameLength { get; }

        /// <summary>
        /// Gets the length of the allowed custom unit.
        /// </summary>
        /// <value>
        /// The length of the allowed custom unit.
        /// </value>
        public abstract int AllowedCustomUnitLength { get; }

        /// <summary>
        /// Gets or sets the significant figures.
        /// </summary>
        /// <value>
        /// The significant figures.
        /// </value>
        public byte SignificantFigures { get; set; }

        /// <summary>
        /// Gets or sets the name of the sensor.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the base type of the sensor.
        /// </summary>
        /// <value>
        /// The type of the base.
        /// </value>
        public eSensorType BaseType { get; set; }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        /// <value>
        /// The offset.
        /// </value>
        public float Offset 
        { 
            get { return Convert.ToSingle(Transformer.Offset); }
            set { Transformer.Bf = value; }
        }

        /// <summary>
        /// Gets or sets the gain.
        /// </summary>
        /// <value>
        /// The gain.
        /// </value>
        public float Gain
        {
            get { return Convert.ToSingle(Transformer.Gain); }
            set { Transformer.Af = value; }
        }

        /// <summary>
        /// Gets the references.
        /// </summary>
        /// <value>
        /// The references.
        /// </value>
        public LoggerReference[] References
        {
            get { return Transformer.References; }
        }

        /// <summary>
        /// Gets a value indicating whether is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        internal virtual bool Valid
        {
            get
            {
                return Transformer.Valid && SignificantFigures >= 0 && SignificantFigures < byte.MaxValue / 2;
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="UDSConfiguration"/> class.
        /// </summary>
        internal UDSConfiguration()
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Transforms value from base sensor value to defined value
        /// </summary>
        /// <param name="baseSensorValue">The value measured by the base sensor</param>
        /// <returns>
        /// The value following the defined transformation
        /// </returns>
        public decimal TransformFromBase(decimal baseSensorValue)
        {
            return Transformer.Calibrate(baseSensorValue);
        }

        /// <summary>
        /// Transforms value from defined value to base sensor value
        /// </summary>
        /// <param name="definedSensorValue">The transformed value</param>
        /// <returns>
        /// The base sensor value before transformation
        /// </returns>
        public decimal TransformToBase(decimal definedSensorValue)
        {
            return Transformer.Decalibrate(definedSensorValue);
        }

        /// <summary>
        /// Sets the transformation parameters (Offset and Gain)
        /// </summary>
        /// <param name="pairs">Two LoggerReference pairs that define the relation between the base sensor and the new defined one</param>
        public void Set(LoggerReference[] pairs)
        {
            Transformer.Set(pairs);
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="UDSConfiguration"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected UDSConfiguration(SerializationInfo info, StreamingContext ctxt)
        {
            SignificantFigures = IO.GetSerializationValue<byte>(ref info, "SignificantFigures");
            Name = IO.GetSerializationValue<string>(ref info, "Name");
            BaseType = IO.GetSerializationValue<eSensorType>(ref info, "Type");
            Offset = IO.GetSerializationValue<float>(ref info, "Offset");
            Gain = IO.GetSerializationValue<float>(ref info, "Gain");
            Transformer = IO.GetSerializationValue<TwoPoint>(ref info, "Transformer");
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("SignificantFigures", SignificantFigures , typeof(byte));
            info.AddValue("Name",Name , typeof(string));
            info.AddValue("Type" ,BaseType , typeof(eSensorType));
            info.AddValue("Offset" ,Offset , typeof(float));
            info.AddValue("Gain" ,Gain , typeof(float));
            info.AddValue("Transformer", Transformer, typeof(TwoPoint));
        }
        #endregion
        #region Object Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return "\tName: " + Name
                + Environment.NewLine
                + "\tType: " + BaseType
                + Environment.NewLine
                + "\tOffset: " + Offset
                + Environment.NewLine
                + "\tGain: " + Gain
                + Environment.NewLine
                + "\tDigits: " + SignificantFigures; ;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            UDSConfiguration other = obj as UDSConfiguration;

            if(other != null)
            {
                return GetHashCode() == other.GetHashCode();
            }

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return (int)(SignificantFigures.GetHashCode()
                + Name.GetHashCode()
                + Gain.GetHashCode()
                + Offset.GetHashCode()
                + BaseType.GetHashCode());
        }

        #endregion
    }
}

