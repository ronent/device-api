﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataStructures.Device
{
    /// <summary>
    /// Status Configuration Settings For GenericDevice.
    /// </summary>
    [Serializable]
    public class BaseDeviceStatusConfiguration : GenericConfiguration, IBaseDeviceSetupConfiguration
    {
        #region Fields
        private string serialNumber;

        #endregion
        #region Constructor
        internal BaseDeviceStatusConfiguration()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the device comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string Comment { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether device flash was erased.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is flash]; otherwise, <c>false</c>.
        /// </value>
        public bool IsFlashErased { get; private set; }

        /// <summary>
        /// Gets the device serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber
        {
            get { return serialNumber; }
            internal set
            {
                serialNumber = value;
                if (serialNumber.Equals("????????"))
                    IsFlashErased = true;
                else
                    IsFlashErased = false;
            }
        }

        /// <summary>
        /// Gets the battery level.
        /// </summary>
        /// <value>
        /// The battery level.
        /// </value>
        public byte BatteryLevel { get; internal set; }

        /// <summary>
        /// Gets the firmware version.
        /// </summary>
        /// <value>
        /// The firmware version.
        /// </value>
        public Version FirmwareVersion { get; internal set; }

        /// <summary>
        /// Gets the fw revision.
        /// </summary>
        /// <value>
        /// The fw revision.
        /// </value>
        public byte FWRevision { get; internal set; }

        /// <summary>
        /// Gets the PCB version.
        /// </summary>
        /// <value>
        /// The PCB version.
        /// </value>
        public byte PCBVersion { get; internal set; }

        /// <summary>
        /// Gets the PCB assembly version.
        /// </summary>
        /// <value>
        /// The PCB assembly.
        /// </value>
        public byte PCBAssembly { get; internal set; }

        #endregion
    }
}
