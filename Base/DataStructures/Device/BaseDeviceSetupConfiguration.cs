﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataStructures.Device
{
    /// <summary>
    /// Setup Configuration Settings For GenericDevice.
    /// </summary>
    [Serializable]
    public abstract class BaseDeviceSetupConfiguration : GenericConfiguration, IBaseDeviceSetupConfiguration
    {
        #region Constructor
        public BaseDeviceSetupConfiguration()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the device comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the device serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber { get; set; }

        #endregion
    }
}
