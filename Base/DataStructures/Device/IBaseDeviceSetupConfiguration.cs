﻿using System;
namespace Base.DataStructures.Device
{
    interface IBaseDeviceSetupConfiguration
    {
        string Comment { get; }
        string SerialNumber { get; }
    }
}
