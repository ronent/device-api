﻿using Base.DataStructures.Device;
using System;

namespace Base.DataStructures.Logger
{
    /// <summary>
    /// Status Configuration Settings For GenericLogger.
    /// </summary>
    [Serializable]
    public class BaseLoggerStatusConfiguration : BaseDeviceStatusConfiguration
    {
        internal BaseLoggerStatusConfiguration()
        {

        }

        public TimeSpan Interval { get; internal set; }

        public bool IsRunning { get; internal set; }

        public bool FahrenheitMode { get; internal set; }

        private Exception LastException { get; set; }
    }
}
