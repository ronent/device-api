﻿using Base.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataStructures.Logger
{
    /// <summary>
    /// Setup Configuration Settings For GenericLogger.
    /// </summary>
    [Serializable]
    public abstract class BaseLoggerSetupConfiguration : BaseDeviceSetupConfiguration
    {
        #region Constructor
        public BaseLoggerSetupConfiguration()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether Fahrenheit mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [Fahrenheit mode]; otherwise, <c>false</c>.
        /// </value>
        public bool FahrenheitMode { get; set; }

        /// <summary>
        /// Gets or sets the interval in seconds.
        /// </summary>
        /// <value>
        /// The interval.
        /// </value>
        public TimeSpan Interval { get; set; }

        #endregion
    }
}
