﻿using Base.Devices;
using System;

namespace Base.DataStructures
{
    /// <summary>
    /// Generic Memory Size.
    /// </summary>
    [Serializable]
    public abstract class GenericMemorySize : GenericComponent
    {
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        internal abstract Enum Type
        {
            get;
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public abstract string Name
        {
            get;
        }

        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public abstract int Size
        {
            get;
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMemorySize"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal GenericMemorySize(GenericDevice device)
            : base(device)
        {
        }
        #endregion
        #region Overridden
        /// <summary>
        /// Determines whether the specified memory size, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return (obj as GenericMemorySize) != null && (obj as GenericMemorySize).Name == this.Name;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
