﻿using Base.Devices;
using System;

namespace Base.DataStructures
{
    /// <summary>
    /// Generic Component.
    /// </summary>
    [Serializable]
    public abstract class GenericComponent
    {
        /// <summary>
        /// Gets or sets the parent device.
        /// </summary>
        /// <value>
        /// The parent device.
        /// </value>
        internal GenericDevice ParentDevice { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericComponent"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal GenericComponent(GenericDevice device)
        {
            Initialize(device);
        }

        /// <summary>
        /// Initializes the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        protected void Initialize(GenericDevice device)
        {
            this.ParentDevice = device;
        }
    }
}
