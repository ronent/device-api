﻿using Base.Devices;
using System;

namespace Base.DataStructures
{
    /// <summary>
    /// Generic Battery.
    /// </summary>
    [Serializable]
    public abstract class GenericBattery : GenericComponent
    {
        internal const int WAIT_FOR_EMPTY_QUEUE = 100;
        internal const int LOW_BATTERY_LEVEL = 25;
        internal const int MED_BATTERY_LEVEL = 50;
        internal const int HIGH_BATTERY_LEVEL = 100;
        internal const int NAN_BATTERY_LEVEL = 255;
        internal const int MAX_BATTERY_PERCENTAGE = 100;

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericBattery"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal GenericBattery(GenericDevice device)
            : base(device)
        {
        }
        #endregion

        /// <summary>
        /// Gets a value indicating whether the battery has alarm.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has alarm]; otherwise, <c>false</c>.
        /// </value>
        public abstract bool HasAlarm
        {
            get;
        }

        /// <summary>
        /// Gets the battery level.
        /// </summary>
        /// <value>
        /// The battery level.
        /// </value>
        public abstract Byte BatteryLevel
        {
            get;
        }
    }
}
