﻿using Auxiliary.Tools;
using Base.OpCodes;
using Log4Tech;
using System.IO;

namespace Base.DataStructures
{
    /// <summary>
    /// Use for loading setup properties from a file
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class SetupFile<T> where T : OpCodeManager
    {
        #region Static
        public enum SetupStatusEnum { Idle, Valid, Corrupt, Missing, Incompatible, UnexpectedError };
        private static readonly string[] SetupStatuses = new string[] { "Idle", "Valid", "Corrupt setup", "File not found", "Incompatible version/device", "Unexpected Error" };
        
        public static string GetSetupFileError(SetupStatusEnum status)
        {
            return SetupStatuses[(int)status];
        }
        #endregion
        #region Properties
        public T ParentOpCodeManager
        {
            get;
            protected set;
        }
        #endregion
        #region Constructors
        internal SetupFile(T parent)
        {
            this.ParentOpCodeManager = parent;
        }
        #endregion
        #region Save
        public Result Save(string path)
        {
            try
            {
                using (IO FileIO = new IO(path, false))
                {
                    FileIO.EncryptAndSerialize<T>(ParentOpCodeManager);
                    return Result.OK;
                }
            }
            catch (System.Exception ex)
            {
                Log.Instance.WriteError("Error saving device configuration",this, ex);
                return Result.ERROR;
            }
        }
        #endregion
        #region Load
        public SetupStatusEnum Load(string path)
        {
            try
            {
                OpCodeManager opCodes;
                SetupStatusEnum status = LoadOpCodes(path, out opCodes);
                if (status == SetupStatusEnum.Valid)
                {
                    ParentOpCodeManager.Assimilate(opCodes);
                }
                return status;
            }
            catch (System.Exception ex)
            {
                Log.Instance.WriteError("There was an error reading configuration file.",this, ex);
                return SetupStatusEnum.UnexpectedError;
            }
        }

        private T LoadFile(string FileName)
        {
            try
            {
                using (IO FileIO = new IO(FileName, false))
                {
                    return FileIO.DecryptAndDeserialize<T>();
                }
            }
            catch (System.Exception ex)
            {
                Log.Instance.WriteError("Fail in LoadFile", this, ex);
                return default(T);
            }
        }

        private SetupStatusEnum LoadOpCodes(string path, out OpCodeManager opCodes)
        {
            FileInfo fileInfo = new FileInfo(path);
            SetupStatusEnum status = SetupStatusEnum.Idle;
            opCodes = null;
            try
            {
                if (fileInfo.Exists)
                {
                    opCodes = LoadFile(path) as OpCodeManager;
                    if (opCodes == null)
                        status = SetupStatusEnum.Corrupt;
                    else if (opCodes.CompatibleWith(typeof(T)))
                    {
                        status = SetupStatusEnum.Valid;
                    }
                    else
                        status = SetupStatusEnum.Incompatible;
                }
                else
                    status = SetupStatusEnum.Missing;
            }
            catch (System.Exception)
            {
                status = SetupStatusEnum.Corrupt;
            }

            return status;
        }
        #endregion
    }
}
