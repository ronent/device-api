﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Base.DataStructures
{
    [Serializable]
    public abstract class GenericConfiguration
    {
        #region Constructor
        public GenericConfiguration()
        {

        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the members.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Tuple<string, object>> GetMembers()
        {
            Type type = this.GetType();
            BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

            FieldInfo[] fields = type.GetFields(bindingFlags);
            foreach (FieldInfo field in fields)
                yield return new Tuple<string, object>(field.Name, field.GetValue(this));

            PropertyInfo[] properties = type.GetProperties(bindingFlags);
            foreach (PropertyInfo property in properties)
                yield return new Tuple<string, object>(property.Name, property.GetValue(this, null));
        }

        /// <summary>
        /// Gets the members alphabetically.
        /// </summary>
        /// <returns></returns>
        internal IEnumerable<Tuple<string, object>> GetMembersAlphabetically()
        {
            return GetMembers().OrderBy(x => x.Item1);
        }

        /// <summary>
        /// Gets the member by name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        internal object GetMember(string name)
        {
            try
            {
                return GetMembers().Where(item => item.Item1.Equals(name)).FirstOrDefault().Item2;
            }
            catch { return null; }
        }

        #endregion
    }
}