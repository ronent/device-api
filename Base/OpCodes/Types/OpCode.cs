﻿using Auxiliary.Tools;
using Base.Devices;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Base.OpCodes.Types
{
    internal delegate void OpCodeEventDelegate(object sender, Result result);

    [Serializable]
    internal abstract class OpCode : IDisposable, ISerializable
    {
        #region Fields
        //private volatile ManualResetEventSlimTimeoutResettable processWaiter;
        private volatile ManualResetEventSlim processWaiter;
        protected Result parseResult;

        private const int NUMBER_OF_RETRIES = 3;

        private volatile bool isComplete;
        private volatile bool isRunning;

        private string name;
        private string nameString;

        #endregion
        #region Events
        public event OpCodeEventDelegate OnFinished;

        internal event OpCodeEventDelegate OnPopulate;
        internal event OpCodeEventDelegate OnParsed;

        #endregion
        #region Properties
        public string Name 
        {
            get
            {
                if (name == null)
                    name = this.GetType().Name;

                return name;
            }
        }

        protected virtual string NameString
        {
            get
            {
                if (nameString == null)
                    nameString = Regex.Replace(Name, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");

                return nameString;
            }
        }

        public DateTime LastCommunication { get; private set; }

        public bool IsDisposed { get; private set; }
        public bool IsComplete { get { return isComplete; } private set { isComplete = value; } }
        public bool IsRunning { get { return isRunning; } private set { isRunning = value; } }

        protected GenericDevice ParentDevice { get; set; }
        protected OpCodeManager ParentOpCodeManager { get { return ParentDevice.OpCodes; } }

        protected IncomingReport InReport { get; private set; }
        protected OutgoingReport OutReport { get; private set; }

        internal bool WaitingForInReport { get; set; }

        #endregion
        #region Constructor

        protected OpCode(GenericDevice device)
        {
            ParentDevice = device;
            IsDisposed = false;
            IsRunning = false;
        }

        internal void Initialize()
        {
            //Log.Instance.Write("Initialize: {0}, #{1}", this, Log.LogSeverity.DEBUG, Name, GetHashCode());
            InitializeReports(ParentDevice);
            initialize();
        }

        protected virtual void initialize() 
        { 

        }

        protected virtual void InitializeReports(GenericDevice device)
        {
            InReport = new IncomingReport(ParentOpCodeManager);
            OutReport = new OutgoingReport(ParentOpCodeManager);
        }

        #endregion
        #region Abstract Properties
        public abstract byte[] SendOpCode { get; }
        public abstract byte[] ReceiveOpCode { get; }

        public virtual byte[] OpCodeSpecificAckHeader { get; protected set; }
        public abstract byte[] AckHeader { get; }

        #endregion
        #region Virtual Properties
        public virtual TimeSpan InvokeTimeOutDuration { get { return new TimeSpan(0, 0, 5); } }
        public virtual bool SupportsAck { get { return true; } }
        public virtual bool SupportsRetry { get { return true; } }
        public virtual bool WriteToLog { get { return true; } }

        #endregion
        #region Public Methods
        public Task<Result> Invoke()
        {
            var retries = NUMBER_OF_RETRIES;
            var result = Result.ERROR;

            while (retries > 0 && !IsDisposed && ParentDevice.IsOnline)
            {
                result = invoke().Result;
                if (result.Value != eResult.TIMED_OUT || !SupportsRetry)
                {
                    return Task.FromResult(result);
                }

                retries--;

                if (WriteToLog)
                    Log.Instance.Write("[API] ==> Device: '{0}' invoke OpCode '{1}' retry #{2}", this, Log.LogSeverity.DEBUG, ParentDevice, NameString, NUMBER_OF_RETRIES - retries);
            }

            return Task.FromResult(result);
        }

        private Task<Result> invoke()
        {
            throwIfDisposed();

            if (IsRunning)
                return Task.FromResult(Result.BUSY);

            //processWaiter = new ManualResetEventSlimTimeoutResettable(InvokeTimeOutDuration);
            processWaiter = new ManualResetEventSlim(false);

            if (WriteToLog)
                Log.Instance.Write("[API] ==> Device: '{0}' started Opcode: '{1}'", this, Log.LogSeverity.DEBUG,
                    ParentDevice, NameString);
            
            var result = Result.TIMED_OUT;
            try
            {
                doBeforePopulate();
                DoBeforePopulate();
                Populate();
                doAfterPopulate();
                DoAfterPopulate();
            }
            catch (Exception ex)
            {
                Finish(new Result(ex));
            }

            result = Result.OK;

            if (NameString.ToUpper() == "DOWNLOAD"
                || NameString.ToUpper().Contains("TIME STAMP"))
            {
                if (processWaiter != null && !processWaiter.IsSet)
                {
                    processWaiter.Set();
                    parseResult.SynthesizeResult(result);
                    return Task.FromResult(parseResult);
                }
            }

            try
            {
                //var timeout = !processWaiter.Wait();
                var timeout = !processWaiter.Wait(InvokeTimeOutDuration);
                throwIfDisposed();

                if (timeout)
                {
                    finish(Result.TIMED_OUT);
                    return Task.FromResult(Result.TIMED_OUT);
                }
                parseResult.SynthesizeResult(result);
                return Task.FromResult(parseResult);
            }
            catch (Exception ex)
            {
                return Task.FromResult(new Result(ex));
            }
        }

        public bool TryProcessReport(byte[] data, bool reportAcks)
        {
            throwIfDisposed();

            var report = IncomingReport.Create(data, ParentOpCodeManager);

            if (!checkForAckHeader(report, reportAcks))
                if (IsReceiveHeader(report))
                {
                    if (!WaitingForInReport)
                        return false;

                    InReport.Dispose();
                    InReport = report;

                    if (ShouldContinueWithProcess())
                    {
                        throwIfDisposed();

                        try
                        {
                            doBeforeParse();
                            DoBeforeParse();
                            Parse();
                            doAfterParse();
                            DoAfterParse();

                            return true;
                        }
                        catch (Exception ex)
                        {
                            var result = new Result(ex);
                            if (IsDisposed)
                                result = new Result(new ObjectDisposedException(ex.Message));

                            Finish(result);
                        }
                    }
                    else
                        return true;
                }
                else
                    return false;

            return true;
        }

        #endregion
        #region In Methods
        protected virtual void DoBeforeParse() { }
        protected abstract void Parse();
        protected virtual void DoAfterParse()
        {
            if (parseResult == null)
                parseResult = Result.OK;

            Finish(parseResult);
        }

        protected virtual void DoAfterFinished() { }

        private void doBeforeParse()
        {
            LastCommunication = DateTime.Now;
            WaitingForInReport = false;
        }

        private void doAfterParse()
        {
            Utilities.InvokeEvent(OnParsed, this, parseResult);
        }

        public void Finish(Result result, bool writeToLog = true)
        {
            finish(result);

            ReleasePopulateWaiter();

            if(writeToLog && WriteToLog)
                WriteFinishToLog(result);

            InvokeFinished(result);
            DoAfterFinished();
        }

        protected void ReleasePopulateWaiter()
        {
            if (processWaiter != null)
            {
                processWaiter.Set();
            }
        }

        protected void InvokeFinished(Result result)
        {
            Utilities.InvokeEvent(OnFinished, this, result);
        }

        protected virtual void WriteFinishToLog(Result result)
        {
            if (result.IsOK)
            {
                Log.Instance.Write("[API] ==> Device: '{0}' finished Opcode: '{1}' with result: {2}", this, Log.LogSeverity.DEBUG, ParentDevice,
                      NameString, result);
            }
            else
            {
                if (ParentDevice != null)
                {
                    if (!ParentDevice.IsDisposed && ParentDevice.IsOnline)
                        Log4Tech.Log.Instance.Write("[API] ==> Device: '{0}' finished Opcode: '{1}' with result: {2}",
                            this, Log.LogSeverity.DEBUG, ParentDevice,
                            NameString, result, result.LastException);
                }
            }
        }

        private void finish(Result result = null)
        {
            parseResult = result;

            IsComplete = true;
            IsRunning = false;
            WaitingForInReport = false;
        }

        protected virtual bool ShouldContinueWithProcess()
        {
            return true;
        }

        #endregion
        #region Out Methods
        protected virtual void DoBeforePopulate() { }
        protected abstract void Populate();
        protected virtual void DoAfterPopulate() { }

        public void Send()
        {
            LastCommunication = DateTime.Now;
            WaitingForInReport = true;

            ParentOpCodeManager.SendData(GetOutReportContent());
        }

        protected abstract byte[] GetOutReportContent();

        private void doBeforePopulate()
        {
            OutReport.Initialize();
            IsComplete = false;
            IsRunning = true;
            parseResult = new Result(eResult.OK);
        }

        private void doAfterPopulate()
        {
            Send();

            Utilities.InvokeEvent(OnPopulate, this, null);
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Checks if the header is "ack"
        /// </summary>
        /// <param name="report"></param>
        /// <param name="propagateResult"></param>
        /// <returns></returns>
        private bool checkForAckHeader(IncomingReport report, bool propagateResult)
        {
            if (IsAckHeader(report))
            {
                if (WriteToLog)
                    Log4Tech.Log.Instance.Write("[API] ==> Device: '{0}' Opcode: '{1}' Acked", this, Log.LogSeverity.DEBUG, ParentDevice,
                          NameString);

                if (propagateResult)
                    AckReceived();

                return true;
            }

            return false;
        }

        protected void ResetProcessTimer()
        {
            if (!processWaiter.IsSet)
            {
                //processWaiter.ResetTimeout();
                processWaiter.Reset();
            }
        }

        protected virtual void AckReceived() { }

        /// <summary>
        /// Determines if the received opcode header matches the current one
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        protected virtual bool IsReceiveHeader(IncomingReport report)
        {
            if (ReceiveOpCode == null)
                return false;

            return ReceiveOpCode.Any(item => item == report.OpCode) && report.SkipIfContainsBlock(ReceiveOpCode);
        }

        protected virtual bool IsAckHeader(IncomingReport report)
        {
            return report.SkipIfContainsBlock(OpCodeSpecificAckHeader);
        }

        #endregion
        #region ISerializable
        protected OpCode(SerializationInfo info, StreamingContext context)
        {

        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            
        }

        #endregion
        #region IDisposable
        public virtual void Dispose()
        {
            try
            {
                if (!IsDisposed)
                {
                    IsDisposed = true;
                    WaitingForInReport = false;
                    IsComplete = false;
                    IsRunning = false;

                    if (processWaiter != null && !processWaiter.IsSet)
                        processWaiter.Set();

                    OnFinished = null;
                    OnParsed = null;
                    OnPopulate = null;

                    ParentDevice = null;
                    InReport = null;
                    OutReport = null;
                    processWaiter = null;
                    parseResult = null;

                }
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("[API] ==> Device: '{0}' Error disposing opcode: '{1}'", this, ex, ParentDevice, NameString);
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}