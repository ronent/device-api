﻿using Base.Devices;
using Base.Devices.Management;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes.Types;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication;
using System.Collections.Concurrent;
using Auxiliary.Tools;

namespace Base.OpCodes
{
    [Serializable]
    public abstract class OpCodeManager : IDisposable, ISerializable
    {
        #region Fields
        private static bool writeDataToLog = false;

        protected GenericDevice ParentDevice;

        private IDeviceIO deviceIo = null;
        private OpCode[] opCodeInstances;
        private BlockingCollection<Tuple<byte[], bool>> dataReceivedQueue;
        private static int dataReceivedBoundedCapacity = 100;

        private CancellationTokenSource cts;

        #endregion
        #region Events
        internal event Infrastructure.Communication.DeviceIO.DataReceivedEventHandler OnDataReceived;
        internal event Infrastructure.Communication.DeviceIO.DataSentEventHandler OnDataSent;
     
        #endregion
        #region Properties
        public virtual IDeviceIO DeviceIO
        {
            get { return deviceIo; }
            protected set
            {
                if (value != deviceIo)
                {
                    if (value == null)
                    {
                        deviceIo.OnDataReceived -= Device_DataReceived;                       
                        deviceIo.OnDataSent -= Device_DataSent;
                        deviceIo.OnOffline -= Device_Offline;

                        if (DeviceIO is IBundle)
                        {
                            var bundleIO = deviceIo as IBundle;

                            if (bundleIO.IsBundleSupported)
                            {
                                bundleIO.OnBundleReceived -= Device_BundleReceived;                                
                            }
                            else
                            {
                                if (DeviceIO is IDownloadProgress && ParentDevice is GenericLogger)
                                {
                                    (ParentDevice as GenericLogger).Functions.OnDownloadProgressReported -= OpCodeManager_OnDownloadProgressReported;                                                    
                                }
                            }
                        }

                        ToggleParentDeviceState(false);
                    }

                    if (value != null)
                    {
                        value.OnDataReceived += Device_DataReceived;
                        value.OnDataSent += Device_DataSent;
                        value.OnOffline += Device_Offline;

                        if (value is IBundle)
                        {
                            var bundleIO = value as IBundle;

                            if (bundleIO.IsBundleSupported)
                            {
                                bundleIO.OnBundleReceived += Device_BundleReceived;
                            }
                            else
                            {
                                if (value is IDownloadProgress && ParentDevice is GenericLogger)
                                {
                                    (ParentDevice as GenericLogger).Functions.OnDownloadProgressReported += OpCodeManager_OnDownloadProgressReported;
                                }
                            }
                        }

                        ToggleParentDeviceState(true);
                    }

                    deviceIo = value;
                }
            }
        }

        /// <summary>
        /// Start byte of HID == 1, Serial == 0
        /// </summary>
        public abstract int OpCodeStartByte { get; }

        /// <summary>
        /// Gets the maximum size of each packet.
        /// </summary>
        /// <value>
        /// The maximum size of the array.
        /// </value>
        public abstract int MaximumArraySize { get; }

        /// <summary>
        /// Turns OpCode fieldInfo into the actual instances of type 'OpCode'
        /// </summary>
        internal IEnumerable<OpCode> OpCodeInstances
        {
            get
            {
                if (opCodeInstances == null)
                {
                    opCodeInstances = (from field in OpCodeFields
                                       select field.GetValue(this) as OpCode).ToArray();
                }

                return opCodeInstances;
            }
        }

        protected List<string> IgnoredOpCodes = new List<string>();

        /// <summary>
        /// Gets all OpCodes fields that are defined in the wrapper
        /// </summary>
        protected IEnumerable<FieldInfo> OpCodeFields
        {
            get
            {
                return from FieldInfo field in this.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance)
                              where field.FieldType.IsSubclassOf(typeof(OpCode)) && !IgnoredOpCodes.Contains(field.Name)
                              select field;
            }
        }

        public string OpCodeNamespacePrefix 
        { 
            get 
            {
                string fullname = ParentDevice.GetType().Namespace;
                return fullname.Substring(0, fullname.IndexOf('.') + 1) + STRUCTURE.OPCODES + "." + ParentDevice.ClassVersion;              
            } 
        }

        protected bool IsDisposed
        {
            get;
            set;
        }

        public bool IsBusy { get { return OpCodeInstances.Any(x => x!=null && x.IsRunning); } }

        public string DeviceName
        {
            get
            {
                if (ParentDevice.Status == null || ParentDevice.Status.SerialNumber == null)
                    return DeviceIO.ShortId;
                else
                    return string.Format("#{0}", ParentDevice.Status.SerialNumber);
            }
        }

        #endregion
        #region Constructors & Initializers
        internal OpCodeManager(GenericDevice parent)
        {
            this.ParentDevice = parent;            
        }

        /// <summary>
        /// Initializes all opcodes that are declared in the device class.
        /// </summary>
        public void Initialize()
        {
            IsDisposed = false;
            dataReceivedQueue = new BlockingCollection<Tuple<byte[], bool>>(dataReceivedBoundedCapacity);
            cts = new CancellationTokenSource();

            Parallel.ForEach(OpCodeFields, field =>
                {
                    try
                    {
                        OpCode opCode = InstantiateOpCodeClass(field.Name);
                        if (opCode != null)
                            field.SetValue(this, opCode);
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.WriteError("On InitializeOpCodes()", this, ex);
                    }
                });

            Parallel.ForEach(OpCodeInstances, opcode => 
                {
                    try
                    {
                        if (opcode != null)
                            opcode.Initialize();
                    }
                    catch (Exception ex)
                    {
                        Log.Instance.WriteError("On InitializeOpCodes()", this, ex);
                    }
                });
        }

        public void SetDeviceInterfacer(IDeviceIO deviceIO)
        {
            this.DeviceIO = deviceIO;
            dataReceivedQueueListener();
        }

        #endregion
        #region Event Methods
        private void Device_Offline()
        {
            try
            {
                ParentDevice.IsOnline = false;

                if (ParentDevice.OpCodes != null)
                    ParentDevice.OpCodes.Dispose();
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Error invoking OnOffline()", this, ex);
            }
        }

        private void Device_BundleReceived(object sender, DataTransmittedEventArgs e)
        {
            BundleReceived(e.Packets);
        }

        protected void Device_DataReceived(object sender, DataTransmittedEventArgs e)
        {
            try
            {
                if (IsDisposed)
                    return;

                DataReceived(e.Data);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On Device_DataReceived()",this, ex);
            }
        }

        protected void Device_DataSent(object sender, DataTransmittedEventArgs e)
        {
            try
            {
                if (IsDisposed)
                    return;

                if (writeDataToLog)
                    Log.Instance.Write("[API] ==> Device: '{0}' sent: {1}", this, Log.LogSeverity.INFO, ParentDevice, BitConverter.ToString(e.Data));
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On Device_DataSent()",this, ex);
            }
        }

        private void OpCodeManager_OnDownloadProgressReported(object sender, ProgressReportEventArgs e)
        {
            (DeviceIO as IDownloadProgress).InvokeDownloadProgress(e.Percent);
        }

        #endregion
        #region Methods
        private void ToggleParentDeviceState(bool online)
        {
            if (ParentDevice != null)
                ParentDevice.IsOnline = online;
        }

        protected virtual void DataReceived(byte[] data)
        {
            DataReceived(data, true);
        }

        /// <summary>
        /// Lets all the OpCodes know that data has been received
        /// Returns after the first OpCodes successfully processes the data
        /// </summary>
        /// <param name="data"></param>
        /// <param name="propogateAck">Whether to invoke OnAckedEvent</param>
        protected virtual void DataReceived(byte[] data, bool propogateAck)
        {
            throwIfDisposed();

            dataReceivedQueue.Add(new Tuple<byte[], bool>(data, propogateAck));
        }

        private void dataReceivedQueueListener()
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    while (!IsDisposed && !DeviceIO.IsDisposed)
                    {
                        var tuple = dataReceivedQueue.Take(cts.Token);
                        dataReceived(tuple.Item1, tuple.Item2);
                    }
                }
                catch
                {
                    // ignored
                }
            }, TaskCreationOptions.LongRunning);
        }

        private void dataReceived(byte[] data, bool propogateAck)
        {
            //if (writeDataToLog)
            //Log.Instance.Write("[API] ==> Device: '{0}' received: {1}", this, Log.LogSeverity.INFO, ParentDevice, BitConverter.ToString(data));

            //var processed = false;
            Parallel.ForEach(OpCodeInstances, opcode =>
            //foreach (var opcode in OpCodeInstances)
            {
                {
                   // if (opcode != null && opcode.TryProcessReport(data, propogateAck))
//                    {
                       // processed = true;
//                    }
                    if (opcode == null) return;
                    //Task t = Task.Run(() => opcode.TryProcessReport(data, propogateAck));
                    //t.Wait();
                    opcode.TryProcessReport(data, propogateAck);
                }
            });

            //if (!processed)
            //{
            //    Log.Instance.Write("TBD", this, Log.LogSeverity.DEBUG);
            //}

            Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(data));
        }

        protected void BundleReceived(IEnumerable<byte[]> bundle)
        {
            foreach (var data in bundle)
            {
                DataReceived(data, false);
            }
        }

        public virtual void SendData(byte[] data)
        {
            throwIfDisposed();

            DeviceIO.SendData(data);
            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));
        }

        /// <summary>
        /// Create instances (initialize) for the OpCode
        /// </summary>
        /// <param name="name"> OpCode name</param>
        /// <returns></returns>
        internal OpCode InstantiateOpCodeClass(string name)
        {
            try
            {
                Type type = getRightOpCode(name);

                if (type != null && !type.IsAbstract)
                    return Activator.CreateInstance(type, this.ParentDevice) as OpCode;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On InstantiateOpCodeClass()",this, ex);
            }

            return null;
        }

        private Type getRightOpCode(string name)
        {
            try
            {
                string fullName = OpCodeNamespacePrefix + "." + name;
                fullName = GetOpCodeFullName(fullName);

                Type type = getOpCode(fullName);
                while (type == null)
                {
                    string downgraded = downgrade(fullName);
                    type = getOpCode(downgraded);
                }

                return type;
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Error on getRightOpCode() on -> {0}", this, ex,name);
                return null;
            }
        }

        protected virtual string GetOpCodeFullName(string name)
        {
            return name;
        }

        private string downgrade(string fullName)
        {
            var splitted = fullName.Split('.');
            
            int version;
            if(int.TryParse(splitted[splitted.Length - 2].Replace("V", ""), out version))
            {
                version--;
                if (version <= 0)
                    return string.Empty;

                splitted[splitted.Length - 2] = "V" + version;

                return string.Join(".", splitted);
            }
            else
                return string.Empty;
        }

        private Type getOpCode(string fullName)
        {
            return ParentDevice.GetType().Assembly.GetType(fullName);
        }

        internal virtual void Assimilate(OpCodeManager newOpCodes)
        {
            ParentDevice.ReplaceOpCodeManager(newOpCodes);
        }

        internal bool CompatibleWith(Type type)
        {
            return this.GetType() == type;
        }

        #endregion
        #region IDisposable Members
        public void Dispose()
        {
            if (!IsDisposed)
            {
                //Log.Instance.Write("[API] ==> Device: '{0}' OpCodeManager is disposing", this, Log.LogSeverity.DEBUG, ParentDevice);
                IsDisposed = true;

                if (cts != null)
                {
                    try { cts.Cancel(false); }
                    catch { }

                    cts.Dispose();
                }

                Parallel.ForEach(OpCodeInstances, opcode =>
                    {
                        try
                        {
                            if (opcode != null)
                                opcode.Dispose();
                        }
                        catch (Exception ex)
                        {
                            Log.Instance.WriteError("Error disposing opcode: {0}", this, ex, opcode);
                        }
                    });

                opCodeInstances = null;
            }
        }

        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
        #region Serialization & Deserialization
        protected OpCodeManager(SerializationInfo info, StreamingContext ctxt)
        {

        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {

        }
        
        #endregion
    }
}