﻿using Auxiliary.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Base.OpCodes
{
    internal class IncomingReport : Report
    {
        public IncomingReport(OpCodeManager manager)
            : base(manager)
        {
            Content = new byte[MaximumSize];
            Initialize();
        }

        public IncomingReport(OpCodeManager manager, byte[] data)
            : base(manager)
        {
            Initialize(data);
        }

        public void Initialize(IncomingReport report)
        {
            this.Initialize(report.Content);
        }

        public void Initialize(byte[] data)
        {
            Initialize();
            Buffer.BlockCopy(data, 0, Content, 0, data.Length);
        }

        public static IncomingReport Create(byte[] data, OpCodeManager manager)
        {
            IncomingReport report = new IncomingReport(manager);
            report.Content = data;
            return report;
        }

        public bool SkipIfContainsBlock(byte[] block)
        {
            if (block == null)
                return false;

            int index = Index;
            foreach (byte b in block)
                if (b != Content[index++])
                    return false;

            Index = index;
            return true;
        }

        /// <summary>
        /// Initializes the byte array and the index of the parameter report
        /// </summary>
        /// <param name="report"></param>
        internal void Assimilate(IncomingReport report)
        {
            Initialize(report);
            Index = report.Index;
        }

        public bool IsFilledWith0xFF()
        {
            int count = Content.Where((b, index) => index >= Index && b == 0xFF).Count();
            return count == SpaceLeft;
        }

        public bool Is0xFF(ushort byteCount)
        {
            for (int i = Index; i < Index+byteCount; i++)
                if (Content[i] != 0xff)
                    return false;
            return true;
        }

        public byte PeekNext()
        {
            return Content[Index];
        }

        #region Get Methods
        /// <summary>
        /// Returns one byte
        /// </summary>
        /// <returns></returns>
        public IEnumerable<bool> Get8Bits()
        {
            for (int i = 0; i < BITS_IN_BYTE; i++)
                yield return getNthBit(i);
            Index++;
        }

        private bool getNthBit(int pos)
        {
            return (Current & (1 << pos)) != 0;
        }

        public string GetHexString(int length, Endianness endianness = Endianness.LITTLE)
        {
            if (length < 0)
                length = Content.Length - Index;

            byte[] receivedBytes = new byte[length];
            Buffer.BlockCopy(Content, Index, receivedBytes, 0, length);

            if (endianness == Endianness.BIG)
                Array.Reverse(receivedBytes);

            Index += length;
            return BitConverter.ToString(receivedBytes).Replace("-", "");
        }

        public string GetString()
        {
            return GetString(SpaceLeft);
        }

        public string GetString(int length)
        {
            if (length < 0)
                length = Content.Length - Index;

            byte[] receivedBytes = new byte[length];
            Buffer.BlockCopy(Content, Index, receivedBytes, 0, length);

            Index += length;
            return System.Text.ASCIIEncoding.ASCII.GetString(receivedBytes).Substring(0, length).Replace("\0", "").Trim();
        }
        
        /// <summary>
        /// Returns block of byte converted to bool
        /// </summary>
        /// <returns></returns>
        public bool GetBool()
        {
            return Convert.ToBoolean(Next);
        }

        public byte[] GetRest()
        {
            int length = Content.Length - Index;
            byte[] receivedBytes = new byte[length];
            Buffer.BlockCopy(Content, Index, receivedBytes, 0, length);

            return receivedBytes;
        }

        public byte[] GetBlock(int size)
        {
            byte[] result = new byte[size];
            Array.Copy(Content, Index, result, 0, size);
            Index += size;
            return result;
        }

        public float GetSingle()
        {
            Single result = BitConverter.ToSingle(Content, Index);
            if (Single.IsNaN(result))
                result = Single.MinValue;
            Index += sizeof(float);
            return result;
        }

        public T Get<T>(Endianness endianness = Endianness.LITTLE)
        {
            var type = typeof(T);
            int size = System.Runtime.InteropServices.Marshal.SizeOf(type);
            try
            {
                var bytes = Utilities.GetBytes(Content, Index, size, endianness);
                return (T)typeof(BitConverter).GetMethod("To" + type.Name, BindingFlags.Static | BindingFlags.Public).Invoke(null, new object[] { bytes, 0 });
            }
            finally
            {
                Index += size;
            }
        }
        #endregion
    }
}
