﻿using Auxiliary.Tools;
using System;

namespace Base.OpCodes
{
    /// <summary>
    /// Byte array wrapper for IO messages.
    /// </summary>
    internal abstract class Report : IDisposable
    {
        #region Fields
        protected const int BITS_IN_BYTE = 8;

        #endregion
        #region Properties
        public int Index { get; protected set; }
        public int SpaceLeft { get { return Content.Length - Index - 1; } }
        public bool HasNext { get { return SpaceLeft > 0; } }
        
        /// <summary>
        /// Returns the current byte and increase the pointer
        /// </summary>
        public byte Next
        {
            get { return Content[Index++]; }
            set { Content[Index++] = value; }
        }

        public byte Current
        {
            get { return Content[Index]; }
            set { Content[Index] = value; }
        }

        protected byte[] Content;

        public byte this[int index]
        {
            get { return Content[index]; }
            set { Content[index] = value; }
        }

        public byte OpCode
        {
            get { return Content[StartByte]; }
        }

        public int StartByte
        {
            get;
            protected set;
        }

        public int MaximumSize
        {
            get;
            private set;
        }

        #endregion
        #region Constructor
        public Report(OpCodeManager manager)
        {
            StartByte = manager.OpCodeStartByte;
            MaximumSize = manager.MaximumArraySize;
        }

        public virtual void Initialize()
        {
            Index = StartByte;
            Content = new byte[MaximumSize];
        }

        #endregion
        #region Methods
        public byte[] GetContent()
        {
            return Content;
        }

        public bool Has(int bytes)
        {
            return Index + bytes <= Content.Length;
        }

        public void Skip(int count)
        {
            Index += count;
        }

        /// <summary>
        /// Skips SpareBytes
        /// </summary>
        /// <param name="spare"></param>
        public void Skip(SpareBytes spare)
        {
            Index += spare.Size;
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            //Nothing here for now
        }
        #endregion
    }
}
