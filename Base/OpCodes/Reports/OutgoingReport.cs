﻿using Auxiliary.Tools;
using System;
using System.Linq;

namespace Base.OpCodes
{
    internal class OutgoingReport : Report
    {
        public OutgoingReport(OpCodeManager manager)
            : base(manager)
        {
            Content = new byte[MaximumSize];
            Initialize();
        }

        /// <summary>
        /// Inserts converted boolean array to the packet
        /// </summary>
        /// <param name="values"></param>
        public void InsertBits(params bool[] values)
        {
            for (int i = 0; i < values.Count(); i += BITS_IN_BYTE)
                Next = collectBitsToByte(values.SubArray(i, BITS_IN_BYTE));
        }

        private static byte collectBitsToByte(bool[] values)
        {
            byte indexByte = 0;
            for (int i = 0; i < values.Count() && i < BITS_IN_BYTE; i++)
                indexByte |= Convert.ToByte(Convert.ToByte(values[i]) << i);

            return indexByte;
        }

        public void Insert(bool value)
        {
            byte[] block = BitConverter.GetBytes(value);
            Insert(block);
        }

        public void Insert(Int16 value, Endianness endianness = Endianness.LITTLE)
        {
            byte[] block = BitConverter.GetBytes(value).AdjustToEndianness(endianness);
            Insert(block);
        }

        public void Insert(UInt16 value, Endianness endianness = Endianness.LITTLE)
        {
            byte[] block = BitConverter.GetBytes(value).AdjustToEndianness(endianness);
            Insert(block);
        }

        public void Insert(Int32 value, Endianness endianness = Endianness.LITTLE)
        {
            byte[] block = BitConverter.GetBytes(value).AdjustToEndianness(endianness);
            Insert(block);    
        }

        public void Insert(UInt32 value, Endianness endianness = Endianness.LITTLE)
        {
            byte[] block = BitConverter.GetBytes(value).AdjustToEndianness(endianness);
            Insert(block);
        }

        public void InsertStringToHexArray(string str, Endianness endianness = Endianness.LITTLE)
        {
            Insert(StringToByteArrayFastest(str), endianness);
        }

        public void Insert(string str)
        {
            Insert(str, str.Length);
        }

        public void Insert(string str, int maxLength)
        {
            str = str.PadRight(maxLength, '\0');
            byte[] block = new System.Text.ASCIIEncoding().GetBytes(str);
            Insert(block);
        }

        public void Insert(byte[] block, Endianness endianness = Endianness.LITTLE)
        {
            int length = Math.Min(SpaceLeft, block.Length);
            if (endianness == Endianness.BIG)
                Array.Reverse(block);

            Buffer.BlockCopy(block, 0, Content, Index, length);
            Index += block.Length;
        }

        public void Insert(byte reportId, byte[] block, Endianness endianness = Endianness.LITTLE)
        {
            int length = Math.Min(SpaceLeft, block.Length);
            if (endianness == Endianness.BIG)
                Array.Reverse(block);

            Content[0] = reportId;

            Buffer.BlockCopy(block, 0, Content, Index, length);
            Index += block.Length;
        }

        public void Insert(float number)
        {
            byte[] block = BitConverter.GetBytes(number);
            Insert(block);
        }

        public byte[] GetTrimmedContent()
        {
            seal();
            return Content;
        }

        private void seal()
        {
            Content = Utilities.SubArray(Content, 0, Index);
        }

        private static byte[] StringToByteArrayFastest(string hex)
        {
            if (hex.Length % 2 == 1)
                throw new Exception("The binary key cannot have an odd number of digits");

            byte[] arr = new byte[hex.Length >> 1];

            for (int i = 0; i < hex.Length >> 1; ++i)
            {
                arr[i] = (byte)((GetHexVal(hex[i << 1]) << 4) + (GetHexVal(hex[(i << 1) + 1])));
            }

            return arr;
        }

        private static int GetHexVal(char hex)
        {
            int val = (int)hex;
            //For uppercase A-F letters:
            return val - (val < 58 ? 48 : 55);
            //For lowercase a-f letters:
            //return val - (val < 58 ? 48 : 87);
            //Or the two combined, but a bit slower:
            //return val - (val < 58 ? 48 : (val < 97 ? 55 : 87));
        }

    }
}
