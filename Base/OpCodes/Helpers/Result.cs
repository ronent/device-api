﻿using Base.OpCodes.Types;
using System;
using System.Collections.Generic;

namespace Base.OpCodes
{
    public enum eResult : byte
    {
        ILLEGAL_CALL,
        OK,
        ERROR,
        OFFLINE,
        TIMED_OUT,
        CANCELED,
        BUSY,
        UNSUPPORTED,    
    };
    [Serializable]
    public class Result
    {
        public static Result OK { get { return new Result(eResult.OK); } }
        public static Result ERROR { get { return new Result(eResult.ERROR); } }
        public static Result CANCELED { get { return new Result(eResult.CANCELED); } }
        public static Result TIMED_OUT { get { return new Result(eResult.TIMED_OUT) { LastException = new Exception("Operation has timed out") }; } }
        public static Result ILLEGAL_CALL { get { return new Result(eResult.ILLEGAL_CALL); } }
        public static Result BUSY { get { return new Result(eResult.BUSY) { LastException = new Exception("Operation cannot be initiated because a prior instance is already running") }; } }
        public static Result UNSUPPORTED { get { return new Result(eResult.UNSUPPORTED); } }

        public List<Exception> Exceptions { get; private set; }
        public Exception LastException 
        {
            get
            {
                if (Exceptions.Count > 0)
                    return Exceptions[Exceptions.Count - 1];

                return null;
            }
            set
            {
                Exceptions.Add(value);
            }
        }

        public eResult Value { get; set; }
        public bool IsOK { get { return Value == eResult.OK; } }
        public string MessageLog { get; set; }

        public Result(eResult resultEnum)
        {
            initializate();

            this.Value = resultEnum;
        }

        public Result(System.Exception ex)
        {
            initializate();

            this.Value = eResult.ERROR;
            this.LastException = ex;
        }

        private void initializate()
        {
            Exceptions = new List<Exception>();
        }

        /// <summary>
        /// 'And' operation between two results
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool SynthesizeResult(Result other)
        {
            if (IsOK && other != null)
                if (other.IsOK)
                    return true;
                else
                {
                    this.Value = other.Value;
                    this.LastException = other.LastException;
                }

            return false;
        }

        /// <summary>
        /// 'Or' operation between two results
        /// </summary>
        /// <param name="other"></param>
        public bool OrResult(Result other)
        {
            if (IsOK || other.IsOK)
            {
                return true;
            }
            else
            {
                this.Value = other.Value;
                this.LastException = other.LastException;

                return false;
            }
        }

        public void SynthesizeResultAndThrowOnError(Result other)
        {
            if (!SynthesizeResult(other))
            {
                if (other.LastException == null)
                    other.LastException = new Exception(other.Value.ToString());

                throw other.LastException;
            }
        }

        internal void SynthesizeResultAndThrowOnError(OpCode opcode)
        {
            if (IsOK)
                SynthesizeResultAndThrowOnError(opcode.Invoke().Result);
            else
                throw LastException;
        }

        internal void AddExceptions(List<System.Exception> exceptions)
        {
            try { Exceptions.AddRange(exceptions); }
            catch { }
        }

        public void SetResult(Result result)
        {
            this.Value = result.Value;
            this.LastException = result.LastException;
        }

        public override string ToString()
        {
            string toReturn;
            if (LastException == null)
                toReturn = string.Format("({0}) {1}", Value, MessageLog);
            else
                if (LastException.InnerException == null)
                    toReturn = string.Format("{0}: {1}", Value, LastException.Message);
                else
                    toReturn = string.Format("{0}: {1}{2}Inner Exception: {3}", Value, LastException.Message, Environment.NewLine, LastException.InnerException.Message);

            return toReturn;
        }
    }
}
