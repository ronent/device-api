﻿using Base.Devices;
using System;

namespace Base.OpCodes
{
    public delegate void ReportProgressDelegate(object sender, ProgressReportEventArgs e);

    public enum eStage
    {
        Started,
        Queued,
        Running,
    }
    [Serializable]
    public class ProgressReportEventArgs : EventArgs
    {
        #region Constructor
        public ProgressReportEventArgs(string serialNumber, eStage stage)
        {
            Stage = stage;

            SerialNumber = serialNumber;
        }

        public ProgressReportEventArgs(string serialNumber, int percent)
        {
            Stage = eStage.Running;

            SerialNumber = serialNumber;
            Percent = percent;
        }

        #endregion
        #region Properties
        public string SerialNumber { get; private set; }
        public int Percent { get; private set; }
        public eStage Stage { get; private set; }

        #endregion
        #region Object overrides
        public override string ToString()
        {
            return string.Format("Device: '{0}' progress: {1}%", SerialNumber, Percent);
        }

        #endregion
    }
}
