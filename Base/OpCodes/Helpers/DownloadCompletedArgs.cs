﻿using Base.Devices;
using Base.Sensors.Management;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.OpCodes.Helpers
{
    public delegate void DownloadCompletedDelegate(object sender, DownloadCompletedArgs args);

    [Serializable]
    public class DownloadCompletedArgs : EventArgs
    {
        #region Properties
        public GenericLogger Logger { get; private set; }

        public string SerialNumber { get { return Logger.Status.SerialNumber; } }

        public IEnumerable<GenericSensor> EnabledSensors { get { return Logger.Sensors.GetAllEnabled(); } }

        #endregion
        #region Constructor

        public DownloadCompletedArgs(GenericLogger logger)
        {
            Logger = GenericDevice.DeepCopy(logger);
            Logger.Sensors.Sort();
            logger.Sensors.Clear();
        }

        #endregion
    }
}
