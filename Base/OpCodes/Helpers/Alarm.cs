﻿using Auxiliary.MathLib;
using Base.Sensors.Types;
using System;

namespace Base.OpCodes
{
    [Serializable]
    public class Alarm
    {
        #region Fields
        public decimal Low { get; set; }
        public decimal High { get; set; }
        #endregion
        #region Properties
        public bool Enabled { get; set; }

        #endregion
        #region Methods
        public void ThrowIfNotValid(GenericSensor sensor)
        {
            DetermineIfAlarmEnabled(sensor);

            if (sensor != null && Enabled)
                if (!(sensor.Minimum <= Low
                   && Low < High
                   && High <= sensor.Maximum))
                    throw new Exception("Invalid alarm values. Values either exceed sensor limits or low >= high");
            
        }

        internal void DetermineIfAlarmEnabled(GenericSensor sensor)
        {
            try
            {
                if (Low == sensor.Minimum && High == sensor.Maximum || Low == High)
                    Enabled = false;
                else
                    Enabled = true;
            }
            catch
            {
                Enabled = false;
            }
        }

        public void Invalidate(GenericSensor sensor)
        {
            Low = sensor.Minimum;
            High = sensor.Maximum;
        }

        public override string ToString()
        {
            return "Alarm Enabled: " + Enabled 
                + (Enabled 
                    ? ", Range: " + Low + " - " + High 
                    : "");
        }
        #endregion
        #region Equals
        public override bool Equals(object obj)
        {
            Alarm other = obj as Alarm;
            if (other == null) return false;

            return Enabled == other.Enabled
                && (!Enabled || alarmEndsReasonablyClose(other));
        }

        private bool alarmEndsReasonablyClose(Alarm other)
        {
            return (IntegerFraction.IsReasonablyCloseTo(Low, other.Low) && IntegerFraction.IsReasonablyCloseTo(High, other.High));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
