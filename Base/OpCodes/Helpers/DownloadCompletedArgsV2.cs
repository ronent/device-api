﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Types;
using Base.Sensors.Types;

namespace Base.OpCodes.Helpers
{
     public delegate void DownloadCompletedV2Delegate(object sender, DownloadCompletedArgsV2 args);
    [Serializable]
    public class DownloadCompletedArgsV2 : EventArgs
    {
          #region Properties
        public GenericLoggerV2 Logger { get; private set; }

        public string SerialNumber { get { return Logger.Status.SerialNumber; } }

        public IEnumerable<GenericSensorV2> EnabledSensors { get { return Logger.Sensors.GetAllEnabled(); } }

        #endregion
        #region Constructor

        public DownloadCompletedArgsV2(GenericLoggerV2 logger)
        {
            Logger = GenericDevice.DeepCopy(logger);
            Logger.Sensors.Sort();
            logger.Sensors.Clear();
        }

        #endregion
    }
}
