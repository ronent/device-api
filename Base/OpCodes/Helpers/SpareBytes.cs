﻿using System;

namespace Base.OpCodes
{
    [Serializable]
    class SpareBytes
    {
        private int start, end;
        public int Size
        {
            get {return end - start + 1;}
        }
        public SpareBytes(int start, int end)
        {
            this.start = start;
            this.end = end;
        }
        public SpareBytes(int length)
        {
            this.start = 1;
            this.end = length;
        }
    }
}
