﻿using Auxiliary.Tools;
using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Base.Firmware
{
    /// <summary>
    /// Firmware Factory.
    /// </summary>
    public class FirmwareFactory
    {
        /// <summary>
        /// Gets or sets the file path.
        /// </summary>
        /// <value>
        /// The file path.
        /// </value>
        internal string FilePath { get; set; }

        private ConcurrentDictionary<string, DeviceFirmware> firmwares;
        private static FirmwareFactory firmwareFactory;

        /// <summary>
        /// Prevents a default instance of the <see cref="FirmwareFactory"/> class from being created.
        /// </summary>
        private FirmwareFactory()
        {
            firmwares = new ConcurrentDictionary<string, DeviceFirmware>();
            initialize();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void initialize()
        {
            FilePath = ".";
            Refresh();
        }

        /// <summary>
        /// Gets the singleton instance.
        /// </summary>
        /// <returns></returns>
        public static FirmwareFactory GetInstance()
        {
            if (firmwareFactory == null)
                firmwareFactory = new FirmwareFactory();

            return firmwareFactory;
        }

        /// <summary>
        /// Refreshes firmware files.
        /// </summary>
        internal void Refresh()
        {
            firmwares.Clear();

            foreach (var item in Directory.GetFiles(FilePath, "*.fw"))
            {
                FileInfo file = new FileInfo(item);
                DeviceFirmware firmware = loadFirmwareFile(file);
                if (firmware != null)
                    firmwares.TryAdd(firmware.Name, firmware);
            }
        }

        /// <summary>
        /// Loads the firmware file.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error loading firmware file</exception>
        private DeviceFirmware loadFirmwareFile(FileInfo filePath)
        {
            DeviceFirmware firmware = null;

            try
            {
                if (filePath.Exists)
                {
                    using (IO fileIO = new IO(filePath.FullName, false))
                    {
                        firmware = fileIO.DecryptAndDeserialize<DeviceFirmware>();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading firmware file", ex);
            }

            return firmware;
        }

        /// <summary>
        /// Gets the specified firmware name.
        /// </summary>
        /// <param name="firmwareName">Name of the firmware.</param>
        /// <returns></returns>
        internal DeviceFirmware Get(string firmwareName, byte PCBVersion = default(byte))
        {
            Refresh();

            DeviceFirmware firmware;
            if (PCBVersion == default(byte))
            {
                firmware = firmwares.Where(x => x.Value.Name.Equals(firmwareName)).FirstOrDefault().Value;
            }
            else
            {
                firmware = firmwares.Where(x => x.Value.Name.Equals(firmwareName)
                    && x.Value.PCBVersion.Equals(PCBVersion)).FirstOrDefault().Value;
            }

            return firmware;
        }

        public DeviceFirmware Get(Type type)
        {
            string fwName = type.GetField(STRUCTURE.FIRMWARE_DEVICE_NAME, BindingFlags.Static | BindingFlags.NonPublic).GetValue(null) as string;

            if (fwName != null)
                return Get(fwName);
            else
                return null;
        }
    }
}
