﻿using System;

namespace Base.Firmware
{
    /// <summary>
    /// Data address of firmware
    /// </summary>
    internal struct DataAddress
    {
        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>
        /// The address.
        /// </value>
        public UInt16 Address { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets the address bytes.
        /// </summary>
        /// <value>
        /// The address bytes.
        /// </value>
        public byte[] AddressBytes { get { return BitConverter.GetBytes(Address); } }
    }
}
