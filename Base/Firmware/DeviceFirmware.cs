﻿using Auxiliary.Tools;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;

namespace Base.Firmware
{
    /// <summary>
    /// Generic Firmware.
    /// </summary>
    [Serializable]
    public abstract class DeviceFirmware : ISerializable
    {
        #region Consts
        /// <summary>
        /// The line length
        /// </summary>
        protected const int LINE_LENGTH = 43;

        /// <summary>
        /// The block index
        /// </summary>
        protected const int BLOCK_INDEX = 9;

        /// <summary>
        /// The block length
        /// </summary>
        protected const int BLOCK_LENGTH = 32;

        /// <summary>
        /// The address index
        /// </summary>
        protected const int ADDRESS_INDEX = 1;

        /// <summary>
        /// The address length
        /// </summary>
        protected const int ADDRESS_LENGTH = 6;

        #endregion
        #region Properties
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public abstract string Name { get; }

        /// <summary>
        /// Gets the start address.
        /// </summary>
        /// <value>
        /// The start address.
        /// </value>
        internal abstract UInt16 StartAddress { get; }

        /// <summary>
        /// Gets the full start address.
        /// </summary>
        /// <value>
        /// The full start address.
        /// </value>
        internal abstract UInt32 FullStartAddress { get; }

        /// <summary>
        /// Gets the size of the block.
        /// </summary>
        /// <value>
        /// The size of the block.
        /// </value>
        internal abstract int BlockSize { get; }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        public Version Version { get; set; }

        /// <summary>
        /// Gets the creation time.
        /// </summary>
        /// <value>
        /// The created.
        /// </value>
        public DateTime Created { get; private set; }

        /// <summary>
        /// Gets or sets the byte list.
        /// </summary>
        /// <value>
        /// The byte list.
        /// </value>
        internal List<byte[]> ByteList { get; set; }

        /// <summary>
        /// Gets the PCB version.
        /// </summary>
        /// <value>
        /// The PCB version.
        /// </value>
        public byte PCBVersion { get; set; }

        /// <summary>
        /// Gets the byte array.
        /// </summary>
        /// <value>
        /// The byte array.
        /// </value>
        internal byte[] ByteArray { get { return ByteList.SelectMany(x => x.AsEnumerable()).ToArray(); } }

        internal bool Valid
        {
            get
            {
                try
                {
                    return Name != string.Empty
                        && ByteArray.Length > 0;
                }
                catch { return false; }
            }
        }
        
        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceFirmware"/> class.
        /// </summary>
        public DeviceFirmware()
        {
            ByteList = new List<byte[]>();
        }

        /// <summary>
        /// Initializes the values.
        /// </summary>
        private void initializeValues()
        {
            ByteList.Clear();
        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the iterator.
        /// </summary>
        /// <returns></returns>
        internal FWIterator GetIterator()
        {
            return new FWIterator(ByteList, BLOCK_LENGTH, StartAddress);
        }

        /// <summary>
        /// Loads the hexadecimal file.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns></returns>
        private bool loadHex(string filename)
        {
            string line;
            int index = 0;
            try
            {
                initializeValues();
                using (TextReader reader = new StreamReader(filename))
                {
                    while ((line = reader.ReadLine()) != null)
                    {
                        analyzeLine(line);
                        index++;
                    }
                }

                Created = DateTime.Now;
                return true;
            }
            catch (Exception ex) { Log.Instance.WriteError("Fail in loadHex",this,ex); return false; }
        }

        /// <summary>
        /// Analyzes the line.
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns></returns>
        private bool analyzeLine(string line)
        {
            if (isLineValid(line))
            {
                byte[] bytes = getBytes(line.Substring(BLOCK_INDEX, BLOCK_LENGTH));
                ByteList.Add(bytes);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Determines whether [is line valid] [the specified line].
        /// </summary>
        /// <param name="line">The line.</param>
        /// <returns></returns>
        private bool isLineValid(string line)
        {
            string address = line.Substring(ADDRESS_INDEX, ADDRESS_LENGTH);
            return line.Length == LINE_LENGTH && isAddressAboveStart(address);
        }

        /// <summary>
        /// Determines whether [is address above start] [the specified address].
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private bool isAddressAboveStart(string address)
        {
            return Convert.ToInt32(address, 16) >= FullStartAddress;
        }

        /// <summary>
        /// Gets the bytes.
        /// </summary>
        /// <param name="hex">The hexadecimal.</param>
        /// <returns></returns>
        private byte[] getBytes(string hex)
        {
            List<byte> bytes = new List<byte>();
            for (int i = 0; i < hex.Length; i += 2)
            {
                bytes.Add(Convert.ToByte(hex.Substring(i, 2), 16));
            }

            return bytes.ToArray();
        }

        #endregion
        #region Serialization
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceFirmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public DeviceFirmware(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            this.Version = (Version)info.GetValue("Version", typeof(Version));
            this.ByteList = (List<byte[]>)info.GetValue("ByteList", typeof(List<byte[]>));
            this.Created = (DateTime)info.GetValue("Created", typeof(DateTime));
            this.PCBVersion = (byte)info.GetValue("PCBVersion", typeof(byte));
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("Version", this.Version, typeof(Version));
            info.AddValue("ByteList", this.ByteList, typeof(List<byte[]>));
            info.AddValue("Created", this.Created, typeof(DateTime));
            info.AddValue("PCBVersion", this.PCBVersion, typeof(byte));
        }

        #endregion
        #region Save as file
        /// <summary>
        /// Saves the firmware file as (*.fw).
        /// </summary>
        /// <param name="hexFilePath">The hexadecimal file to load.</param>
        /// <param name="newFilePath">Path of the new file.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Error saving firmware file</exception>
        internal bool SaveFirmwareFile(string hexFilePath, string newFilePath)
        {
            try
            {
                loadHex(hexFilePath);

                using (IO fileIO = new IO(newFilePath, true))
                {
                    fileIO.FileName = newFilePath;
                    fileIO.EncryptAndSerialize<DeviceFirmware>(this);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error saving firmware file", ex);
            }
        }

        #endregion
    }
}
