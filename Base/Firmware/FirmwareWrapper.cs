﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using Auxiliary.Tools;


namespace Base.Firmware
{

    //Rendered obsolete by a string-based nomenclature (ZAK, 12/04/11)
//     internal enum FirmwareTypeEnum : int
//     {
//         DataNetRepeater = 0,
//         DataNetLogger = 1,
//         MiniDataNet = 2,
//         DataNetFirmwareLastType = 2,
//         USBLogger = 3,
//         EC800 = 4,
//         EC850 = 5,
//         NumberOfFirmwareTypes = 5,
//     }

    [Serializable]
    public class DeviceFirmwareInformation : ISerializable
    {
        public string DeviceName;
        public float FirmwareVersion = 0;
        public string FirmwareFileName;
        public byte[] FirmwareByteArray;
        public int FirmwareFirstMemoryAddress;
        #region Constructor Serialization\Deserialization
        public DeviceFirmwareInformation(string DeviceName)
        {
            this.DeviceName = DeviceName;
        }

        //Deserialization constructor.
        public DeviceFirmwareInformation(SerializationInfo info, StreamingContext ctxt)
        {
            //Get the values from info and assign them to the appropriate properties
            this.DeviceName = (string)info.GetValue("DeviceName", typeof(string));
            this.FirmwareVersion = (float)info.GetValue("FirmwareVersion", typeof(float));
            this.FirmwareFileName = (string)info.GetValue("FirmwareFileName", typeof(string));
            this.FirmwareByteArray = (byte[])info.GetValue("FirmwareByteArray", typeof(byte[]));
            this.FirmwareFirstMemoryAddress = (int)info.GetValue("FirmwareFirstMemoryAddress", typeof(int));
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("DeviceName", this.DeviceName, typeof(string));
            info.AddValue("FirmwareVersion", this.FirmwareVersion, typeof(float));
            info.AddValue("FirmwareFileName", this.FirmwareFileName, typeof(string));
            info.AddValue("FirmwareByteArray", this.FirmwareByteArray, typeof(byte[]));
            info.AddValue("FirmwareFirstMemoryAddress", this.FirmwareFirstMemoryAddress, typeof(int));
        }
        #endregion
    }

    [Serializable]
    public class DeviceFirmwareInformationList : List<DeviceFirmwareInformation>, ISerializable
    {
        #region Variables
        private ArrayList arrayList;
        #endregion
        #region Properties
        public bool Valid
        {
            get { return this.Count > 1; }    
        }
        #endregion
        #region Constructors
        public DeviceFirmwareInformationList()
        { }
        #endregion
        #region Methods
        public void AddRange(string[] DeviceTypeNames)
        {
            List<string> NewDeviceNames = new List<string>(DeviceTypeNames);
            foreach (DeviceFirmwareInformation DeviceFWInfo in this)
                if (DeviceTypeNames.Contains(DeviceFWInfo.DeviceName))
                    NewDeviceNames.Remove(DeviceFWInfo.DeviceName);

            foreach (string DeviceTypeName in NewDeviceNames)
                Add(new DeviceFirmwareInformation(DeviceTypeName));
        }
        public DeviceFirmwareInformation this[string deviceName]
        {
            get 
            {
                return this.Find(delegate(DeviceFirmwareInformation DeviceFirmwareInfo) { return DeviceFirmwareInfo.DeviceName == deviceName; });
            }
        }
        public bool ContainsList(string[] DeviceNames)
        {
            foreach (string DeviceName in DeviceNames)
                if (this[DeviceName] == null)
                    return false;

            //If you got this far means all list members are present in DeviceFirmwareInformationLsit
            return true;
        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        public DeviceFirmwareInformationList(SerializationInfo info, StreamingContext ctxt)
        {
            try
            {
                //Get the values from info and assign them to the appropriate properties
                arrayList = (ArrayList)info.GetValue("List", typeof(ArrayList));
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        [OnDeserialized]
        public void OnDeserialization(StreamingContext ctxt)
        {
            foreach (DeviceFirmwareInformation t in arrayList)
                this.Add(t);
        }
        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            try
            {
                arrayList = new ArrayList();
                arrayList.AddRange(this);
                info.AddValue("List", arrayList, typeof(ArrayList));
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);           	
            }
        }
        #endregion
    }

    [Serializable]
    public class FirmwareWrapperFile : ISerializable, IDeserializationCallback
    {
        #region Variables
        private ArrayList DFIS;
        public DateTime FirmwareDate = DateTime.Now;// new DateTime(2000, 1, 1);
        public string Comments = "";
        public DeviceFirmwareInformationList DeviceFirmwareList = new DeviceFirmwareInformationList();
        public float StackVersion = 0;
        public byte StackVersionBuild = 0;
        public string StackFileName;
        public byte[] StackByteArray;
        public static IO FileIO = new IO();
        #endregion
        #region Properties
        public bool Valid
        {
            get
            {
                if (DeviceFirmwareList.Count < 1)
                    return false;

                for (int Index = 0; Index < DeviceFirmwareList.Count; Index++)
                {
                    if ((DeviceFirmwareList[Index].FirmwareVersion == 0) || (DeviceFirmwareList[Index].FirmwareByteArray == null) || (DeviceFirmwareList[Index].FirmwareByteArray.Length < 10000))
                        return false;
                }

                if ((StackByteArray == null) || (StackByteArray.Length < 1000))
                    return false;

                return true;
            }
        }
        public string[] DeviceNames
        {
            get
            {
                return (from deviceFW in DeviceFirmwareList
                        select deviceFW.DeviceName).ToArray();
            }
        }
        #endregion
        #region Constructors
        public FirmwareWrapperFile()
            : this(null)
        {
            
        }
        public FirmwareWrapperFile(string[] deviceTypeNames)
        {
            if (deviceTypeNames != null)
                for (int Index = 0; Index < deviceTypeNames.Length; Index++)
                {
                    DeviceFirmwareList.Add(new DeviceFirmwareInformation(deviceTypeNames[Index]));
                }

        }
        #endregion
        #region Inner Classes
        sealed class AllowAllAssemblyVersionsDeserializationBinder : SerializationBinder
        {
            public override Type BindToType(string assemblyName, string typeName)
            {
                Type typeToDeserialize = null;

                String currentAssembly = Assembly.GetExecutingAssembly().FullName;

                // Always use the currently assembly   
                assemblyName = currentAssembly;

                // Get the type using the typeName and assemblyName   
                typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                    typeName, assemblyName));

                return typeToDeserialize;
            }
        }
        #endregion
        #region Methods
        public void AddRange(string[] DeviceTypeNames)
        {
            DeviceFirmwareList.AddRange(DeviceTypeNames);
        }

        public void Set(string[] DeviceTypeNames)
        {
            List<string> newDevices = new List<string>(DeviceTypeNames);
            string[] removeDevices = (from existingDevice in this.DeviceNames
                                      where !newDevices.Contains(existingDevice)
                                      select existingDevice).ToArray();
            RemoveRange(removeDevices);
            AddRange(DeviceTypeNames);
        }

        public void RemoveRange(string[] DeviceTypeNames)
        {
            DeviceFirmwareList.RemoveAll(x => DeviceTypeNames.Contains(x.DeviceName));
        }

        //Zak: Oren, this is VERY BAD design!!
        //I suggest we render this obsolete
        [Obsolete]
        void UpdateDevicesNames(string[] DeviceTypeNames)
        {
            int Index;
            try
            {
                for (Index = 0; Index < DeviceFirmwareList.Count; Index++)
                {
                    if (Index < DeviceTypeNames.Length)
                        DeviceFirmwareList[Index].DeviceName = DeviceTypeNames[Index];
                }
            }
            catch (System.Exception ex)
            {
            }
        }

        public void SaveFirmwareWrapperFile(string FileName)
        {
            try
            {
                FileIO.FileName = FileName;
                FileIO.EncryptAndSerialize<FirmwareWrapperFile>(this);
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static FirmwareWrapperFile LoadFirmwareWrapperFile(string FileName)
        {
            FirmwareWrapperFile FirmwareWrapper = null;
            try
            {
                FileInfo FI = new FileInfo(FileName);

                if (FI.Exists)
                {
                    using (IO FileIO = new IO(FileName, false))
                    {
                        FirmwareWrapper = FileIO.DecryptAndDeserialize<FirmwareWrapperFile>();
                    }
                }
            }
            catch(Exception ex)
            {
                Console.Beep();
            }

            if (FirmwareWrapper == null || FirmwareWrapper.DeviceFirmwareList == null)
                return null;
            else
            {
                return FirmwareWrapper;
            }
        }

        public string GetFirmwareInfo()
        {
            DeviceFirmwareInformation CurrentDeviceFirmware;
            string InfoList = string.Empty;
            for (int Index = 0; Index < DeviceFirmwareList.Count; Index++)
            {
                CurrentDeviceFirmware = DeviceFirmwareList[Index];
                InfoList += (CurrentDeviceFirmware.DeviceName + ": " + CurrentDeviceFirmware.FirmwareVersion.ToString("f2") + "\n");
            }

            return InfoList;
        }

        public string[] GetFirmwareInfos()
        {
            DeviceFirmwareInformation CurrentDeviceFirmware;
            List<string> InfoList = new List<string>();
            for (int Index = 0; Index < DeviceFirmwareList.Count; Index++)
            {
                CurrentDeviceFirmware = DeviceFirmwareList[Index];
                InfoList.Add(CurrentDeviceFirmware.DeviceName + ": " + CurrentDeviceFirmware.FirmwareVersion.ToString("f2") + "\n");
            }

            return InfoList.ToArray();
        }

        public string GetFirmwareInfo(params string[] DeviceNames)
        {
            DeviceFirmwareInformation CurrentDeviceFirmware;
            string InfoList = string.Empty;
            foreach(string DeviceName in DeviceNames)
            {
                CurrentDeviceFirmware = DeviceFirmwareList[DeviceName];
                InfoList += (CurrentDeviceFirmware.DeviceName + ": " + CurrentDeviceFirmware.FirmwareVersion.ToString("f2") + "\n");
            }

            return InfoList;
        }
        #endregion
        #region Serialization
        //Deserialization constructor.
        public FirmwareWrapperFile(SerializationInfo info, StreamingContext ctxt)
        {
            try
            {
                //Get the values from info and assign them to the appropriate properties
                this.FirmwareDate = (DateTime)info.GetValue("FirmwareDate", typeof(DateTime));
                this.Comments = (string)info.GetValue("Comments", typeof(string)); //MSGIGNORE
                DeviceFirmwareList.Clear();
                DFIS = (ArrayList)info.GetValue("DeviceFirmwareList", typeof(ArrayList));
                this.StackFileName = (string)info.GetValue("StackFileName", typeof(string));
                this.StackVersion = (float)info.GetValue("StackVersion", typeof(float));
                this.StackVersionBuild = (byte)info.GetValue("StackVersionBuild", typeof(byte));
                this.StackByteArray = (byte[])info.GetValue("StackByteArray", typeof(byte[]));
            }
            catch (System.Exception ex)
            {
            }
        }

        public void OnDeserialization(object sender)
        {
            foreach (DeviceFirmwareInformation DFI in DFIS)
                DeviceFirmwareList.Add(DFI);
        }

        //Serialization function.
        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("FirmwareDate", this.FirmwareDate, typeof(DateTime));
            info.AddValue("Comments", this.Comments, typeof(string));
            ArrayList arrayList = new ArrayList();
            foreach (DeviceFirmwareInformation DFI in DeviceFirmwareList)
                arrayList.Add(DFI);
            info.AddValue("DeviceFirmwareList", arrayList, typeof(ArrayList));
            info.AddValue("StackVersion", this.StackVersion, typeof(float));
            info.AddValue("StackVersionBuild", this.StackVersionBuild, typeof(byte));
            info.AddValue("StackFileName", this.StackFileName, typeof(string));
            info.AddValue("StackByteArray", this.StackByteArray, typeof(byte[]));
        }
        #endregion
    }
}
