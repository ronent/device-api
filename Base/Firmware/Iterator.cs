﻿using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Base.Firmware
{
    /// <summary>
    /// Firmware iterator class
    /// </summary>
    internal class FWIterator : IEnumerator<DataAddress>
    {
        private List<byte[]> data;
        private UInt16 blockSize;
        private UInt16 startAddress;
        private UInt16 currentAddress;
        private UInt16 addressStepSize;

        private int index { get { return getIndexFromAddress(currentAddress); } }
        private int maxIndex { get { return data.Count; } }

        /// <summary>
        /// Gets the progress percents.
        /// </summary>
        /// <value>
        /// The percent progress.
        /// </value>
        public int PercentProgress
        {
            get
            {
                return Convert.ToInt16((100f * index) / maxIndex);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FWIterator"/> class.
        /// </summary>
        /// <param name="data">The data.</param>
        /// <param name="blockSize">Size of the block.</param>
        /// <param name="startAddress">The start address.</param>
        public FWIterator(List<byte[]> data, UInt16 blockSize, UInt16 startAddress)
        {
            this.data = data;
            this.blockSize = blockSize;
            this.currentAddress = this.startAddress = startAddress;
            this.addressStepSize = blockSize;
        }

        /// <summary>
        /// Gets the index from address.
        /// </summary>
        /// <param name="address">The address.</param>
        /// <returns></returns>
        private int getIndexFromAddress(UInt16 address)
        {
            return (address - startAddress) / addressStepSize*2;
        }

        /// <summary>
        /// Gets the DataAddress at the current position of the enumerator.
        /// </summary>
        /// <returns>The element in the collection at the current position of the enumerator.</returns>
        public DataAddress Current
        {
            get { return new DataAddress { Data = getCurrentBlock(), Address = currentAddress }; }
        }

        /// <summary>
        /// Gets the current block.
        /// </summary>
        /// <returns></returns>
        private byte[] getCurrentBlock()
        {
            try
            {
                return data[index].Concat(data[index + 1]).ToArray();
            }
            catch { Log.Instance.Write("NULL BLOCK",this); return null; }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Gets the element in the collection at the current position of the enumerator.
        /// </summary>
        /// <returns>The element in the collection at the current position of the enumerator.</returns>
        object System.Collections.IEnumerator.Current
        {
            get { return Current; }
        }

        /// <summary>
        /// Advances the enumerator to the next element of the collection.
        /// </summary>
        /// <returns>
        /// true if the enumerator was successfully advanced to the next element; false if the enumerator has passed the end of the collection.
        /// </returns>
        public bool MoveNext()
        {
            currentAddress += addressStepSize;
            return true;
        }

        /// <summary>
        /// Sets the enumerator to its initial position, which is before the first element in the collection.
        /// </summary>
        public void Reset()
        {
            currentAddress = startAddress;
        }

        /// <summary>
        /// Gets a value indicating whether [has next].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has next]; otherwise, <c>false</c>.
        /// </value>
        public bool HasNext
        {
            get
            {
                if (currentAddress > 0xffa0)
                    Console.WriteLine("");
                int address = currentAddress + 2*addressStepSize;
                bool hasNext = address < UInt16.MaxValue &&
                    getIndexFromAddress(Convert.ToUInt16(address)) < data.Count;

                return hasNext;
            }
        }
    }
}
