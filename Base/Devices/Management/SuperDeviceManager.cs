﻿using Auxiliary.Tools;
using Base.Modules;
using Log4Tech;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.EventsAndExceptions;
using Base.Devices.Management.DeviceManager.HID;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules.HID;
using Infrastructure.Communication.Modules;
using Infrastructure;
using System;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace Base.Devices.Management
{
    public sealed partial class SuperDeviceManager
    {
        #region Fields
        private object moduleDeviceReportLock = new object();

        #endregion
        #region Properties
        private ConcurrentDictionary<IDeviceManagerMetadata, IDeviceManager> deviceModules = new ConcurrentDictionary<IDeviceManagerMetadata, IDeviceManager>();
        private ConcurrentDictionary<ICommunicationMetadata, ICommunicationManager> communicationModules = new ConcurrentDictionary<ICommunicationMetadata, ICommunicationManager>();

        private ReadOnlyCollection<ICommunicationManager> CommunicationModules
        {
            get { return new ReadOnlyCollection<ICommunicationManager>(communicationModules.Values.ToList()); }
        }

        private ReadOnlyCollection<IDeviceManager> DeviceModules
        {
            get { return new ReadOnlyCollection<IDeviceManager>(deviceModules.Values.ToList()); }
        }

        internal static readonly SuperDeviceManager instance = new SuperDeviceManager();

        #endregion
        #region Constructors
        static SuperDeviceManager()
        {
            try
            {
                ModuleManager.Instance.UpdateModules();
            }
            catch (Exception ex)
            {
                throw new Exception("Error loading modules.", ex);
            }
        }

        ~SuperDeviceManager()
        {
            Dispose();
        }

        internal SuperDeviceManager()
        {
       
        }

        #endregion
        #region Methods
        private void RegisterDeviceIdsFor(IHIDCommunicationManager module)
        {
            foreach (var deviceModule in DeviceModules)
                if (deviceModule is IHIDDeviceManager)
                    module.RegisterDeviceIds((deviceModule as IHIDDeviceManager).DeviceIDs);
        }

        private void RegisterDeviceIds()
        {
            foreach (var module in CommunicationModules)
                if(module is IHIDCommunicationManager)
                    RegisterDeviceIdsFor(module as IHIDCommunicationManager);
        }

        private void UnregisterDeviceIdsFor(IHIDCommunicationManager module)
        {
            module.UnregisterDeviceIds();
        }

        private void UnregisterDeviceIds()
        {
            foreach (var module in CommunicationModules)
                if(module is IHIDCommunicationManager)
                    UnregisterDeviceIdsFor(module as IHIDCommunicationManager);
        }

        private void onDeviceFound(object sender, ConnectionEventArgs e)
        {
            deviceFound(sender, e);
        }

        private void deviceFound(object sender, ConnectionEventArgs e)
        {
            //var foundModule = false;

            if (DeviceModules.Any(deviceModule => deviceModule.OnDeviceFound(sender, e)))
            {
               // foundModule = true;
                return;
            }

           // if (!foundModule)
           // {
                invokeOnNotSupportedDeviceConnected(sender, e);
           // }
        }

        private void onDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            deviceRemoved(sender, e);
        }

        private void deviceRemoved(object sender, ConnectionEventArgs e)
        {
            //bool foundModule = false;

            if (DeviceModules.Any(deviceModule => deviceModule.OnDeviceRemoved(sender, e)))
            {
                return;
            }

           // if (!foundModule)
           // {
                invokeOnNotSupportedDeviceRemoved(sender, e);
           // }
        }

        private void ScanForDevices()
        {
            Parallel.ForEach(CommunicationModules, item =>
            {
                try
                {
                    item.ScanForDevices();
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("On ScanForDevices()", this, ex);
                }
            });
        }

        private void RegisterCommunicationEvents(ICommunicationManager module)
        {
            module.OnDeviceConnected += instance.onDeviceFound;
            module.OnDeviceRemoved += instance.onDeviceRemoved;

            if (module is IHIDCommunicationManager)
                RegisterDeviceIdsFor(module as IHIDCommunicationManager);
        }

        private void RegisterCommunicationLocalHost(string localHost)
        {
            foreach (var module in CommunicationModules)
            {
                if (module is INeedLocalHost)
                    (module as INeedLocalHost).LocalHost = localHost;
            }
        }

        private void UnregisterCommunicationEvents(ICommunicationManager module)
        {
            module.OnDeviceConnected -= instance.onDeviceFound;
            module.OnDeviceRemoved -= instance.onDeviceRemoved;

            if (module is IHIDCommunicationManager)
                UnregisterDeviceIdsFor(module as IHIDCommunicationManager);
        }

        internal static void Add(ICommunicationManager communicationModule, ICommunicationMetadata metadata)
        {
            if (!instance.communicationModules.ContainsKey(metadata) && (checkCommunicationRestriction(metadata.Name) || !Settings.Settings.Current.UseRestrictCommunicationModules))
            {
                Log.Instance.Write("[API] ==> Communication module [" + metadata.Name + "] loaded successfully", string.Empty);

                instance.RegisterCommunicationEvents(communicationModule);
                instance.communicationModules.TryAdd(metadata, communicationModule);
            }
        }

        internal static ICommunicationManager Get(Type type)
        {
            return (from x in instance.CommunicationModules
                     where x.GetType().Equals(type)
                     select x).FirstOrDefault();
        }

        internal static void Remove(ICommunicationManager communicationModule, ICommunicationMetadata metadata)
        {
            if (!instance.communicationModules.ContainsKey(metadata) && (checkCommunicationRestriction(metadata.Name) || !Settings.Settings.Current.UseRestrictCommunicationModules))
            {
                instance.UnregisterCommunicationEvents(communicationModule);

                ICommunicationManager removed;
                instance.communicationModules.TryRemove(metadata, out removed);
            }
        }

        private static bool checkCommunicationRestriction(string name)
        {
            return !Settings.Settings.Current.RestrictCommunicationModules.Exists(item => name.ToLower().Contains(item.ToString().ToLower()));
        }

        internal static void Add(IDeviceManager deviceModule, IDeviceManagerMetadata metadata)
        {
            if (!instance.deviceModules.ContainsKey(metadata))
            {
                Log.Instance.Write("[API] ==> Device Manager module [" + metadata.Name + "] loaded successfully", string.Empty);

                deviceModule.OnReportDevice += instance.module_ReportDeviceEvent;
                instance.deviceModules.TryAdd(metadata, deviceModule);
            }
        }

        void module_ReportDeviceEvent(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            lock (moduleDeviceReportLock)
                switch (e.State)
                {
                    case eDeviceState.RECONNECTED:
                    case eDeviceState.FOUND:
                        if (e.Device.OpCodes.DeviceIO is IDeviceIOExtra)
                            e = new ExtraConnectionEventsArgs<GenericDevice>(e);

                        Utilities.InvokeEvent(OnDeviceConnected, sender, e);
                        break;
                    case eDeviceState.REMOVED:
                        Utilities.InvokeEvent(OnDeviceRemoved, sender, e);
                        break;
                    case eDeviceState.FAILED:
                        Utilities.InvokeEvent(OnDeviceFailedToConnect, sender, e);
                        break;
                }
        }

        #endregion
    }
}