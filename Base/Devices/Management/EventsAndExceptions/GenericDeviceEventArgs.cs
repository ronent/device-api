﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Runtime.Serialization;

namespace Base.Devices.Management.EventsAndExceptions
{
    public enum eDeviceState { FOUND, REMOVED, FAILED, NONE, RECONNECTED };

    [DataContract]
    [Serializable]
    public class ConnectionEventArgs<T> : EventArgs where T : GenericDevice
    {
        [DataMember]
        public T Device { get; set; }

        // public Type Type { get { return Device.GetType(); } }
        [DataMember]
        public eDeviceState State { get; set; }

//        public string SerialNumber 
//        {
//            get
//            {
//                return Device.Status.SerialNumber; 
//                
//            }
//        }
        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public int UserId { get; set; }


        internal ConnectionEventArgs()
        {

        }

        internal ConnectionEventArgs<GenericDevice> GetGenericCopy()
        {
            return new ConnectionEventArgs<GenericDevice>
            {
                Device = Device,
                Message = Message,
                State = State,
                UserId = UserId
            };
        }
    }

    public class ExtraConnectionEventsArgs<T> : ConnectionEventArgs<T> where T : GenericDevice
    {
        public IDeviceIO DeviceIOExtra { get { return Device.OpCodes.DeviceIO; } }

        internal ExtraConnectionEventsArgs(ConnectionEventArgs<GenericDevice> e)
        {
            this.Device = (T)e.Device;
            this.State = e.State;
            this.Message = e.Message;
        }
    }
}

