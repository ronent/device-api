﻿
namespace Base.Devices.Management.EventsAndExceptions
{
    internal static class STRUCTURE
    {
        public static readonly string DEVICE = "Device";
        public static readonly string SENSORS = "Sensors";
        public static readonly string OPCODES = "OpCodes";
        public static readonly string MIN_FW_VERSION = "MINIMUM_FW_VERSION";
        public static readonly string SUBTYPE = "SUBTYPE";
        public static readonly string FIRMWARE_DEVICE_NAME = "firmwareDeviceName";
    }
}
