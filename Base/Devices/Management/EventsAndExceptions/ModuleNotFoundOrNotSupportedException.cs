﻿using System;

namespace Base.Devices.Management.EventsAndExceptions
{
    [Serializable]
    class ModuleNotFoundOrNotSupportedException : Exception
    {
        internal ModuleNotFoundOrNotSupportedException()
        {

        }
    }
}
