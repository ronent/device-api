﻿using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Infrastructure.Communication;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Base.Devices.Management.DeviceManager
{
    public delegate bool ReportNewlyDeviceDelegate(object sender, ConnectionEventArgs e);
    public delegate void ReportGenericDeviceDelegate(object sender, ConnectionEventArgs<GenericDevice> e);
    public delegate void ReportDeviceDelegate<T>(object sender, ConnectionEventArgs<T> e) where T : GenericDevice;
    
    /// <summary>
    /// Once a communication manager class finds a "DeviceIO" it sends it to SuperDeviceManager,
    /// The SuperDeviceManager sends it to an IDeviceManager class, that operates on it if matches the DeviceID
    /// </summary>
    public interface IDeviceManager
    {
        event ReportGenericDeviceDelegate OnReportDevice;

        IEnumerable<GenericDevice> Devices { get; }

        bool OnDeviceFound(object sender, ConnectionEventArgs e);
        bool OnDeviceRemoved(object sender, ConnectionEventArgs e);

        bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod) where E : GenericDevice;

        void Add(GenericDevice device);
        void Remove(GenericDevice device);

        void ReportDevice(object sender, ConnectionEventArgs<GenericDevice> e);
    }
}
