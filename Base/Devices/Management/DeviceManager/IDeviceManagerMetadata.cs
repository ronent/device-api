﻿
namespace Base.Devices.Management.DeviceManager
{
    public interface IDeviceManagerMetadata
    {
        string Name { get; }
    }
}
