﻿using Auxiliary.Tools;
using Base.Devices.Management.EventsAndExceptions;
using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

namespace Base.Devices.Management.DeviceManager
{
    /// <summary>
    /// Generic Device Manager.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public abstract class GenericDeviceManager<T> : IDeviceManager, ISerializable where T : GenericDevice
    {
        #region Members
        protected ConcurrentDictionary<string, GenericDevice> devices;

        #endregion
        #region Events
        /// <summary>
        /// Reports new status of device.
        /// </summary>
        public event ReportGenericDeviceDelegate OnReportDevice;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDeviceManager{T}"/> class.
        /// </summary>
        internal GenericDeviceManager()
        {
            devices = new ConcurrentDictionary<string, GenericDevice>();
        }

        #endregion
        #region Methods
        /// <summary>
        /// Reports the device.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs{GenericDevice}"/> instance containing the event data.</param>
        public void ReportDevice(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            Log.Instance.Write("[API] ==> Device {0}: [ {1} ]", this, Log.LogSeverity.DEBUG, e.State.ToString().ToLower(), e.Device);
            Utilities.InvokeEvent(OnReportDevice, sender, e);
        }

        /// <summary>
        /// Adds the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        public void Add(GenericDevice device)
        {
            devices.AddOrUpdate(device.GetHashCode().ToString(), device, (key, oldDevice) => device);
        }

        /// <summary>
        /// Removes the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        public void Remove(GenericDevice device)
        {
            try
            {
                if (devices.TryRemove(device.GetHashCode().ToString(), out device))
                    device.Dispose();
            }
            catch (Exception ex)
            {
                if (ex.Message != "Safe handle has been closed")
                    Log.Instance.WriteError("Fail in Remove", this, ex);
            }

        }

        /// <summary>
        /// Turns the offline.
        /// </summary>
        /// <param name="device">The device.</param>
        protected void TurnOffline(GenericDevice device)
        {
            try
            {
                device.OpCodes.DeviceIO.Dispose();
            }
            catch { }
        }

        #endregion
        #region IDeviceManager Methods
        /// <summary>
        /// Called when device found.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        public abstract bool OnDeviceFound(object sender, ConnectionEventArgs e);

        /// <summary>
        /// Subscribes the specified report method.
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="reportMethod">The report method.</param>
        /// <returns></returns>
        public abstract bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod) where E : GenericDevice;

        /// <summary>
        /// Gets the device list.
        /// </summary>
        /// <value>
        /// The device list.
        /// </value>
        public IEnumerable<GenericDevice> Devices
        {
            get { return devices.Values; }
        }

        /// <summary>
        /// Called when device removed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        public bool OnDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            bool toReturn = false;

            try
            {
                var tempDevices = devices.Where(device => device.Value.Id == null || device.Value.Id == e.UID);

                if (tempDevices.Count() != 0)
                {
                    toReturn = true;

                    foreach (var device in tempDevices)
                    {
                        onDeviceRemove(device.Value);
                        ReportDevice(this, new ConnectionEventArgs<GenericDevice> { Device = device.Value, State = eDeviceState.REMOVED });
                    }
                }
            }

            catch (System.Exception ex)
            {
                Log.Instance.WriteError("Device [ID: '{0}'] failed on remove", this, ex, e.UID);
            }

            return toReturn;
        }

        /// <summary>
        /// What to do when device disconnected.
        /// </summary>
        /// <param name="device">The device.</param>
        protected virtual void onDeviceRemove(GenericDevice device)
        {
            TurnOffline(device);
            Remove(device);
        }

        #endregion
        #region Serialization & Deserialization
        protected GenericDeviceManager(SerializationInfo info, StreamingContext ctxt)
        {

        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {

        }       

        #endregion
    }
}