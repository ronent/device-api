﻿using Base.Functions.Management;
using Base.OpCodes;
using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Devices.Management.DeviceManager.HID
{
    public interface IHIDDeviceManager : IDeviceManager
    {
        List<DeviceID> DeviceIDs { get; }
    }
}
