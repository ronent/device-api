﻿using Auxiliary.Tools;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.EventsAndExceptions;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Base.Devices.Management
{
    /// <summary>
    /// Main entry point of Fourtec Device API
    /// </summary>
    public partial class SuperDeviceManager : IDisposable
    {
        #region fields
        private static object syncStart = new object();

        #endregion
        #region Events
        /// <summary>
        /// Occurs when any device connected.
        /// </summary>
        public static event ReportGenericDeviceDelegate OnDeviceConnected;

        /// <summary>
        /// Occurs when any device disconnected.
        /// </summary>
        public static event ReportGenericDeviceDelegate OnDeviceRemoved;

        /// <summary>
        /// Occurs when any device failed to connect.
        /// </summary>
        public static event ReportGenericDeviceDelegate OnDeviceFailedToConnect;

        /// <summary>
        /// Occurs when unsupported device by the API is connected.
        /// </summary>
        public static event ReportNewlyDeviceDelegate OnNotSupportedDeviceConnected;

        /// <summary>
        /// Occurs when unsupported device by the API is disconnected.
        /// </summary>
        public static event ReportNewlyDeviceDelegate OnNotSupportedDeviceRemoved;

        #endregion
        #region Properties
        /// <summary>
        /// Gets the count of online & offline devices.
        /// </summary>
        /// <value>
        /// The count.
        /// </value>
        internal static int DevicesCount
        {
            get 
            {
                throwIfNotStarted();

                return GetDevices().Count();
            }
        }

        /// <summary>
        /// Gets the online device count.
        /// </summary>
        /// <value>
        /// The online device count.
        /// </value>
        public static int OnlineDevicesCount
        {
            get
            {
                throwIfNotStarted();

                return GetDevices().Count((device) => device.IsOnline);
            }
        }

        /// <summary>
        /// Gets a value indicating whether the manager started.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [started]; otherwise, <c>false</c>.
        /// </value>
        public static bool Started { get; private set; }

        #endregion
        #region Methods
        /// <summary>
        /// Starts the Device API Manager.
        /// </summary>
        public static void Start(string localHost = null)
        {
            if (!Started)
            {
                lock (syncStart)
                {
                    if (localHost == null)
                        localHost = "localhost";

                    instance.RegisterCommunicationLocalHost(localHost);

                    instance.RegisterDeviceIds();

                    Started = true;

                    instance.ScanForDevices();
                }
            }
        }

        public static void ReScanForDevices()
        {
            instance.UnregisterDeviceIds();
            instance.RegisterDeviceIds();
            instance.ScanForDevices();
        }

        /// <summary>
        /// Shutdowns the Device API Manager.
        /// </summary>
        public static void Shutdown()
        {
            if (Started)
            {
                lock (syncStart)
                {
                    throwIfNotStarted();

                    instance.UnregisterDeviceIds();

                    instance.Dispose();
                    Started = false;
                }
            }
        }

        /// <summary>
        /// Scans for supported devices
        /// </summary>
        internal static void ScanForDevice()
        {
            instance.ScanForDevices();
        }

        /// <summary>
        /// Gets all devices.
        /// </summary>
        /// <returns>Enumeration of all currently active devices</returns>
        public static IEnumerable<GenericDevice> GetDevices()
        {
            throwIfNotStarted();

            foreach (var module in instance.DeviceModules)
            {
                foreach (GenericDevice device in module.Devices)
                    yield return device;
            }
        }

        /// <summary>
        /// Gets all devices of type T.
        /// </summary>
        /// <typeparam name="T">GenericDevice</typeparam>
        /// <returns>Enumeration of all currently active devices</returns>
        public static IEnumerable<T> GetDevicesOfType<T>()
        {
            throwIfNotStarted();

            foreach (var module in instance.DeviceModules)
            {
                var selected = module.Devices.OfType<T>();
                foreach (T device in selected)
                    yield return device;
            }
        }

        /// <summary>
        /// Gets the device.
        /// </summary>
        /// <param name="serialNumber">The serial number.</param>
        /// <returns></returns>
        public static GenericDevice GetDevice(string serialNumber)
        {
            throwIfNotStarted();

            return GetDevices().FirstOrDefault(x => x.Status.SerialNumber == serialNumber);
        }

        #endregion
        #region Subscribe Methods
        /// <summary>
        /// Subscribes the specified device event.
        /// </summary>
        /// <typeparam name="T">Device type</typeparam>
        /// <param name="reportMethod">The report method.</param>
        /// <exception cref="ModuleNotFoundOrNotSupportedException"></exception>
        public static void Subscribe<T>(ReportDeviceDelegate<T> reportMethod) where T : GenericDevice
        {
            foreach (var module in instance.DeviceModules)
                if (module.Subscribe<T>(reportMethod))
                    return;

            throw new ModuleNotFoundOrNotSupportedException();
        }

        /// <summary>
        /// Throws exception if not started.
        /// </summary>
        /// <exception cref="System.InvalidOperationException">SuperDeviceManager has not been started yet</exception>
        private static void throwIfNotStarted()
        {
            if (!Started)
                throw new InvalidOperationException("SuperDeviceManager has not been started yet");
        }

        internal static void invokeOnNotSupportedDeviceConnected(object sender, ConnectionEventArgs e)
        {
            Utilities.InvokeEvent(OnNotSupportedDeviceConnected, sender, e);
        }

        internal static void invokeOnNotSupportedDeviceRemoved(object sender, ConnectionEventArgs e)
        {
            Utilities.InvokeEvent(OnNotSupportedDeviceRemoved, sender, e);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            foreach (var item in instance.CommunicationModules)
            {
                item.Dispose();
            }
        }

        #endregion
    }
}