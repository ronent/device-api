﻿using System;

namespace Base.Devices.Features
{
    /// <summary>
    /// MicroX Device Functions
    /// </summary>
    public enum eDeviceFunction : byte
    {
        None,
        Setup,
        UpdateFirmware,
        CancelUpdateFirmware,
        Run,
        Stop,
        Download,
        CancelDownload,
        Calibrate,
        SaveDefaultCalibration,
        LoadDefaultCalibration,
        TurnOff,
        DebugMode,
        MarkTimeStamp,
        DeepSleep,
        LeaveNetwork,
        CallDevice,
    }

    /// <summary>
    /// Available Function.
    /// </summary>
    [Serializable]
    public class AvailableFunction
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableFunction"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        internal AvailableFunction(byte code)
        {
            this.Code = code;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AvailableFunction"/> class.
        /// </summary>
        /// <param name="function">The function.</param>
        internal AvailableFunction(eDeviceFunction function)
        {
            this.Code = (byte)function;
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public byte Code
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [visible].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [visible]; otherwise, <c>false</c>.
        /// </value>
        public bool Visible
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether [enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled
        {
            get;
            internal set;
        }

        /// <summary>
        /// Gets the function.
        /// </summary>
        /// <value>
        /// The function.
        /// </value>
        public eDeviceFunction Function
        {
            get
            {
                if (Enum.IsDefined(typeof(eDeviceFunction), Code))
                    return (eDeviceFunction)Code;
                else
                    return eDeviceFunction.None;
            }
        }

        #endregion
        #region Object Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("Function: {0}, Enabled: {1}", Function, Enabled);
        }

        #endregion
    }
}
