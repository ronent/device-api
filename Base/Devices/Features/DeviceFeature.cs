﻿
namespace Base.Devices.Features
{
    /// <summary>
    /// Device Feature.
    /// </summary>
    public abstract class DeviceFeature
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceFeature"/> class.
        /// </summary>
        internal DeviceFeature()
        {

        }

        #endregion        
        #region Properties
        public abstract byte Code { get; }
        public abstract string Text { get; }

        #endregion
    }
}
