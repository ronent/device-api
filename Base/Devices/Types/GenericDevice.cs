﻿using Auxiliary.Tools;
using Base.DataStructures;
using Base.DataStructures.Device;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.EventsAndExceptions;
using Base.Functions.Management;
using Base.OpCodes;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

namespace Base.Devices
{
    /// <summary>
    /// Device operation delegate.
    /// </summary>
    /// <typeparam name="T">Generic device</typeparam>
    /// <param name="sender"></param>
    /// <param name="e">ConnectionEventArgs</param>
    public delegate void DeviceOperationDelegate<T>(object sender, ConnectionEventArgs<T> e) where T : GenericDevice;

    [Serializable]
    public abstract class GenericDevice : IDisposable, ISerializable
    {
        #region Events
        /// <summary>
        /// Occurs when device turns offline.
        /// </summary>
        internal event DeviceOperationDelegate<GenericDevice> OnOffline;

        /// <summary>
        /// Occurs when device turns online from offline.
        /// </summary>
        internal event DeviceOperationDelegate<GenericDevice> OnReconnected;

        #endregion
        #region Fields
        protected static readonly Version DEFAULT_MAXIMUM_FW_VERSION = new Version(999, 999, 999, 999);

        [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
        private string id;
        [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
        private bool online = true;
        [DebuggerBrowsableAttribute(DebuggerBrowsableState.Never)]
        private int? guid;

        #endregion
        #region Public Properties
        /// <summary>
        /// Gets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        public DeviceFunctionsManager Functions { get; internal set; }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        internal OpCodeManager OpCodes { get; set; }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        public BaseDeviceStatusConfiguration Status { get; internal set; }

        /// <summary>
        /// Gets the battery.
        /// </summary>
        /// <value>
        /// The battery.
        /// </value>
        public GenericBattery Battery { get; internal set; }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        public GenericVersion Version { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether this device is online.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is online; otherwise, <c>false</c>.
        /// </value>
        public bool IsOnline
        {
            get { return online; }
            internal set
            {
                if (online != value)
                {
                    online = value;

                    if (online)
                        InvokeOnReconnected();
                    else
                        InvokeOnOffline();
                }
            }
        }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public abstract string DeviceTypeName { get; }

        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public abstract Version MinimumRequiredFirmwareVersion { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        #endregion
        #region Internal Properties
        /// <summary>
        /// Gets or sets the parent device manager.
        /// </summary>
        /// <value>
        /// The parent device manager.
        /// </value>
        internal IDeviceManager ParentDeviceManager { get; set; }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal abstract string FirmwareDeviceName { get; }

        /// <summary>
        /// Gets a value indicating whether the device supports firmware update.
        /// </summary>
        /// <value>
        /// <c>true</c> if [supports firmware update]; otherwise, <c>false</c>.
        /// </value>
        internal virtual bool SupportsFirmwareUpdate
        {
            get { return true; }
        }
  
        /// <summary>
        /// Gets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        internal string Id
        {
            get
            {
                if (id == null || (OpCodes != null) && id != OpCodes.DeviceIO.Id)
                    id = OpCodes.DeviceIO.Id;

                return id;
            }
        }

        /// <summary>
        /// Gets the class version of the assembly.
        /// </summary>
        /// <value>
        /// The class version.
        /// </value>
        internal string ClassVersion
        {
            get
            {
                return Regex.Match(this.GetType().FullName, @"\.V[0-9]+\.").Groups[0].Value.Replace(".", "");
            }
        }

        #endregion
        #region Constructors & Initializers
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDevice"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal GenericDevice(IDeviceManager parent)
        {
            this.ParentDeviceManager = parent;

            InitializeStatus();
            InitializeFunctionManager();
            InitializeOpCodesManager();

            Initialize();
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal abstract void InitializeStatus();

        /// <summary>
        /// Initializes the OpCodes.
        /// </summary>
        internal abstract void InitializeOpCodesManager();

        /// <summary>
        /// Initializes the functions.
        /// </summary>
        internal abstract void InitializeFunctionManager();

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal virtual void Initialize()
        {
            OpCodes.Initialize();
        }

        /// <summary>
        /// Initializes the interfacer.
        /// </summary>
        /// <param name="id">The identifier. <see cref="Id"/></param>
        /// <param name="deviceIO">The device io.</param>
        internal virtual void InitializeInterfacer(IDeviceIO deviceIO)
        {
            OpCodes.SetDeviceInterfacer(deviceIO);
            Functions.Initialize();
        }

        #endregion
        #region Methods
        /// <summary>
        /// Replaces the op code manager.
        /// Implementing classes must carry out the substitution.
        /// </summary>
        /// <param name="newOpCodes">The new op codes.</param>
        internal virtual void ReplaceOpCodeManager(OpCodeManager newOpCodes)
        {
            OpCodes.Dispose();
            newOpCodes.Initialize();
        }

        /// <summary>
        /// Invokes event when device turns offline.
        /// </summary>
        protected void InvokeOnOffline()
        {
            OnDeviceOffline();

            var e = new ConnectionEventArgs<GenericDevice> { Device = this, State = eDeviceState.REMOVED };
            Utilities.InvokeEvent(OnOffline, this, e);
        }

        /// <summary>
        /// Called when device turns offline.
        /// </summary>
        protected virtual void OnDeviceOffline() {}

        /// <summary>
        /// Invokes event when device reconnect
        /// </summary>
        protected void InvokeOnReconnected()
        {
            OnDeviceReconnected();

            var e = new ConnectionEventArgs<GenericDevice> { Device = this, State = eDeviceState.RECONNECTED };
            Utilities.InvokeEvent(OnReconnected, this, e);
        }

        /// <summary>
        /// Called when device reconnected.
        /// </summary>
        protected virtual void OnDeviceReconnected() { }

        /// <summary>
        /// Determines whether the device is of the same type as the other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True if the devices type are equals</returns>
        public virtual bool IsOfSameType(GenericDevice other)
        {
            return this.DeviceTypeName == other.DeviceTypeName
                    && this.Version.Equals(other.Version);
        }

        #endregion
        #region Static Methods
        internal static T DeepCopy<T>(T other)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(ms, other);
                    ms.Seek(0, SeekOrigin.Begin);
                    return (T)formatter.Deserialize(ms);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On DeepCopy<T>()", string.Empty, ex);
            }

            return default(T);
        }

        #endregion
        #region Object Methods
        /// <summary>
        /// Determines whether the device, is equal to this device instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            try
            {
                if (obj is GenericDevice)
                {
                    return (obj as GenericDevice).Status.SerialNumber == this.Status.SerialNumber
                        && (obj as GenericDevice).DeviceTypeName == this.DeviceTypeName;
                }
                else
                    return base.Equals(obj);
            }
            catch { return false; }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            if (guid == null)
                guid = Guid.NewGuid().GetHashCode();

            return (int)guid;
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            try
            {
                if (Status != null && Status.SerialNumber != null)
                    return string.Format("{0}: #{1}", DeviceTypeName, Status.SerialNumber);
                else
                    return DeviceTypeName + ": " + OpCodes.DeviceName;
            }
            catch { return null; }
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDevice"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected GenericDevice(SerializationInfo info, StreamingContext ctxt)
        {
            Status = (BaseDeviceStatusConfiguration)info.GetValue("Status", typeof(BaseDeviceStatusConfiguration));
            Version = (GenericVersion)info.GetValue("Version", typeof(GenericVersion));
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("Status", Status, typeof(BaseDeviceStatusConfiguration));
            info.AddValue("Version", Version, typeof(GenericVersion));
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            try
            {
                if (!IsDisposed)
                {
                    //Log.Instance.Write("[API] ==> Device: '{0}' is disposing", this, Log.LogSeverity.DEBUG, this);
                    IsDisposed = true;

                    Functions.Dispose();

                    if (OpCodes != null)
                        OpCodes.Dispose();

                    OnOffline = null;

                    ParentDeviceManager.Remove(this);
                    OpCodes = null;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("[API] ==> Device: '{0}' Error on disposing", this, ex, this);
            }
        }
        #endregion
    }
}