﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using Base.DataStructures.Logger;
using Base.Devices.Management.DeviceManager;
using Base.Functions.Management;
using Base.Sensors.Management;

namespace Base.Devices.Types
{
    [Serializable]
    public abstract class GenericLoggerV2 : GenericDevice, ISerializable
    {
           #region Constructors & Initializers
        internal GenericLoggerV2(IDeviceManager parent)
            : base(parent)
        {

        }

        internal override void Initialize()
        {
            base.Initialize();

            InitializeSensorManager();
            Sensors.Initialize(this);
        }

        internal abstract void InitializeSensorManager();

        #endregion
        #region Properties
        public SensorManagerV2 Sensors { get; internal set; }

        new public BaseLoggerStatusConfiguration Status
        {
            get { return base.Status as BaseLoggerStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public LoggerFunctionsManagerV2 Functions
        {
            get { return base.Functions as LoggerFunctionsManagerV2; }
            set { base.Functions = value; }
        }

        public GenericMemorySize Memory { get; internal set; }

        public abstract eSensorType[] AvailableFixedSensors { get; }

        public abstract eSensorType[] AvailableDetachableSensors { get; }

        #endregion
        #region Serialization & Deserialization
        protected GenericLoggerV2(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            Sensors = (SensorManagerV2)info.GetValue("Sensors", typeof(SensorManagerV2));
            Memory = (GenericMemorySize)info.GetValue("Memory", typeof(GenericMemorySize));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);

            info.AddValue("Sensors", Sensors, typeof(SensorManagerV2));
            info.AddValue("Memory", Memory, typeof(GenericMemorySize));
        }

        #endregion
    }
}
