﻿using Base.DataStructures;
using Base.DataStructures.Logger;
using Base.Devices.Management.DeviceManager;
using Base.Functions.Management;
using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Devices
{
    [Serializable]    
    public abstract class GenericLogger : GenericDevice, ISerializable
    {
        #region Constructors & Initializers
        internal GenericLogger(IDeviceManager parent)
            : base(parent)
        {

        }

        internal override void Initialize()
        {
            base.Initialize();

            InitializeSensorManager();
            Sensors.Initialize(this);
        }

        internal abstract void InitializeSensorManager();

        #endregion
        #region Properties
        public SensorManager Sensors { get; internal set; }

        new public BaseLoggerStatusConfiguration Status
        {
            get { return base.Status as BaseLoggerStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public LoggerFunctionsManager Functions
        {
            get { return base.Functions as LoggerFunctionsManager; }
            set { base.Functions = value; }
        }

        public GenericMemorySize Memory { get; internal set; }

        public abstract eSensorType[] AvailableFixedSensors { get; }

        public abstract eSensorType[] AvailableDetachableSensors { get; }

        #endregion
        #region Serialization & Deserialization
        protected GenericLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            Sensors = (SensorManager)info.GetValue("Sensors", typeof(SensorManager));
            Memory = (GenericMemorySize)info.GetValue("Memory", typeof(GenericMemorySize));
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);

            info.AddValue("Sensors", Sensors, typeof(SensorManager));
            info.AddValue("Memory", Memory, typeof(GenericMemorySize));
        }

        #endregion
    }
}