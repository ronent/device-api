﻿
namespace Base.Modules.Interfacer
{
    public interface IDeviceInterfacerMetadata
    {
        string Name { get; }
    }
}
