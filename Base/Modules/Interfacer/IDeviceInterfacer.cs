﻿using Base.Devices;
using Base.Devices.Management;
using System;
using System.Collections.Generic;
using Base.Devices.Management.DeviceManager;
using Infrastructure.Communication;

namespace Base.Modules.Interfacer
{
    /// <summary>
    /// Semi device manager that wraps logger type
    /// </summary>
    internal interface IDeviceInterfacer
    {
        /// <summary>
        /// Can Create from DeviceID
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        bool CanCreate(ConnectionEventArgs e);

        /// <summary>
        /// Can create from an existing device
        ///     1. Status has been received and device type can now be resolved
        ///     2. Determine the correct class version for the device
        /// </summary>
        /// <param name="genericDevice"></param>
        /// <returns></returns>
        bool CanCreateFrom(GenericDevice genericDevice);

        /// <summary>
        /// After CanCreate == true, create device and report it
        /// </summary>
        /// <param name="e"></param>
        void CreateFromIdAndReport(ConnectionEventArgs e);

        void CreateFromDeviceAndReport(GenericDevice genericDevice, int userId);

        bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod) where E : GenericDevice;

        Type GetDeviceClassType();
    }
}
