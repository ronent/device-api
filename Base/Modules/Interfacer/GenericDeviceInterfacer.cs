﻿using Auxiliary.Tools;
using Base.Devices;
using Base.Devices.Management;
using Base.OpCodes;
using Log4Tech;
using System;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Threading;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.EventsAndExceptions;
using Base.Devices.Management.DeviceManager.HID;
using System.Collections.Concurrent;
using Infrastructure.Communication;

namespace Base.Modules.Interfacer
{
    internal abstract class GenericDeviceInterfacer<T> : IDeviceInterfacer where T : GenericDevice
    {
        #region Events
        public event ReportDeviceDelegate<T> OnDeviceConnected;
        public event ReportDeviceDelegate<T> OnDeviceRemoved;

        #endregion
        #region Fields
        protected abstract byte Type { get; }

        #endregion
        #region Constructors

        #endregion
        #region Properties
        protected abstract IDeviceManager ParentDeviceManager { get; }

        #endregion
        #region Methods
        /// <summary>
        /// Creates the new device.
        /// </summary>
        /// <returns></returns>
        protected abstract GenericDevice CreateNewDevice();
      
        protected void InitializeNewDevice(GenericDevice device, ConnectionEventArgs e)
        {
            try
            {
                device.InitializeInterfacer(e.DeviceIO);
                var result = GetDeviceDetails(device);

                if (result.IsOK)
                {
                    Log.Instance.Write(
                        "[API] ==> Sorting out new device: [ " + device + " Id: " + device.OpCodes.DeviceIO.ShortId +
                        " ]", this);
                    if (!TryAttachToExistingDevice(device))
                    {
                        Log.Instance.Write("[API] ==> Creating new leaf device from: [ " + device + " ]", this);
                        CreateFromDeviceAndReport(device,e.UserId);
                        Log.Instance.Write("[API] ==> After calling CreateFromDeviceAndReport : [ " + device + " ]", this);
                    }
                }
                else
                {
                    if (!device.IsDisposed)
                    {
                        Log.Instance.Write("[API] ==> Fail on getting status from new device: [ " + device + " ]",
                            this);

                        device.OpCodes.DeviceIO.Dispose();
                        device.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                if (device.IsDisposed)
                    ReportDevice(device, eDeviceState.REMOVED);
                else
                    ReportDevice(device, eDeviceState.FAILED, ex.Message);
            }

        }

        protected Result GetDeviceDetails(GenericDevice device)
        {
            return device.Functions.GetDeviceType().Result;
        }

        protected Result GetDeviceStatus(GenericDevice device)
        {
            return device.Functions.GetStatusNoBusy().Result;
        }

        public bool TryAttachToExistingDevice(GenericDevice device)
        {
            T existingDevice = checkForExistingDevice(device);

            if (existingDevice != null)
            {
                bool notify = existingDevice.IsOnline;

                hookUpDevice(existingDevice, device);

                if (!notify)
                    ReportDevice(existingDevice, eDeviceState.RECONNECTED);

                return true;
            }

            return false;
        }

        private T checkForExistingDevice(GenericDevice device)
        {
            try
            {
                GenericDevice existingDevice = ParentDeviceManager.Devices.FirstOrDefault(x => device.Status.SerialNumber == x.Status.SerialNumber);

                if (existingDevice != null)
                    return (T)existingDevice;
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw new Exception("A device by that serial number already exists");
            }
        }

        protected void ReportDevice(GenericDevice device, eDeviceState state, string message = null, int userId = 0)
        {
            if (!(device is T))
                return;

            var e = new ConnectionEventArgs<T>
            {
                Device = (T) device,
                State = state,
                Message = message,
                UserId = userId
            };

            switch (state)
            {
                case eDeviceState.RECONNECTED:
                    if (!ParentDeviceManager.Devices.Any(item => item.Equals(e.Device)))
                    {
                        ParentDeviceManager.Add(device);
                        Utilities.InvokeEvent(OnDeviceConnected, this, e, null);
                    }

                    break;
                case eDeviceState.FOUND:
                    ParentDeviceManager.Add(device);
                    Utilities.InvokeEvent(OnDeviceConnected, this, e, null);
                    break;
                case eDeviceState.FAILED:
                    break;
                case eDeviceState.REMOVED:
                    Utilities.InvokeEvent(OnDeviceRemoved, this, e, null);
                    break;
            }

            ParentDeviceManager.ReportDevice(this, e.GetGenericCopy());
        }

        protected Type LookUpCorrectFirmwareVersion(Type[] types, Version version)
        {
            try
            {
                return types
                    .Where((t) => version >= getMinFirmwareVersion(t))
                    .OrderByDescending(getMinFirmwareVersion)
                    .First();
            }
            catch 
            {
                throw new Exception("Support for this firmware version ("
                    + version
                    + ") of type "
                    + GetDeviceClassType().Name
                    + " has been discontinued. Please upgrade firmware.");
            }
        }

        protected virtual Type[] LookUpCandidateTypes(GenericDevice device)
        {
            var asm = GetType().Assembly;
            try
            {
                return asm.GetTypes()
                    .Where((t) => isOfType(t) && !t.IsAbstract)
                    .ToArray();
            }
            catch { return null; }
        }

        protected bool isOfType(Type type)
        {
            return type.IsSubclassOf(typeof(T));
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Hooks up old device with new device.
        /// </summary>
        /// <param name="targetDevice">The new device.</param>
        /// <param name="genericDevice">The old device.</param>
        private void hookUpDevice(GenericDevice targetDevice, GenericDevice genericDevice)
        {
            if (!targetDevice.IsOnline)
                targetDevice.Initialize();

            targetDevice.InitializeInterfacer(genericDevice.OpCodes.DeviceIO);
            genericDevice.OpCodes.Dispose();
            genericDevice.Dispose();

            Result result = GetDeviceStatus(targetDevice);
            if (!result.IsOK)
                throw result.LastException;
        }

        /// <summary>
        /// Gets the minimum firmware version.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private Version getMinFirmwareVersion(Type type)
        {
            var fieldInfo = type.GetField(STRUCTURE.MIN_FW_VERSION, BindingFlags.Static | BindingFlags.NonPublic);
            if (fieldInfo != null)
                return fieldInfo.GetValue(null) as Version;
            return new Version(0,1);
        }

        #endregion
        #region IDeviceInterfacer
        public virtual void CreateFromDeviceAndReport(GenericDevice genericDevice,int userId)
        {
            Log.Instance.Write("[API] ==> Checks if can create: [ " + genericDevice + " ]", this);
            if (!CanCreateFrom(genericDevice))
                throw new ArgumentException("Device cannot be created using this interfacer", "device");

            Log.Instance.Write("[API] ==> Looks for candidate type: [ " + genericDevice + " ]", this);
            var types = LookUpCandidateTypes(genericDevice);

            Log.Instance.Write("[API] ==> Checks correct firmware version: [ " + genericDevice + " ]", this);
            var type = LookUpCorrectFirmwareVersion(types, genericDevice.Version.Firmware);

            Log.Instance.Write("[API] ==> Checks if device is generic: [ " + genericDevice + " ]", this);
            // checks if this device is generic 
            if (type != genericDevice.GetType())
            {
                Log.Instance.Write("[API] ==> Create instance of the device: [ " + genericDevice + " ]", this);

                var device =
                    (T)
                        Activator.CreateInstance(type, BindingFlags.NonPublic | BindingFlags.Instance, null,
                            new object[] {ParentDeviceManager}, null);

                Log.Instance.Write("[API] ==> Hookup the device: [ " + genericDevice + " ]", this);
                hookUpDevice(device, genericDevice);

                genericDevice = device;
            }
            else
            {
                Log.Instance.Write("[API] ==> Get the device status: [ " + genericDevice + " ]", this);
                GetDeviceStatus(genericDevice);
            }

            Log.Instance.Write("[API] ==> Report the device: [ " + genericDevice + " ]", this);
            ReportDevice(genericDevice, eDeviceState.FOUND, userId: userId);

            Log.Instance.Write("[API] ==> Done create device and report...: [ " + genericDevice + " ]", this);
        }

        /// <summary>
        /// Can Create from DeviceID
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public virtual bool CanCreate(ConnectionEventArgs e)
        {
            return false;
        }

        /// <summary>
        /// After CanCreate == true, creates the device, initialize it, and report at the end.
        /// </summary>
        /// <param name="e"></param>
        public void CreateFromIdAndReport(ConnectionEventArgs e)
        {
            try
            {
                if (CanCreate(e))
                {
                    GenericDevice device = CreateNewDevice();
                    InitializeNewDevice(device, e);
                }
            }
            catch(Exception ex)
            {
                Log.Instance.WriteError("Fail on CreateFromIdAndReport()", this, ex);
            }
        }

        /// <summary>
        /// Determines whether this instance [can create from] the specified device.
        /// </summary>
        /// <param name="device">The device.</param>
        /// <returns></returns>
        public virtual bool CanCreateFrom(GenericDevice device)
        {
            return false;
        }

        /// <summary>
        /// Subscribes the specified report method.
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="reportMethod">The report method.</param>
        /// <returns></returns>
        public bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod) where E : GenericDevice
        {
            var handler = reportMethod as ReportDeviceDelegate<T>;
            if (handler != null)
            {
                OnDeviceConnected += handler;
                OnDeviceRemoved += handler;

                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the type of the device class.
        /// </summary>
        /// <returns></returns>
        public Type GetDeviceClassType()
        {
            return typeof(T);
        }

        #endregion
    }
}