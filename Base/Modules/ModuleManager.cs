﻿using Base.Devices.Management;
using Log4Tech;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.Linq;
using Base.Devices.Management.DeviceManager;
using Infrastructure.Communication.Modules;

namespace Base.Modules
{
    internal class ModuleManager : IDisposable
    {
        #region Static
        private static ModuleManager instance = new ModuleManager();
        public static ModuleManager Instance { get { return instance; } }
        public static DirectoryCatalog BinCatalog
        {
            get
            {
                try 
                {
                    return new DirectoryCatalog(@".", "*.dll"); 
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion
        #region Fields/Properties
        private CompositionContainer _container;

        #pragma warning disable
        [ImportMany]
        IEnumerable<Lazy<IDeviceManager, IDeviceManagerMetadata>> deviceManagers;

        [ImportMany]
        List<Lazy<ICommunicationManager, ICommunicationMetadata>> communicationManagers;
        #pragma warning enable


        #endregion
        #region Constructors
        private ModuleManager()
        {
            loadModuleDLLs();
        }

        private void loadModuleDLLs()
        {
            var catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(this.GetType().Assembly));
            catalog.Catalogs.Add(BinCatalog);

            _container = new CompositionContainer(catalog);

            try
            {
                this._container.ComposeParts(this);
            }
            catch (ReflectionTypeLoadException rtle)
            {
                Log.Instance.WriteError("[API] ==> On loadModuleDLLs", this, rtle);

                rtle.LoaderExceptions.ToList().ForEach(ex =>
                    {
                        Log.Instance.WriteError("[API] ==> Loader Exception: ", this, ex);
                    });
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On loadModuleDLLs",this, ex);
            }
        }

        public void UpdateModules()
        {
            if (communicationManagers != null)
            {
                removeBaseClasses();

                foreach (var module in communicationManagers)
                {
                    SuperDeviceManager.Add(module.Value, module.Metadata);
                }
            }

            if (deviceManagers != null)
            {
                foreach (var module in deviceManagers)
                {
                    SuperDeviceManager.Add(module.Value, module.Metadata);
                }
            }
        }

        private void removeBaseClasses()
        {
            List<ICommunicationManager> removed = new List<ICommunicationManager>();       
            var enu = communicationManagers.GetEnumerator();

            while (enu.MoveNext())
            {
                Assembly assembly = Assembly.GetAssembly(enu.Current.Value.GetType());
                var communicationClass = from t in assembly.GetTypes()
                                         where (t.GetInterfaces().Contains(typeof(ICommunicationManager))
                                                  && t.GetConstructor(Type.EmptyTypes) != null)
                                         select t;

                if (communicationClass.ToList().Count > 0)
                {
                    string baseName = communicationClass.ToList()[0].BaseType.FullName;

                    if (!baseName.Equals("System.Object"))
                    {
                        var toRemove = checkBaseName(baseName);
                        if (toRemove != null)
                            removed.Add(toRemove);
                    }
                }
            }

            communicationManagers.RemoveAll(x => removed.Contains(x.Value));
        }

        private ICommunicationManager checkBaseName(string baseName)
        {
            var enu = communicationManagers.GetEnumerator();
            while (enu.MoveNext())
            {
                Assembly assembly = Assembly.GetAssembly(enu.Current.Value.GetType());
                if (assembly.DefinedTypes.First(item => item.FullName.Equals(baseName)) != null)
                {
                    return enu.Current.Value;
                }
            }

            return null;
        }

        #endregion

        public void Dispose()
        {
            _container.Dispose();
        }
    }
}
