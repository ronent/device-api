﻿using System;
using System.Collections.Generic;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Logger References.
    /// </summary>
    [Serializable]
    public struct LoggerReference : IEnumerable<decimal>
    {
        /// <summary>
        /// The logger
        /// </summary>
        public decimal Logger;
        /// <summary>
        /// The reference
        /// </summary>
        public decimal Reference;

        /// <summary>
        /// Loggers the reference.
        /// </summary>
        /// <param name="pair">The pair.</param>
        /// <returns></returns>
        public static implicit operator LoggerReference(Tuple<decimal,decimal> pair)
        {
            return new LoggerReference { Logger = pair.Item1, Reference = pair.Item2 };
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.IEnumerator`1" /> that can be used to iterate through the collection.
        /// </returns>
        public IEnumerator<decimal> GetEnumerator()
        {
            yield return Logger;
            yield return Reference;
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through the collection.
        /// </returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        int index;
        /// <summary>
        /// Adds the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        private void add(decimal item)
        {
            switch (index)
            {
                case 0:
                    Logger = item;
                    index++;
                    break;
                case 1:
                    Reference = item;
                    index++;
                    break;
                default:
                    break;
            }
        }
    }
}
