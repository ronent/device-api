﻿using Auxiliary.Tools;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Calibration Information.
    /// </summary>
    [Serializable]
    public abstract class CalibrationInformation
    {
        #region Fields
        /// <summary>
        /// Gets the default coefficients A.
        /// </summary>
        /// <value>
        /// The default coefficients a.
        /// </value>
        public abstract decimal DefaultCoeffA { get; }

        /// <summary>
        /// Gets the default coefficients B.
        /// </summary>
        /// <value>
        /// The default coefficients b.
        /// </value>
        public abstract decimal DefaultCoeffB { get; }

        /// <summary>
        /// Gets the number of calibration points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public abstract int Points { get; }
        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets A.
        /// </summary>
        /// <value>
        /// A.
        /// </value>
        public Decimal A
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the B.
        /// </summary>
        /// <value>
        /// The b.
        /// </value>
        public Decimal B
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the A.
        /// </summary>
        /// <value>
        /// The af.
        /// </value>
        public Single Af
        {
            get { return Convert.ToSingle(A); }
            set { A = tryToConvert(value); }
        }

        /// <summary>
        /// Gets or sets the B.
        /// </summary>
        /// <value>
        /// The bf.
        /// </value>
        public Single Bf
        {
            get { return Convert.ToSingle(B); }
            set { B = tryToConvert(value); }
        }

        /// <summary>
        /// Tries to convert.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private decimal tryToConvert(float value)
        {
            try 
            { 
                return (decimal)value; 
            }
            catch 
            { 
                return 0; 
            }
        }

        /// <summary>
        /// Gets or sets the gain.
        /// </summary>
        /// <value>
        /// The gain.
        /// </value>
        public decimal Gain
        {
            get { return A; }
            set { A = value; }
        }

        /// <summary>
        /// Gets or sets the offset.
        /// </summary>
        /// <value>
        /// The offset.
        /// </value>
        public decimal Offset
        {
            get { return B; }
            set { B = value; }
        }

        /// <summary>
        /// Gets a value indicating whether values are valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        public abstract bool Valid
        {
            get;
        }

        /// <summary>
        /// Gets the references.
        /// </summary>
        /// <value>
        /// The references.
        /// </value>
        public LoggerReference[] References
        {
            get;
            internal set;
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="CalibrationInformation"/> class.
        /// </summary>
        internal CalibrationInformation()
        {
            InitializeValues();
        }

        /// <summary>
        /// Initializes the values.
        /// </summary>
        internal virtual void InitializeValues()
        {
            A = B = Gain = 0;
            Offset = 0;
        }
        #endregion
        #region Abstract Methods
        /// <summary>
        /// Calibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal abstract decimal Calibrate(decimal value);

        /// <summary>
        /// Decalibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal abstract decimal Decalibrate(decimal value);

        /// <summary>
        /// Sets the specified pairs.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        internal abstract void Set(LoggerReference[] pairs);

        /// <summary>
        /// Resets this instance.
        /// </summary>
        internal abstract void Reset();

        /// <summary>
        /// Sets the specified coefficients.
        /// </summary>
        /// <param name="coeff">The coefficients.</param>
        internal abstract void Set(CalibrationCoefficients coeff);

        /// <summary>
        /// Generates the coefficients.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        /// <returns></returns>
        internal abstract CalibrationCoefficients GenerateCoefficients(LoggerReference[] pairs);
        #endregion
        #region Methods
        /// <summary>
        /// Gets the default coefficients.
        /// </summary>
        /// <returns></returns>
        public virtual CalibrationCoefficients GetDefaultCoefficients()
        {
            return new CalibrationCoefficients
            {
                A = DefaultCoeffA,
                B = DefaultCoeffB,
            };
        }

        /// <summary>
        /// Gets the coefficients.
        /// </summary>
        /// <returns></returns>
        public virtual CalibrationCoefficients GetCoefficients()
        {
            return new CalibrationCoefficients
            {
                A = this.A,
                B = this.B,
            }; 
        }

        /// <summary>
        /// Generates the random coefficients.
        /// </summary>
        /// <returns></returns>
        internal CalibrationCoefficients GenerateRandomCoefficients()
        {
            return new CalibrationCoefficients
            {
                A = Transformation.ModifyBySmallDegree(DefaultCoeffA),
                B = Transformation.ModifyBySmallDegree(DefaultCoeffB),
            };
        }
        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="CalibrationInformation"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected CalibrationInformation(SerializationInfo info, StreamingContext ctxt)
        {
            A = (Decimal)IO.GetSerializationValue<Decimal>(ref info, "A");
            B = (Decimal)IO.GetSerializationValue<Decimal>(ref info, "B");
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("A", A);
            info.AddValue("B", B);
        }

        #endregion
    }
}
