﻿using Auxiliary.MathLib;
using Base.Sensors.Types;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Generic NTC Calibration.
    /// </summary>
    [Serializable]
    public abstract class GenericNTC : ThreePoint, ISerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericNTC"/> class.
        /// </summary>
        internal GenericNTC()
            :base()
        {

        }

        #region Properties
        /// <summary>
        /// Gets a value indicating whether values are valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        public override bool Valid
        {
            get { return A != 0; }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Sets the specified pairs.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        internal override void Set(LoggerReference[] pairs)
        {
            var coefficients = GenerateCoefficients(pairs);

            A = coefficients.A;
            B = coefficients.B;
            C = coefficients.C;
        }

        /// <summary>
        /// Generates the coefficients.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        /// <returns></returns>
        internal override CalibrationCoefficients GenerateCoefficients(LoggerReference[] pairs)
        {
            var loggerValues = (from pair in pairs
                                select pair.Logger).ToArray();

            var referenceValues = (from pair in pairs
                                select pair.Reference).ToArray();

            decimal[] Rs = ExtractRValues(loggerValues);
            double[] invertedKelvin = Array.ConvertAll(Celsius2InvertedKelvin(referenceValues), x => (double)x);
            return RecalculateCoefficients(Rs, invertedKelvin);
        }

        /// <summary>
        /// Inverses the function.
        /// </summary>
        /// <param name="celsiusTemperature">The Celsius temperature.</param>
        /// <param name="defaultCoefficients">if set to <c>true</c> [default coefficients].</param>
        /// <returns></returns>
        protected override decimal InverseFunction(decimal celsiusTemperature, bool defaultCoefficients)
        {
            decimal a = defaultCoefficients ? DefaultCoeffA : A;
            decimal b = defaultCoefficients ? DefaultCoeffB : B;
            decimal c = defaultCoefficients ? DefaultCoeffC : C;
            
            decimal temperature = GenericSensor.KELVIN_0 + celsiusTemperature;
            
            //The following was constructed using the Wikipedia description 
            //of the inverse of the Steinhart-Hart equation
            decimal y = (a - 1 / temperature) / c;
            decimal x = MathEx.Sqrt(MathEx.Pow(b / (3 * c), 3) + MathEx.Pow(y / 2m, 2));
            decimal result = MathEx.Exp(MathEx.Pow(x - y / 2m, 1 / 3m) - MathEx.Pow(x + y / 2m, 1 / 3m));

            return result;
        }

        #endregion
        #region Serialization & Deserialization
        protected GenericNTC(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
