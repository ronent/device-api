﻿using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.Sensors.Types;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Three Point Calibration.
    /// </summary>
    [Serializable]
    public abstract class ThreePoint : CalibrationInformation, ISerializable
    {
        #region Fields
        /// <summary>
        /// Gets the default coefficients C.
        /// </summary>
        /// <value>
        /// The default coefficients c.
        /// </value>
        public abstract decimal DefaultCoeffC { get; }

        /// <summary>
        /// Gets the number of calibration points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public override int Points { get { return 3; } }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreePoint"/> class.
        /// </summary>
        internal ThreePoint()
            :base()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the C.
        /// </summary>
        /// <value>
        /// The c.
        /// </value>
        public Decimal C
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the C.
        /// </summary>
        /// <value>
        /// The cf.
        /// </value>
        public Single Cf
        {
            get { return Convert.ToSingle(C); }
        }

        #endregion
        #region Abstract Methods
        /// <summary>
        /// Inverses the function.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="defaultCoefficients">if set to <c>true</c> [default coefficients].</param>
        /// <returns></returns>
        protected abstract decimal InverseFunction(decimal x, bool defaultCoefficients);
        #endregion
        #region Public Methods
        /// <summary>
        /// Gets the default coefficients.
        /// </summary>
        /// <returns></returns>
        public override CalibrationCoefficients GetDefaultCoefficients()
        {
            return new CalibrationCoefficients
            {
                A = DefaultCoeffA,
                B = DefaultCoeffB,
                C = DefaultCoeffC
            };
        }
        /// <summary>
        /// Gets the coefficients.
        /// </summary>
        /// <returns></returns>
        public override CalibrationCoefficients GetCoefficients()
        {
            return new CalibrationCoefficients
            {
                A = this.A,
                B = this.B,
                C = this.C
            };
        }
        /// <summary>
        /// Resets this instance.
        /// </summary>
        internal override void Reset()
        {
            A = DefaultCoeffA;
            B = DefaultCoeffB;
            C = DefaultCoeffC;
        }

        /// <summary>
        /// Sets the specified coefficients.
        /// </summary>
        /// <param name="coeff">The coefficients.</param>
        internal override void Set(CalibrationCoefficients coeff)
        {
            A = coeff.A;
            B = coeff.B;
            C = coeff.C;
        }

        /// <summary>
        /// Initializes the values.
        /// </summary>
        internal override void InitializeValues()
        {
            A = B = C = Gain = 0;
            Offset = 0;
        }
        #endregion
        #region Private Methods
        /// <summary>
        /// Converts Celsius to Kelvin.
        /// </summary>
        /// <param name="values">The values.</param>
        /// <returns></returns>
        internal decimal[] Celsius2InvertedKelvin(decimal[] values)
        {
            List<decimal> results = new List<decimal>();
            foreach (var val in values)
            {
                results.Add(1 / (val + GenericSensor.KELVIN_0));
            }

            return results.ToArray();
        }

        /// <summary>
        /// Extracts the logger values.
        /// </summary>
        /// <param name="loggerValues">The logger values.</param>
        /// <returns></returns>
        internal decimal[] ExtractRValues(decimal[] loggerValues)
        {
            List<decimal> results = new List<decimal>();
            foreach (var val in loggerValues)
            {
                results.Add(Convert.ToDecimal(InverseFunction(val, true)));
            }
            return results.ToArray();
        }

        /// <summary>
        /// Recalculates the coefficients.
        /// </summary>
        /// <param name="Rs">The rs.</param>
        /// <param name="referenceValues">The reference values.</param>
        /// <returns></returns>
        internal CalibrationCoefficients RecalculateCoefficients(decimal[] Rs, double[] referenceValues)
        {
            List<double[]> data = new List<double[]>();
            data.Add(GetFunctionVector(Rs[0]));
            data.Add(GetFunctionVector(Rs[1]));
            data.Add(GetFunctionVector(Rs[2]));

            Matrix m = new Matrix(data.ToArray());
            Vector v = new Vector(referenceValues);
            Matrix solution = m.Solve(v);

            return new CalibrationCoefficients
                {
                    A = Convert.ToDecimal(solution[0, 0]),
                    B = Convert.ToDecimal(solution[1, 0]),
                    C = Convert.ToDecimal(solution[2, 0])
                };
        }

        /// <summary>
        /// Gets the function vector.
        /// </summary>
        /// <param name="r">The r.</param>
        /// <returns></returns>
        private double[] GetFunctionVector(decimal r)
        {
            return new double[]{
                1, 
                Math.Log(Convert.ToDouble(r)), 
                Math.Pow(Math.Log(Convert.ToDouble(r)), 3)
            };
        }
        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="ThreePoint"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected ThreePoint(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {
            C = (Decimal)IO.GetSerializationValue<Decimal>(ref info, "C");
        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("C", C, typeof(Decimal));
        }

        #endregion
    }
}
