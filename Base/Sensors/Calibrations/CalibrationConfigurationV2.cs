﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;
using Base.Sensors.Types;

namespace Base.Sensors.Calibrations
{
    [Serializable]
    public sealed class CalibrationConfigurationV2 : Dictionary<eSensorIndex, CalibrationCoefficients>
    {
        internal SensorManagerV2 _sensorManager { get; set; }

        public CalibrationConfigurationV2(SensorManagerV2 sensorManager)
            : base()
        {
            _sensorManager = sensorManager;
            Initialize();
        }

        public CalibrationConfigurationV2(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        new public void Add(eSensorIndex index, CalibrationCoefficients coeff)
        {
            if (ContainsKey(index))
                Remove(index);

            base.Add(index, coeff);
        }

        private GenericSensorV2 findSensor(eSensorIndex index)
        {
            switch (index)
            {
                case eSensorIndex.Temperature:
                case eSensorIndex.Humidity:
                    return _sensorManager.GetFixedByIndex(index);
                case eSensorIndex.External1:
                    return _sensorManager.GetEnabledDetachable();
                default:
                    throw new Exception("Invalid index. Supports only Temperature, Humidity and External1");
            }
        }

        internal void Initialize()
        {
            Clear();
            var temperature = _sensorManager.GetFixedByIndex(eSensorIndex.Temperature);
            var humidity = _sensorManager.GetFixedByIndex(eSensorIndex.Humidity);
            var external = _sensorManager.GetEnabledDetachable();

            addExistingFrom(eSensorIndex.Temperature, temperature);
            addExistingFrom(eSensorIndex.Humidity, humidity);
            addExistingFrom(eSensorIndex.External1, external);
        }

        private void addExistingFrom(eSensorIndex index, GenericSensorV2 sensor)
        {
            Add(index, sensor == null ? new CalibrationCoefficients() : sensor.Calibration.GetCoefficients());
        }
    }
}
