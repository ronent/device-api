﻿using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Two Point Calibration.
    /// </summary>
    [Serializable]
    public class TwoPoint : CalibrationInformation
    {
        #region Fields
        /// <summary>
        /// Gets the number of calibration points.
        /// </summary>
        /// <value>
        /// The points.
        /// </value>
        public override int Points { get { return 2; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TwoPoint"/> class.
        /// </summary>
        internal TwoPoint()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the default coefficients A.
        /// </summary>
        /// <value>
        /// The default coefficients a.
        /// </value>
        public override decimal DefaultCoeffA
        {
            get { return 1; }
        }

        /// <summary>
        /// Gets the default coefficients B.
        /// </summary>
        /// <value>
        /// The default coefficients b.
        /// </value>
        public override decimal DefaultCoeffB
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets a value indicating whether values are valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        public override bool Valid
        {
            get { return Gain != 0; }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Resets this instance.
        /// </summary>
        internal override void Reset()
        {
            A = DefaultCoeffA;
            B = DefaultCoeffB;
        }

        /// <summary>
        /// Sets the specified coefficients.
        /// </summary>
        /// <param name="coeff">The coefficients.</param>
        internal override void Set(CalibrationCoefficients coeff)
        {
            A = coeff.A;
            B = coeff.B;
        }

        /// <summary>
        /// Sets the specified pairs.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        internal override void Set(LoggerReference[] pairs)
        {
            References = pairs;
            var coefficients = GenerateCoefficients(pairs);

            A = coefficients.A;
            B = coefficients.B;
        }

        /// <summary>
        /// Generates the coefficients.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        /// <returns></returns>
        internal override CalibrationCoefficients GenerateCoefficients(LoggerReference[] pairs)
        {
            LoggerReference pair1 = pairs[0];
            LoggerReference pair2 = pairs[1];

            decimal gain = (pair1.Reference - pair2.Reference) / (pair1.Logger - pair2.Logger);
            decimal offset = gain * (-pair1.Logger) + pair1.Reference;

            return new CalibrationCoefficients
            {
                A = gain,
                B = offset
            };
        }

        /// <summary>
        /// Calibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal override decimal Calibrate(decimal value)
        {
            return (value * Gain + Offset);
        }

        /// <summary>
        /// Decalibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        internal override decimal Decalibrate(decimal value)
        {
            return ((value - Offset) / Gain);
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="TwoPoint"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected TwoPoint(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            
        }
        #endregion
    }
}
