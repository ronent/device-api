﻿using System;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Calibration Coefficients.
    /// </summary>
    [Serializable]
    public class CalibrationCoefficients
    {
        public decimal A, B, C;

        /// <summary>
        /// Gets or sets the A.
        /// </summary>
        /// <value>
        /// The af.
        /// </value>
        public Single Af { get { return Convert.ToSingle(A); } set { A = tryGetDecimal(value); } }

        /// <summary>
        /// Gets or sets the B.
        /// </summary>
        /// <value>
        /// The bf.
        /// </value>
        public Single Bf { get { return Convert.ToSingle(B); } set { B = tryGetDecimal(value); } }

        /// <summary>
        /// Gets or sets the C.
        /// </summary>
        /// <value>
        /// The cf.
        /// </value>
        public Single Cf { get { return Convert.ToSingle(C); } set { C = tryGetDecimal(value); } }

        /// <summary>
        /// Gets the gain.
        /// </summary>
        /// <value>
        /// The int gain.
        /// </value>
        public Int16 IntGain { get { return tryGetGainOrDefault(); } }

        /// <summary>
        /// Gets the offset.
        /// </summary>
        /// <value>
        /// The int offset.
        /// </value>
        public Int16 IntOffset { get { return tryGetOffsetOrDefault(); } }

        /// <summary>
        /// Initializes a new instance of the <see cref="CalibrationCoefficients"/> class.
        /// </summary>
        public CalibrationCoefficients()
        {
            A = 1;
            B = 0;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            CalibrationCoefficients other = obj as CalibrationCoefficients;
            if (other == null)
                return false;

            //return A == other.A
            //    && B == other.B
            //    && C == other.C;
            return IntGain == other.IntGain
                && IntOffset == other.IntOffset;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Gain: '{0}', Offset: '{1}'", IntGain, IntOffset);
        }

        /// <summary>
        /// Tries the get decimal.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        private static decimal tryGetDecimal(float value)
        {
            try
            {
                return Convert.ToDecimal(value);
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Tries the get gain or default.
        /// </summary>
        /// <returns></returns>
        private short tryGetGainOrDefault()
        {
            try { return Convert.ToInt16(A); }
            catch { return 1; }
        }

        /// <summary>
        /// Tries the get offset or default.
        /// </summary>
        /// <returns></returns>
        private short tryGetOffsetOrDefault()
        {
            try { return Convert.ToInt16(B); }
            catch { return 0; }
        }

    }
}
