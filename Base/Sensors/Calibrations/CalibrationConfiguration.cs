﻿using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Types;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Base.Sensors.Calibrations
{
    /// <summary>
    /// Calibration Configuration.
    /// </summary>
    [Serializable]
    public sealed class CalibrationConfiguration : Dictionary<eSensorIndex, CalibrationCoefficients>
    {
        internal SensorManager SensorManager { get; set; }

        public CalibrationConfiguration(SensorManager sensorManager)
            : base()
        {
            SensorManager = sensorManager;
            Initialize();
        }

        public CalibrationConfiguration(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        new public void Add(eSensorIndex index, CalibrationCoefficients coeff)
        {
            if (ContainsKey(index))
                Remove(index);

            base.Add(index, coeff);
        }

        private GenericSensor findSensor(eSensorIndex index)
        {
            switch (index)
            {
                case eSensorIndex.Temperature:
                case eSensorIndex.Humidity:
                    return SensorManager.GetFixedByIndex(index);
                case eSensorIndex.External1:
                    return SensorManager.GetEnabledDetachable();
                default:
                    throw new Exception("Invalid index. Supports only Temperature, Humidity and External1");
            }
        }

        internal void Initialize()
        {
            Clear();
            GenericSensor temperature = SensorManager.GetFixedByIndex(eSensorIndex.Temperature);
            GenericSensor humidity = SensorManager.GetFixedByIndex(eSensorIndex.Humidity);
            GenericSensor external = SensorManager.GetEnabledDetachable();

            addExistingFrom(eSensorIndex.Temperature, temperature);
            addExistingFrom(eSensorIndex.Humidity, humidity);
            addExistingFrom(eSensorIndex.External1, external);
        }

        private void addExistingFrom(eSensorIndex index, GenericSensor sensor)
        {
            Add(index, sensor == null ? new CalibrationCoefficients() : sensor.Calibration.GetCoefficients());
        }
    }
}