﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;

namespace Base.Sensors.Types.Temperature
{
    [Serializable]
    public class PT100 : GenericPT1KSensor
    {
        #region Properties
        public override eSensorType Type
        {
            get { return eSensorType.PT100; }
        }

        public override eSensorIndex Index
        {
            get { return eSensorIndex.Temperature; }
        }

        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(100); }
        }

        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-100); }
        }

        public override string Name
        {
            get { return "PT 1K"; }
        }

        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion

         #region Constructor
        internal PT100(SensorManagerV2 parent)
            :base(parent)
        {
        }
        #endregion
    }
}
