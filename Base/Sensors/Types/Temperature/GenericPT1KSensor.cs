﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.Sensors.Management;
using Base.Sensors.Units;

namespace Base.Sensors.Types.Temperature
{
    [Serializable]
    public abstract class GenericPT1KSensor : GenericSensorV2
    {
        internal GenericPT1KSensor(SensorManagerV2 parent)
            :base(parent)
        {
        }

        public override IUnit Unit
        {
            get
            {
                if (ParentSensorManager == null || !ParentSensorManager.FahrenheitMode)
                    return new Celcius();
                return new Fahrenheit();
            }

            internal set { throw new NotSupportedException(); }
        }

        protected virtual decimal ConvertToCorrectUnit(decimal value)
        {
            return ParentSensorManager.FahrenheitMode ? Transformation.ConvertToFahrenheit(value) : value;
        }

        protected decimal ConvertToCelsiusIfRequired(decimal value)
        {
            return ParentSensorManager.FahrenheitMode ? Transformation.ConvertToCelsius(value) : value;
        }
    }
}
