﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types.Temperature
{
    /// <summary>
    /// Internal NTC sensor class
    /// </summary>
    [Serializable]
    public abstract class InternalNTC : GenericTemperatureSensor
    {
        /// <summary>
        /// Gets the sensor type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.InternalNTC; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.Temperature; }
        }

        /// <summary>
        /// Gets the sensor name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "Temperature"; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="InternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal InternalNTC(SensorManager parent)
            :base(parent)
        {
        }
    }
}
