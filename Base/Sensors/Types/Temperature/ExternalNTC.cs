﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types.Temperature
{
    /// <summary>
    /// External NTC sensor class
    /// </summary>
    [Serializable]
    public abstract class ExternalNTC : GenericTemperatureSensor
    {
        /// <summary>
        /// Gets the sensor type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.ExternalNTC; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.External1; }
        }

        /// <summary>
        /// Gets the sensor name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "External Temperature"; }
        }

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(150); }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-50); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal ExternalNTC(SensorManager parent)
            :base(parent)
        {
        }
    }
}
