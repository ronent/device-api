﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;


namespace Base.Sensors.Types.Temperature
{
    /// <summary>
    /// Digital temperature sensor class
    /// </summary>
    [Serializable]
    public class DigitalTemperature : GenericTemperatureSensor
    {
        #region Properties
        public override eSensorType Type
        {
            get { return eSensorType.DigitalTemperature; }
        }

        public override eSensorIndex Index
        {
            get { return eSensorIndex.Temperature; }
        }

        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(80); }
        }

        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-40); }
        }

        public override string Name
        {
            get { return "Temperature"; }
        }

        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructor
        internal DigitalTemperature(SensorManager parent)
            :base(parent)
        {
        }
        #endregion
        #region Methods
       
        internal override long GetDigitalValue(decimal sensorValue)
        {
            return Convert.ToInt64((sensorValue + 39.63m) / 0.01m);
        }

        internal override decimal GetSensorValue(long digitalValue)
        {
            return -39.63m + Convert.ToDecimal(0.01 * digitalValue);
        }
        #endregion
    }
}
