﻿using Auxiliary.Tools;
using Base.Sensors.Management;
using Base.Sensors.Units;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types.Temperature
{
    [Serializable]
    public abstract class GenericTemperatureSensor : GenericSensor
    {
        internal GenericTemperatureSensor(SensorManager parent)
            :base(parent)
        {
        }

        public override IUnit Unit
        {
            get
            {
                if (ParentSensorManager == null || !ParentSensorManager.FahrenheitMode)
                {
                    return new Celcius();
                }
                else
                {
                    return new Fahrenheit();
                }
            }

            internal set { throw new NotSupportedException(); }
        }

        protected virtual decimal ConvertToCorrectUnit(decimal value)
        {
            if (ParentSensorManager.FahrenheitMode)
                return Transformation.ConvertToFahrenheit(value);
            else
                return value;
        }

        protected decimal ConvertToCelsiusIfRequired(decimal value)
        {
            if (ParentSensorManager.FahrenheitMode)
                return Transformation.ConvertToCelsius(value);
            else
                return value;
        }
    }
}
