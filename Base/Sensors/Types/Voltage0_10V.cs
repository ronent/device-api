﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Voltage 0-10V Sensor.
    /// </summary>
    [Serializable]
    public abstract class Voltage0_10V : GenericSensor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Voltage0_10V"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Voltage0_10V(SensorManager parent)
            :base(parent)
        {
        }

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return 10; }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the sensor unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public override Units.IUnit Unit
        {
            get { return new Base.Sensors.Units.Voltage(); }
        }
        /// <summary>
        /// Gets the sensor type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.Voltage0_10V; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.External1; }
        }

        /// <summary>
        /// Gets the sensor name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "Voltage 0-10 V"; }
        }
    }
}
