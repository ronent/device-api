﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Humidity Sensor.
    /// </summary>
    [Serializable]
    public abstract class Humidity : GenericSensor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Humidity"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Humidity(SensorManager parent)
            :base(parent)
        {
            Calibration = new Base.Sensors.Calibrations.TwoPoint();
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.Humidity; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.Humidity; }
        }

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return 100; }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return 0; }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "Relative Humidity"; }
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public override Units.IUnit Unit
        {
            get { return new Base.Sensors.Units.Humidity(); }
        }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }
    }
}
