﻿using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Current 4-20mA Sensor.
    /// </summary>
    [Serializable]
    public abstract class Current4_20mA : GenericSensor
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Current4_20mA"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Current4_20mA(SensorManager parent)
            :base(parent)
        {
        }

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return 20; }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return 4; }
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public override Units.IUnit Unit
        {
            get { return new Base.Sensors.Units.Current(); }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.Current4_20mA; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.External1; }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "Current 4-20 mA"; }
        }
    }
}
