﻿using Base.Sensors.Management;
using Base.Sensors.Units;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Defined Sensor.
    /// </summary>
    public interface IDefinedSensor
    {
        /// <summary>
        /// Gets or sets the type of the base sensor.
        /// </summary>
        /// <value>
        /// The type of the base.
        /// </value>
        eSensorType BaseType { get; }
    }
}
