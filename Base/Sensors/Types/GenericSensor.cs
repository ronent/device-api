﻿using Auxiliary.Tools;
using Base.Sensors.Alarms;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Samples;
using Base.Sensors.Units;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Sensor enabled delegate
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="e">The <see cref="EnableEventArgs"/> instance containing the event data.</param>
    public delegate void SensorEnabledDelegate(object sender, EnableEventArgs e);

    /// <summary>
    /// Generic Sensor.
    /// </summary>
    [Serializable]
    public abstract class GenericSensor : IDisposable
    {
        #region Const
        /// <summary>
        /// The kelvin
        /// </summary>
        internal static readonly decimal KELVIN_0 = 273.15m;

        #endregion
        #region Fields
        internal SampleList samples { get; set; }
        private bool enabled;
        private CalibrationInformation calibration;
        private Sample lastSample;
        private Sample lastTimeStamp;

        #endregion
        #region Events
        /// <summary>
        /// Occurs when sensor become enabled.
        /// </summary>
        internal event SensorEnabledDelegate OnEnabled;

        #endregion
        #region Abstract Members
        public abstract eSensorType Type { get; }

        public virtual string Name { get; internal set; }

        public virtual IUnit Unit { get; internal set; }

        public virtual bool SWGenerated { get { return false; } }

        public abstract decimal Maximum { get; }

        public abstract decimal Minimum { get; }

        public abstract bool USBRunnable { get; }

        #endregion
        #region Properties
        public List<Sample> Samples { get { return new List<Sample>(samples.Samples); } }

        public CalibrationInformation Calibration 
        {
            get { return calibration; }
            internal set { calibration = value; }
        }

        public SensorAlarm Alarm { get; internal set; }

        public bool Enabled
        {
            get { return enabled; }
            internal set
            {
                enabled = value;
                if (value)
                    InvokeOnEnabled();
            }
        }

        internal SensorManager ParentSensorManager { get; set; }

        public abstract eSensorIndex Index { get; }

        public bool IsEmpty
        {
            get 
            {
                if (Samples.Count == 0)
                {
                    return true;
                }
                return Samples.Find(sample => !sample.IsDummy) == null;
            }
        }

        internal Sample LastSample
        {
            get
            {
                if (Samples.Count == 0) return lastSample;
                lastSample = null;
                return Samples.FindLast(item => !(item is TimeStamp));
            }
            set
            {
                lastSample = value;
            }
        }

        internal Sample LastTimeStamp
        {
            get
            {
                if (Samples.Count == 0) return lastTimeStamp;
                lastTimeStamp = null;
                return Samples.FindLast(item => item is TimeStamp);
            }
            set
            {
                lastTimeStamp = value;
            }
        }

        #endregion
        #region Constructors

        protected GenericSensor(SensorManager parent)
        {
            Initialize(parent);
        }

        internal void Initialize()
        {
            Enabled = false;
            Alarm.Enabled = false;
        }
       
        protected virtual void Initialize(SensorManager parent)
        {
            throwIfNotISensor();

            Alarm = new SensorAlarm(this);
            InitializeCalibration();

            if (parent != null)
            {
                samples = new SampleList(this);
                InitializeParent(parent);
            }
        }

        private void throwIfNotISensor()
        {
            if (this == null)
                throw new InvalidCastException("Sensor must implement GenericSensor");
        }

        protected virtual void InitializeCalibration()
        {
            Calibration = new TwoPoint();
        }

        internal void InitializeParent(SensorManager parent)
        {
            ParentSensorManager = parent;
        }

        #endregion
        #region Abstract Methods
        internal abstract long GetDigitalValue(decimal sensorValue);

        internal abstract decimal GetSensorValue(long digitalValue);
        #endregion
        #region Methods
        private void InvokeOnEnabled()
        {
            var e = new EnableEventArgs { Enabled = true };
            //Utilities.InvokeEvent(OnEnabled, this, e, Log.Log.HandlerFailed);
            Utilities.InvokeEvent(OnEnabled, this, e, null);
        }

        internal CalibrationCoefficients GetDefaultCalibration()
        {
            return Calibration.GetDefaultCoefficients();
        }

        internal Sample GenerateSample(long i_Value, DateTime i_Date, bool i_NotifyAlarm, bool isDummy = false)
        {
            if (isDummy)
                return new Sample(i_Date);
           
            var sensorValue = GetSensorValue(i_Value);
            var alarmStatus = eAlarmStatus.Normal;

            if (Alarm.Enabled)
                alarmStatus = Alarm.CheckSampleAndNotifyOnAlarm(sensorValue, i_NotifyAlarm);

            return new Sample(i_Date, sensorValue, alarmStatus);
        }

        internal Sample GenerateTimeStamp(long i_Value, DateTime i_Date, string i_Comment, bool i_NotifyAlarm, bool isDummy = false)
        {
            if (isDummy)
            {
                return new TimeStamp(i_Date, i_Comment);
            }
            var sensorValue = GetSensorValue(i_Value);
            var alarmStatus = Alarm.CheckSampleAndNotifyOnAlarm(sensorValue, i_NotifyAlarm && Alarm.Enabled);

            return new TimeStamp(i_Date, sensorValue, alarmStatus, i_Comment);
        }

        public void ClearData()
        {
            samples.Clear();
        }

        public CalibrationCoefficients GenerateCalibration(LoggerReference[] pairs)
        {
            try
            {
                return Calibration.GenerateCoefficients(pairs);
            }
            catch(Exception ex)
            {
                throw new Exception("Logger values can't be equal.", ex);
            }
        }

        internal CalibrationCoefficients GenerateRandomCalibration()
        {
            return Calibration.GenerateRandomCoefficients();
        }

        internal GenericSensor TransfromUnitToFahrenheit()
        {
            try
            {
                if (!(Unit is Celcius))
                    throw new FormatException(string.Format("Can't convert Unit: {0} to Fahrenheit", Unit.GetType()));

                ParentSensorManager.ParentLogger.Status.FahrenheitMode = true;

                Samples.ForEach(sample =>
                    {
                        if (!sample.IsDummy)
                            sample.Value = Auxiliary.Tools.Transformation.ConvertToFahrenheit(sample.Value);
                    });

                if (Alarm.Enabled)
                {
                    decimal low = Auxiliary.Tools.Transformation.ConvertToFahrenheit(Alarm.Low);
                    decimal high = Auxiliary.Tools.Transformation.ConvertToFahrenheit(Alarm.High);

                    if (Alarm is SensorAlarmWarning)
                    {
                        var alarm = Alarm as SensorAlarmWarning;

                        decimal preLow = Auxiliary.Tools.Transformation.ConvertToFahrenheit(alarm.LowWarning);
                        decimal preHigh = Auxiliary.Tools.Transformation.ConvertToFahrenheit(alarm.HighWarning);

                        alarm.Set(preLow, low, high, preHigh);
                    }
                    else
                        Alarm.Set(low, high);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On GenericSensor.TransfromUnitToFahrenheit()",this, ex);
            }

            return this;
        }

        internal GenericSensor TransfromUnitToCelsius()
        {
            try
            {
                if (!(Unit is Fahrenheit))
                    throw new FormatException(string.Format("Can't convert Unit: {0} to Celsius", Unit.GetType()));

                ParentSensorManager.ParentLogger.Status.FahrenheitMode = false;

                Samples.ForEach(sample =>
                {
                    if (!sample.IsDummy)
                        sample.Value = Auxiliary.Tools.Transformation.ConvertToCelsius(sample.Value);
                });

                if (Alarm.Enabled)
                {
                    decimal low = Auxiliary.Tools.Transformation.ConvertToCelsius(Alarm.Low);
                    decimal high = Auxiliary.Tools.Transformation.ConvertToCelsius(Alarm.High);

                    if (Alarm is SensorAlarmWarning)
                    {
                        var alarm = Alarm as SensorAlarmWarning;

                        decimal preLow = Auxiliary.Tools.Transformation.ConvertToCelsius(alarm.LowWarning);
                        decimal preHigh = Auxiliary.Tools.Transformation.ConvertToCelsius(alarm.HighWarning);

                        alarm.Set(preLow, low, high, preHigh);
                    }
                    else
                        Alarm.Set(low, high);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On GenericSensor.TransfromUnitToCelsius()",this, ex);
            }
            return this;
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            Alarm.Dispose();
            Calibration = null;

            if (Samples != null)
            {
                samples.Dispose();
            }
        }

        #endregion
    }
}