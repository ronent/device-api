﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.Sensors.Alarms;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Samples;
using Base.Sensors.Units;

namespace Base.Sensors.Types
{
    public delegate void SensorV2EnabledDelegate(object sender, EnableEventArgs e);

    [Serializable]
    public abstract class GenericSensorV2
    {
        internal SampleListV2 samples { get; set; }
        private bool enabled;
        private CalibrationInformation calibration;
        private SampleV2 lastSample;
        private SampleV2 lastTimeStamp;

        internal event SensorV2EnabledDelegate OnEnabled;

        protected GenericSensorV2()
        {
            Initialize();
        }

        internal GenericSensorV2(SensorManagerV2 parent)
        {
            Initialize(parent);
        }

        protected virtual void Initialize(SensorManagerV2 parent)
        {
            throwIfNotISensor();

            Alarm = new SensorAlarmV2(this);
            InitializeCalibration();

            if (parent != null)
            {
                samples = new SampleListV2(this);
                InitializeParent(parent);
            }
        }

        internal void InitializeParent(SensorManagerV2 parent)
        {
            ParentSensorManager = parent;
        }

        private void throwIfNotISensor()
        {
            if (this == null)
                throw new InvalidCastException("Sensor must implement GenericSensorV2");
        }


        internal void Initialize()
        {
            Enabled = false;
            Alarm.HighEnabled = false;
            Alarm.PreHighEnabled = false;
            Alarm.LowEnabled = false;
            Alarm.PreLowEnabled = false;
        }

        public abstract eSensorType Type { get; }

        public virtual string Name { get; internal set; }

        public virtual IUnit Unit { get; internal set; }

        public virtual bool SWGenerated { get { return false; } }

        public abstract decimal Maximum { get; }

        public abstract decimal Minimum { get; }

        public abstract bool USBRunnable { get; }

        public List<SampleV2> Samples { get { return new List<SampleV2>(samples.Samples); } }

        public CalibrationInformation Calibration
        {
            get { return calibration; }
            internal set { calibration = value; }
        }

        public SensorAlarmV2 Alarm { get; internal set; }

        public bool Enabled
        {
            get { return enabled; }
            internal set
            {
                enabled = value;
                if (value)
                    InvokeOnEnabled();
            }
        }

        internal SensorManagerV2 ParentSensorManager { get; set; }

        public abstract eSensorIndex Index { get; }

//        public bool IsEmpty
//        {
//            get
//            {
//                if (Samples.Count == 0)
//                    return true;
//               
//                return Samples.Find(sample => !sample.IsDummy) == null;
//            }
//        }

        internal SampleV2 LastSample
        {
            get
            {
                if (Samples.Count == 0) return lastSample;
                lastSample = null;
                return Samples.FindLast(item => !(item is TimeStamp));
            }
            set
            {
                lastSample = value;
            }
        }

        internal SampleV2 LastTimeStamp
        {
            get
            {
                if (Samples.Count == 0) return lastTimeStamp;
                lastTimeStamp = null;
                return Samples.FindLast(item => item is TimeStamp);
            }
            set
            {
                lastTimeStamp = value;
            }
        }

        protected virtual void InitializeCalibration()
        {
            Calibration = new TwoPoint();
        }

        internal CalibrationCoefficients GetDefaultCalibration()
        {
            return Calibration.GetDefaultCoefficients();
        }

        internal SampleV2 GenerateSample(float i_Value, uint i_Date, eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isDummy = false)
        {
//            if (isDummy)
//                return new SampleV2(i_Date);


//            var alarmStatus = eAlarmStatus.Normal;
//
//            if (Alarm.HighEnabled || Alarm.LowEnabled)
//                alarmStatus = Alarm.CheckSampleAndNotifyOnAlarm(i_Value, i_NotifyAlarm);

            
            Alarm.CheckSampleAndNotifyOnAlarm(alarmStatus);
            return new SampleV2(i_Date, i_Value, alarmStatus, lightStatus, errorCode);
        }

        public void ClearData()
        {
            samples.Clear();
        }

        public CalibrationCoefficients GenerateCalibration(LoggerReference[] pairs)
        {
            try
            {
                return Calibration.GenerateCoefficients(pairs);
            }
            catch (Exception ex)
            {
                throw new Exception("Logger values can't be equal.", ex);
            }
        }

        private void InvokeOnEnabled()
        {
            var e = new EnableEventArgs { Enabled = true };
            //Utilities.InvokeEvent(OnEnabled, this, e, Log.Log.HandlerFailed);
            Utilities.InvokeEvent(OnEnabled, this, e, null);
        }

    }
}
