﻿using Auxiliary.Tools;
using Base.DataStructures;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Units;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Two Point User Defined Sensor.
    /// </summary>
    [Serializable]
    public abstract class TwoPointUserDefinedSensor : GenericSensor, IDefinedSensor
    {
        #region Fields
        /// <summary>
        /// The user defined sensor configuration.
        /// </summary>
        protected UDSConfiguration UDS;

        /// <summary>
        /// The transformer.
        /// </summary>
        protected TwoPoint Transformer = new TwoPoint();

        /// <summary>
        /// The base sensor.
        /// </summary>
        private GenericSensor baseSensor = new Sensors.Types.EmptySensor();

        #endregion
        #region Properties
        /// <summary>
        /// Gets the base sensor.
        /// </summary>
        /// <value>
        /// The base sensor.
        /// </value>
        public GenericSensor BaseSensor
        {
            get
            {
                reloadBaseSensorIfNecessary();
                return baseSensor;
            }
        }

        public override string Name
        {
            get
            {
                return UDS.Name;
            }
        }

        /// <summary>
        /// Gets or sets the type of the base sensor.
        /// </summary>
        /// <value>
        /// The type of the base.
        /// </value>
        public eSensorType BaseType { get { return UDS.BaseType; } internal set {UDS.BaseType = value;}}

        public override eSensorType Type
        {
            get { return eSensorType.UserDefined; }
        }

        /// <summary>
        /// Gets or sets the significant figures.
        /// </summary>
        /// <value>
        /// The significant figures.
        /// </value>
        public virtual byte SignificantFigures { get { return UDS.SignificantFigures; } set { UDS.SignificantFigures = value; } }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.External1; }
        }

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return TransformFromBase(BaseSensor.Maximum); }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return TransformFromBase(BaseSensor.Minimum); }
        }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return BaseSensor.USBRunnable; }
        }

        /// <summary>
        /// Gets a value indicating whether [software generated].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [software generated]; otherwise, <c>false</c>.
        /// </value>
        public override bool SWGenerated { get { return false; } }

        /// <summary>
        /// Gets the offset.
        /// </summary>
        /// <value>
        /// The offset.
        /// </value>
        public float Offset { get { return Convert.ToSingle(Transformer.Offset); } }

        /// <summary>
        /// Gets the gain.
        /// </summary>
        /// <value>
        /// The gain.
        /// </value>
        public float Gain { get { return Convert.ToSingle(Transformer.Gain); } }

        /// <summary>
        /// Gets a value indicating whether [valid].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool Valid { get { return Transformer.Valid; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="TwoPointUserDefinedSensor"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal TwoPointUserDefinedSensor(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            return BaseSensor.GetDigitalValue(TransformToBase(sensorValue));
        }

        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            return TransformFromBase(BaseSensor.GetSensorValue(digitalValue));
        }

        /// <summary>
        /// Reloads the base sensor.
        /// </summary>
        private void reloadBaseSensor()
        {
            if(ParentSensorManager != null)
                baseSensor = ParentSensorManager.CreatePredefined(BaseType);
        }

        /// <summary>
        /// Reloads the base sensor if necessary.
        /// </summary>
        private void reloadBaseSensorIfNecessary()
        {
            if (baseSensor == null || baseSensor.Type != BaseType)
                reloadBaseSensor();
        }


        /// <summary>
        /// Transforms from base.
        /// </summary>
        /// <param name="baseSensorValue">The base sensor value.</param>
        /// <returns></returns>
        public decimal TransformFromBase(decimal baseSensorValue)
        {
            return UDS.TransformFromBase(baseSensorValue);
        }

        /// <summary>
        /// Transforms to base.
        /// </summary>
        /// <param name="definedSensorValue">The defined sensor value.</param>
        /// <returns></returns>
        public decimal TransformToBase(decimal definedSensorValue)
        {
            return UDS.TransformToBase(definedSensorValue);
        }

        /// <summary>
        /// Sets the specified pairs.
        /// </summary>
        /// <param name="pairs">The pairs.</param>
        public void Set(LoggerReference[] pairs)
        {
            UDS.Set(pairs);
        }      
  
        #endregion
        #region Override Methods
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Name + " (" + Unit + ")";
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Type.GetHashCode()
                + Name.GetHashCode()
                + Unit.GetHashCode()
                + Offset.GetHashCode()
                + Gain.GetHashCode();
        }

        #endregion
    }
}
