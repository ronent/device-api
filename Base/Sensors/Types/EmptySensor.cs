﻿using Base.Sensors.Management;
using Base.Sensors.Units;
using System;

namespace Base.Sensors.Types
{
    /// <summary>
    /// Empty Sensor.
    /// </summary>
    [Serializable]
    public class EmptySensor : GenericSensor
    {
        #region Fields & Properties
        private Custom unit = new Custom { Name = "N/A" };

        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return decimal.MaxValue; }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return decimal.MinValue; }
        }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return false; }
        }

        /// <summary>
        /// Gets the index of the sensor.
        /// </summary>
        /// <value>
        /// The index.
        /// </value>
        public override eSensorIndex Index
        {
            get { return eSensorIndex.External4; }
        }

        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            return long.MinValue;
        }

        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            return decimal.MinValue;
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public override eSensorType Type
        {
            get { return eSensorType.None; }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return "N/A"; }
        }

        /// <summary>
        /// Gets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public override IUnit Unit
        {
            get { return unit; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EmptySensor"/> class.
        /// </summary>
        internal EmptySensor()
            :base(null)
        {

        }
        #endregion
    }
}
