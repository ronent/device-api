﻿using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Units
{
    /// <summary>
    /// Celsius.
    /// </summary>
    [Serializable]
    public sealed class Celcius : ITemperatureUnit, ISerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Celcius"/> class.
        /// </summary>
        public Celcius()
        {

        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return "°C"; }
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            bool toReturn = false;

            if (this.Name.Equals((obj as IUnit).Name))
            {
                toReturn = true;
            }

            return toReturn;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Celcius"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected Celcius(SerializationInfo info, StreamingContext context)
        {

        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }

        #endregion
    }
}
