﻿
namespace Base.Sensors.Units
{
    /// <summary>
    /// Sensor Unit.
    /// </summary>
    public interface IUnit
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        string Name { get; }
    }

}
