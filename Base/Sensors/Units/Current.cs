﻿using System;

namespace Base.Sensors.Units
{
    /// <summary>
    /// Current.
    /// </summary>
    [Serializable]
    public sealed class Current : IUnit
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Current"/> class.
        /// </summary>
        internal Current()
        {

        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return "mA"; }
        }
    }
}
