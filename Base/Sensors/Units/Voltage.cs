﻿using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Units
{
    /// <summary>
    /// Voltage.
    /// </summary>
    [Serializable]
    public sealed class Voltage : IUnit, ISerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Voltage"/> class.
        /// </summary>
        internal Voltage()
        {

        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get { return "V"; }
        }

        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Voltage"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected Voltage(SerializationInfo info, StreamingContext context)
        {

        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
        }

        #endregion
    }
}
