﻿using Auxiliary.Tools;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Units
{
    /// <summary>
    /// Custom.
    /// </summary>
    [Serializable]
    public sealed class Custom : IUnit, ISerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Custom"/> class.
        /// </summary>
        internal Custom()
        {

        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name
        {
            get;
            set;
        }

        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Custom"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected Custom(SerializationInfo info, StreamingContext context)
        {
            Name = (string)IO.GetSerializationValue<string>(ref info, "Name");
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name, typeof(string));
        }

        #endregion
    }
}
