﻿using System;

namespace Base.Sensors.Alarms
{
    public class SensorAlarmStatusArgs : EventArgs
    {
        public eAlarmStatus NewStatus { get; private set; }

        public eAlarmStatus PrevStatus { get; private set; }

        internal SensorAlarmStatusArgs(eAlarmStatus newStatus, eAlarmStatus prevStatus)
        {
            NewStatus = newStatus;
            PrevStatus = prevStatus;
        }
    }
}
