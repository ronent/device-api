﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.Sensors.Management;

namespace Base.Sensors.Alarms
{
    [Serializable]
    public sealed class SensorAlarmManagerV2
    {
        internal event LoggerStatusChangedDelegate OnLoggerStatusChanged;

        private eAlarmStatus _status = eAlarmStatus.Normal;
        private readonly SensorManagerV2 _parentSensorManager;

        public eAlarmStatus AggregateAlarm
        {
            get;
            protected set;
        }

        public eAlarmStatus Status
        {
            get { return _status; }
            private set
            {
                if (_status != value)
                {
                    var previousValue = _status;
                    _status = value;
                    if (_status != eAlarmStatus.Normal)
                        AggregateAlarm = _status;
                    notifyStatusChange(previousValue);
                }

                _status = value;
            }
        }

        public ushort Delay { get; set; }

        public ushort WarningDelay { get; set; }

        public ushort Duration { get; set; }

        public bool SoundAlarmOnWarning { get; set; }

        internal SensorAlarmManagerV2(SensorManagerV2 parent)
        {
            _parentSensorManager = parent;
        }

        #region Events

        private void Alarm_SensorStatusChangedEvent(object sender, SensorAlarmStatusArgs e)
        {
            var sensorsInAlarm = (from sensor in _parentSensorManager.GetAll()
                                  where sensor.Alarm.Status != eAlarmStatus.Normal
                                  select sensor).ToArray();
            Status = sensorsInAlarm.Any() ? eAlarmStatus.Alarm : eAlarmStatus.Normal;
        }

        private void notifyStatusChange(eAlarmStatus previousValue)
        {
            var e = new SensorAlarmStatusArgs(Status, previousValue);
            Utilities.InvokeEvent(OnLoggerStatusChanged, _parentSensorManager.ParentLogger, e);
        }
        #endregion

        internal void RegisterForStatusChange(SensorAlarmV2 alarm)
        {
            alarm.OnSensorStatusChanged += Alarm_SensorStatusChangedEvent;
        }

        internal void UnregisterForStatusChange(SensorAlarmV2 alarm)
        {
            alarm.OnSensorStatusChanged -= Alarm_SensorStatusChangedEvent;
        }
    }
}
