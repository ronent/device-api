﻿using Auxiliary.Tools;
using Base.Sensors.Types;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Alarms
{
    /// <summary>
    /// Sensor Alarm With Warnings.
    /// </summary>
    [Serializable]    
    public sealed class SensorAlarmWarning : SensorAlarm, ISerializable
    {
        #region Fields
        /// <summary>
        /// Gets the low warning.
        /// </summary>
        /// <value>
        /// The low warning.
        /// </value>
        public decimal LowWarning
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the high warning.
        /// </summary>
        /// <value>
        /// The high warning.
        /// </value>
        public decimal HighWarning
        {
            get;
            private set;
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="SensorAlarmWarning"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal SensorAlarmWarning(GenericSensor parent)
            :base(parent)
        {
        }
        #endregion
        #region Methods
        /// <summary>
        /// Sets the specified low, high, low warning and high warning alarm.
        /// </summary>
        /// <param name="i_LowAlarm">The i_ low alarm.</param>
        /// <param name="i_LowWarning">The i_ low warning.</param>
        /// <param name="i_HighAlarm">The i_ high alarm.</param>
        /// <param name="i_HighWarning">The i_ high warning.</param>
        public void Set(decimal? i_LowAlarm, decimal? i_LowWarning, decimal? i_HighAlarm, decimal? i_HighWarning)
        {
            base.Set(i_LowAlarm, i_HighAlarm);

            if (i_LowWarning == null)
            {
                LowWarning = decimal.MinValue;
            }
            else
            {
                LowWarning = i_LowWarning.Value;
            }

            if (i_HighWarning == null)
            {
                HighWarning = decimal.MinValue;
            }
            else
            {
                HighWarning = i_HighWarning.Value;
            }
        }

        #endregion
        #region Override Methods
        /// <summary>
        /// Checks the low and high alarms.
        /// </summary>
        /// <param name="i_Value">The i_ value.</param>
        /// <returns></returns>
        internal override eAlarmStatus checkLowHighAlarms(decimal i_Value)
        {
            var baseResult = base.checkLowHighAlarms(i_Value);
            if (baseResult == eAlarmStatus.Normal)
            {
                if (i_Value < LowWarning)
                {
                    return eAlarmStatus.LowWarning;
                }
                else if (i_Value > HighWarning)
                {
                    return eAlarmStatus.HighWarning;
                }

                return baseResult;
            }
            
            return baseResult;
        }

        /// <summary>
        /// Will not support set without alarm warnings
        /// </summary>
        /// <param name="i_LowAlarm">The i_ low alarm.</param>
        /// <param name="i_HighAlarm">The i_ high alarm.</param>
        /// <exception cref="System.NotSupportedException">Please use overloaded Set</exception>
        public override void Set(decimal? i_LowAlarm, decimal? i_HighAlarm)
        {
            throw new NotSupportedException("Please use overloaded Set");
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="SensorAlarmWarning"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected SensorAlarmWarning(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            LowWarning = (decimal)info.GetValue("LowWarning", typeof(decimal));
            HighWarning = (decimal)info.GetValue("HighWarning", typeof(decimal));
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);

            info.AddValue("LowWarning", LowWarning, typeof(decimal));
            info.AddValue("HighWarning", HighWarning, typeof(decimal));
        }

        #endregion
    }
}
