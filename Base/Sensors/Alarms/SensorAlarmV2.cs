﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Types;

namespace Base.Sensors.Alarms
{
    [Serializable]
    public class SensorAlarmV2 : ISerializable
    {
        #region Fields
        internal SensorAlarmManagerV2 AlarmManager { get { return parentSensor.ParentSensorManager.AlarmManager; } }
        internal event SensorStatusChangedDelegate OnSensorStatusChanged;

        private GenericSensorV2 parentSensor;
        private eAlarmStatus status;
        private Timer delayTimer;
        private eAlarmStatus prevStatus;

        #endregion

        #region Properties

        public bool PreLowEnabled { get; set; }
        public bool LowEnabled { get; set; }
        public bool PreHighEnabled { get; set; }
        public bool HighEnabled { get; set; }

        public uint PreLowAllowDuration { get; set; }
        public uint LowAllowDuration { get; set; }
        public uint PreHighAllowDuration { get; set; }
        public uint HighAllowDuration { get; set; }

        public decimal PreLow
        {
            get;
            private set;
        }

        public decimal Low
        {
            get;
            private set;
        }

        public decimal PreHigh
        {
            get;
            private set;
        }

        public decimal High
        {
            get;
            private set;
        }

        public eAlarmStatus Status
        {
            get { return status; }
            private set
            {
                prevStatus = status;
                status = value;
            }
        }

        #endregion

         #region Constructors

        internal SensorAlarmV2(GenericSensorV2 parent)
        {
            Initialize(parent);
        }

        private void Initialize(GenericSensorV2 parent)
        {
            parentSensor = parent;
            PreLow = decimal.MinValue;
            Low = decimal.MinValue;
            PreHigh = decimal.MaxValue;
            High = decimal.MaxValue;

            PreLowAllowDuration = 0;
            LowAllowDuration = 0;
            PreHighAllowDuration = 0;
            HighAllowDuration = 0;

            Status = default(eAlarmStatus);

            delayTimer = new Timer(notifyStatusChange);
        }

        #endregion

        #region Methods
        internal eAlarmStatus CheckSampleAndNotifyOnAlarm(eAlarmStatus alarmStatus)
        {
            Status = alarmStatus;

            if (statusChanged())
                DelayNotificationOfStatusChange();

            return Status;
        }

        private bool statusChanged()
        {
            return status != prevStatus;
        }

        private void DelayNotificationOfStatusChange()
        {
            if (AlarmManager.Delay > 0)
            {
                delayTimer.Change(AlarmManager.Delay, AlarmManager.Delay);
            }
            else
            {
                notifyStatusChange(null);
            }
        }

        private void notifyStatusChange(object sender)
        {
            if (sender is Timer)
            {
                delayTimer.Change(Timeout.Infinite, Timeout.Infinite);
            }

            var e = generateAlarmStatusArgs();
            Utilities.InvokeEvent(OnSensorStatusChanged, this, e, null);
        }

        private SensorAlarmStatusArgs generateAlarmStatusArgs()
        {
            return new SensorAlarmStatusArgs(Status, prevStatus);
        }

        internal void Assign(AlarmV2 alarm)
        {
            /*Enabled = alarm.Enabled;
            if (Enabled)
            {
                Low = alarm.Low;
                High = alarm.High;
            }*/
        }
        #endregion

        protected SensorAlarmV2(SerializationInfo info, StreamingContext ctxt)
        {
            parentSensor = (GenericSensorV2)info.GetValue("parentSensor", typeof(GenericSensorV2));
            HighEnabled = (bool)info.GetValue("HighEnabled", typeof(bool));
            LowEnabled = (bool)info.GetValue("LowEnabled", typeof(bool));
            Low = (decimal)info.GetValue("Low", typeof(decimal));
            High = (decimal)info.GetValue("High", typeof(decimal));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("parentSensor", parentSensor, typeof(GenericSensorV2));
            info.AddValue("HighEnabled", HighEnabled , typeof(bool));
            info.AddValue("LowEnabled", LowEnabled, typeof(bool));
            info.AddValue("Low", Low, typeof(decimal));
            info.AddValue("High", High, typeof(decimal));
        }
    }
}
