﻿using Auxiliary.Tools;
using Base.Sensors.Management;
using Log4Tech;
using System;
using System.Linq;

namespace Base.Sensors.Alarms
{
    public delegate void LoggerStatusChangedDelegate(object sender, SensorAlarmStatusArgs e);

    public enum eAlarmStatus
    {
        Normal,
        Alarm,
        HighAlarm,
        HighWarning,
        LowAlarm,
        LowWarning,
    }

    [Serializable]
    public sealed class SensorAlarmManager
    {
        internal event LoggerStatusChangedDelegate OnLoggerStatusChanged;
    
        private eAlarmStatus status = eAlarmStatus.Normal;
        private SensorManager parentSensorManager;

        public eAlarmStatus AggregateAlarm
        {
            get;
            protected set;
        }

        public eAlarmStatus Status 
        {
            get { return status; }
            private set
            {
                if (status != value)
                {
                    var previousValue = status;
                    status = value;
                    if (status != eAlarmStatus.Normal)
                        AggregateAlarm = status;
                    notifyStatusChange(previousValue);
                }

                status = value;
            }
        }

        public ushort Delay { get; set; }

        public ushort WarningDelay { get; set; }

        public ushort Duration { get; set; }

        public bool SoundAlarmOnWarning { get; set; }

        internal SensorAlarmManager(SensorManager parent)
        {
            parentSensorManager = parent;
        }

        #region Events
       
        private void Alarm_SensorStatusChangedEvent(object sender, SensorAlarmStatusArgs e)
        {
            var sensorsInAlarm = (from sensor in parentSensorManager.GetAll()
                                  where sensor.Alarm.Status != eAlarmStatus.Normal
                                  select sensor).ToArray();
            Status = sensorsInAlarm.Count() > 0 ? eAlarmStatus.Alarm : eAlarmStatus.Normal;
        }

        private void notifyStatusChange(eAlarmStatus previousValue)
        {
            var e = new SensorAlarmStatusArgs(Status, previousValue);
            //Utilities.InvokeEvent(OnLoggerStatusChanged, parentSensorManager.ParentLogger, e, Log.Log.HandlerFailed);
            Utilities.InvokeEvent(OnLoggerStatusChanged, parentSensorManager.ParentLogger, e, null);
        }
        #endregion

        internal void RegisterForStatusChange(SensorAlarm alarm)
        {
            alarm.OnSensorStatusChanged += Alarm_SensorStatusChangedEvent;
        }

        internal void UnregisterForStatusChange(SensorAlarm alarm)
        {
            alarm.OnSensorStatusChanged -= Alarm_SensorStatusChangedEvent;
        }
    }
}
