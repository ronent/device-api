﻿using Auxiliary.Tools;
using Base.OpCodes;
using Base.Sensors.Types;
using Log4Tech;
using System;
using System.Runtime.Serialization;
using System.Threading;

namespace Base.Sensors.Alarms
{
    public delegate void SensorStatusChangedDelegate(object sender, SensorAlarmStatusArgs e);

    [Serializable]
    public class SensorAlarm : ISerializable, IDisposable
    {
        #region Fields
        internal SensorAlarmManager AlarmManager { get { return parentSensor.ParentSensorManager.AlarmManager; } }
        internal event SensorStatusChangedDelegate OnSensorStatusChanged;

        private GenericSensor parentSensor;
        private eAlarmStatus status;
        private Timer delayTimer;
        private eAlarmStatus prevStatus;

        #endregion
        #region Properties
      
        public bool Enabled { get; set; }

        public decimal Low
        {
            get;
            private set;
        }

        public decimal High
        {
            get;
            private set;
        }

        public eAlarmStatus Status 
        { 
            get {return status;}
            private set
            {
                prevStatus = status;
                status = value;
            }
        }

        #endregion
        #region Constructors
       
        internal SensorAlarm(GenericSensor parent)
        {
            Initialize(parent);
        }

        private void Initialize(GenericSensor parent)
        {
            this.parentSensor = parent;
            Low = decimal.MinValue;
            High = decimal.MaxValue;
            Status = default(eAlarmStatus);
            delayTimer = new Timer(new TimerCallback(notifyStatusChange));
        }

        #endregion
        #region Methods
        internal eAlarmStatus CheckSampleAndNotifyOnAlarm(decimal i_Value, bool i_NotifyAlarm)
        {
            Status = checkLowHighAlarms(i_Value);

            if (i_NotifyAlarm && statusChanged())
            {
                DelayNotificationOfStatusChange();
            }

            return Status;
        }

        private bool statusChanged()
        {
            return status != prevStatus;
        }

        private void DelayNotificationOfStatusChange()
        {
            if (AlarmManager.Delay > 0)
	        {
                delayTimer.Change(AlarmManager.Delay, AlarmManager.Delay);
	        }
            else
            {
                notifyStatusChange(null);
            }
        }

        private void notifyStatusChange(object sender)
        {
            if (sender is System.Threading.Timer)
            {
                delayTimer.Change(Timeout.Infinite, Timeout.Infinite);                
            }

            var e = generateAlarmStatusArgs();
           // Utilities.InvokeEvent(OnSensorStatusChanged, this, e, Log.Log.HandlerFailed);
            Utilities.InvokeEvent(OnSensorStatusChanged, this, e, null);
        }

        private SensorAlarmStatusArgs generateAlarmStatusArgs()
        {
            return new SensorAlarmStatusArgs(Status, prevStatus);
        }

        internal void Assign(Alarm alarm)
        {
            Enabled = alarm.Enabled;
            if (Enabled)
            {
                Low = alarm.Low;
                High = alarm.High;
            }
        }
        #endregion
        #region Virtual Methods
        public virtual void Set(decimal? i_LowAlarm, decimal? i_HighAlarm)
        {
            if (i_LowAlarm == null)
            {
                Low = decimal.MinValue;
            }
            else
            {
                if (i_LowAlarm < parentSensor.Minimum)
                {
                    throw new ArgumentException("Low alarm is smaller than sensor minimum", "i_LowAlarm");
                }

                Low = i_LowAlarm.Value;
            }

            if (i_HighAlarm == null)
            {
                High = decimal.MaxValue;
            }
            else
            {
                if (i_HighAlarm > parentSensor.Maximum)
                {
                    throw new ArgumentException("High alarm is bigger than sensor minimum", "i_HighAlarm");
                }

                High = i_HighAlarm.Value;
            }

            if (i_LowAlarm == null && i_HighAlarm == null)
            {
                Enabled = false;
            }
            else
            {
                if (Low >= High)
                {
                    Low = decimal.MinValue;
                    High = decimal.MaxValue;
                    Enabled = false;

                    throw new ArgumentException("Low alarm is bigger than high alarm");
                }
                else
                {
                    Enabled = true;                    
                }
            }
        }

        internal virtual eAlarmStatus checkLowHighAlarms(decimal i_Value)
        {
            eAlarmStatus statusToReturn = eAlarmStatus.Normal;

            if (i_Value <= Low)
            {
                statusToReturn = eAlarmStatus.LowAlarm;
            }
            else if (i_Value >= High)
            {
                statusToReturn = eAlarmStatus.HighAlarm;
            }

            return statusToReturn;
        }

        #endregion
        #region Serialization & Deserialization
       
        protected SensorAlarm(SerializationInfo info, StreamingContext ctxt)
        {
            parentSensor = (GenericSensor)info.GetValue("parentSensor", typeof(GenericSensor));
            Enabled = (bool)info.GetValue("Enabled", typeof(bool));
            Low = (decimal)info.GetValue("Low", typeof(decimal));
            High = (decimal)info.GetValue("High", typeof(decimal));
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("parentSensor", parentSensor, typeof(GenericSensor));
            info.AddValue("Enabled", Enabled, typeof(bool));
            info.AddValue("Low", Low, typeof(decimal));
            info.AddValue("High", High, typeof(decimal));
        }
        #endregion
        #region IDisposable
        public void Dispose()
        {
        }
        #endregion
    }
}
