﻿using Base.Devices;
using Base.Sensors.Management;
using Base.Sensors.Types;
using System;

namespace Base.Sensors.Samples
{
    [Serializable]
    public sealed class SampleAddedEventArgs : EventArgs 
    {
      
        public Sample Sample { get; private set; }

        public eSensorType Type { get; private set; }

        public string SerialNumber { get; private set; }

        public string Name { get; private set; }

        public bool IsTimeStamp
        {
            get { return Sample is TimeStamp; }
        }

        public eSensorIndex Index { get; private set; }

        public string Unit { get; private set; }

        public SampleAddedEventArgs(GenericLogger logger, GenericSensor sensor, Sample sample)
        {
            SerialNumber = logger.Status.SerialNumber;
            Type = sensor.Type;
            Sample = sample;
            Name = sensor.Name;
            Index = sensor.Index;
            Unit = sensor.Unit.Name;
        }
    }
}
