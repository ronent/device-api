﻿using Auxiliary.Tools;
using Base.Sensors.Alarms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Base.Sensors.Samples
{
    [Serializable]
    public class TimeStamp : Sample, ISerializable
    {
        internal TimeStamp(DateTime i_Date, decimal i_Value, eAlarmStatus i_AlarmStatus, string i_Comment)
            : base(i_Date, i_Value, i_AlarmStatus)
        {
            Comment = i_Comment;
        }

        internal TimeStamp(DateTime i_Date, string i_Comment)
            :base(i_Date)
        {
            Comment = i_Comment;
        }

        /// <summary>
        /// Gets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string Comment { get; internal set; }

        #region Object Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            if (IsDummy)
                return "( DUMMY" + ", " + Date.ToString() + ", " + Comment + " )";
            else
                return "( " + Value.ToString("0.0#") + ", " + AlarmStatus + ", " + Date.ToString() + ", " + Comment + " )";
        }
        #endregion
                #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="Sample"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected TimeStamp(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {
            Comment = (string)IO.GetSerializationValue<string>(ref info, "Comment");
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("Comment", Comment);
        }
        #endregion
    }
}
