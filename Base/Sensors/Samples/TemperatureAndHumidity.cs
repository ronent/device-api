﻿using Auxiliary.Tools;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Samples
{
    /// <summary>
    /// Temperature And Humidity.
    /// In order to calculate dew point
    /// </summary>
    [Serializable]
    public class TemperatureAndHumidity : ISerializable
    {
        /// <summary>
        /// Gets the temperature sample.
        /// </summary>
        /// <value>
        /// The temperature.
        /// </value>
        public Sample Temperature { get; private set; }

        /// <summary>
        /// Gets the humidity sample.
        /// </summary>
        /// <value>
        /// The humidity.
        /// </value>
        public Sample Humidity { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="TemperatureAndHumidity"/> class.
        /// </summary>
        /// <param name="i_Temperature">The i_ temperature.</param>
        /// <param name="i_Humidity">The i_ humidity.</param>
        /// <exception cref="System.Exception">Temperature and Humidity DateTime are different</exception>
        internal TemperatureAndHumidity(Sample i_Temperature, Sample i_Humidity)
        {
            if (i_Temperature.Date == i_Humidity.Date)
            {
                Temperature = i_Temperature;
                Humidity = i_Humidity;
            }
            else
            {
                throw new Exception("Temperature and Humidity DateTime are different");
            }
        }

        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="TemperatureAndHumidity"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="context">The context.</param>
        protected TemperatureAndHumidity(SerializationInfo info, StreamingContext context)
        {
            Temperature = (Sample)IO.GetSerializationValue<Sample>(ref info, "Temperature");
            Humidity = (Sample)IO.GetSerializationValue<Sample>(ref info, "Humidity");
        }

        /// <summary>
        /// Populates a <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with the data needed to serialize the target object.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> to populate with data.</param>
        /// <param name="context">The destination (see <see cref="T:System.Runtime.Serialization.StreamingContext" />) for this serialization.</param>
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Temperature", Temperature);
            info.AddValue("Humidity", Humidity);            
        }

        #endregion
    }
}
