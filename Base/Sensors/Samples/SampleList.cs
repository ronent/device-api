﻿using Base.Sensors.Types;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Base.Sensors.Samples
{
     public delegate void SampleAddedDelegate(object sender, SampleAddedEventArgs e);

    [Serializable]
    public sealed class SampleList : IDisposable
    {
        #region Events
        public event SampleAddedDelegate OnSampleAdded;

        #endregion
        #region Fields
        private List<Sample> items = new List<Sample>();
        private GenericSensor parentSensor;

        #endregion
        #region Properties
        public int Count { get { return items.Count; } }

        #endregion
        #region Constructors
        
        internal SampleList(GenericSensor parent)
        {
            Initialize(parent);
        }

        internal void Initialize(GenericSensor parent)
        {
            parentSensor = parent;
        }

        #endregion
        #region Methods
        public ReadOnlyCollection<Sample> Samples
        {
            get { return items.AsReadOnly(); }
        }

        internal void Sort()
        {
            items = items.OrderBy(sample => sample.Date).ToList();
        }

        internal void Notify(Sample sample)
        {
            if (OnSampleAdded != null)
                OnSampleAdded.Invoke(this, new SampleAddedEventArgs(parentSensor.ParentSensorManager.ParentLogger, parentSensor, sample));
        }

        internal void Add(Sample sample)
        {
            items.Add(sample);
        }

        internal void AddOnline(long digitalValue, DateTime time, bool isDummy = false)
        {
            Notify(GenerateSample(digitalValue, time, isDummy));
        }

        internal void AddOffline(long digitalValue, DateTime time, bool isDummy = false)
        {
            Add(GenerateSample(digitalValue, time, isDummy));
        }

        internal void AddOnline(long digitalValue, DateTime time, string comment, bool isDummy = false)
        {
            Notify(GenerateTimeStamp(digitalValue, time, comment, isDummy));
        }

        internal void AddOffline(long digitalValue, DateTime time, string comment, bool isDummy = false)
        {
            Add(GenerateTimeStamp(digitalValue, time, comment, isDummy));
        }

        private Sample GenerateSample(long digitalValue, DateTime time, bool isDummy = false)
        {
            Sample sample = parentSensor.GenerateSample(digitalValue, time, true, isDummy);
            parentSensor.LastSample = sample;
            return sample;
        }

        private Sample GenerateTimeStamp(long digitalValue, DateTime time, string comment, bool isDummy = false)
        {
            Sample timeStamp = parentSensor.GenerateTimeStamp(digitalValue, time, comment, true, isDummy);
            parentSensor.LastTimeStamp = timeStamp;
            return timeStamp;
        }

        public void Clear()
        {
            items.Clear();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            OnSampleAdded = null;
            parentSensor = null;
            Clear();
        }
        #endregion
    }
}
