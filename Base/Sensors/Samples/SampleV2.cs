﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Base.Sensors.Alarms;

namespace Base.Sensors.Samples
{
    [Serializable]
    public class SampleV2
    {
        public enum eErrorCodes
        {
            NoError=0,
            NotConnected=1,
        }

         #region Properties
        public float Value { get; internal set; }

        public uint Date { get; internal set; }

        public eAlarmStatus AlarmStatus { get; internal set; }
        
        public bool LightStatus { get; internal set; }

        public eErrorCodes ErrorCode { get; internal set; }

        #endregion
        #region Constructor
        
        internal SampleV2(uint i_Date, float i_Value, eAlarmStatus alarmStatus,bool lightStatus, eErrorCodes errorCode)
        {
            Date = i_Date;
            Value = i_Value;
            AlarmStatus = alarmStatus;
            LightStatus = lightStatus;
            ErrorCode = errorCode;
            
        }

        #endregion
        #region Override Methods
       
        public override bool Equals(object obj)
        {
            var sample = obj as SampleV2;
            if (obj == null)
            {
                return false;
            }

            return sample == null || (sample.AlarmStatus.Equals(AlarmStatus) && sample.Date.Equals(Date) && sample.Value.Equals(Value));
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
        
        #region Serialization & Deserialization
        protected SampleV2(SerializationInfo info, StreamingContext ctxt)
        {
            Value = IO.GetSerializationValue<float>(ref info, "Value");
            Date = IO.GetSerializationValue<uint>(ref info, "Date");
            AlarmStatus = IO.GetSerializationValue<eAlarmStatus>(ref info, "AlarmStatus");
            LightStatus = IO.GetSerializationValue<bool>(ref info, "LightStatus");
            ErrorCode = IO.GetSerializationValue<eErrorCodes>(ref info, "ErrorCode");
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Value", Value);
            info.AddValue("Date", Date);
            info.AddValue("AlarmStatus", AlarmStatus);
            info.AddValue("LightStatus", LightStatus);
            info.AddValue("ErrorCode", ErrorCode);
        }
        #endregion
        #region Object Overrides
        public override string ToString()
        {
            return "( " + new DateTime(1970,1,1).AddSeconds(Date) + ", " +  Value.ToString("0.0#") + ", " + AlarmStatus + ", " + LightStatus + ", " + ErrorCode + " )";
        }

        #endregion
    }
}
