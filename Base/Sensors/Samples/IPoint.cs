﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Sensors.Samples
{
    public interface IPoint
    {
        double X { get; }
        double Y { get; }
    }
}
