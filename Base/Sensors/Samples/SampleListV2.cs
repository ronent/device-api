﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Alarms;
using Base.Sensors.Types;

namespace Base.Sensors.Samples
{
      public delegate void SampleV2AddedDelegate(object sender, SampleArrivedV2EventArgs e);

    [Serializable]
    public sealed class SampleListV2
    {
        #region Events
        public event SampleV2AddedDelegate OnSampleAdded;

        #endregion
        #region Fields
        private List<SampleV2> items = new List<SampleV2>();
        private GenericSensorV2 parentSensor;

        #endregion
        #region Properties
        public int Count { get { return items.Count; } }

        #endregion
        #region Constructors
        
        internal SampleListV2(GenericSensorV2 parent)
        {
            Initialize(parent);
        }

        internal void Initialize(GenericSensorV2 parent)
        {
            parentSensor = parent;
        }

        #endregion
        #region Methods
        public ReadOnlyCollection<SampleV2> Samples
        {
            get { return items.AsReadOnly(); }
        }

        internal void Sort()
        {
            items = items.OrderBy(sample => sample.Date).ToList();
        }

        internal void Notify(SampleV2 sample)
        {
            if (OnSampleAdded != null)
                OnSampleAdded.Invoke(this, new SampleArrivedV2EventArgs(parentSensor.ParentSensorManager.ParentLogger, parentSensor, sample));
        }

        internal void Add(SampleV2 sample)
        {
            items.Add(sample);
        }

        internal void AddOnline(float value, uint time, eAlarmStatus alarmStatus,  bool lightStatus, SampleV2.eErrorCodes errorCode, bool isTimeStamp = false)
        {
            Notify(GenerateSample(value, time, alarmStatus, lightStatus, errorCode, isTimeStamp));
        }

        internal void AddOffline(float value, uint time, eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isTimeStamp = false)
        {
            Add(GenerateSample(value, time, alarmStatus, lightStatus, errorCode, isTimeStamp));
        }

        private SampleV2 GenerateSample(float value, uint time, eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isTimeStamp = false)
        {
            var sample = parentSensor.GenerateSample(value, time, alarmStatus, lightStatus, errorCode, isTimeStamp);
            parentSensor.LastSample = sample;
            return sample;
        }

        public void Clear()
        {
            items.Clear();
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            OnSampleAdded = null;
            parentSensor = null;
            Clear();
        }
        #endregion
    }
}
