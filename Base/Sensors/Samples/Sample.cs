﻿using Auxiliary.Tools;
using Base.Sensors.Alarms;
using System;
using System.Runtime.Serialization;

namespace Base.Sensors.Samples
{
    /// <summary>
    /// Logging Sample.
    /// </summary>
    [Serializable]
    public class Sample : IPoint, ISerializable
    {
        #region Properties
        public decimal Value { get; internal set; }

        public DateTime Date { get; internal set; }

        public eAlarmStatus AlarmStatus { get; internal set; }

        public bool IsTimeStamp { get { return this is TimeStamp; } }

        public bool IsDummy { get; private set; }

        #endregion
        #region Constructor
        
        public  Sample(DateTime i_Date, decimal i_Value, eAlarmStatus i_AlarmStatus)
        {
            Date = i_Date;
            Value = i_Value;
            AlarmStatus = i_AlarmStatus;
            IsDummy = false;
        }

        public Sample(DateTime i_Date)
            :this(i_Date, decimal.MaxValue, eAlarmStatus.Normal)
        {
            IsDummy = true;
        }

        #endregion
        #region Override Methods
       
        public override bool Equals(object obj)
        {
            Sample sample = obj as Sample;
            if (obj == null)
            {
                return false;
            }

            if (!sample.AlarmStatus.Equals(this.AlarmStatus) || !sample.Date.Equals(this.Date) || !sample.Value.Equals(this.Value))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
        #region IPoint 
        double IPoint.X
        {
            get { return Date.Subtract(new DateTime(2010, 1, 1)).TotalSeconds; }
        }

        double IPoint.Y
        {
            get { return Convert.ToDouble(Value); }
        }

        #endregion
        #region Serialization & Deserialization
        protected Sample(SerializationInfo info, StreamingContext ctxt)
        {
            Value = (decimal)IO.GetSerializationValue<decimal>(ref info, "Value");
            Date = (DateTime)IO.GetSerializationValue<DateTime>(ref info, "Date");
            //Comment = (string)IO.GetSerializationValue<string>(ref info, "Comment");
            AlarmStatus = (eAlarmStatus)IO.GetSerializationValue<eAlarmStatus>(ref info, "AlarmStatus");
            IsDummy = (bool)IO.GetSerializationValue<bool>(ref info, "IsDummy");
        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Value", Value);
            info.AddValue("Date", Date);
            //info.AddValue("Comment", Comment);
            info.AddValue("AlarmStatus", AlarmStatus);
            info.AddValue("IsDummy", IsDummy);
        }
        #endregion
        #region Object Overrides
        public override string ToString()
        {
            if (IsDummy)
                return "( DUMMY" + ", " + Date.ToString() + " )";
            else
                return "( " + Value.ToString("0.0#") + ", " + AlarmStatus + ", " + Date.ToString() + " )";
        }

        #endregion
    }
}
