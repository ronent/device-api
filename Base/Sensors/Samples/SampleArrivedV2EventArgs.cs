﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Types;
using Base.Sensors.Management;
using Base.Sensors.Types;

namespace Base.Sensors.Samples
{
    public sealed class SampleArrivedV2EventArgs : EventArgs 
    {
        public SampleV2 Sample { get; private set; }
        
        public eSensorType Type { get; private set; }

        public string SerialNumber { get; private set; }

        public string Name { get; private set; }

        public bool IsTimeStamp
        {
            get { return Sample is TimeStamp; }
        }

        public eSensorIndex Index { get; private set; }

        public string Unit { get; private set; }

        public SampleArrivedV2EventArgs(GenericLoggerV2 logger, GenericSensorV2 sensor, SampleV2 sample)
        {
            SerialNumber = logger.Status.SerialNumber;
            Type = sensor.Type;
            Sample = sample;
            Name = sensor.Name;
            Index = sensor.Index;
            Unit = sensor.Unit!=null ? sensor.Unit.Name : "RH";
        }
    }
}
