﻿using System;

namespace Base.Sensors.Management
{
    /// <summary>
    /// Enable Event Arguments.
    /// </summary>
    public sealed class EnableEventArgs : EventArgs
    {
        public bool Enabled { get; set; }
    }
}
