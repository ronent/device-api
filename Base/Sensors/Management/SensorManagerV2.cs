﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using Base.Devices.Types;
using Base.Sensors.Alarms;
using Base.Sensors.Types;

namespace Base.Sensors.Management
{
    [Serializable]
    public abstract class SensorManagerV2
    {
         #region Fields
        internal List<GenericSensorV2> Items { get; set; }

        public SensorAlarmManagerV2 AlarmManager { get; protected set; }

        internal GenericLoggerV2 ParentLogger { get; set; }

        #endregion
        #region Constructors
        internal SensorManagerV2(GenericLoggerV2 parent)
        {
            Initialize(parent);
        }

        internal virtual void Initialize(GenericLoggerV2 parent)
        {
            ParentLogger = parent;
            Items = new List<GenericSensorV2>();
            AlarmManager = new SensorAlarmManagerV2(this);
            ParentLogger.Functions.OnStatusReceived += ParentLogger_OnStatusReceived;
        }

        #endregion
        #region Properties
        internal bool IsEmpty { get { return !Items.Any(); } }

        public bool FahrenheitMode
        {
            get { return ParentLogger.Status.FahrenheitMode; }
        }

        public bool HasData
        {
            get
            { return EnabledSensors.Any(); }
        }

        public IEnumerable<GenericSensorV2> EnabledSensors
        {
            get
            {
                return from sensor in Items
                       where sensor.Enabled
                       select sensor;
            }
        }

        #endregion
        #region Methods
        internal void AddRange(IEnumerable<GenericSensorV2> items)
        {
            AddRange(items.ToArray());
        }

        internal void AddRange(GenericSensorV2[] items)
        {
            foreach (var item in items)
                Add(item);
        }

        internal void Add(GenericSensorV2 item)
        {
            Items.Add(item);
            if (ParentLogger.Functions != null)
                item.samples.OnSampleAdded += ParentLogger.Functions.InvokeOnSampleAdded;
            AlarmManager.RegisterForStatusChange(item.Alarm);
        }

        internal void Remove(GenericSensorV2 item)
        {
            Items.Remove(item);
            item.samples.OnSampleAdded -= ParentLogger.Functions.InvokeOnSampleAdded;
            AlarmManager.UnregisterForStatusChange(item.Alarm);
        }

        internal abstract void ParentLogger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e);

        internal void InitializeSensors()
        {
            AddRange(CreateFixedSensors());
            AddRange(CreateDetachableSensors());
        }

        internal GenericSensorV2 CreatePredefined(eSensorType sensorType)
        {
            var type = GetSensorClassType(sensorType);

            return (GenericSensorV2)Activator.CreateInstance(type, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { this }, null);
        }

        internal IEnumerable<GenericSensorV2> CreateFixedSensors()
        {
            return ParentLogger.AvailableFixedSensors.Select(CreatePredefined);
        }

        internal IEnumerable<GenericSensorV2> CreateDetachableSensors()
        {
            return ParentLogger.AvailableDetachableSensors.Select(CreatePredefined);
        }

        internal Type GetSensorClassType(eSensorType sensorType)
        {
            var asm = ParentLogger.GetType().Assembly;
            var fullname = ParentLogger.GetType().Namespace;

            var root = fullname.Substring(0, fullname.IndexOf('.') + 1);
            var sensors = STRUCTURE.SENSORS + ".";
            var version = ParentLogger.ClassVersion;
            var name = "." + sensorType;

            var type = asm.GetType(root + sensors + version + name);
            while (type == null)
            {
                type = asm.GetType(root + sensors + downgrade(version) + name);
            }

            return type;
        }

        private string downgrade(string name)
        {
            int version;
            if (int.TryParse(name.Replace("V", ""), out version))
            {
                version--;
                if (version <= 0)
                    return string.Empty;
                return "V" + version;
            }

            return string.Empty;
        }

        internal void Sort()
        {
            foreach (var sensor in Items)
            {
                sensor.samples.Sort();
            }
        }

        internal void Clear()
        {
            foreach (var sensor in Items)
            {
                sensor.ClearData();
            }
        }

        #endregion
        #region Get Methods
        public IEnumerable<GenericSensorV2> GetAll()
        {
            return Items;
        }

        public IEnumerable<GenericSensorV2> GetAllEnabled()
        {
            return Items.FindAll(x => x.Enabled);
        }

        public IEnumerable<GenericSensorV2> this[eSensorType type]
        {
            get
            {
                return Items.FindAll(x => x.Type == type);
            }
        }

        public GenericSensorV2 this[eSensorIndex index]
        {
            get
            {
                return Items.FindAll(x => x.Index == index).FirstOrDefault();
            }
        }

        public IEnumerable<GenericSensorV2> GetAllDetachables() { try { return Items.FindAll(x => x.Index >= eSensorIndex.External1); } catch { return new List<GenericSensorV2> { }; } }

        public IEnumerable<GenericSensorV2> GetAllFixed() { try { return Items.FindAll(x => x.Index < eSensorIndex.External1); } catch { return new List<GenericSensorV2> { }; } }

        public GenericSensorV2 GetFixedByIndex(eSensorIndex index)
        {
            try
            {
                return Items.SingleOrDefault(x => x.Index == index);
            }
            catch { return null; }
        }

        public GenericSensorV2 GetDetachableByType(eSensorType type)
        {
            try
            {
                return Items.SingleOrDefault(x =>
                    x.Index == eSensorIndex.External1
                    && x.Type == type);
            }
            catch { return null; }
        }

        public GenericSensorV2 GetEnabledDetachable()
        {
            try
            {
                return Items.SingleOrDefault(x =>
                    x.Index == eSensorIndex.External1
                    && x.Enabled);
            }
            catch { return null; }
        }

        #endregion
    }
}
