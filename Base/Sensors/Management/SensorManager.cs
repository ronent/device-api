﻿using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Base.Sensors.Alarms;
using Base.Sensors.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Base.Sensors.Management
{
    [Serializable]
    public abstract class SensorManager
    {
        #region Fields
        internal List<GenericSensor> Items { get; set; }

        public SensorAlarmManager AlarmManager { get; protected set; }

        internal GenericLogger ParentLogger { get; set; }

        #endregion
        #region Constructors
        internal SensorManager(GenericLogger parent)
        {
            Initialize(parent);
        }

        internal virtual void Initialize(GenericLogger parent)
        {
            ParentLogger = parent;
            Items = new List<GenericSensor>();
            AlarmManager = new SensorAlarmManager(this);
            ParentLogger.Functions.OnStatusReceived += ParentLogger_OnStatusReceived;
        }

        #endregion
        #region Properties
        internal bool IsEmpty { get { return Items.Count() < 1; } }

        public bool FahrenheitMode
        {
            get { return ParentLogger.Status.FahrenheitMode; }
        }

        public bool HasData
        {
            get
            {
                foreach (GenericSensor sensor in EnabledSensors)
                {
                    if (!sensor.IsEmpty)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        public IEnumerable<GenericSensor> EnabledSensors
        {
            get
            {
                return from sensor in Items
                       where sensor.Enabled
                       select sensor;
            }
        }

        #endregion
        #region Methods
        internal void AddRange(IEnumerable<GenericSensor> items)
        {
            AddRange(items.ToArray());
        }

        internal void AddRange(GenericSensor[] items)
        {
            foreach (var item in items)
                Add(item);
        }

        internal void Add(GenericSensor item)
        {
            Items.Add(item);
            if (ParentLogger.Functions!=null)
                item.samples.OnSampleAdded += ParentLogger.Functions.InvokeOnSampleAdded;
            AlarmManager.RegisterForStatusChange(item.Alarm);
        }

        internal void Remove(GenericSensor item)
        {
            Items.Remove(item);
            item.samples.OnSampleAdded -= ParentLogger.Functions.InvokeOnSampleAdded;
            AlarmManager.UnregisterForStatusChange(item.Alarm);
        }

        internal abstract void ParentLogger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e);

        internal void InitializeSensors()
        {
            AddRange(CreateFixedSensors());
            AddRange(CreateDetachableSensors());
        }

        internal GenericSensor CreatePredefined(eSensorType sensorType)
        {
            Type type = GetSensorClassType(sensorType);

            return (GenericSensor)Activator.CreateInstance(type, BindingFlags.NonPublic | BindingFlags.Instance, null, new object[] { this }, null);
        }

        internal IEnumerable<GenericSensor> CreateFixedSensors()
        {
            foreach (var sensorType in ParentLogger.AvailableFixedSensors)
                yield return CreatePredefined(sensorType) as GenericSensor;
        }

        internal IEnumerable<GenericSensor> CreateDetachableSensors()
        {
            foreach (var sensorType in ParentLogger.AvailableDetachableSensors)
                yield return CreatePredefined(sensorType) as GenericSensor;
        }

        internal Type GetSensorClassType(eSensorType sensorType)
        {
            Assembly asm = ParentLogger.GetType().Assembly;
            string fullname = ParentLogger.GetType().Namespace;

            string root = fullname.Substring(0, fullname.IndexOf('.') + 1);
            string sensors = STRUCTURE.SENSORS + ".";
            string version = ParentLogger.ClassVersion;
            string name = "." + sensorType.ToString();

            Type type = asm.GetType(root + sensors + version + name);
            while (type == null)
            {
                type = asm.GetType(root + sensors + downgrade(version) + name);
            }

            return type;
        }

        private string downgrade(string name)
        {
            int version;
            if (int.TryParse(name.Replace("V", ""), out version))
            {
                version--;
                if (version <= 0)
                    return string.Empty;
                else
                    return "V" + version;
            }

            return string.Empty;
        }

        internal void Sort()
        {
            foreach (var sensor in Items)
            {
                sensor.samples.Sort();
            }
        }

        internal void Clear()
        {
            foreach (var sensor in Items)
            {
                sensor.ClearData();
            }
        }

        #endregion
        #region Get Methods
        public IEnumerable<GenericSensor> GetAll()
        {
            return Items;
        }

        public IEnumerable<GenericSensor> GetAllEnabled()
        {
            return Items.FindAll(x => x.Enabled);
        }

        public IEnumerable<GenericSensor> this[eSensorType type]
        {
            get
            {
                return Items.FindAll(x => x.Type == type);
            }
        }

        public GenericSensor this[eSensorIndex index]
        {
            get
            {
                return Items.FindAll(x => x.Index == index).FirstOrDefault();
            }
        }

        public IEnumerable<GenericSensor> GetAllDetachables() { try { return Items.FindAll(x => x.Index >= eSensorIndex.External1); } catch { return new List<GenericSensor> { }; } }

        public IEnumerable<GenericSensor> GetAllFixed() { try { return Items.FindAll(x => x.Index < eSensorIndex.External1); } catch { return new List<GenericSensor> { }; } }

        public GenericSensor GetFixedByIndex(eSensorIndex index)
        {
            try
            {
                return Items.SingleOrDefault(x => x.Index == index);
            }
            catch { return null; }
        }

        public GenericSensor GetDetachableByType(eSensorType type)
        {
            try
            {
                return Items.SingleOrDefault(x =>
                    x.Index == eSensorIndex.External1
                    && x.Type == type);
            }
            catch { return null; }
        }

        public GenericSensor GetEnabledDetachable()
        {
            try
            {
                return Items.SingleOrDefault(x =>
                    x.Index == eSensorIndex.External1
                    && x.Enabled);
            }
            catch { return null; }
        }

        #endregion
    }
}
