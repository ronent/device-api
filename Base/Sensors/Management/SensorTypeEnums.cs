﻿
namespace Base.Sensors.Management
{
    /// <summary>
    /// Sensor Types.
    /// </summary>
    public enum eSensorType : byte
    {
        None = 0,
        InternalNTC,
        DigitalTemperature,
        Humidity,
        DewPoint,
        Current4_20mA,
        Voltage0_10V,
        ExternalNTC,
        PT100,
        UserDefined,
        DryIce
    }

    /// <summary>
    /// Sensor Index.
    /// </summary>
    public enum eSensorIndex : byte
    {
        Temperature = 0,
        Humidity = 1,
        DewPoint = 2,
        External1 = 3,
        External2 = 4,
        External3 = 5,
        External4 = 6,
    }
}
