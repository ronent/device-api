﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Settings
{
    internal enum eEnvironment
    {
        User,
        Producation,
    }

    internal enum eCommunicationModule
    {
        HID,
        TCP,
        USB,
        WCF,
    }

    internal class EnvironmentSettings
    {
        public eEnvironment Name { get; set; }
        public bool UseRestrictCommunicationModules { get; set; }
        public List<eCommunicationModule> RestrictCommunicationModules { get; set; }
        public bool ShowLog { get; set; }
    }
}
