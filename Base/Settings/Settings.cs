﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Base.Settings
{
    internal static class Settings
    {
        public static eEnvironment Enviroment
        {
            get { return eEnvironment.Producation; }
        }

        #region Constructor
        static Settings()
        {
            AllEnvironments = new List<EnvironmentSettings>();
            addEnviromentsSettings();
        }

        #endregion
        #region Properties
        public static List<EnvironmentSettings> AllEnvironments { get; private set; }
        public static EnvironmentSettings Current
        {
            get
            {
                return AllEnvironments.Find((env) => env.Name == Enviroment);
            }
        }

        #endregion
        #region Methods
        private static void addEnviromentsSettings()
        {
            AllEnvironments.Add(new EnvironmentSettings()
            {
                Name = eEnvironment.Producation,
                UseRestrictCommunicationModules = false,
                RestrictCommunicationModules = get(eEnvironment.Producation),
                ShowLog = false,
            });

            AllEnvironments.Add(new EnvironmentSettings()
            {
                Name = eEnvironment.User,
                UseRestrictCommunicationModules = false,
                RestrictCommunicationModules = get(eEnvironment.User),
                ShowLog = true,
            });
        }

        private static List<eCommunicationModule> get(eEnvironment enviroment)
        {
            var list = new List<eCommunicationModule>();

            switch (enviroment)
            {
                case eEnvironment.User:
                    //list.Add(eCommunicationModule.HID);
                    //list.Add(eCommunicationModule.USB);
                    //list.Add(eCommunicationModule.WCF);
                    //list.Add(eCommunicationModule.TCP);
                    break;
                case eEnvironment.Producation:
                    //list.Add(eCommunicationModule.HID);
                    //list.Add(eCommunicationModule.USB);
                    //list.Add(eCommunicationModule.WCF);
                    //list.Add(eCommunicationModule.TCP);
                    break;
                default:
                    break;
            }

            return list;
        }

        #endregion
    }
}
