﻿using Auxiliary.Tools;
using Base.DataStructures.Device;
using Base.Devices;
using Base.Devices.Features;
using Base.Devices.Management.EventsAndExceptions;
using Base.Firmware;
using Base.OpCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Base.Functions.Management
{
    /// <summary>
    /// Manager for all device functions.
    /// </summary>
    [Serializable]
    public abstract class DeviceFunctionsManager : IDisposable, ISerializable
    {
        #region Events
        /// <summary>
        /// Occurs when new status arrived.
        /// </summary>
        public event DeviceOperationDelegate<GenericDevice> OnStatusReceived;

        /// <summary>
        /// Reports firmware update progress.
        /// </summary>
        public event ReportProgressDelegate OnFirmwareUpdateProgressReported;

        #endregion
        #region Fields
        protected const string UPDATE_ERROR = "Device is updating firmware.";
        protected const string NOT_UPDATE_ERROR = "Device isn't updating firmware.";

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceFunctionsManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        protected DeviceFunctionsManager(GenericDevice device)
        {
            IsDisposed = false;
            Device = device;
        }

        /// <summary>
        /// Subscribes the op codes events.
        /// </summary>
        protected abstract void SubscribeOpCodesEvents();

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this instance is busy.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is busy; otherwise, <c>false</c>.
        /// </value>
        public virtual bool IsBusy { get { return Device.OpCodes.IsBusy; } }

        /// <summary>
        /// Gets a value indicating whether this instance is updating firmware.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is updating firmware; otherwise, <c>false</c>.
        /// </value>
        public abstract bool IsUpdatingFirmware { get; }

        /// <summary>
        /// Gets the busy error message.
        /// </summary>
        /// <value>
        /// The busy error message.
        /// </value>
        protected abstract string BusyErrorMessage { get; }

        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        protected internal GenericDevice Device { get; set; }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public virtual IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                yield return new AvailableFunction(eDeviceFunction.Setup) { Visible = Device.IsOnline, Enabled = !IsBusy };
                yield return new AvailableFunction(eDeviceFunction.UpdateFirmware) { Visible = Device.IsOnline, Enabled = !IsBusy };
                //yield return new AvailableFunction(eDeviceFunction.CancelUpdateFirmware) { Visible = Device.IsOnline, Enabled = IsUpdatingFirmware };
            }
        }

        /// <summary>
        /// Gets the available functions.
        /// </summary>
        /// <value>
        /// The available functions.
        /// </value>
        public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                yield return eDeviceFunction.Setup;
                yield return eDeviceFunction.UpdateFirmware;
                //yield return eDeviceFunction.CancelUpdateFirmware;
            }
        }

        #endregion
        #region Functions
        /// <summary>
        /// Sends the setup.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        public abstract Task<Result> SendSetup(BaseDeviceSetupConfiguration configuration);

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <returns></returns>
        public abstract Task<Result> GetStatus();

        /// <summary>
        /// Uploads the firmware.
        /// </summary>
        /// <returns></returns>
        public abstract Task<Result> UploadFirmware();

        /// <summary>
        /// Gets the status no busy.
        /// </summary>
        /// <returns></returns>
        internal abstract Task<Result> GetStatusNoBusy();

        /// <summary>
        /// Gets the type of the device.
        /// </summary>
        /// <returns></returns>
        internal abstract Task<Result> GetDeviceType();

        #endregion
        #region Methods
        /// <summary>
        /// Initializes this instance.
        /// </summary>
        public virtual void Initialize()
        {
            SubscribeOpCodesEvents();            
        }

        /// <summary>
        /// Invokes on new status.
        /// </summary>
        protected virtual void InvokeOnStatus()
        {
            Device.Functions.InvokeOnStatusReceived();
        }

        /// <summary>
        /// Invokes the fw update progress report.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ProgressReportEventArgs"/> instance containing the event data.</param>
        protected virtual void InvokeFWUpdateProgressReport(object sender, ProgressReportEventArgs e)
        {
            Utilities.InvokeEvent(OnFirmwareUpdateProgressReported, sender, e);
        }

        /// <summary>
        /// Invokes the on status received.
        /// </summary>
        internal void InvokeOnStatusReceived()
        {
            Utilities.InvokeEvent(OnStatusReceived, this, new ConnectionEventArgs<GenericDevice> { Device = this.Device, State = eDeviceState.NONE });
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public virtual void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                OnFirmwareUpdateProgressReported = null;
                OnStatusReceived = null;
            }
        }

        #endregion
        #region Serialization & Deserialization
        public DeviceFunctionsManager(SerializationInfo info, StreamingContext context)
        {

        }

        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {
          
        }

        #endregion
    }
}
