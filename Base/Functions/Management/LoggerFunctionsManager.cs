﻿using Auxiliary.Tools;
using Base.Devices;
using Base.Devices.Features;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Calibrations;
using Base.Sensors.Samples;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Base.Functions.Management
{
    /// <summary>
    /// Manager for all loggers functions.
    /// </summary>
    [Serializable]
    public abstract class LoggerFunctionsManager : DeviceFunctionsManager
    {
        #region Events
        public event DownloadCompletedDelegate OnDownloadCompleted;

        public event SampleAddedDelegate OnOnlineSampleAdded;

        public event ReportProgressDelegate OnDownloadProgressReported;

        #endregion
        #region Fields
        protected const string DOWNLOAD_ERROR = "Device is downloading data.";
        protected const string NOT_DOWNLOAD_ERROR = "Device isn't downloading data.";
        protected const string RUNNING_ERROR = "Device is running.";
        protected const string NOT_RUNNING_ERROR = "Device isn't running.";
        protected const string TIMER_RUN_ERROR = "Device is set to timer run.";
        protected const string PUSH_TO_RUN_ERROR = "Device is set to push to run.";

        #endregion
        #region Constructor
        public LoggerFunctionsManager(GenericLogger logger)
            :base(logger)
        {

        }

        #endregion
        #region Properties
        new protected internal GenericLogger Device { get { return base.Device as GenericLogger; } set { base.Device = value; } }

        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    item.Enabled &= !IsRunning;

                    yield return item;
                }

                yield return new AvailableFunction(eDeviceFunction.Download) { Visible = Device.IsOnline, Enabled = !IsBusy };
                //yield return new AvailableFunction(eDeviceFunction.CancelDownload) { Visible = Device.IsOnline, Enabled = IsDownloading };
                yield return new AvailableFunction(eDeviceFunction.Run) { Visible = Device.IsOnline, Enabled = !IsRunning && !IsBusy && !IsSupposeToRun };
                yield return new AvailableFunction(eDeviceFunction.Stop) { Visible = Device.IsOnline, Enabled = IsRunning && !IsBusy };
                yield return new AvailableFunction(eDeviceFunction.Calibrate) { Visible = Device.IsOnline, Enabled = !IsRunning && !IsBusy && !IsSupposeToRun };
            }
        }

        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                foreach (var item in DeviceFunctionsManager.AvailableFunctions)
                {
                    yield return item;
                }

                yield return eDeviceFunction.Download;
                //yield return eDeviceFunction.CancelDownload;
                yield return eDeviceFunction.Run;
                yield return eDeviceFunction.Stop;
                yield return eDeviceFunction.Calibrate;
            }
        }

        public bool IsSupposeToRun { get { return TimerRunEnabled || PushToRunMode; } }

        public abstract bool IsRunning { get; }

        public abstract bool IsDownloading { get; }

        public abstract bool TimerRunEnabled { get; }

        public abstract bool PushToRunMode { get; }

        #endregion
        #region Functions
        public abstract Task<Result> Run();

        public abstract Task<Result> Stop();

        public abstract Task<Result> Download();

        public abstract Task<Result> ResetCalibration();

        public abstract Task<Result> SendCalibration(CalibrationConfiguration calibration);

        #endregion
        #region Functions Availability
        protected Task<Result> CheckRunningAndBusy()
        {
            if (IsRunning || IsSupposeToRun)
                return Task.FromResult(new Result(eResult.ILLEGAL_CALL) { MessageLog = RunningErrorMessage });
            
            return Task.FromResult(IsBusy ? new Result(eResult.BUSY) { MessageLog = BusyErrorMessage } : Result.OK);
        }

        protected string RunningErrorMessage
        {
            get
            {
                string toReturn = string.Empty;

                if (IsRunning)
                    toReturn = RUNNING_ERROR;
                else if (IsSupposeToRun)
                    if (TimerRunEnabled)
                        return TIMER_RUN_ERROR;
                    else if (PushToRunMode)
                        return PUSH_TO_RUN_ERROR;

                return toReturn;
            }
        }

        protected override string BusyErrorMessage
        {
            get
            {
                string toReturn = string.Empty;

                if (IsDownloading)
                    toReturn = DOWNLOAD_ERROR;
                else if (IsUpdatingFirmware)
                    toReturn = UPDATE_ERROR;

                return toReturn;
            }
        }

        #endregion
        #region Methods
        internal void InvokeOnSampleAdded(object sender, SampleAddedEventArgs e)
        {
            Utilities.InvokeEvent(OnOnlineSampleAdded, sender, e);
        }

        internal void InvokeOnDownloadProgressReported(object sender, ProgressReportEventArgs e)
        {
            //Log.Instance.Write("Percents: {0}", this, Log.LogSeverity.INFO, e.Percent);
            Utilities.InvokeEvent(OnDownloadProgressReported, sender, e);
        }

        internal virtual void InvokeOnDownloadCompleted(object sender, DownloadCompletedArgs e)
        {
            Utilities.InvokeEvent(OnDownloadCompleted, sender, e);
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {
                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public LoggerFunctionsManager(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}