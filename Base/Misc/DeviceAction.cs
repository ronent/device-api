﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Calibrations;

namespace Base.Misc
{
    public enum eDeviceActions
    {
        STOP=1,
        SETUP,
        GETDEVICE,
        RUN,
        DOWNLOAD,
        RESETCALIBRATION,
        UPLOADFIRMWARE,
        SENDCALIBRATION,
        MARKTIMESTAMP,
        RESTOREDEFAULTCALIBRATION,
        SAVEDEFAULTCALIBRATION,
    }

    [Serializable]
    public class DeviceAction
    {
        public int SerialNumber { get; set; }
        public eDeviceActions Action { get; set; }
        public dynamic Setup { get; set; }
        public List<Tuple<decimal, decimal>> CalibrationPairs { get; set; }
        public int CalibrationSensorIndex { get; set; }
    }
}
