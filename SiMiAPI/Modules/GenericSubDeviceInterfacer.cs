﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using SiMiAPI.Devices;
using SiMiBase.Devices.Management;
using SiMiBase.Modules.Interfacer;

namespace SiMiAPI.Modules
{
    internal abstract class GenericSubDeviceInterfacer<T> : SiMiBaseLoggerInterfacer<T>, ISubDeviceInterfacer
        where T : GenericSiMiLogLogger
    {
        #region Constructor

        public GenericSubDeviceInterfacer()
            : base()
        {

        }

        #endregion

        #region Methods

        public override bool CanCreate(ConnectionEventArgs e)
        {
            if (!(e is HIDConnectionEventArgs))
                return false;

            return
                DeviceIDs.Any(
                    x => x.VID.Equals(SiMiBaseLoggerManager.VID) && x.Equals((e as HIDConnectionEventArgs).DeviceID));
        }

        #endregion
    }
}
