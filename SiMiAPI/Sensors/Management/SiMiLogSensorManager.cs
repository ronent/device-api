﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Log4Tech;
using SiMiAPI.DataStructures.ESetup;
using SiMiAPI.DataStructures.THDSetup;
using SiMiAPI.Devices;
using SiMiBase.DataStructures.TSetup;
using SiMiBase.Sensors.Management;

namespace SiMiAPI.Sensors.Management
{
    [Serializable]
    public class SiMiLogSensorManager : SiMiBaseSensorManager
    {
        #region Properties
        new internal GenericSiMiLogLogger ParentLogger
        {
            get { return base.ParentLogger as GenericSiMiLogLogger; }
            set { base.ParentLogger = value; }
        }

        #endregion
        #region Constructors
        internal SiMiLogSensorManager(GenericSiMiLogLogger parent)
            : base(parent)
        {
        }

        #endregion
        #region Methods
        internal override void AssignConfiguration()
        {
            if (this.IsEmpty)
                return;

            assignTemperatureSensor();
            assignHumiditySensor();
            //assignExternalSensor();
        }

        private void assignTemperatureSensor()
        {
            var configT = ParentLogger.Status as ITSensorSetup;
            if (configT != null)
                AssignFixedSensor(eSensorIndex.Temperature, configT.TemperatureSensor.TemperatureEnabled, configT.TemperatureSensor.TemperatureAlarm);
            else
            {
                var configTHD = ParentLogger.Status as ITHDSensorSetup;
                if (configTHD != null)
                    AssignFixedSensor(eSensorIndex.Temperature, configTHD.FixedSensors.TemperatureEnabled, configTHD.FixedSensors.TemperatureAlarm);
            }
        }
        
        private void assignHumiditySensor()
        {
            var config = ParentLogger.Status as ITHDSensorSetup;
            if (config != null)
            {
                AssignFixedSensor(eSensorIndex.Humidity, config.FixedSensors.HumidityEnabled, config.FixedSensors.HumidityAlarm);
//                AssignFixedSensor(eSensorIndex.DewPoint, config.FixedSensors.DewPointEnabled, config.FixedSensors.DewPointAlarm);
            }
        }

//        private void assignExternalSensor()
//        {
//            IESensorSetup config;
//            var sensor = resolveUserDefinedSensor(out config);
//            if (sensor != null)
//                try
//                {
//                    var alarm = config.ExternalSensor.Alarm;
//
//                    if (config.ExternalSensor.Enabled)
//                        EnableDetachable(sensor.Type);
//                    else
//                        DisableAllDetachables();
//
//                    if (sensor.Enabled)
//                        sensor.Alarm.Assign(alarm);
//                }
//                catch (Exception ex) { Log.Instance.WriteError("Fail in assignExternalSensor", this, ex); }
//        }

//        private GenericSensor resolveUserDefinedSensor(out IESensorSetup config)
//        {
//            config = ParentLogger.Status as IESensorSetup;
//            //if (config.ExternalSensor.UserDefinedSensor.Name == null)
//            //{
//            //    Debugger.Break();
//            // }
//
//            if (config != null)
//                if (config.ExternalSensor.UserDefined)
//                {
//                    SetUserDefined(config.ExternalSensor.UserDefinedSensor);
//                    return GetDetachableByType(eSensorType.UserDefined);
//                }
//                else
//                {
//                    return GetDetachableByType(config.ExternalSensor.Type);
//                }
//
//            return null;
//        }

//        internal IDefinedSensor CreateUserDefinedSensor(UDSConfiguration configuration)
//        {
//            return new UserDefinedSensor(this, configuration as SiMiLogUDSConfiguration);
//        }

        
//        internal void SetUserDefined(UDSConfiguration configuration)
//        {
//            IDefinedSensor sensor = CreateUserDefinedSensor(configuration);
//            Items.RemoveAll(x => x.Type == eSensorType.UserDefined);
//            Add(sensor as GenericSensor);
//        }

//        public GenericSensor GetUserDefined()
//        {
//            try
//            {
//                return Items.SingleOrDefault(x => x.Type == eSensorType.UserDefined);
//            }
//            catch { return null; }
//        }
        #endregion
    }
}
