﻿using System;
using Base.Sensors.Management;
using Base.Sensors.Units;

namespace SiMiAPI.Sensors.V1
{
    [Serializable]
    public abstract class Humidity : SiMiBase.Sensors.Generic.Humidity
    {
        #region Constructors
        internal Humidity(SensorManagerV2 parent)
            : base(parent)
        {
        }

        #endregion

        public override eSensorType Type
        {
            get { return eSensorType.Humidity; }
        }

        public override decimal Maximum
        {
            get { return 100; }
        }

        public override decimal Minimum
        {
            get { return 0; }
        }

        public override bool USBRunnable
        {
            get { return false; }
        }

        public override eSensorIndex Index
        {
            get { return eSensorIndex.Humidity; }
        }
    }
}
