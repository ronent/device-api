﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;

namespace SiMiAPI.Sensors.V1
{
    [Serializable]
    public abstract class PT100 : Base.Sensors.Types.Temperature.PT100
    {
         #region Constructors
        internal PT100(SensorManagerV2 parent)
            : base(parent)
        {
        }

        #endregion
    }
}
