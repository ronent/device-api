﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Devices.Features;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using SiMiAPI.Devices;
using SiMiBase.Functions.Management;

namespace SiMiAPI.Functions.Management.V1
{
    [Serializable]
    public class SiMiLogLoggerFunctionsManager : SiMiBaseLoggerFunctionsManager
    {
        #region Constructor
        public SiMiLogLoggerFunctionsManager(GenericSiMiLogLogger parent)
            : base(parent)
        {

        }


        #endregion
        #region Properties
        new protected internal GenericSiMiLogLogger Device { get { return base.Device as GenericSiMiLogLogger; } set { base.Device = value; } }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    yield return item;
                }

                yield return new AvailableFunction(eDeviceFunction.LoadDefaultCalibration) { Visible = Device.IsOnline, Enabled = !IsRunning && !IsBusy };
                yield return new AvailableFunction(eDeviceFunction.SaveDefaultCalibration) { Visible = Device.IsOnline, Enabled = !IsUpdatingFirmware };
            }
        }

        /// <summary>
        /// Gets the available functions.
        /// </summary>
        /// <value>
        /// The available functions.
        /// </value>
        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                foreach (var item in SiMiBaseLoggerFunctionsManager.AvailableFunctions)
                {
                    yield return item;
                }

                yield return eDeviceFunction.LoadDefaultCalibration;
                yield return eDeviceFunction.SaveDefaultCalibration;
            }
        }

        #endregion
        #region Override Functions
        public override Task<Result> GetStatus()
        {
            {
                var isBusy = false;

                try
                {
                    isBusy = IsBusy;
                }
                catch
                {
                    // ignored
                }

                return Task.FromResult( isBusy ? new Result(eResult.BUSY) { MessageLog = BusyErrorMessage } : GetStatusNoBusy().Result);
            }
        }

        internal override Task<Result> GetStatusNoBusy()
        {
            var result =  Device.OpCodes.StaticStatus.Invoke().Result;
            if (result.IsOK)
            {
                result = Device.OpCodes.DynamicStatus.Invoke().Result;

                if (result.IsOK)
                    InvokeOnStatus();
            }
            return Task.FromResult(result);
        }

        protected override Result DoAfterSetup()
        {
            var result = Result.OK;
            return result;
        }

        #endregion
        #region New Functions

        public Task<Result> RestoreDefaultCalibration()
        {
            var result = CheckRunningAndBusy().Result;
            return !result.IsOK ? Task.FromResult(result) : Device.OpCodes.DefaultCalibration.Restore();
        }

        public virtual Task<Result> SaveDefaultCalibration()
        {
            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
            {
                var result = CheckRunningAndBusy().Result;
                return !result.IsOK ? Task.FromResult(result) : Device.OpCodes.DefaultCalibration.Save();
            }

            return Task.FromResult(new Result(new Exception("Unsupported feature in this version.")));
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public SiMiLogLoggerFunctionsManager(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion

        public override bool TimerRunEnabled
        {
            get { return Device.Status.TimerRun > 0; }
        }
    }
}
