﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.DataStructures;
using SiMiAPI.OpCodes.Management;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal abstract class SetTemperatureAlarms : SiMiBase.OpCodes.Common.SetTemperatureAlarms
    {
        #region Members

        private static readonly byte[] sendOpCode = {0x08};
        private static readonly byte[] receiveOpCode = {0x08};

        #endregion

        #region Constructors

        protected SetTemperatureAlarms(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion

        #region Properties

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        public new SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }

         new public SiMiLogSetupConfiguration Configuration
        {
            get { return base.Configuration as SiMiLogSetupConfiguration; }
            set { base.Configuration = value; }
        }

        #endregion

        #region Populate Methods

        
        protected override void Parse()
        {
            Populate();
        }


        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateHighAlarms();
            PopulateLowAlarms();
        }

        protected override void PopulateHighAlarms()
        {
            const int shiftIndex = 0;
//            OutReport.Next = (byte) (
//                (Convert.ToByte(Configuration.TemperatureAlarms.High1Enabled) << shiftIndex + 1) |
//                ((Configuration.TemperatureAlarms.High1Value) << shiftIndex + 2) |
//                ((Configuration.TemperatureAlarms.High1AllowedDuration) << shiftIndex + 3) |
//                (Convert.ToByte(Configuration.TemperatureAlarms.High2Enabled) << shiftIndex + 4) |
//                ((Configuration.TemperatureAlarms.High2Value) << shiftIndex + 2) |
//                ((Configuration.TemperatureAlarms.High2AllowedDuration) << shiftIndex + 3));

        }

        protected override void PopulateLowAlarms()
        {

        }
        #endregion
    }
}
