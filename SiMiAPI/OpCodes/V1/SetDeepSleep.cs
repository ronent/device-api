﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class SetDeepSleep : SiMiBase.OpCodes.Common.DynamicStatus
    {
        #region Members
        private static readonly byte[] sendOpCode = { 0x07 };
        private static readonly byte[] receiveOpCode = { 0x01 };
        #endregion
        #region Constructors
        public SetDeepSleep(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }
        #endregion
        #region Methods

        protected override void Parse()
        {
            parseSetup();
        }

        private void parseSetup()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseUtcOffset();
            ParseDisplayConfiguration();
            ParseInterval();
            ParseNumberOfSamples();
            ParsePdfOptions();
            ParseRunDelay();
            ParseTimerRun();
        }

        protected override void DoAfterFinished()
        {
            ParentDevice.Status.DeepSleepMode = true;
        }

        #endregion
    }
}
