﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.DataStructures;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Management;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class Run : SiMiBase.OpCodes.Common.DynamicStatus
    {
        #region Members
        private static readonly byte[] sendOpCode = { 0x04 };
        private static readonly byte[] receiveOpCode = { 0x01 };
        #endregion
        #region Constructors
        public Run(SiMiBaseLogger device)
            : base(device)
        {
           
        }

        #endregion
        #region Properties

        public SiMiBaseSetupConfiguration Configuration;

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

//        new public SiMiLogSetupConfiguration Configuration
//        {
//            get { return base.Configuration as SiMiLogSetupConfiguration; }
//            set { base.Configuration = value; }
//        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }
        #endregion
        #region Methods

        protected override void Parse()
        {
            parseSetup();
        }

        private void parseSetup()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseUtcOffset();
            ParseDisplayConfiguration();
            ParseInterval();
            ParseNumberOfSamples();
            ParsePdfOptions();
            ParseRunDelay();
            ParseTimerRun();
        }

//        protected override void Populate()
//        {
//            //set the timer run parameter  
//           // OutReport.Next = (byte) Configuration.TimerRun;
//        }

        protected override void DoAfterFinished()
        {
            ParentDevice.Status.IsRunning = true;
        }

        #endregion
    }
}
