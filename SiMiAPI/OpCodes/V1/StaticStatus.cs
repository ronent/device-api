﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.MathLib;
using Auxiliary.Tools;
using Base.OpCodes;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Log4Tech;
using SiMiAPI.DataStructures;
using SiMiAPI.Devices;
using SiMiAPI.OpCodes.Management;
using SiMiBase.DataStructures;
using SiMiBase.DataStructures.Units;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class StaticStatus : SiMiBase.OpCodes.Common.StaticStatus
    {
        #region Fields
        private static readonly byte[] sendOpCode = { 0x02 };
        private static readonly byte[] receiveOpCode = { 0x02 };

        #endregion
        #region Members

        private IncomingReport status1;
        #endregion
        #region Constructors
        public StaticStatus(SiMiBaseLogger device)
            : base(device) //OpCodes are overridden below
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new protected GenericSiMiLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericSiMiLogLogger; }
            set { base.ParentDevice = value; }
        }

        new protected SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            parseStatus();
        }

        private void parseStatus()
        {
            ParseDeviceType();
            ParseVersion();
            ParseSwVersion();
            ParseSerialNumber();
            ParseComment();
            ParseTripNumber();
            ParseNumberOfActivations();
        }

        #endregion
        
        #region Override Methods
        protected override bool IsReceiveHeader(IncomingReport report)
        {
            if (base.IsReceiveHeader(report))
            {
                status1 = report;
                return true;
            }
            

            return false;
        }

        protected override bool ShouldContinueWithProcess()
        {
            return status1 != null;
        }

        #endregion
    }
}
