﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : SiMiBase.OpCodes.Common.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
    }
}
