﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using SiMiAPI.DataStructures;
using SiMiAPI.Devices;
using SiMiAPI.OpCodes.Management;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class DynamicStatus : SiMiBase.OpCodes.Common.DynamicStatus
    {
        #region Fields

        private static readonly byte[] sendOpCode = {0x03};
        private static readonly byte[] receiveOpCode = {0x01};

        #endregion

        #region Members

        private IncomingReport status1;

        #endregion

        #region Constructors

        public DynamicStatus(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion

        #region Properties

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected new GenericSiMiLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericSiMiLogLogger; }
            set { base.ParentDevice = value; }
        }

        protected new SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }

        #endregion

        #region Parse Methods

        protected override void Parse()
        {
            parseSetup();
        }

        private void parseSetup()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseUtcOffset();
            ParseDisplayConfiguration();
            ParseInterval();
            ParseNumberOfSamples();
            ParsePdfOptions();
            ParseRunDelay();
            ParseTimerRun();
            ParseBatteryLevel();
        }

        #endregion
    }
}
