﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using SiMiAPI.Devices;
using SiMiAPI.OpCodes.Management;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : SiMiLogOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x18 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x22 };

        #endregion
        #region Constructors
        public GetCalibration(GenericSiMiLogLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            ParentDevice.Sensors.Calibration.Initialize();
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            ParseGainOffsets();
        }

        protected virtual void ParseGainOffsets()
        {
            ParseGainOffsetAux(ParentDevice.Sensors.Calibration[eSensorIndex.Temperature]);
            ParseGainOffsetAux(ParentDevice.Sensors.Calibration[eSensorIndex.External1]);
            ParseGainOffsetAux(ParentDevice.Sensors.Calibration[eSensorIndex.Humidity]);
        }

        protected virtual void ParseGainOffsetAux(CalibrationCoefficients coeff)
        {
            coeff.Af = parseCoefficient();
            coeff.Bf = parseCoefficient();
            coeff.Cf = parseCoefficient();
        }

        private float parseCoefficient()
        {
            return InReport.GetSingle();
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
    }
}
