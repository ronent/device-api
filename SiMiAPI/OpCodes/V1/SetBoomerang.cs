﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : SiMiBase.OpCodes.Common.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
    }
}
