﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Base.Sensors.Alarms;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using SiMiAPI.DataStructures;
using SiMiAPI.OpCodes.Helpers;
using SiMiBase.Devices.Types;
using SiMiBase.OpCodes.Common;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : BaseDataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = { 0x13 };

        #endregion
        #region Fields
        protected object processingPacket = new object();
        private Queue<TempOnlineV2> WaitingSamples;

        #endregion
        #region Constructors
        public OnlineDataPacket(SiMiBaseLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            WaitingSamples = new Queue<TempOnlineV2>();
            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            lock (processingPacket)
            {
                PacketTime = InReport.Get<uint>();
                TemperatureValue = InReport.Get<float>();
                HumidityValue = InReport.Get<float>();
                LightStatus = Convert.ToBoolean(InReport.Next);
                TemperatureAlarmStatus = (eAlarmStatus) InReport.Next;
                HumidityAlarmStatus = (eAlarmStatus) InReport.Next;
                ErrorCode = (SampleV2.eErrorCodes) InReport.Next;

                ExtractAndAdd();
            }
        }

        protected override void AddSampleToSensor(GenericSensorV2 sensor, float value, uint dateTime, eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isDummy = false)
        {
//            if (value == Consts.DataPacket.EXTERNAL_DUMMY)
//                return;

//            if (ParentOpCodeManager.DynamicStatus.IsRunning)
//                WaitingSamples.Enqueue(new TempOnlineV2(sensor, value,dateTime));
//            else
                addSampleToSensor(sensor, value, dateTime, alarmStatus, lightStatus, errorCode);
        }

        protected virtual void addSampleToSensor(GenericSensorV2 sensor, float value, uint dateTime, eAlarmStatus alarmStatus, bool lightStatus, SampleV2.eErrorCodes errorCode, bool isDummy = false)
        {
            sensor.samples.AddOnline(value, dateTime, alarmStatus, lightStatus, errorCode);
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Methods
        protected override void DoBeforeParse()
        {
           // ParentOpCodeManager.Download.OnFinished += Download_OnFinished;
        }

//        void Download_OnFinished(object sender, Result result)
//        {
//            while (WaitingSamples.Count > 0)
//            {
//                var temp = WaitingSamples.Dequeue();
//                temp.Sensor.samples.AddOnline(temp.Value, temp.Time);
//            }
//        }

        #endregion
        #region NotSupported
        protected override void Populate()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
