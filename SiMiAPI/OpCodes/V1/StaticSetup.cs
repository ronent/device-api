﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.OpCodes.Management;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal abstract class StaticSetup : SiMiBase.OpCodes.Common.StaticSetup
    {
        #region Members
        private static readonly byte[] sendOpCode = { 0x06 };
        private static readonly byte[] receiveOpCode = { 0x02 };
        #endregion

        #region Constructors

        protected StaticSetup(SiMiBaseLogger device)
            : base(device)
        {

        }
        #endregion

        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new public SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }

        #endregion

        #region Populate Methods

        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateSerialNumber();
            PopulateComment();
        }

        protected override void PopulateSerialNumber()
        {
            OutReport.Next = (byte)Configuration.SerialNumber;
        }

        protected override void PopulateComment()
        {
            OutReport.Insert(Configuration.Comment, SiMiStaticSetupConfiguration.AllowedCommentLength); 
        }

        protected override void Parse()
        {
            parseStatus();
        }

        private void parseStatus()
        {
            ParseDeviceType();
            ParseVersion();
            ParseSwVersion();
            ParseSerialNumber();
            ParseComment();
            ParseTripNumber();
            ParseNumberOfActivations();
        }

        #endregion
    }
}
