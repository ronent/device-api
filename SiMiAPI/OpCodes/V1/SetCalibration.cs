﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;
using SiMiAPI.Devices;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : SiMiBase.OpCodes.Common.SetCalibration
    {
        #region Constructors
        public SetCalibration(GenericSiMiLogLogger device)
            : base(device)
        {

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateGainOffset();
        }

        protected override void PopulateGainOffset()
        {
            PopulateGainOffsetAux(Calibration[eSensorIndex.Temperature]);
            PopulateGainOffsetAux(Calibration[eSensorIndex.External1]);
            PopulateGainOffsetAux(Calibration[eSensorIndex.Humidity]);
        }

        #endregion
    }
}
