﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.MathLib;
using Base.OpCodes;
using Base.Sensors.Management;
using SiMiAPI.DataStructures;
using SiMiAPI.DataStructures.ESetup;
using SiMiAPI.Devices;
using SiMiAPI.OpCodes.Management;
using SiMiBase.DataStructures.Units;
using SiMiBase.Devices.Types;

namespace SiMiAPI.OpCodes.V1
{
    [Serializable]
    internal abstract class DynamicSetup : SiMiBase.OpCodes.Common.DynamicSetup
    {
        #region Members
        private static readonly byte[] sendOpCode = { 0x01 };
        private static readonly byte[] receiveOpCode = { 0x01 };

        private IncomingReport status1;

        #endregion
        #region Fields
        SpareBytes spare = new SpareBytes(1);
        SpareBytes spareByte = new SpareBytes(7);
        #endregion
        #region Constructors

        protected DynamicSetup(SiMiBaseLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new public SiMiLogSetupConfiguration Configuration
        {
            get { return base.Configuration as SiMiLogSetupConfiguration; }
            set { base.Configuration = value; }
        }

        new public GenericSiMiLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericSiMiLogLogger; }
            set { base.ParentDevice = value; }
        }

        new public SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }
        #endregion
        #region Configuration Properties
            protected virtual bool DeepSleepMode { get { return false; } }
        protected virtual SiMiLogUDSConfiguration UserDefinedSensor { get { return null; } }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateSetupBooleans();
            PopulateTime();
            PopulateUtcOffset();
            PopulateDisplayConfiguration();
            PopulateInterval();
            PopulateNumberOfSamples();
            PopulatePdfOptions();
            PopulateRunDelay();
            PopulateTimerRun();
        }

        protected override void PopulateSetupBooleans()
        {
            OutReport.Insert(BitConverter.GetBytes((short) (
                (Convert.ToInt16(Configuration.CyclicMode) << 1) |
                (Convert.ToInt16(Configuration.PushToRunMode) << 2) |
                (Convert.ToInt16(Configuration.FahrenheitMode) << 3) |
                (Convert.ToInt16(Configuration.StopOnKeyPress) << 4) |
                (Convert.ToInt16(Configuration.TestMode) << 5) |
                (Convert.ToInt16(DeepSleepMode) << 6))));
        }

        protected override void PopulateTime()
        {
            //var time = new SiMiLogTime
           // {
               // TimeStamp = (int) (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
            //};
            OutReport.Insert(BitConverter.GetBytes((int)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds));
        }

        protected override void PopulateTimerRun()
        {
            OutReport.Insert(Convert.ToUInt32(Configuration.TimerRun));
        }

        protected override void PopulateDisplayConfiguration()
        {
            OutReport.Next = (byte)Configuration.LCDConfiguration;
        }

        protected override void PopulateNumberOfSamples()
        {
            OutReport.Insert(Convert.ToUInt16(Configuration.NumberOfSamples));
        }

        protected override void PopulateRunDelay()
        {
            OutReport.Insert(Convert.ToUInt32(Configuration.RunDelay));
        }

        protected override void PopulatePdfOptions()
        {
            const int shiftIndex = 0;
            OutReport.Next = (byte) ((Convert.ToByte(Configuration.GeneratePdf) << shiftIndex) |
                                     (Convert.ToByte(Configuration.AddDataPages) << shiftIndex + 1) |
                                     (Convert.ToByte(Configuration.LightSensorEnabled) << shiftIndex + 2));
        }

        protected override void PopulateInterval()
        {
            OutReport.Insert(Convert.ToUInt16(Configuration.Interval.TotalSeconds));
        }

        protected override void Parse()
        {
            parseSetup();
        }

        private void parseSetup()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseUtcOffset();
            ParseDisplayConfiguration();
            ParseInterval();
            ParseNumberOfSamples();
            ParsePdfOptions();
            ParseRunDelay();
            ParseTimerRun();
        }

        #endregion
        
       

       
    }
}
