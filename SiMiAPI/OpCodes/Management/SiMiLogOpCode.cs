﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.Devices;
using SiMiBase.OpCodes.Management;

namespace SiMiAPI.OpCodes.Management
{
    [Serializable]
    internal abstract class SiMiLogOpCode : SiMiBaseOpCode
    {
        public SiMiLogOpCode(GenericSiMiLogLogger device)
            : base(device)
        {

        }

        new protected GenericSiMiLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericSiMiLogLogger; }
            set { base.ParentDevice = value; }
        }

        new protected SiMiLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as SiMiLogOpCodeManager; }
        }
    }
}
