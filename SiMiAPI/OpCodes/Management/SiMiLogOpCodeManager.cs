﻿using SiMiAPI.Devices;
using SiMiAPI.OpCodes.V1;
using SiMiBase.OpCodes.Management;

namespace SiMiAPI.OpCodes.Management
{
    internal class SiMiLogOpCodeManager : SiMiBaseOpCodeManager
    {
        public SiMiLogOpCodeManager(GenericSiMiLogLogger device)
            : base(device)
        {

        }

        #region OpCode Fields

        internal DynamicSetup Setup
        {
            get { return DynamicSetup as DynamicSetup; }
            set { DynamicSetup = value; }
        }

        internal GetCalibration GetCalibration;
        internal DefaultCalibration DefaultCalibration;
        #endregion
    }
}
