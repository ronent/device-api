﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Types;

namespace SiMiAPI.OpCodes.Helpers
{
    internal class TempOnline
    {
        public GenericSensor Sensor { get; private set; }
        public long Value { get; private set; }
        public DateTime DateTime { get; private set; }

        public TempOnline(GenericSensor sensor, long value, DateTime dateTime)
        {
            Sensor = sensor;
            Value = value;
            DateTime = dateTime;
        }
    }
}
