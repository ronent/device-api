﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Types;

namespace SiMiAPI.OpCodes.Helpers
{
    internal class TempOnlineV2
    {
        public GenericSensorV2 Sensor { get; private set; }
        public float Value { get; private set; }
        public uint Time { get; private set; }

        public TempOnlineV2(GenericSensorV2 sensor, float value, uint time)
        {
            Sensor = sensor;
            Value = value;
            Time = time;
        }
    }
}
