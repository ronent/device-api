﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiAPI.OpCodes.Helpers
{
    class Consts
    {
        public struct Time
        {
            public const int TIME_SIZE = 4;
        }

        public struct Readings
        {
            public const int SAMPLE_SIZE = 2;
        }

        public struct DataPacket
        {
            public const long EXTERNAL_DUMMY = 65535;
        }

    }
}
