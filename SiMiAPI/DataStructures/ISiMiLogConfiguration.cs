﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiAPI.DataStructures
{
    interface ISiMiLogConfiguration
    {
//        LCDConfigurationEnum LCDConfiguration { get; }

        bool StopOnKeyPress { get; }

        int TimerRun { get; }
    }
}
