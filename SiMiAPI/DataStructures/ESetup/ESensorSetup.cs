﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Management;
using SiMiAPI.Sensors.Management;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiAPI.DataStructures.ESetup
{
    [Serializable]
    public class ESensorSetup : ISensorSetup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ESensorSetup"/> class.
        /// </summary>
        public ESensorSetup()
        {
            Alarm = new AlarmV2();
            UserDefinedSensor = new SiMiLogUDSConfiguration();
        }

        public AlarmV2 Alarm { get; set; }

        public SiMiLogUDSConfiguration UserDefinedSensor { get; set; }

        public eSensorType Type { get; set; }

        public bool Enabled { get { return Type != eSensorType.None; } }

        public bool UserDefined { get { return Type == eSensorType.UserDefined; } }

        #region Methods
        public void ThrowIfInvalidFor(SiMiBaseLogger dummyDevice)
        {
            ThrowIfInvalidDevice(dummyDevice);
            if (Enabled)
            {
                throwIfInvalidType();
                throwIfInvalidUserDefinedSensor();
                ThrowIfInvalidAlarms(dummyDevice);
            }
        }

        public void ThrowIfInvalidDevice(SiMiBaseLogger dummyDevice)
        {
            if (!(dummyDevice.Status is IESensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support external sensor setup");
        }

        private void throwIfInvalidType()
        {
            switch (Type)
            {
                case eSensorType.Current4_20mA:
                case eSensorType.Voltage0_10V:
                case eSensorType.ExternalNTC:
                case eSensorType.PT100:
                case eSensorType.UserDefined:
                    break;
                default:
                    throw new Exception("Type must be an external type or user-defined");
            }
        }

        private void throwIfInvalidUserDefinedSensor()
        {
            if (UserDefined && !UserDefinedSensor.Valid)
                throw new Exception("Invalid user defined sensor");
        }
        
        public void ThrowIfInvalidAlarms(SiMiBaseLogger dummyDevice)
        {
            if (Enabled && Alarm.Enabled)
                Alarm.ThrowIfNotValid(dummyDevice.Sensors.GetDetachableByType(Type));
        }

//        internal static void adjustExternalSensor(SiMiBaseLogger dummyDevice, ESensorSetup externalSensor)
//        {
//            if (externalSensor.UserDefined)
//            {
//                if (!externalSensor.UserDefinedSensor.Valid)
//                    throw new Exception("User define sensor parameters are not valid.");
//
//                var sensor = dummyDevice.Sensors.GetDetachableByType(externalSensor.UserDefinedSensor.BaseType);
//                (sensor.ParentSensorManager as SiMiLogSensorManager).SetUserDefined(externalSensor.UserDefinedSensor);
//            }
//        }

        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("External Sensor");
            str.AppendLine("\tType: " + Type);
            if (UserDefined)
                str.AppendLine("\tUser Defined: " + UserDefinedSensor.ToString());
            str.AppendLine("\tAlarm: " + Alarm.ToString());

            return str.ToString();
        }
        #endregion
        #region Equals
        
        public override bool Equals(object obj)
        {
            ESensorSetup other = obj as ESensorSetup;
            if (other == null) return false;

            return Alarm.Equals(other.Alarm)
                && isUserDefinedEqualTo(other)
                && Type == other.Type
                && Enabled == other.Enabled;
        }

        private bool isUserDefinedEqualTo(ESensorSetup other)
        {
            return (!UserDefined && !other.UserDefined)
                || UserDefinedSensor.Equals(other.UserDefinedSensor);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
