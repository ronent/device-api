﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using SiMiBase.DataStructures.Units;

namespace SiMiAPI.DataStructures.ESetup
{
    [Serializable]
    public class SiMiLogUDSConfiguration : Base.DataStructures.UDSConfiguration
    {
        private ExternalSensorUnitEnum unit;

        public override int AllowedNameLength { get { return 10; } }

        public override int AllowedCustomUnitLength { get { return 4; } }

        public ExternalSensorUnitEnum Unit
        {
            get { return unit; }
            set
            {
                unit = value;

                if (value != ExternalSensorUnitEnum.Custom)
                    CustomUnit = FixedUnitTypes.Get(value);
            }
        }

        public string CustomUnit { get; set; }

        #region Constructors
        public SiMiLogUDSConfiguration()
        {
        }

        #endregion
        #region Serialization & Deserialization
        protected SiMiLogUDSConfiguration(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            Unit = IO.GetSerializationValue<ExternalSensorUnitEnum>(ref info, "Unit");
            CustomUnit = IO.GetSerializationValue<string>(ref info, "CustomUnit");
        }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
            info.AddValue("Unit", Unit, typeof(ExternalSensorUnitEnum));
            info.AddValue("CustomUnit", CustomUnit, typeof(string));
        }
        #endregion
        #region Object Overrides
       
        public override string ToString()
        {
            return base.ToString()
                + Environment.NewLine
                + "Unit: " + CustomUnit;
        }
        #endregion
    }
}
