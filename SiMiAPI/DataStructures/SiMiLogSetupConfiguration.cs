﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.Devices;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Types;

namespace SiMiAPI.DataStructures
{
//    public enum LCDConfigurationEnum : byte
//    {
//        AlwaysOn = 0x00,
//        OnFor30Sec = 0x01,
//        OnFor90Sec = 0x02,
//        OnFor120Sec = 0x03,
//    }

    /// <summary>
    /// MicroLog sensor configuration.
    /// </summary>
    public enum ConfigurationSensorEnum : byte
    {
        NONE = 0,
        TEMPERATURE = 1,
        HUMIDITY = 2,
        DEWPOINT = 4,
        EXTERNAL = 8
    }

    /// <summary>
    /// Configuration settings for SiMi devices.
    /// </summary>
    [Serializable]
    public class SiMiLogSetupConfiguration : SiMiBaseSetupConfiguration, ISiMiLogConfiguration
    {
        #region Constructor
        internal SiMiLogSetupConfiguration()
            : base()
        {
            Initialize();
        }

        #endregion
        #region Properties
        public virtual LCDConfigurationEnum LCDConfiguration { get; set; }

        public virtual bool StopOnKeyPress { get; set; }

        public virtual bool ShowMinMax { get; set; }

        public virtual bool ShowPast24HMinMax { get; set; }

        public virtual bool EnableLEDOnAlarm { get; set; }

        public virtual byte AveragePoints { get; set; }

        public virtual short NumberOfSamples { get; set; }

        public virtual bool GeneratePdf { get; set; }

        public virtual bool AddDataPages { get; set; }

        public virtual bool LightSensorEnabled { get; set; }

        public virtual uint RunDelay { get; set; }

        internal override DateTime DeviceTime
        {
            get { return DateTime.UtcNow.AddHours(UtcOffset); }
            set { throw new NotSupportedException(); }
        }

        #endregion
        #region Validation
        internal override void checkSensors(SiMiBaseLogger device)
        {

        }

        #endregion
    }
}
