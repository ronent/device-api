﻿using Base.DataStructures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiAPI.DataStructures
{
    internal class SiMiLogTime 
    {
        #region Static
        public static uint Parse(byte[] block)
        {
            return BitConverter.ToUInt16(block, 0);
        }

        public int TimeStamp { get; set; }

        #endregion
        #region Methods
       
        #endregion

    }
}
