﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiAPI.DataStructures.ESetup;
using SiMiAPI.DataStructures.THDSetup;
using SiMiBase.DataStructures;
using SiMiBase.DataStructures.TSetup;

namespace SiMiAPI.DataStructures
{
    [Serializable]
    public class SiMiLogStatusConfiguration : SiMiBaseStatusConfiguration,  ITSensorSetup, IESensorSetup, ITHDSensorSetup
    {
        #region Constructor
        internal SiMiLogStatusConfiguration()
        {
            ExternalSensor = new ESensorSetup();
            FixedSensors = new THDSensorSetup();
        }

        #endregion
        #region Properties
        public ESensorSetup ExternalSensor { get; internal set; }

        public THDSensorSetup FixedSensors { get; internal set; }

        public TSensorSetup TemperatureSensor { get { return FixedSensors.GetTemperatureSensor(); } }

       
        #endregion
    }
}
