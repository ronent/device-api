﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Management;
using SiMiBase.DataStructures.TSetup;
using SiMiBase.Devices.Types;

namespace SiMiAPI.DataStructures.THDSetup
{
    public enum FixedSensorCombinationEnum : byte
    {
        NONE = 0,
        TEMPERATURE = 1,
        TEMPERATURE_HUMIDITY = 3,
    }

    [Serializable]
    public class THDSensorSetup : TSensorSetup
    {
        public THDSensorSetup()
        {
            HumidityAlarm = new AlarmV2();
        }

        internal FixedSensorCombinationEnum FixedSensorCombination { get; set; }

        public override bool TemperatureEnabled
        {
            get { return ((byte)FixedSensorCombination & (byte)ConfigurationSensorEnum.TEMPERATURE) != 0; }
            set
            {
                if (value)
                {
                    if (FixedSensorCombination == FixedSensorCombinationEnum.NONE)
                        FixedSensorCombination = FixedSensorCombinationEnum.TEMPERATURE;
                }
                else
                    FixedSensorCombination = FixedSensorCombinationEnum.NONE;
            }
        }

        public bool HumidityEnabled
        {
            get { return ((byte)FixedSensorCombination & (byte)ConfigurationSensorEnum.HUMIDITY) != 0; }
            set
            {
                if (value)
                {
                    if (FixedSensorCombination < FixedSensorCombinationEnum.TEMPERATURE_HUMIDITY)
                        FixedSensorCombination = FixedSensorCombinationEnum.TEMPERATURE_HUMIDITY;
                }
                else
                    FixedSensorCombination = (FixedSensorCombinationEnum)((byte)FixedSensorCombination & (byte)FixedSensorCombinationEnum.TEMPERATURE);
            }
        }

        public AlarmV2 HumidityAlarm { get; set; }

       
        #region Methods
        public TSensorSetup GetTemperatureSensor()
        {
            var temperatureSensor = new TSensorSetup
            {
                TemperatureEnabled = TemperatureEnabled,
                TemperatureAlarm = TemperatureAlarm
            };

            return temperatureSensor;
        }

        public override void ThrowIfInvalidDevice(SiMiBaseLogger dummyDevice)
        {
            if (!(dummyDevice.Status is ITHDSensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support humidity sensor setup");
        }

        public override void ThrowIfInvalidAlarms(SiMiBaseLogger dummyDevice)
        {
            if (HumidityEnabled)
                HumidityAlarm.ThrowIfNotValid(dummyDevice.Sensors.GetFixedByIndex(eSensorIndex.Humidity));

            base.ThrowIfInvalidAlarms(dummyDevice);
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            str.AppendLine("Fixed Sensors");
            str.AppendLine("\tTypes: " + FixedSensorCombination);
            str.AppendLine("\tTemp Alarm: " + TemperatureAlarm);
            str.AppendLine("\tHumidity Alarm: " + HumidityAlarm);

            return str.ToString();
        }

        #endregion
        #region Equals
      
        public override bool Equals(object obj)
        {
            var other = obj as THDSensorSetup;
            if (other == null) return false;

            return base.Equals(obj)
                && HumidityAlarm.Equals(other.HumidityAlarm)
                && FixedSensorCombination == other.FixedSensorCombination;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
