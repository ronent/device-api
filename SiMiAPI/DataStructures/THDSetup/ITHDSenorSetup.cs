﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMiAPI.DataStructures.THDSetup
{
    public interface ITHDSensorSetup
    {
        THDSensorSetup FixedSensors { get; }
    }
}
