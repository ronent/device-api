﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.DataStructures;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using Log4Tech;
using SiMiAPI.DataStructures;
using SiMiAPI.Functions.Management.V1;
using SiMiAPI.OpCodes.Management;
using SiMiAPI.Sensors.Management;
using SiMiBase.DataStructures;
using SiMiBase.Devices.Features;
using SiMiBase.Devices.Types;

namespace SiMiAPI.Devices
{
    [Serializable]
    public abstract class GenericSiMiLogLogger : SiMiBaseLogger, IDisposable, ISerializable
    {
        #region Members
        private static readonly string deviceTypeName = "Generic SiMiLog Device";
        private static readonly string firmwareDeviceName = "SiMi";
        private static readonly Version MINIMUM_FW_VERSION = new Version(0, 0);
       // private static readonly byte allowedAveragePoints = 7;

        #endregion
        #region Properties
       // public virtual byte AllowedAveragePoints { get { return allowedAveragePoints; } }

        protected override byte Type { get { return 0; } }
       
        protected override byte Bits { get { return 0; } }
        
        new public SiMiBaseVersion Version
        {
            get { return base.Version as SiMiBaseVersion; }
            protected set { base.Version = value; }
        }

        new internal SiMiLogOpCodeManager OpCodes
        {
            get { return base.OpCodes as SiMiLogOpCodeManager; }
            set { base.OpCodes = value; }
        }

        new public SiMiLogStatusConfiguration Status
        {
            get { return base.Status as SiMiLogStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public SiMiLogLoggerFunctionsManager Functions
        {
            get { return base.Functions as SiMiLogLoggerFunctionsManager; }
            set { base.Functions = value; }
        }

        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

       

        public override SiMiBaseFeature[] Features
        {
            get
            {
                return base.Features.Concat(new[]
                { 
                    SiMiBaseFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }
       
        public override eSensorType[] AvailableFixedSensors
        {
            get { return new eSensorType[] { }; }
        }

        public override eSensorType[] AvailableDetachableSensors
        {
            get { return new eSensorType[] { }; }
        }

        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
        #region Constructors
        internal GenericSiMiLogLogger(IHIDDeviceManager parent)
            : base(parent)
        {
        }

//        internal override void Initialize()
//        {
//            base.Initialize();
//
//            if (ParentDeviceManager != null)
//            {
//                try
//                {
//                    Memory = new GenericMemorySize(this);
//                }
//                catch (Exception ex)
//                {
//                    Log.Instance.WriteError("Fail in Initialize GenericSiMiLogDevice", this, ex);
//                }
//            }
//        }

        internal override void InitializeFunctionManager()
        {
            Functions = new SiMiLogLoggerFunctionsManager(this);
        }

        internal override void InitializeOpCodesManager()
        {
            OpCodes = new SiMiLogOpCodeManager(this);
        }

        internal override void InitializeStatus()
        {
            Status = new SiMiLogStatusConfiguration();
        }

        internal override void InitializeSensorManager()
        {
            Sensors = new SiMiLogSensorManager(this);
        }

        #endregion
        #region Serialization & Deserialization
        protected GenericSiMiLogLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
