﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Base.Devices.Management.DeviceManager.HID;
using SiMiAPI.DataStructures;
using SiMiBase.Devices.Features;

namespace SiMiAPI.Devices
{
    public abstract class SiMiLogger : GenericSiMiLogLogger
    {
        #region Properties

//        public override MemorySizeEnum[] AvailableMemorySizes
//        {
//            get
//            {
//                return new[] {
//                    MemorySizeEnum.K8,
//                    MemorySizeEnum.K32,
//                };
//            }
//        }
        #endregion
        #region Constructors
        internal SiMiLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        #endregion
    }
}
