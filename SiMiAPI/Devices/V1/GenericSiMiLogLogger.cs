﻿using Base.Devices.Management.DeviceManager.HID;
using System;


namespace SiMiAPI.Devices.V1
{
    [Serializable]
    public class GenericSiMiLogLogger : Devices.GenericSiMiLogLogger
    {
        #region Constructors
        internal GenericSiMiLogLogger(IHIDDeviceManager parent)
            : base(parent)
        {
        }

        #endregion
    }
}
