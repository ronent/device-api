﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using SiMiAPI.Devices.V1;
using SiMiBase.Devices.Types;
using SiMiBase.Modules.Interfacer;
using SiMiBase.Modules;

namespace SiMiAPI.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "Generic SiMiLog")]
    internal class GenericSiMiLogInterfacer : SiMiBaseLoggerInterfacer<GenericSiMiLogLogger>
    {
        #region Override Properties
        protected override byte Type { get { return 0x00; } }
        protected override byte Bits { get { return 0x00; } }

        public override List<DeviceID> DeviceIDs
        {
            get
            {
                return (from module in ModuleManager.Modules
                        where module.GetDeviceClassType().IsSubclassOf(GetDeviceClassType()) && module is IHIDLoggerInterfacer
                        from deviceid in (module as IHIDLoggerInterfacer).DeviceIDs
                        select deviceid).Distinct().ToList();
            }
        }

        #endregion
        #region Constructor
        public GenericSiMiLogInterfacer()
            : base()
        {
        }

        #endregion
        #region Override Methods
        public override bool CanCreateFrom(GenericDevice device)
        {
            return false;
        }

        public override void CreateFromDeviceAndReport(GenericDevice genericDevice, int userId)
        {
            moduleCreateFrom(genericDevice as SiMiBaseLogger,userId);
        }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.GenericSiMiLogLogger(ParentDeviceManager as IHIDDeviceManager);
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Finds the right sub interfacer and creates the device
        /// </summary>
        /// <param name="microxDevice"></param>
        private void moduleCreateFrom(SiMiBaseLogger microxDevice, int userId)
        {
            ISubDeviceInterfacer module = findSubDeviceModuleFrom(microxDevice);

            if (module != null)
            {
                module.CreateFromDeviceAndReport(microxDevice,userId);
            }
        }

        private static ISubDeviceInterfacer findSubDeviceModuleFrom(SiMiBaseLogger genericDevice)
        {
            var module = ModuleManager.Modules.FirstOrDefault(
                x => x is ISubDeviceInterfacer
                    && (x as ISubDeviceInterfacer).CanCreateFrom(genericDevice));

            return module as ISubDeviceInterfacer;
        }

        #endregion
    }
}
