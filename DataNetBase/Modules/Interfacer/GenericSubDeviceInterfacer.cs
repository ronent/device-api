﻿using Base.Devices;
using Base.Modules.Interfacer;
using DataNetBase.Devices.Types;
using DataNetBase.Devices.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Modules.Interfacer
{
    internal abstract class GenericSubDeviceInterfacer<T> : DataNetDevicesInterfacer, ISubDeviceInterfacer
    {
        #region Constructor
        public GenericSubDeviceInterfacer()
            : base()
        {

        }

        #endregion
        #region Overrides
        public override void CreateFromDeviceAndReport(GenericDevice genericDevice, int userId)
        {
            BaseCreateFromDeviceAndReport(genericDevice,userId);
        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            if (device is DataNetDevice)
                return (device as DataNetDevice).Status.Type == (eDataNetDevicesType)Type;
            else if (device is DataNetLogger)
                return (device as DataNetLogger).Status.Type == (eDataNetDevicesType)Type;
            else
                return false;
        }

        #endregion
    }
}
