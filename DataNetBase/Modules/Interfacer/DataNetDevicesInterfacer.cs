﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.Serial;
using Base.Modules.Interfacer;
using DataNetBase.Devices.Types;
using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Infrastructure.Communication.DeviceIO;
using DataNetBase.Devices.V1;
using System.Reflection;

namespace DataNetBase.Modules.Interfacer
{
    public enum eDataNetDevicesType : byte
    {
        DNR900 = 0x00,
        DNL910 = 0x01,
        DNL920 = 0x02,
        DNL804 = 0x03,
        DNL808 = 0x04,
        DNL810 = 0x05,
    }

    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "Generic DataNet Devices")]
    internal class DataNetDevicesInterfacer : GenericDeviceInterfacer<GenericDevice>, ISerialDeviceInterfacer
    {
        #region Fields
        [Import]
        private ISerialDeviceManager parentDeviceManager;

        private readonly string NEW_LINE = "END";
        private readonly int BAUD_RATE = 19200;
        private readonly Encoding ENCODING = System.Text.Encoding.GetEncoding("iso-8859-1");

        #endregion
        #region Constructor
        public DataNetDevicesInterfacer()
            :base()
        {

        }

        #endregion
        #region Properties
        protected override byte Type
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #region IDeviceInterfacer
        protected override IDeviceManager ParentDeviceManager
        {
            get { return parentDeviceManager; }
        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            return false;
        }

        public override bool CanCreate(ConnectionEventArgs e)
        {
            bool toReturn = false;

            if (e.DeviceIO.ShortId.IsComPort())
            {
                ISerialDeviceIO serial = e.DeviceIO as ISerialDeviceIO;
                serial.ApplyChanges(BAUD_RATE, ENCODING, NEW_LINE);

                toReturn = true;
            }
            else if (e.DeviceIO is ILanReceiverDeviceIO)
            {
                toReturn = true;
            }

            return toReturn;
        }

        protected override GenericDevice CreateNewDevice()
        {
            return new GenericDataNetDevice(ParentDeviceManager);
        }
  
        #endregion
        #region Private Methods

        #endregion
        public override void CreateFromDeviceAndReport(GenericDevice genericDevice, int userId)
        {
            moduleCreateFrom(genericDevice,userId);
        }

        protected void BaseCreateFromDeviceAndReport(GenericDevice device, int userId)
        {
            base.CreateFromDeviceAndReport(device,userId);
        }

        /// <summary>
        /// Finds the right sub interfacer and creates the device
        /// </summary>
        /// <param name="dataNetDevice"></param>
        private void moduleCreateFrom(GenericDevice dataNetDevice, int userId)
        {
            ISubDeviceInterfacer module = findSubDeviceModuleFrom(dataNetDevice);

            if (module != null)
            {
                module.CreateFromDeviceAndReport(dataNetDevice,userId);
            }
        }

        private static ISubDeviceInterfacer findSubDeviceModuleFrom(GenericDevice genericDevice)
        {
            var module = ModuleManager.Modules.FirstOrDefault(
                x => x is ISubDeviceInterfacer
                    && (x as ISubDeviceInterfacer).CanCreateFrom(genericDevice));

            return module as ISubDeviceInterfacer;
        }
    }
}