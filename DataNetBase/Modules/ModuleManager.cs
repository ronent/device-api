﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.Serial;
using Base.Modules.Interfacer;
using DataNetBase.Devices.Management;
using DataNetBase.Modules.Interfacer;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Modules
{
    public class ModuleManager : IDisposable
    {
        #region Static
        private static ModuleManager instance = new ModuleManager();

        internal static List<IDeviceInterfacer> Modules
        {
            get
            {
                return (from module in instance.modules
                        select module.Value).ToList();
            }
        }

        #endregion
        #region Fields/Properties
        private CompositionContainer _container;

        #pragma warning disable
        [ImportMany]
        IEnumerable<Lazy<IDeviceInterfacer, IDeviceInterfacerMetadata>> modules;
        #pragma warning enable

        #endregion
        #region Initializers
        internal static void Start(DataNetDeviceManager parent)
        {
            instance.loadModuleDLLs(parent);
        }

        private void loadModuleDLLs(DataNetDeviceManager parent)
        {
            var catalog = new AggregateCatalog();

            catalog.Catalogs.Add(new DirectoryCatalog(".", "ModuleData*.dll"));
            catalog.Catalogs.Add(new DirectoryCatalog(".", Assembly.GetExecutingAssembly().ManifestModule.Name));
            _container = new CompositionContainer(catalog);

            try
            {
                _container.ComposeExportedValue(parent as ISerialDeviceManager);
                _container.ComposeParts(this);
                goOverModules();
            }
            catch (Exception e)
            {
                Log.Instance.WriteError("[API] ==> Fail in loadModuleDLLs", this, e);
            }
        }

        private void goOverModules()
        {
            foreach (var module in modules)
                Log.Instance.Write("[API] ==> DataNet module [" + module.Metadata.Name + "] loaded successfully", this);
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        #endregion
    }
}
