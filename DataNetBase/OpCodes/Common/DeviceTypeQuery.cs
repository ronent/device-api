﻿using Auxiliary.MathLib;
using DataNetBase.DataStructures.Logger;
using DataNetBase.Devices.Types;
using DataNetBase.Modules.Interfacer;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.OpCodes.Common
{
    [Serializable]
    internal class DeviceTypeQuery : DataNetOpCode
    {
        #region Fields
        private static readonly byte[] opCode = new byte[] { 0x01 };

        #endregion
        #region Constructor
        public DeviceTypeQuery(DataNetDevice device)
            :base(device)
        {

        }

        public DeviceTypeQuery(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return opCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return opCode; }
        }

        private string SerialNumber
        {
            get
            {
                if (ParentDevice != null)
                    return ParentDevice.Status.SerialNumber;
                else if (ParentLogger != null)
                    return ParentLogger.Status.SerialNumber;
                else
                    return string.Empty;
            }
            set
            {
                if (ParentDevice != null)
                    ParentDevice.Status.SerialNumber = value;
                else if (ParentLogger != null)
                    ParentLogger.Status.SerialNumber = value;
            }
        }

        private Version FirmwareVersion
        {
            get
            {
                if (ParentDevice != null)
                    return ParentDevice.Status.FirmwareVersion;
                else if (ParentLogger != null)
                    return ParentLogger.Status.FirmwareVersion;
                else
                    return default(Version);
            }
            set
            {
                if (ParentDevice != null)
                    ParentDevice.Status.FirmwareVersion = value;
                else if (ParentLogger != null)
                    ParentLogger.Status.FirmwareVersion = value;
            }
        }

        private eDataNetDevicesType Type
        {
            get
            {
                if (ParentDevice != null)
                    return ParentDevice.Status.Type;
                else if (ParentLogger != null)
                    return ParentLogger.Status.Type;
                else
                    return default(eDataNetDevicesType);
            }
            set
            {
                if (ParentDevice != null)
                    ParentDevice.Status.Type = value;
                else if (ParentLogger != null)
                    ParentLogger.Status.Type = value;
            }
        }

        private Version ZigBeeStack
        {
            get
            {
                if (ParentDevice != null)
                    return ParentDevice.Status.ZigBeeStack;
                else if (ParentLogger != null)
                    return ParentLogger.Status.ZigBeeStack;
                else
                    return default(Version);
            }
            set
            {
                if (ParentDevice != null)
                    ParentDevice.Status.ZigBeeStack = value;
                else if (ParentLogger != null)
                    ParentLogger.Status.ZigBeeStack = value;
            }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            if (ParentDevice != null)
                ParentDevice.InitializeStatus();
            else if (ParentLogger != null)
                ParentLogger.InitializeStatus();
        }

        protected override void Parse()
        {
            base.Parse();

            parseDeviceType();
            parseFirmwareVersion();
            parseSN();
            parseStackVersion();
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Parse Methods
        private void parseSN()
        {
            StringBuilder sb = new StringBuilder();
            byte[] sn = InReport.GetBlock(DataNetLoggerSetupConfiguration.AllowedSNLength);

            foreach (byte item in sn)
            {
                sb.Append((item >> 4).ToString() + (item & 0x0F).ToString());
            }

            SerialNumber = Convert.ToInt64(sb.ToString()).ToString();
        }

        private void parseFirmwareVersion()
        {
            byte pcb = InReport.Next;

            byte integer = InReport.Next;
            byte fraction = InReport.Next;
            byte build = InReport.Next;

            FirmwareVersion = new Version(integer, fraction, build);
        }

        private void parseDeviceType()
        {
            byte type = (byte)InReport.Next;

            if (Enum.IsDefined(typeof(eDataNetDevicesType), type))
                Type = (eDataNetDevicesType)type;
        }

        private void parseStackVersion()
        {
            byte build = InReport.Next;
            byte version = InReport.Next;

            ZigBeeStack = new Version(version >> 4, version & 0x0F, (build & 0x0F) + (10 * (build & 0xF0) >> 4));
        }

        #endregion
    }
}
