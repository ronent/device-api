﻿using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.OpCodes.Common
{
    [Serializable]
    internal class SetTimeAndDate : DataNetOpCode
    {
        #region Fields

        #endregion
        #region Constructor
        public SetTimeAndDate(DataNetDevice device)
            :base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { throw new NotImplementedException(); }
        }

        public override byte[] ReceiveOpCode
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #region Methods
        protected override void Parse()
        {
            throw new NotImplementedException();
        }

        protected override void Populate()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
