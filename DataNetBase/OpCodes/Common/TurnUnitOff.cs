﻿using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.OpCodes.Common
{
    [Serializable]
    internal class TurnUnitOff : OpCodeNoParseAck
    {
        #region Fields
        private static readonly byte[] opCode = new byte[] { 0x02 };

        #endregion
        #region Constructor
        public TurnUnitOff(DataNetDevice device)
            :base(device)
        {

        }

        public TurnUnitOff(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return opCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return opCode; }
        }

        #endregion
        #region Methods

        #endregion
    }
}
