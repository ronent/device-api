﻿using Base.OpCodes.Types;
using DataNetBase.DataStructures;
using DataNetBase.Devices.Types;
using Infrastructure.Communication.DeviceIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.OpCodes.Management
{
    [Serializable]
    internal abstract class DataNetOpCode : OpCode
    {
        #region Fields
        private static readonly byte[] ackHeader = new byte[] { 0x7F };
        private static readonly TimeSpan INVOKE_TIME_OUT_DURATION = new TimeSpan(0, 1, 0);

        #endregion
        #region Constructor
        public DataNetOpCode(DataNetDevice device)
            :base(device)
        {

        }

        public DataNetOpCode(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        public override byte[] AckHeader
        {
            get { return ackHeader; }
        }

        public override TimeSpan InvokeTimeOutDuration
        {
            get
            {
                if (ParentOpCodeManager.DeviceIO is ILanReceiverDeviceIO)
                    return INVOKE_TIME_OUT_DURATION;
                else
                    return base.InvokeTimeOutDuration;
            }
        }

        #endregion
        #region Override
        new protected DataNetOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as DataNetOpCodeManager; }
        }

        new protected DataNetDevice ParentDevice
        {
            get { return base.ParentDevice as DataNetDevice; }
            set { base.ParentDevice = value; }
        }

        protected DataNetLogger ParentLogger
        {
            get { return base.ParentDevice as DataNetLogger; }
            set { base.ParentDevice = value; }
        }

        #endregion
        #region Methods
        protected override void Parse()
        {
            byte lqi = InReport.Next;
        }

        protected void PopulateSendOpCode()
        {
            OutReport.Insert(SendOpCode);
        }

        protected override byte[] GetOutReportContent()
        {
            return OutReport.GetTrimmedContent();            
        }

        #endregion
    }
}
