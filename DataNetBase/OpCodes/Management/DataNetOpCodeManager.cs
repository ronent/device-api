﻿using Auxiliary.Tools;
using Base.OpCodes;
using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Extentions;
using Infrastructure.Communication.DeviceIO;

namespace DataNetBase.OpCodes.Management
{
    [Serializable]
    internal class DataNetOpCodeManager : OpCodeManager
    {
        #region Consts
        public const int GENERIC_REPORT_SIZE = 65;
        public static readonly byte[] STRT = new byte[] { 0x53, 0x54, 0x52, 0xEC };
        public const string CRLF = "\r\n";

        #endregion
        #region Fields
        private bool showLoggerLog = false;
        private byte sequence = 0; 

        #endregion
        #region Constructor
        public DataNetOpCodeManager(DataNetDevice device)
            :base(device)
        {

        }

        public DataNetOpCodeManager(DataNetLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override int OpCodeStartByte
        {
            get { return 0; }
        }

        public override int MaximumArraySize
        {
            get { return GENERIC_REPORT_SIZE; }
        }

        private bool IsLanReceiver
        {
            get { return DeviceIO is ILanReceiverDeviceIO; }
        }

        #endregion
        #region OpCodeManager
        protected override void DataReceived(byte[] packet, bool propogateAck)
        {
            try
            {
                if (IsLanReceiver)
                {
                    base.DataReceived(packet, propogateAck);
                }
                else
                {
                    byte[] data = parseMessageHeader(packet);
                    base.DataReceived(data, propogateAck);
                }
            }
            catch { }
        }

        public override void SendData(byte[] data)
        {
            try
            {
                if (IsLanReceiver)
                {
                    base.SendData(data);
                }
                else
                {
                    byte[] packet = populateMessageHeader(data);
                    base.SendData(packet);
                }
            }
            catch { }
        }

        protected override string GetOpCodeFullName(string name)
        {
            if (name.Contains("Base"))
                return name.Replace(".V1", ".Common");
            else
                return base.GetOpCodeFullName(name);
        }

        #endregion
        #region Private Methods
        #region Parse
        private byte[] parseMessageHeader(byte[] packet)
        {
            checkSTRT(ref packet);
            int dataLength = trim(ref packet);
            if (checkCRC(packet, dataLength))
                return removeMessageHeaders(packet, dataLength);
            else
                throw new Exception("checksum error.");
        }

        private int trim(ref byte[] packet)
        {
            int length = (packet[4] << 8) + packet[5];
            packet = Utilities.SubArray(packet, 0, length + 12);

            return length;
        }

        private byte[] removeMessageHeaders(byte[] packet, int length)
        {
            return Utilities.SubArray(packet, 7, length + 1);
        }

        private bool checkCRC(byte[] data, int dataLength)
        {
            int received = BitConverter.ToInt32(Utilities.SubArray(data, dataLength + 8, 4), 0);
            int computed = Auxiliary.Tools.CRC32.Compute(Utilities.SubArray(data, 0, dataLength + 8));

            return received == computed;
        }

        private void checkSTRT(ref byte[] data)
        {
            int index = data.IndexOf(STRT);

            if (index < 0)
                throw new Exception("Illegal message, STRT error.");
            else if (index != 0)
            {
                if (showLoggerLog)
                {
                    byte[] logData = Utilities.SubArray(data, 0, index);
                    Log4Tech.Log.Instance.Write("[API] ==> Device: '{0}' Inner log: {1}", this, Log4Tech.Log.LogSeverity.INFO, ParentDevice, Encoding.UTF8.GetString(logData).Replace(CRLF, string.Empty));
                }

                data = Utilities.SubArray(data, index, data.Length - index);
            }
        }

        #endregion
        #region Populate
        private byte[] populateMessageHeader(byte[] data)
        {
            byte[] arr = new byte[data.Length + 11];

            addSTRT(ref arr);
            addLength(ref arr, data.Length - 1); // excluding the opcode from the data
            addSequence(ref arr);
            addData(ref arr, data);
            addCRC(ref arr, data.Length + 7);
            trim(ref arr);

            return arr;
        }

        private void addSTRT(ref byte[] arr)
        {
            for (int i = 0; i < STRT.Length; i++)
            {
                arr[i] = STRT[i];
            }
        }

        private void addLength(ref byte[] arr, int length)
        {
            byte[] len = BitConverter.GetBytes((Int16)length);
            Array.Reverse(len);

            for (int i = 0; i < len.Length; i++)
            {
                arr[4 + i] = len[i];
            }
        }

        private void addSequence(ref byte[] arr)
        {
            arr[6] = (byte)sequence++;
        }

        private void addData(ref byte[] arr, byte[] data)
        {
            for (int index = 0; index < data.Length; index++)
            {
                arr[7 + index] = data[index];
            }
        }

        private void addCRC(ref byte[] arr, int index)
        {
            int crc = Auxiliary.Tools.CRC32.Compute(Utilities.SubArray(arr, 0, index));
            byte[] crcArr = BitConverter.GetBytes(crc);

            for (int i = 0; i < crcArr.Length; i++)
            {
                arr[index + i] = crcArr[i];
            }
        }

        #endregion
        #endregion
        #region OpCodes
        //internal AntiTamper AntiTamper;
        //internal BatteryLevel BatteryLevel;
        //internal DeviceLeftNetwork DeviceLeftNetwork;
        internal DeviceTypeQuery DeviceTypeQuery;
        //internal FirmwareUpdate FirmwareUpdate;
        //internal IndicateDevice IndicateDevice;
        //internal LeaveNetwork LeaveNetwork;
        //internal ResetDevice ResetDevice;
        //internal SetExtendedPANID SetExtendedPANID;
        //internal SetSerialNumber SetSerialNumber;
        //internal SetTimeAndDate SetTimeAndDate;
        //internal StartSignalTest StartSignalTest;
        //internal TurnUnitOff TurnUnitOff;

        #endregion
    }
}
