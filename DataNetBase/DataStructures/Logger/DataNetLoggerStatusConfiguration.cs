﻿using Base.DataStructures.Logger;
using DataNetBase.Modules.Interfacer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.DataStructures.Logger
{
    /// <summary>
    /// Status Configuration Settings For DataNet logger.
    /// </summary>
    [Serializable]
    public abstract class DataNetLoggerStatusConfiguration : BaseLoggerStatusConfiguration, IDataNetStatusConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetLoggerStatusConfiguration"/> class.
        /// </summary>
        public DataNetLoggerStatusConfiguration()
            :base()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the logger type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public eDataNetDevicesType Type { get; internal set; }

        /// <summary>
        /// Gets the zig bee stack.
        /// </summary>
        /// <value>
        /// The zig bee stack.
        /// </value>
        public Version ZigBeeStack { get; internal set; }

        #endregion
    }
}
