﻿using Base.DataStructures.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.DataStructures.Logger
{
    /// <summary>
    /// Setup Configuration Settings For DataNet logger.
    /// </summary>
    [Serializable]
    public abstract class DataNetLoggerSetupConfiguration : BaseLoggerSetupConfiguration
    {
        #region Fields
        public static readonly int AllowedSNLength = 4;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetLoggerSetupConfiguration"/> class.
        /// </summary>
        public DataNetLoggerSetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
