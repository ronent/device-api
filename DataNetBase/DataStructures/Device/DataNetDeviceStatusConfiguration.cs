﻿using Base.DataStructures.Device;
using DataNetBase.Modules.Interfacer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.DataStructures.Device
{
    /// <summary>
    /// Status Configuration Settings For DataNet device.
    /// </summary>
    [Serializable]
    public class DataNetDeviceStatusConfiguration : BaseDeviceStatusConfiguration, IDataNetStatusConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetDeviceStatusConfiguration"/> class.
        /// </summary>
        internal DataNetDeviceStatusConfiguration()
            :base()
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the device type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public eDataNetDevicesType Type { get; internal set; }

        /// <summary>
        /// Gets the zig bee stack.
        /// </summary>
        /// <value>
        /// The zig bee stack.
        /// </value>
        public Version ZigBeeStack { get; internal set; }

        #endregion
    }
}
