﻿using Base.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.DataStructures.Device
{
    /// <summary>
    /// Setup Configuration Settings For DataNet device.
    /// </summary>
    [Serializable]
    public abstract class DataNetDeviceSetupConfiguration : BaseDeviceSetupConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetDeviceSetupConfiguration"/> class.
        /// </summary>
        public DataNetDeviceSetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
