﻿using Base.DataStructures;
using Base.Devices;
using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.DataStructures
{
    [Serializable]
    public class DataNetVersion : GenericVersion
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericVersion"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal DataNetVersion(GenericDevice device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the parent device.
        /// </summary>
        /// <value>
        /// The parent device.
        /// </value>
        new internal DataNetDevice ParentDevice
        {
            get { return base.ParentDevice as DataNetDevice; }
            set { base.ParentDevice = value; }
        }

        internal DataNetLogger ParentLogger
        {
            get { return base.ParentDevice as DataNetLogger; }
            set { base.ParentDevice = value; }
        }

        public override Version Firmware
        {
            get { return ParentDevice.Status.FirmwareVersion; }
        }

        public override byte PCBAssembly
        {
            get { throw new NotImplementedException(); }
        }

        public override byte PCBVersion
        {
            get { throw new NotImplementedException(); }
        }

        public override byte FirmwareRevision
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
