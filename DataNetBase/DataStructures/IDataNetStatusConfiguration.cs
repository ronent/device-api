﻿using DataNetBase.Modules.Interfacer;
using System;
namespace DataNetBase.DataStructures
{
    interface IDataNetStatusConfiguration
    {
        eDataNetDevicesType Type { get; }
        Version ZigBeeStack { get; }
    }
}
