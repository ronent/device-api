﻿using Base.DataStructures.Device;
using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.OpCodes;
using DataNetBase.DataStructures;
using DataNetBase.DataStructures.Device;
using DataNetBase.Functions.Management;
using DataNetBase.OpCodes.Management;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Devices.Types
{
    /// <summary>
    /// DataNet Device
    /// </summary>
    [Serializable]
    public abstract class DataNetDevice : GenericDevice
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal DataNetDevice(IDeviceManager parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            try
            {
                Version = new DataNetVersion(this);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Initialize DataNetDevice", this, ex);
            }
        }

        #endregion
        #region GenericDevice
        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal DataNetOpCodeManager OpCodes
        {
            get { return base.OpCodes as DataNetOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public DataNetDeviceStatusConfiguration Status
        {
            get { return base.Status as DataNetDeviceStatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public DataNetDeviceFunctionsManager Functions
        {
            get { return base.Functions as DataNetDeviceFunctionsManager; }
            protected set { base.Functions = value; }
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected DataNetDevice(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}