﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using DataNetBase.DataStructures;
using DataNetBase.DataStructures.Logger;
using DataNetBase.Functions.Management;
using DataNetBase.OpCodes.Management;
using DataNetBase.Sensors.Management;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Devices.Types
{
    /// <summary>
    /// DataNet Logger.
    /// </summary>
    [Serializable]
    public abstract class DataNetLogger : GenericLogger
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        public DataNetLogger(IDeviceManager parent)
            :base(parent)
        {

        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            try
            {
                Version = new DataNetVersion(this);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Initialize DataNetLogger", this, ex);
            }
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the sensors manager.
        /// </summary>
        /// <value>
        /// The sensors.
        /// </value>
        new public DataNetSensorManager Sensors
        {
            get { return base.Sensors as DataNetSensorManager; }
            protected set { base.Sensors = value; }
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal DataNetOpCodeManager OpCodes
        {
            get { return base.OpCodes as DataNetOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public DataNetLoggerStatusConfiguration Status
        {
            get { return base.Status as DataNetLoggerStatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public DataNetLoggerFunctionsManager Functions
        {
            get { return base.Functions as DataNetLoggerFunctionsManager; }
            protected set { base.Functions = value; }
        }

        #endregion
        #region GenericLogger

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected DataNetLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
