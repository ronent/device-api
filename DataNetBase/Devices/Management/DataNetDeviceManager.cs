﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.Serial;
using DataNetBase.Devices.Types;
using DataNetBase.Modules;
using DataNetBase.Modules.Interfacer;
using Infrastructure.Communication;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Devices.Management
{
    /// <summary>
    /// DataNet Device Manager.
    /// </summary>
    [Export(typeof(IDeviceManager))]
    [ExportMetadata("Name", "DataNet Devices Manager")]
    [Serializable]
    public class DataNetDeviceManager : GenericDeviceManager<GenericDevice>, ISerialDeviceManager, ISerializable
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetDeviceManager"/> class.
        /// </summary>
        internal DataNetDeviceManager()
            : base()
        {
            ModuleManager.Start(this);
        }

        #endregion
        #region IDeviceManager Methods
        /// <summary>
        /// Called when [device found].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        public override bool OnDeviceFound(object sender, ConnectionEventArgs e)
        {
            bool toReturn = false;

            try
            {
                var module = ModuleManager.Modules.FirstOrDefault(x => x.CanCreate(e));
                if (module != null)
                {
                    module.CreateFromIdAndReport(e);
                    toReturn = true;
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On OnDeviceFound()",this, ex);
            }

            return toReturn;
        }

        /// <summary>
        /// Subscribes the specified report method.
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="reportMethod">The report method.</param>
        /// <returns></returns>
        public override bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod)
        {
            foreach (var module in ModuleManager.Modules)
                if (module.GetDeviceClassType() == typeof(E))
                {
                    module.Subscribe<E>(reportMethod);
                    return true;
                }

            return false;
        }

        /// <summary>
        /// What to do when device disconnected.
        /// </summary>
        /// <param name="device">The device.</param>
        protected override void onDeviceRemove(GenericDevice device)
        {
            TurnOffline(device);
        }

        #endregion
        #region Serialization & Deserialization
        protected DataNetDeviceManager(SerializationInfo info, StreamingContext ctxt)
            :base(info,ctxt)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }       

        #endregion
    }
}