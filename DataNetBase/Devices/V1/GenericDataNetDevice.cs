﻿using Base.Devices.Management.DeviceManager;
using DataNetBase.DataStructures.Device;
using DataNetBase.Devices.Types;
using DataNetBase.Functions;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Devices.V1
{
    public class GenericDataNetDevice : DataNetDevice
    {
        #region Constructor
        internal GenericDataNetDevice(IDeviceManager parent)
            : base(parent)
        {

        }

        internal override void InitializeStatus()
        {
            Status = new DataNetDeviceStatusConfiguration();
        }

        internal override void InitializeOpCodesManager()
        {
            OpCodes = new DataNetOpCodeManager(this);
        }

        internal override void InitializeFunctionManager()
        {
            Functions = new GenericDataNetDeviceFunctionsManager(this);
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal DataNetOpCodeManager OpCodes
        {
            get { return base.OpCodes as DataNetOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public DataNetDeviceStatusConfiguration Status
        {
            get { return base.Status as DataNetDeviceStatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public GenericDataNetDeviceFunctionsManager Functions
        {
            get { return base.Functions as GenericDataNetDeviceFunctionsManager; }
            protected set { base.Functions = value; }
        }

        public override string DeviceTypeName
        {
            get { return "Generic DataNet Device"; }
        }

        public override Version MinimumRequiredFirmwareVersion
        {
            get { throw new NotImplementedException(); }
        }

        internal override string FirmwareDeviceName
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}
