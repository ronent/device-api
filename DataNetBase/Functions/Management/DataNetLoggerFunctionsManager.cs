﻿using Base.Functions.Management;
using Base.OpCodes;
using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Functions.Management
{
    /// <summary>
    /// Manager for all DataNet loggers functions.
    /// </summary>
    [Serializable]
    public abstract class DataNetLoggerFunctionsManager : LoggerFunctionsManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetLoggerFunctionsManager"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public DataNetLoggerFunctionsManager(DataNetLogger logger)
            :base(logger)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        new protected internal DataNetLogger Device { get { return base.Device as DataNetLogger; } set { base.Device = value; } }

        #endregion
        #region LoggerFunctionsManager

        /// <summary>
        /// Gets the type of the device.
        /// </summary>
        /// <returns></returns>
        internal override Task<Result> GetDeviceType()
        {
            return Device.OpCodes.DeviceTypeQuery.Invoke();
        }

        #endregion
    }
}
