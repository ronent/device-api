﻿using Base.Devices.Features;
using Base.Functions.Management;
using Base.OpCodes;
using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Functions.Management
{
    /// <summary>
    /// Manager for all DataNet devices functions.
    /// </summary>
    [Serializable]
    public abstract class DataNetDeviceFunctionsManager : DeviceFunctionsManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetDeviceFunctionsManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public DataNetDeviceFunctionsManager(DataNetDevice device)
            :base(device)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        new protected internal DataNetDevice Device { get { return base.Device as DataNetDevice; } set { base.Device = value; } }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    yield return item;
                }

                yield return new AvailableFunction(eDeviceFunction.TurnOff) { Visible = Device.IsOnline, Enabled = !IsBusy };
            }
        }

        #endregion
        #region DeviceFunctionsManager

        /// <summary>
        /// Gets the type of the device.
        /// </summary>
        /// <returns></returns>
        internal override Task<Result> GetDeviceType()
        {
            return Device.OpCodes.DeviceTypeQuery.Invoke();
        }

        #endregion
        #region New Functions
        public Task<Result> TurnOff()
        {
            return Task.FromResult(IsBusy ? new Result(eResult.BUSY) { MessageLog = BusyErrorMessage } : Result.OK);
        }

        #endregion
    }
}
