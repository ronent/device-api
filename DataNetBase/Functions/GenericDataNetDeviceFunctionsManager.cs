﻿using Base.OpCodes;
using DataNetBase.Devices.Types;
using DataNetBase.Devices.V1;
using DataNetBase.Functions.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Functions
{
    [Serializable]
    public class GenericDataNetDeviceFunctionsManager : DataNetDeviceFunctionsManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetDeviceFunctionsManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public GenericDataNetDeviceFunctionsManager(DataNetDevice device)
            :base(device)
        {

        }

        protected override void SubscribeOpCodesEvents()
        {

        }

        #endregion
        #region Properties
        new protected internal GenericDataNetDevice Device { get { return base.Device as GenericDataNetDevice; } set { base.Device = value; } }

        #endregion
        #region DataNetDeviceFunctionsManager
        public override bool IsUpdatingFirmware
        {
            get { return false; }
        }

        protected override string BusyErrorMessage
        {
            get { throw new NotImplementedException(); }
        }

        public override Task<Base.OpCodes.Result> SendSetup(Base.DataStructures.Device.BaseDeviceSetupConfiguration configuration)
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        public override Task<Result> GetStatus()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        public override Task<Base.OpCodes.Result> UploadFirmware()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        internal override Task<Result> GetStatusNoBusy()
        {
            return  Task.FromResult(Result.UNSUPPORTED);
        }

        #endregion
    }
}
