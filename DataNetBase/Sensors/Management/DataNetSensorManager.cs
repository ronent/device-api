﻿using Base.Devices;
using Base.Devices.Management.EventsAndExceptions;
using Base.Sensors.Management;
using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetBase.Sensors.Management
{
    /// <summary>
    /// DataNet Sensor Manager.
    /// </summary>
    [Serializable]
    public abstract class DataNetSensorManager : SensorManager
    {
        #region Fields

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetSensorManager"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DataNetSensorManager(DataNetLogger parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Initializes the specified parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal override void Initialize(GenericLogger parent)
        {
            base.Initialize(parent);
        }

        #endregion
        #region Properties

        #endregion
        #region SensorManager
        /// <summary>
        /// On status received.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        internal override void ParentLogger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            
        }

        #endregion
    }
}
