﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using SiMiAPI.Devices;
using SiMiBase.Devices.Features;

namespace SiMiTemperature.Devices
{
    [Serializable]
    public class SiMiTemperatureLogger : SiMiLogger
    {
        #region Members
        private static readonly string deviceTypeName = "SiMi Temperature Logger";
//        internal static readonly string firmwareDeviceName = "SiMi Temperature Logger";
        #endregion
        #region Properties
        protected override byte Type { get { return SiMiTemperatureInterfacer.TYPE; } }

        protected override byte Bits { get { return SiMiTemperatureInterfacer.BITS; } }

        public override string DeviceTypeName
        {
            get { return deviceTypeName; }
        }

        public override SiMiBaseFeature[] Features
        {
            get
            {
                return base.Features.Concat(new[]
                {
                    SiMiBaseFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }

        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new[] { eSensorType.PT100 };
            }
        }

        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        #endregion
        #region Constructors
        internal SiMiTemperatureLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiTemperatureLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

      
        #endregion
    }
}
