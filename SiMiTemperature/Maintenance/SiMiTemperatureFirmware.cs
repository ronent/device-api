﻿using System;
using System.Runtime.Serialization;

namespace SiMiTemperature.Maintenance
{
    [Serializable]
    internal class SiMiTemperatureFirmware : SiMiBase.Maintenance.SiMiBaseFirmware
    {
        public override string Name { get { return "SiMi Temperature"; } }

        public SiMiTemperatureFirmware(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public SiMiTemperatureFirmware()
        {

        }
    }
}
