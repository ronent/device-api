﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class StaticStatus: SiMiAPI.OpCodes.V1.StaticStatus
    {
        public StaticStatus(SiMiTemperatureLogger device)
            : base(device)
        {

        }

        #region Overrides of OpCode

        protected override void DoAfterFinished()
        {
            base.DoAfterFinished();
            ParentDevice.Status.FixedSensors.HumidityEnabled = true;
        }

        #endregion
    }
}
