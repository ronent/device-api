﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class DynamicStatus : SiMiAPI.OpCodes.V1.DynamicStatus
    {
        public DynamicStatus(SiMiTemperatureLogger device)
            : base(device)
        {

        }

        
    }
}
