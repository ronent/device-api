﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : SiMiAPI.OpCodes.V1.SetCalibration
    {
        public SetCalibration(SiMiTemperatureLogger device)
            : base(device)
        {

        }

      
    }
}
