﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : SiMiAPI.OpCodes.V1.GetCalibration
    {
        public GetCalibration(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
