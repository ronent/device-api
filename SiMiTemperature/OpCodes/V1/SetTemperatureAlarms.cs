﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class SetTemperatureAlarms: SiMiAPI.OpCodes.V1.SetTemperatureAlarms
    {
        public SetTemperatureAlarms(SiMiTemperatureLogger device)
            : base(device)
        {

        }

       
    }
}
