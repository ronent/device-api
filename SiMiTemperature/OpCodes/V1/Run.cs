﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class Run : SiMiAPI.OpCodes.V1.Run
    {
        public Run(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
