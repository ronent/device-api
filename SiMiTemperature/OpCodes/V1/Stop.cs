﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class Stop:SiMiAPI.OpCodes.V1.Stop
    {
        public Stop(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
