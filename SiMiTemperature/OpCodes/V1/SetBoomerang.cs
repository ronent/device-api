﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : SiMiAPI.OpCodes.V1.SetBoomerang
    {
        public SetBoomerang(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
