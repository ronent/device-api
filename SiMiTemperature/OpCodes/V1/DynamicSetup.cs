﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class DynamicSetup : SiMiAPI.OpCodes.V1.DynamicSetup
    {
        public DynamicSetup(SiMiTemperatureLogger device)
            : base(device)
        {

        }

        
    }
}
