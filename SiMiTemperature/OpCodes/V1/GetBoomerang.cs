﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : SiMiAPI.OpCodes.V1.GetBoomerang
    {
        public GetBoomerang(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
