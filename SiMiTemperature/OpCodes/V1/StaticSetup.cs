﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class StaticSetup: SiMiAPI.OpCodes.V1.StaticSetup
    {
        public StaticSetup(SiMiTemperatureLogger device)
            : base(device)
        {

        }

       
    }
}
