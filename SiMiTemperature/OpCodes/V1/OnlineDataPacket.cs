﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : SiMiAPI.OpCodes.V1.OnlineDataPacket
    {
        public OnlineDataPacket(SiMiTemperatureLogger device)
            : base(device)
        {

        }
    }
}
