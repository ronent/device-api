﻿using System;
using SiMiTemperature.Devices;

namespace SiMiTemperature.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : SiMiAPI.OpCodes.V1.DefaultCalibration
    {
        public DefaultCalibration(SiMiTemperatureLogger device) : base(device)

        {

        }
    }
}
