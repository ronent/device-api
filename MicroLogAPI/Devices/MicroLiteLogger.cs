﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using MicroLogAPI.DataStructures;
using MicroXBase.Devices.Features;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace MicroLogAPI.Devices
{
    /// <summary>
    /// MicroLite Device.
    /// </summary>
    public abstract class MicroLiteLogger : GenericMicroLogLogger, ISerializable
    {
        #region Properties
        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[]
                {
                    MicroXFeature.SHOW_MINMAX,
                    MicroXFeature.SET_MEMORY_SIZE,
                    MicroXFeature.LED_ON_ALARM,                    
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets the available memory sizes.
        /// </summary>
        /// <value>
        /// The available memory sizes.
        /// </value>
        public override MemorySizeEnum[] AvailableMemorySizes
        {
            get
            {
                return new MemorySizeEnum[] {
                    MemorySizeEnum.K8,
                    MemorySizeEnum.K32,
                };
            }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLiteLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLiteLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLiteLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLiteLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
