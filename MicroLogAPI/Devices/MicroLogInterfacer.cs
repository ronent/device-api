﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using Maintenance.Communication;
using MicroLogAPI.Devices;
using Base.Devices.Management;
using Base.OpCodes;
using Maintenance;

namespace MicroLogAPI.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "Generic MicroLog")]
    public class MicroLogInterfacer : GenericDeviceInterfacer<GenericMicroLogDevice>
    {
        public override DeviceID[] DeviceIDs
        {
            get
            {
                return (from module in ModuleManager.Modules
                        where module.GetDeviceClassType().IsSubclassOf(GetDeviceClassType())
                        from deviceid in module.DeviceIDs
                        select deviceid).ToArray();
            }
        }

        protected override byte Type { get { return 0x00; } }
        protected override byte Bits { get { return 0x00; } }

        public MicroLogInterfacer()
            : base()
        {
        }

        public override bool CanCreateFrom(MicroXDevice device)
        {
            return false;
        }

        public override void CreateFromAndReport(MicroXDevice genericDevice)
        {
            ModuleCreateFrom(genericDevice);
        }

        protected override MicroXDevice CreateNewDevice()
        {
            return new V1.GenericMicroLogDevice(ParentDeviceManager);
        }

        protected void ModuleCreateFrom(MicroXDevice microxDevice)
        {
            ISubDeviceInterfacer module = findSubDeviceModuleFrom(microxDevice);

            if (module != null)
            {
                module.CreateFromAndReport(microxDevice);
            }
        }

        private static ISubDeviceInterfacer findSubDeviceModuleFrom(MicroXDevice genericDevice)
        {
            var module = ModuleManager.Modules.FirstOrDefault(
                x => x is ISubDeviceInterfacer
                    && (x as ISubDeviceInterfacer).CanCreateFrom(genericDevice));
            
            return module as ISubDeviceInterfacer;
        }
    }
}
