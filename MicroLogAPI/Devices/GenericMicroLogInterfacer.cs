﻿using Base.Devices.Management;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Infrastructure.Communication;

namespace MicroLogAPI.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "Generic MicroLog")]
    internal class GenericMicroLogInterfacer : MicroXDeviceInterfacer<GenericMicroLogLogger>
    {
        #region Override Properties
        protected override byte Type { get { return 0x00; } }
        protected override byte Bits { get { return 0x00; } }

        public override List<DeviceID> DeviceIDs
        {
            get
            {
                return (from module in ModuleManager.Modules
                        where module.GetDeviceClassType().IsSubclassOf(GetDeviceClassType()) && module is IHIDLoggerInterfacer
                        from deviceid in (module as IHIDLoggerInterfacer).DeviceIDs
                        select deviceid).Distinct().ToList();
            }
        }

        #endregion
        #region Constructor
        public GenericMicroLogInterfacer()
            : base()
        {
        }

        #endregion
        #region Override Methods
        public override bool CanCreateFrom(GenericDevice device)
        {
            return false;
        }

        public override void CreateFromDeviceAndReport(GenericDevice genericDevice, int userId)
        {
            moduleCreateFrom(genericDevice as MicroXLogger,userId);
        }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.GenericMicroLogLogger(ParentDeviceManager as IHIDDeviceManager);
        }

        #endregion
        #region Private Methods
        /// <summary>
        /// Finds the right sub interfacer and creates the device
        /// </summary>
        /// <param name="microxDevice"></param>
        private void moduleCreateFrom(MicroXLogger microxDevice, int userId)
        {
            ISubDeviceInterfacer module = findSubDeviceModuleFrom(microxDevice);

            if (module != null)
            {
                module.CreateFromDeviceAndReport(microxDevice,userId);
            }
        }

        private static ISubDeviceInterfacer findSubDeviceModuleFrom(MicroXLogger genericDevice)
        {
            var module = ModuleManager.Modules.FirstOrDefault(
                x => x is ISubDeviceInterfacer
                    && (x as ISubDeviceInterfacer).CanCreateFrom(genericDevice));
            
            return module as ISubDeviceInterfacer;
        }

        #endregion
    }
}