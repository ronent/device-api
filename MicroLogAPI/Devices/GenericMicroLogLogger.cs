﻿using Base.DataStructures.Device;
using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Base.Sensors.Management;
using Log4Tech;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Functions.Management.V1;
using MicroLogAPI.OpCodes.Management;
using MicroLogAPI.Sensors.Management;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Features;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace MicroLogAPI.Devices
{
    [Serializable]
    public abstract class GenericMicroLogLogger : MicroXLogger, IDisposable, ISerializable
    {
        #region Members
        private static readonly string deviceTypeName = "Generic MicroLog Device";
        private static readonly string firmwareDeviceName = "MicroLite II";
        private static readonly Version MINIMUM_FW_VERSION = new Version(0, 0);
        private static readonly byte allowedAveragePoints = 7;

        #endregion
        #region Properties
        /// <summary>
        /// Gets the allowed average points.
        /// </summary>
        /// <value>
        /// The allowed average points.
        /// </value>
        public virtual byte AllowedAveragePoints { get { return allowedAveragePoints; } }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return 0; } }
        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return 0; } }

        /// <summary>
        /// Gets the version.
        /// </summary>
        /// <value>
        /// The version.
        /// </value>
        new public MicroXVersion Version
        {
            get { return base.Version as MicroXVersion; }
            protected set { base.Version = value; }
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal MicroLogOpCodeManager OpCodes
        {
            get { return base.OpCodes as MicroLogOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public MicroLogStatusConfiguration Status
        {
            get { return base.Status as MicroLogStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public MicroLogLoggerFunctionsManager Functions
        {
            get { return base.Functions as MicroLogLoggerFunctionsManager; }
            set { base.Functions = value; }
        }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the available memory sizes.
        /// </summary>
        /// <value>
        /// The available memory sizes.
        /// </value>
        public virtual MemorySizeEnum[] AvailableMemorySizes
        {
            get
            {
                return new MemorySizeEnum[] { MemorySizeEnum.K52 };
            }
        }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new []
                { 
                    MicroXFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }
        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get { return new eSensorType[] { }; }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get { return new eSensorType[] { }; }
        }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMicroLogLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal GenericMicroLogLogger(IHIDDeviceManager parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            if (ParentDeviceManager != null)
            {
                try
                {
                    Memory = new MicroLogMemorySize(this);
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in Initialize GenericMicroLogDevice", this, ex);
                }
            }
        }

        internal override void InitializeFunctionManager()
        {
            Functions = new MicroLogLoggerFunctionsManager(this);
        }

        internal override void InitializeOpCodesManager()
        {
            OpCodes = new MicroLogOpCodeManager(this);
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal override void InitializeStatus()
        {
            Status = new MicroLogStatusConfiguration();
        }

        /// <summary>
        /// Initializes the sensor manager.
        /// </summary>
        internal override void InitializeSensorManager()
        {
            Sensors = new MicroLogSensorManager(this);
        }

        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMicroLogLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected GenericMicroLogLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion 
    }
}