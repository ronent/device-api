﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace MicroLogAPI.Devices
{
    [Serializable]
    public abstract class MicroLog16BitLogger : GenericMicroLogLogger
    {
        #region Members
        private static readonly string firmwareDeviceName = "EC8xx 16Bit";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }
        #endregion
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLog16BitLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLog16BitLogger(IHIDDeviceManager parent)
            :base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLog16BitLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLog16BitLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
