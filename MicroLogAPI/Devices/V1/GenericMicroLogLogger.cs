﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using System;

namespace MicroLogAPI.Devices.V1
{
    /// <summary>
    /// Generic MicroLog device
    /// </summary>
    [Serializable]
    public class GenericMicroLogLogger : Devices.GenericMicroLogLogger
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericMicroLogLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal GenericMicroLogLogger(IHIDDeviceManager parent)
            : base(parent)
        {
        }

        #endregion
    }
}

