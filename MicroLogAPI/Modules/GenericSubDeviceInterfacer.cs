﻿using Auxiliary.Tools;
using Base.Devices.Management;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using System;
using Base.Devices.Management.EventsAndExceptions;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using System.Linq;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLogAPI.Modules
{
    internal abstract class GenericSubDeviceInterfacer<T> : MicroXDeviceInterfacer<T>, ISubDeviceInterfacer where T : GenericMicroLogLogger
    {
        #region Constructor
        public GenericSubDeviceInterfacer()
            :base()
        {

        }

        #endregion
        #region Methods
        public override bool CanCreate(ConnectionEventArgs e)
        {
            if (!(e is HIDConnectionEventArgs))
                return false;

            return DeviceIDs.Any(x => x.VID.Equals(MicroXLoggerManager.VID) && x.Equals((e as HIDConnectionEventArgs).DeviceID));
        }

        #endregion
    }
}
