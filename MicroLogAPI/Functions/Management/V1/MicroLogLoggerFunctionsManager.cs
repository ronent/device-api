﻿using Base.Devices.Features;
using Base.Functions.Management;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Features;
using MicroXBase.Functions.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogAPI.Functions.Management.V1
{
    [Serializable]
    public class MicroLogLoggerFunctionsManager : MicroXLoggerFunctionsManager
    {
        #region Constructor
        public MicroLogLoggerFunctionsManager(GenericMicroLogLogger parent)
            :base(parent)
        {

        }

        protected override void SubscribeOpCodesEvents()
        {
            base.SubscribeOpCodesEvents();

            if (Device.OpCodes.Download != null)
                Device.OpCodes.Download.OnFinished += (sender, e) =>
                {
                    if (e.IsOK)
                        InvokeOnDownloadCompleted(sender, new DownloadCompletedArgs(Device));
                };
        }

        #endregion
        #region Properties
        new protected internal GenericMicroLogLogger Device { get { return base.Device as GenericMicroLogLogger; } set { base.Device = value; } }

        /// <summary>
        /// Gets the state of each function.
        /// </summary>
        /// <value>
        /// The state of the functions.
        /// </value>
        public override IEnumerable<AvailableFunction> FunctionsState
        {
            get
            {
                foreach (var item in base.FunctionsState)
                {
                    yield return item;
                }

                yield return new AvailableFunction(eDeviceFunction.LoadDefaultCalibration) { Visible = Device.IsOnline, Enabled = !IsRunning && !IsBusy };
                yield return new AvailableFunction(eDeviceFunction.SaveDefaultCalibration) { Visible = Device.IsOnline, Enabled = !IsUpdatingFirmware };
            }
        }

        /// <summary>
        /// Gets the available functions.
        /// </summary>
        /// <value>
        /// The available functions.
        /// </value>
        new public static IEnumerable<eDeviceFunction> AvailableFunctions
        {
            get
            {
                foreach (var item in MicroXLoggerFunctionsManager.AvailableFunctions)
                {
                    yield return item;
                }

                yield return eDeviceFunction.LoadDefaultCalibration;
                yield return eDeviceFunction.SaveDefaultCalibration;
            }
        }

        #endregion
        #region Override Functions

        public override Task<Result> GetStatus()
        {
            var isBusy = false;

            try
            {
                isBusy = IsBusy;
            }
            catch
            {
                // ignored
            }
            var result = new Result(eResult.BUSY) {MessageLog = BusyErrorMessage};
            if (!isBusy)
                result = GetStatusNoBusy().Result;

            return Task.FromResult(result);
        }

        internal override Task<Result> GetStatusNoBusy()
        {
                var result = Device.OpCodes.Status.Invoke().Result;

                result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetCalibration);
                result.SynthesizeResultAndThrowOnError(GetBoomerang().Result);

                if (result.IsOK)
                    InvokeOnStatus();

                return Task.FromResult(result);
        }

        protected override Task<Result> DoAfterSetup()
        {
            var result = Result.OK;

            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                result.SynthesizeResultAndThrowOnError(Device.OpCodes.Setup.SetMemorySize());

            result.SynthesizeResultAndThrowOnError(Device.OpCodes.Setup.RunSetDefinedSensor());
            result.SynthesizeResultAndThrowOnError(Device.OpCodes.Setup.RunTimerRun());

            return Task.FromResult(result);
        }

        #endregion
        #region New Functions

        public Task<Result> RestoreDefaultCalibration()
        {
            var result = CheckRunningAndBusy().Result;
            return !result.IsOK ? Task.FromResult(result) : Device.OpCodes.DefaultCalibration.Restore();
        }

        public virtual Task<Result> SaveDefaultCalibration()
        {
            if (Base.Settings.Settings.Current.Name != Base.Settings.eEnvironment.Producation)
                return Task.FromResult(new Result(new Exception("Unsupported feature in this version.")));
            var result = CheckRunningAndBusy().Result;
            return !result.IsOK ? Task.FromResult(result) : Device.OpCodes.DefaultCalibration.Save();
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public MicroLogLoggerFunctionsManager(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}