﻿
namespace MicroLogAPI.OpCodes.Helpers
{
    class Consts
    {
        public struct Time
        {
            public const int TIME_SIZE = 4;
        }
    
        public struct Readings
        {
            public const int SAMPLE_SIZE = 2;
        }

        public struct DataPacket
        {
            public const long EXTERNAL_DUMMY = 65535;
        }

    }
}
