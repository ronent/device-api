﻿using MicroLogAPI.Devices;
using MicroLogAPI.OpCodes.V1;
using MicroXBase.OpCodes.Management;

namespace MicroLogAPI.OpCodes.Management
{
    internal class MicroLogOpCodeManager : MicroXOpCodeManager
    {
        public MicroLogOpCodeManager(GenericMicroLogLogger device)
            :base(device)
        {

        }

        #region OpCode Fields
        new internal Setup Setup
        {
            get { return base.Setup as Setup; }
            set { base.Setup = value; }
        }

        //internal MicroLogOpCode Status2;
        internal GetCalibration GetCalibration;
        internal SetDefinedSensor SetDefinedSensor;
        internal DefaultCalibration DefaultCalibration;
        internal SetMemorySize SetMemorySize;
        internal DataPacketWithTimeStamp DataPacketWithTimeStamp;
        internal TimerRun TimerRun;
        #endregion
    }
}
