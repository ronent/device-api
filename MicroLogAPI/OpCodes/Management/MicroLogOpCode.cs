﻿using MicroLogAPI.Devices;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroLogAPI.OpCodes.Management
{
    [Serializable]
    internal abstract class MicroLogOpCode : MicroXOpCode
    {
        public MicroLogOpCode(GenericMicroLogLogger device)
            : base(device)
        {

        }

        new protected GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        new protected MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }
    }
}
