﻿using Auxiliary.MathLib;
using Base.OpCodes;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Log4Tech;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.OpCodes.Management;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroXBase.OpCodes.Common.Status
    {
        #region Fields
        private static readonly byte[] sendOpCode = { 0x12 };
        private static readonly byte[] receiveOpCode = { 0x21 };
        private static readonly byte[] status2ReceiveOpCode = { 0x27 };

        #endregion
        #region Members
        IncomingReport status1;
        IncomingReport status2;

        SpareBytes customUnitSpare = new SpareBytes(18, 21);
        SpareBytes userDefined1 = new SpareBytes(25, 41);
        SpareBytes spare1 = new SpareBytes(37, 40);
        SpareBytes spare2 = new SpareBytes(42, 44);
        SpareBytes userDefined2 = new SpareBytes(47, 56);

        #endregion
        #region Constructors
        public Status(MicroXLogger device)
            : base(device) //OpCodes are overridden below
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new protected GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        new protected MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            parseStatus1();
            parseStatus2();

            status1 = status2 = null;
        }

        private void parseStatus1()
        {
            ParseSetupBooleans();
            ParseTime();
            ParseExternalSensorStatus();
            ParseDisplayConfiguration();
            ParseInterval();
            ParseAlarms();
            ParseUtcOffset();
            ParseAlarmInfo();
            ParseDeviceType();
            ParseFirmwareVersion();
            ParseAssembly();
            ParseBatteryLevel();
            ParseTimerStatus();
            parseSetup2();
            parseFirmwareVersion2();
        }

        private void parseStatus2()
        {
            parseSN();
            parseComment();

            if (ParentDevice.Status.ExternalSensor.UserDefined)
            {
                parseDefinedValues();
                parseUnitAndSignificantFigures();
            }
            else
                status2.Skip(userDefined1);

            status2.Skip(spare2);

            parseMemorySize();
            if (ParentDevice.Status.ExternalSensor.UserDefined)
                parseDefinedSensorName();
            else
                status2.Skip(userDefined2);
        }

        #endregion
        #region Status1 Parse Methods
        protected override void ParseSetupBooleans()
        {
            ParentDevice.Status.IsRunning = Convert.ToBoolean((byte)(status1.Current & 0x01));
            ParentDevice.Status.CyclicMode = Convert.ToBoolean((byte)(status1.Current & 0x02));
            ParentDevice.Status.PushToRunMode = Convert.ToBoolean((byte)(status1.Current & 0x04));
            ParentDevice.Status.FahrenheitMode = Convert.ToBoolean((byte)(status1.Current & 0x08));
            ParentDevice.Status.StopOnKeyPress = Convert.ToBoolean((byte)(status1.Current & 0x10));
            ParentDevice.Status.TestMode = Convert.ToBoolean((byte)(status1.Current & 0x20));
            ParentDevice.Status.StopOnDisconnect = Convert.ToBoolean((byte)(status1.Current & 0x40));
            ParentDevice.Status.BoomerangEnabled = Convert.ToBoolean((byte)(status1.Next & 0x80));

            ParentDevice.Status.FixedSensors.TemperatureEnabled = Convert.ToBoolean((byte)(status1.Current & 0x01));
            ParentDevice.Status.FixedSensors.HumidityEnabled = Convert.ToBoolean((byte)(status1.Current & 0x02));
            ParentDevice.Status.FixedSensors.DewPointEnabled = Convert.ToBoolean((byte)(status1.Current & 0x04));
            ParentDevice.Status.AveragePoints = Convert.ToByte((status1.Current & 0x38) >> 3);
            ParentDevice.Status.UnitRequiresSetup = Convert.ToBoolean((byte)(status1.Next & 0x80));
        }

        protected override void ParseTime()
        {
            byte[] block = status1.GetBlock(4);

            try
            {
                ParentDevice.Status.FirstPacketTime = MicroLogTime.Parse(block).ToDateTime();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("First packet time parsing error. 4 bytes block are: {0}", BitConverter.ToString(block)), ex);
            }
        }

        protected virtual void ParseExternalSensorStatus()
        {
            ParentDevice.Status.ExternalSensor.Type = getSensorType((byte)(status1.Current & 0x0F));

            bool userDefined = Convert.ToBoolean(status1.Current & 0x80);

            if (userDefined)
            {
                getUserDefinedUnit();
                ParentDevice.Status.ExternalSensor.UserDefinedSensor.BaseType = ParentDevice.Status.ExternalSensor.Type;
                ParentDevice.Status.ExternalSensor.Type = eSensorType.UserDefined;
            }

            status1.Skip(1);
        }

        protected override void ParseDisplayConfiguration()
        {
            ParentDevice.Status.LCDConfiguration = (LCDConfigurationEnum)((byte)(status1.Next & 0x0f));
        }

        private void getUserDefinedUnit()
        {
            byte unit = ((byte)((status1.Current & 0x70) >> 4));
            if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), unit))
                ParentDevice.Status.ExternalSensor.UserDefinedSensor.Unit = (ExternalSensorUnitEnum)unit;
            else
                ParentDevice.Status.ExternalSensor.UserDefinedSensor.Unit = ExternalSensorUnitEnum.Custom;
        }

        private eSensorType getSensorType(byte b)
        {
            switch (b)
            {
                case 0:
                    return eSensorType.None;
                case 1:
                    return eSensorType.Current4_20mA;
                case 2:
                    return eSensorType.Voltage0_10V;
                case 3:
                    return eSensorType.ExternalNTC;
                case 4:
                    return eSensorType.PT100;
                default:
                    Log.Instance.WriteError("",this,new Exception("Invalid sensor type. Assigning sensor type None."));
                    return eSensorType.None;
            }
        }

        protected override void ParseInterval()
        {
            ParentDevice.Status.Interval = new TimeSpan(0, 0, status1.Get<UInt16>());
        }

        protected override void ParseAlarms()
        {
            ParentDevice.Status.FixedSensors.TemperatureAlarm = ParseAlarm();
            ParentDevice.Status.FixedSensors.HumidityAlarm = ParseAlarm();
            ParentDevice.Status.FixedSensors.DewPointAlarm = ParseAlarm();
            ParentDevice.Status.ExternalSensor.Alarm = ParseAlarm();
        }

        protected override decimal ParseAlarmEnd()
        {
            Int32 integer = default(Int32);
            UInt32 fraction = default(UInt32);
            byte[] integerBlock = status1.GetBlock(2);
            byte fractionBlock = status1.Next;

            try
            {
                integer = BitConverter.ToInt16(integerBlock, 0);
                fraction = Convert.ToUInt16(fractionBlock);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in ParseAlarmEnd",this,ex);
            }

            IntegerFraction intFrac = new IntegerFraction(integer, fraction);
            return intFrac.Value;
        }

        protected override void ParseAlarmInfo()
        {
            byte[] alarm = status1.GetBlock(2);
        }

        protected override void ParseDeviceType()
        {
            ParentDevice.Status.DeviceType = status1.Next;
        }

        protected override void ParseFirmwareVersion()
        {
            byte integer = status1.Next;
            byte fraction = status1.Next;

            IntegerFraction intFrac = new IntegerFraction(integer, fraction);
            ParentDevice.Status.FirmwareVersion = new Version(intFrac.ToString()); //string.Format("{0}.{1:D2}", integer, fraction)
        }

        protected override void ParseAssembly()
        {
            ParentDevice.Status.PCBAssembly = status1.Next;
        }

        protected override void ParseBatteryLevel()
        {
            ParentDevice.Status.BatteryLevel = status1.Next;
        }

        protected override void ParseTimerStatus()
        {
            byte[] timerRun = status1.GetBlock(4);

            if (IsValidStartTime(timerRun))
            {
                try
                {
                    ParentDevice.Status.TimerStart = MicroLogTime.Parse(timerRun).ToDateTime();
                    if (ParentDevice.Status.TimerStart > DateTime.Now)
                        ParentDevice.Status.TimerRunEnabled = true;
                }
                catch (Exception ex)
                {
                    Log.Instance.WriteError("Fail in ParseTimerStatus",this, ex);
                }
            }
        }

        private void parseSetup2()
        {
            ParentDevice.Status.ShowMinMax = Convert.ToBoolean((byte)(status1.Current & 0x01));
            ParentDevice.Status.ShowPast24HMinMax = Convert.ToBoolean((byte)(status1.Current & 0x02));
            ParentDevice.Status.DeepSleepMode = Convert.ToBoolean((byte)(status1.Current & 0x04));
            ParentDevice.Status.FixedSensors.TemperatureAlarm.Enabled = Convert.ToBoolean((byte)(status1.Current & 0x08));
            ParentDevice.Status.FixedSensors.HumidityAlarm.Enabled = Convert.ToBoolean((byte)(status1.Current & 0x10));
            ParentDevice.Status.FixedSensors.DewPointAlarm.Enabled = Convert.ToBoolean((byte)(status1.Current & 0x40));
            ParentDevice.Status.ExternalSensor.Alarm.Enabled = Convert.ToBoolean((byte)(status1.Current & 0x20));
            ParentDevice.Status.EnableLEDOnAlarm = Convert.ToBoolean((byte)(status1.Next & 0x80));
        }

        private void parseFirmwareVersion2()
        {
            ParentDevice.Status.PCBVersion = status1.Next;
            ParentDevice.Status.FWRevision = status1.Next;

            byte[] supportedVersion = status1.GetBlock(4);
            supportedVersion[3] = 0; // This is just 3 bytes long but treated as 4 for convenience
            ParentDevice.Status.MinimumRequiredSWVersion = BitConverter.ToUInt32(supportedVersion, 0);
        }

        protected override void ParseUtcOffset()
        {
            ParentDevice.Status.UtcOffset = (sbyte)status1.Next;
//            ParentDevice.Status.UtcOffset -= 12; //For legacy purposes with minus
        }

        #endregion
        #region Status2 Parse Methods
        private void parseSN()
        {
            ParentDevice.Status.SerialNumber = status2.GetString(MicroLogSetupConfiguration.AllowedSNLength);
        }

        private void parseComment()
        {
            ParentDevice.Status.Comment = status2.GetString(MicroLogSetupConfiguration.AllowedCommentLength);
        }

        private void parseDefinedValues()
        {
            var outputValues = parseTwoSingles();
            var realValues = parseTwoSingles();

            LoggerReference[] pairs = 
            { 
                new LoggerReference { Logger = (decimal)outputValues.Item1, Reference = (decimal)realValues.Item1},
                new LoggerReference { Logger = (decimal)outputValues.Item2, Reference = (decimal)realValues.Item2},
            };

            ParentDevice.Status.ExternalSensor.UserDefinedSensor.Set(pairs);
        }

        private void parseCustomUnit()
        {
            if (ParentDevice.Status.ExternalSensor.UserDefinedSensor.Unit == MicroXBase.DataStructures.ExternalSensorUnitEnum.Custom)
                ParentDevice.Status.ExternalSensor.UserDefinedSensor.CustomUnit =
                    status2.GetString(ParentDevice.Status.ExternalSensor.UserDefinedSensor.AllowedCustomUnitLength);
            else
                status2.Skip(customUnitSpare);
        }

        private void parseUnitAndSignificantFigures()
        {
            byte b = (byte)(status2.Next & 0x0f);
            ParentDevice.Status.ExternalSensor.UserDefinedSensor.SignificantFigures = b;
        }

        private void parseMemorySize()
        {
            ushort memory = status2.Get<UInt16>(endianness: Auxiliary.Tools.Endianness.BIG);
            if (Enum.IsDefined(typeof(MemorySizeEnum), memory))
                ParentDevice.Status.MemorySize = (MemorySizeEnum)memory;
            else
                ParentDevice.Status.MemorySize = MemorySizeEnum.K8;
        }

        private void parseDefinedSensorName()
        {
            ParentDevice.Status.ExternalSensor.UserDefinedSensor.Name =
                status2.GetString(ParentDevice.Status.ExternalSensor.UserDefinedSensor.AllowedNameLength);
        }

        private Tuple<float, float> parseTwoSingles()
        {
            return new Tuple<float, float>(
                status2.GetSingle(),
                status2.GetSingle());
        }

        #endregion
        #region Override Methods
        protected override bool IsReceiveHeader(IncomingReport report)
        {
            if (base.IsReceiveHeader(report))
            {
                status1 = report;
                return true;
            }
            else
                if (report.SkipIfContainsBlock(status2ReceiveOpCode))
                {
                    status2 = report;
                    return true;
                }

            return false;
        }

        protected override bool ShouldContinueWithProcess()
        {
            if (status1 != null && status2 != null)
                return true;
            else
                return false;
        }
        #endregion
    }
}

