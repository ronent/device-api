﻿using MicroLogAPI.OpCodes.Management;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroXBase.OpCodes.Common.Download
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x15 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x23 };

        #endregion
        #region Constructors
        public Download(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        new public MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }


        #endregion
        #region Methods
        protected override bool ReachedEndOfData()
        {
            return MessageType == MessageTypeEnum.EndOfPackets || PacketCount == 0;
        }

        public override List<byte[]> GetReturnOpCodesToProcess()
        {
            List<byte[]> returnList = new List<byte[]>() { ParentOpCodeManager.DataPacketWithTimeStamp.ReceiveOpCode, ReceiveOpCode };
            returnList.AddRange(base.GetReturnOpCodesToProcess());

            return returnList;
        }

        protected override uint? GetExpectedPacketsCount()
        {
            return this.PacketCount + 1;
        }

        protected override List<byte[]> GetReturnOpCodesToFinish()
        {
            return new List<byte[]>() { new byte[] { ReceiveOpCode[0], (byte)MessageTypeEnum.EndOfPackets } };
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            ParseMessage();
            ParsePacketCount();
        }

        protected void ParseMessage()
        {
            MessageType = (MessageTypeEnum)InReport.Next;
            if (!Enum.IsDefined(typeof(MessageTypeEnum), MessageType))
                MessageType = MessageTypeEnum.EndOfPackets;
        }

        protected void ParsePacketCount()
        {
            if (MessageType == MessageTypeEnum.PacketCount || MessageType == MessageTypeEnum.MemoryFull)
            {
                PacketCount = InReport.Get<UInt16>(endianness:Auxiliary.Tools.Endianness.BIG);
            }
        }
        
        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateCommand();
        }

        protected override void PopulateCommand()
        {
            OutReport.Next = Convert.ToByte(Command);
        }

        protected override void SubscribeToOnDataPacketFinished()
        {
            base.SubscribeToOnDataPacketFinished();

            try
            {
                ParentOpCodeManager.DataPacketWithTimeStamp.OnFinished += DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On SubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        protected override void UnsubscribeToOnDataPacketFinished()
        {
            base.UnsubscribeToOnDataPacketFinished();

            try
            {
                ParentOpCodeManager.DataPacketWithTimeStamp.OnFinished -= DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch(Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On UnsubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        #endregion
    }
}
