﻿using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.OpCodes.Management;
using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class SetDefinedSensor : MicroXBase.OpCodes.Common.SetDefinedSensor
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x1B };

        #endregion
        #region Fields
        //SpareBytes spare1 = new SpareBytes(37, 63);

        #endregion
        #region Constructors
        public SetDefinedSensor(MicroXLogger deviceInfo)
            : base(deviceInfo)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        new protected GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        new protected MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }

        protected MicroLogUDSConfiguration DefinedSensor
        {
            get 
            {
                var config = ParentOpCodeManager.Setup.Configuration as IESensorSetup;
                if (config != null)
                    return config.ExternalSensor.UserDefinedSensor;
                else
                    return null;
            }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Populate data Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateOffset();
            PopulateGain();
            PopulateDefinedValues();
            //PopulateCustomUnit();
            PopulateUnitAndSignificantFigures();
            PopulateName();
        }

        protected virtual void PopulateOffset()
        {
            OutReport.Insert(DefinedSensor.Offset);
        }

        protected virtual void PopulateGain()
        {
            OutReport.Insert(DefinedSensor.Gain);
        }

        protected override void PopulateDefinedValues()
        {
            OutReport.Insert((float)DefinedSensor.References[0].Logger);
            OutReport.Insert((float)DefinedSensor.References[1].Logger);
            OutReport.Insert((float)DefinedSensor.References[0].Reference);
            OutReport.Insert((float)DefinedSensor.References[1].Reference);
        }

        protected override void PopulateCustomUnit()
        {
            OutReport.Insert(DefinedSensor.CustomUnit, DefinedSensor.AllowedCustomUnitLength);            
        }

        protected override void PopulateUnitAndSignificantFigures()
        {
            OutReport.Next = (byte)((byte)(DefinedSensor.Unit) << 4 | (byte)DefinedSensor.SignificantFigures);
        }

        protected override void PopulateName()
        {
            OutReport.Insert(DefinedSensor.Name, DefinedSensor.AllowedNameLength);
        }

        #endregion
    }
}
