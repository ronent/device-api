﻿using Base.Sensors.Types;
using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : OnlineDataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x29 };

        #endregion
        #region Constructors
        public OnlineTimeStamp(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Methods
        protected override void addSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOnline(value, dateTime, string.Empty);
        }

        #endregion
    }
}
