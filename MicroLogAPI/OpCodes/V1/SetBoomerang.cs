﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroXBase.OpCodes.Common.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
    }

}
