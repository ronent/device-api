﻿using Auxiliary.MathLib;
using Base.OpCodes;
using Base.Sensors.Management;
using Log4Tech;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.OpCodes.Management;
using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Threading.Tasks;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal abstract class Setup : MicroXBase.OpCodes.Common.Setup
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x11 };

        #endregion
        #region Fields
        SpareBytes alarmDelay = new SpareBytes(2);
        SpareBytes spare2 = new SpareBytes(9);
        SpareBytes alarmSpare = new SpareBytes(6);
        #endregion
        #region Constructors
        public Setup(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        new public MicroLogSetupConfiguration Configuration
        {
            get { return base.Configuration as MicroLogSetupConfiguration; }
            set { base.Configuration = value; }
        }

        new public GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        new public MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }
        #endregion
        #region Configuration Properties
        protected virtual bool TemperatureEnabled { get { return false; } }
        protected virtual bool HumidityEnabled { get { return false; } }
        protected virtual bool DewPointEnabled { get { return false; } }
        protected virtual bool ExternalEnabled { get { return false; } }
        protected virtual Alarm TemperatureAlarm { get { return new Alarm(); } }
        protected virtual Alarm HumidityAlarm { get { return new Alarm(); } }
        protected virtual Alarm DewPointAlarm { get { return new Alarm(); } }
        protected virtual Alarm ExternalAlarm { get { return new Alarm(); } }
        protected virtual bool DeepSleepMode { get { return false; } }
        protected virtual bool StopOnDisconnect { get { return false; } }
        protected virtual eSensorType ExternalType { get { return eSensorType.None; } }
        protected virtual bool UserDefined { get { return false; } }
        protected virtual MicroLogUDSConfiguration UserDefinedSensor { get { return null; } }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateSetupBooleans();
            PopulateTime();
            PopulateExternalSensorStatus();
            PopulateDisplayConfiguration();
            PopulateInterval();
            PopulateAlarms();
            PopulateUtcOffset();
            PopulateAlarmInfo();
            populateSetup2();
            OutReport.Skip(spare2);
            OutReport.Insert(Configuration.Comment, MicroLogSetupConfiguration.AllowedCommentLength); 
        }

        protected void PopulateExternalSensorStatus()
        {
            if (ExternalEnabled)
            {
                var unitType = getUnitType();

                OutReport.Next = (byte)
                    ((byte)getByteCode(ExternalType)
                    | ((byte)unitType << 4) 
                    | (Convert.ToByte(UserDefined) << 7));
            }
            else
                OutReport.Next = 0;
        }

        private byte getByteCode(eSensorType type)
        {
            if (UserDefined)
                type = UserDefinedSensor.BaseType;
            switch (type)
            {
                case eSensorType.None:
                    return 0;
                case eSensorType.Current4_20mA:
                    return 1;
                case eSensorType.Voltage0_10V:
                    return 2;
                case eSensorType.ExternalNTC:
                    return 3;
                case eSensorType.PT100:
                    return 4;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private ExternalSensorUnitEnum getUnitType()
        {
            if (UserDefinedSensor == null)
                return ExternalSensorUnitEnum.Custom;
            else
                return UserDefinedSensor.Unit;
        }

        protected override void PopulateSetupBooleans()
        {
            int ShiftIndex = 0;
            OutReport.Next = (byte)(
                        (Convert.ToByte(Configuration.CyclicMode) << ShiftIndex+1) |
                        (Convert.ToByte(Configuration.PushToRunMode) << ShiftIndex+2) |
                        (Convert.ToByte(Configuration.FahrenheitMode) << ShiftIndex + 3) |
                        (Convert.ToByte(Configuration.StopOnKeyPress) << ShiftIndex + 4) |
                        (Convert.ToByte(Configuration.TestMode) << ShiftIndex + 5) |
                        (Convert.ToByte(StopOnDisconnect) << ShiftIndex + 6) |
                        (Convert.ToByte(Configuration.BoomerangEnabled) << ShiftIndex + 7));
            ShiftIndex = 0;
            OutReport.Next = (byte)(
                (Convert.ToByte(TemperatureEnabled) << ShiftIndex) |
                (Convert.ToByte(HumidityEnabled) << ShiftIndex + 1) |
                (Convert.ToByte(DewPointEnabled) << ShiftIndex + 2) |
                (Convert.ToByte(Configuration.AveragePoints) << ShiftIndex + 3));
        }

        protected override void PopulateTime()
        {
            var time = new MicroLogTime();
            time.Set(DateTime.UtcNow);
            OutReport.Insert(time.GetBytes());
        }

        protected override void PopulateDisplayConfiguration()
        {
            OutReport.Next = (byte)Configuration.LCDConfiguration;
        }

        protected override void PopulateInterval()
        {
            OutReport.Insert(Convert.ToUInt16(Configuration.Interval.TotalSeconds));
        }

        protected override void PopulateAlarms()
        {
            PopulateAlarm(TemperatureAlarm);
            PopulateAlarm(HumidityAlarm);
            PopulateAlarm(DewPointAlarm);
            PopulateAlarm(ExternalAlarm);
        }

        protected override void PopulateAlarmInfo()
        {
            OutReport.Skip(alarmDelay);
        }

        protected override void PopulateAlarm(Alarm alarmValue)
        {
            if (alarmValue.Enabled)
            {
                populateAlarmEnd(alarmValue.High);
                populateAlarmEnd(alarmValue.Low);
            }
            else
                OutReport.Skip(alarmSpare);
        }

        private void populateAlarmEnd(decimal alarmEnd)
        {
            IntegerFraction intFrac = new IntegerFraction(alarmEnd);

            byte[] bytes = BitConverter.GetBytes(intFrac.Integer);
            OutReport.Next = bytes[0];
            OutReport.Next = bytes[1];

            OutReport.Next = intFrac.FractionAsByte;
        }

        private void populateSetup2()
        {
            int ShiftIndex = 0;
            OutReport.Next = (byte)((Convert.ToByte(Configuration.ShowMinMax) << ShiftIndex) |
                        (Convert.ToByte(Configuration.ShowPast24HMinMax) << ShiftIndex + 1) |
                        (Convert.ToByte(DeepSleepMode) << ShiftIndex + 2) |
                        (Convert.ToByte(TemperatureAlarm.Enabled) << ShiftIndex + 3) |
                        (Convert.ToByte(HumidityAlarm.Enabled) << ShiftIndex + 4) |
                        (Convert.ToByte(ExternalAlarm.Enabled) << ShiftIndex + 5) |
                        (Convert.ToByte(DewPointAlarm.Enabled) << ShiftIndex + 6) |
                        (Convert.ToByte(Configuration.EnableLEDOnAlarm) << ShiftIndex + 7));

        }
        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Setup Methods
        internal Result RunSetDefinedSensor()
        {
            return UserDefined ? ParentOpCodeManager.SetDefinedSensor.Invoke().Result : Result.OK;
        }

        internal Result RunTimerRun()
        {
            return Configuration.TimerRunEnabled ? ParentOpCodeManager.TimerRun.Invoke().Result : Result.OK;
        }

        internal Result SetMemorySize()
        {
            return ParentOpCodeManager.SetMemorySize.Invoke().Result;
        }

        #endregion
    }
}