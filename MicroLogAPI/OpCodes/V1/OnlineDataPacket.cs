﻿using Base.OpCodes;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroLogAPI.OpCodes.Helpers;
using MicroXBase.Devices.Types;
using System;
using System.Collections;
using System.Collections.Generic;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : DataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x26 };

        #endregion
        #region Fields
        protected object processingPacket = new object();
        private Queue<TempOnline> WaitingSamples;

        #endregion
        #region Constructors
        public OnlineDataPacket(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            WaitingSamples = new Queue<TempOnline>();
            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            lock (processingPacket)
            {
                byte[] block = InReport.GetBlock(Consts.Time.TIME_SIZE);
                PacketTime = MicroLogTime.ParseDataPacketTime(block, ParentDevice.Status.FirstPacketTime);
                ExtractAndAdd();
            }
        }

        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            if (value == Consts.DataPacket.EXTERNAL_DUMMY)
                return;

            if (ParentOpCodeManager.Download.IsRunning)
                WaitingSamples.Enqueue(new TempOnline(sensor, value, dateTime));
            else
                addSampleToSensor(sensor, value, dateTime);
        }

        protected virtual void addSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOnline(value, dateTime);
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Methods
        protected override void DoBeforeParse()
        {
            ParentOpCodeManager.Download.OnFinished += Download_OnFinished;
        }

        void Download_OnFinished(object sender, Result result)
        {
            while (WaitingSamples.Count > 0)
            {
                var temp = WaitingSamples.Dequeue();
                temp.Sensor.samples.AddOnline(temp.Value, temp.DateTime);
            }
        }

        #endregion
        #region NotSupported
        protected override void Populate()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
