﻿using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class Run : OpCodeNoParseAck
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x13 };

        #endregion
        #region Constructors
        public Run(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
        #region Methods
        protected override void DoAfterFinished()
        {
            ParentDevice.Status.IsRunning = true;
            ParentDevice.Functions.InvokeOnStatusReceived();
        }

        #endregion
    }

}
