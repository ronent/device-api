﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroXBase.OpCodes.Common.FirmwareUploadManager
    {
        #region Constructors
        public FirmwareUploadManager(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
    }
}
