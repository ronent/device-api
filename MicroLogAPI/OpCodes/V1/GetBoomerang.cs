﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroXBase.OpCodes.Common.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
    }

}
