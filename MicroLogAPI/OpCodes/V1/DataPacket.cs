﻿using Base.OpCodes;
using Base.Sensors.Types;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.OpCodes.Helpers;
using MicroXBase.Devices.Types;
using System;
using System.Diagnostics;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class DataPacket : MicroXBase.OpCodes.Common.DataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x24 };

        #endregion
        #region Constructors
        public DataPacket(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return null; }
        }
        
        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected int AverageEntryCount
        {
            get { return ParentOpCodeManager.MaximumArraySize / Consts.Readings.SAMPLE_SIZE; }
        }

        new public GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        protected override ushort MaximumDataBlockSize
        {
            get { return Convert.ToUInt16(Consts.Readings.SAMPLE_SIZE + Consts.Time.TIME_SIZE); }
        }

        private bool isCapDisconnected()
        {
            /*If external sensor is runnable that means it a single sensor logger
             * and before passing it on to be processed by the base ParseDataAux
             * the data needs to be checked for a series of 0xFF's
             */

            GenericSensor sensor = ParentLogger.Sensors.GetEnabledDetachable();

            if (sensor != null && !sensor.USBRunnable && InReport.Has(Consts.Readings.SAMPLE_SIZE))
            {
                int index = InReport.Index;
                if (InReport[index] == 0xff && InReport[index + 1] == 0xff)
                {
                    InReport.Skip(Consts.Readings.SAMPLE_SIZE);
                    return true;
                }
            }

            return false;
        }

        #endregion
        #region Helper Methods
        protected void ReportProgressBasedOnEntryIndex(int index)
        {
            try
            {
                int percent = Convert.ToInt16(100 * (PacketIndex * AverageEntryCount + index) / (TotalPackets * AverageEntryCount));
                ReportProgress(percent);
            }
            catch { }
        }

        protected void assignTimeOnFirstRun()
        {
            PacketTime = MicroLogTime.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
            assignStatusTimeToAvoidFWBug();
        }

        /// <summary>
        /// Year of packet time is 2000, so need to correct it according to first packet time of the status
        /// </summary>
        private void assignStatusTimeToAvoidFWBug()
        {
            if (PacketTime.ToDateTime() < ParentDevice.Status.FirstPacketTime)
                PacketTime.SetYear(ParentDevice.Status.FirstPacketTime);

            if (PacketTime.Month < ParentDevice.Status.FirstPacketTime.Month)
                PacketTime.Year++;
        }

        protected override void ReportDoneIfLastPacket()
        {
            if (PacketIndex + 1 == TotalPackets)
                ReportProgressEnded();
            else
                ReportProgressBasedOnEntryIndex(AverageEntryCount);
        }

        private void ReportProgressEnded()
        {
            ReportProgress(100);
            InitializeLastPercent();
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            try
            {
                assignTimeOnFirstRun();

                while (!ParentOpCodeManager.Download.CancelationPending && ReportHasAnyDataLeft() && ReportHasMeaningfulData())
                {
                    if (!tryParseTimeStamp())
                        parseDataEntries();
                }

                ReportDoneIfLastPacket();
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On Parse()", this, ex);
                throw ex;
            }
        }

        protected bool tryParseTimeStamp()
        {
            bool timeStampEntry = getTimeStampEntryMode();

            if (timeStampEntry)
            {
                MicroLogTime packetTime = MicroLogTime.Parse(InReport.GetBlock(Consts.Time.TIME_SIZE));
                packetTime.AdjustYearAccordingTo(ParentDevice.Status.FirstPacketTime);
                ExtractAndAdd(packetTime.ToDateTime());

                Log4Tech.Log.Instance.Write("Packet time: {0}", this, Log4Tech.Log.LogSeverity.INFO, packetTime.ToDateTime());
            }

            return timeStampEntry;
        }

        private bool getTimeStampEntryMode()
        {
            byte b = InReport.Current;
            if (b == 0xFF)
                return false;

            return Convert.ToBoolean((b & 0x40) >> 6);
        }

        protected void parseDataEntries()
        {
            int entryCount = (int)(InReport.Next & 0x7F);

            if (entryCount == 0)
                tryParseTimeStamp();

            for (int i = 0; i < entryCount && !ParentOpCodeManager.Download.CancelationPending; i++)
            {
                if (!isCapDisconnected())
                    ExtractAndAdd();

                ReportProgressBasedOnEntryIndex(i);
                IncreasePacketTime();
            }
        }

        protected int parseDateTimeSection()
        {
            return (int)(InReport.Next & 0x7F);
        }

        #endregion
        #region Override Methods
        protected override long GetRawValue()
        {
            return InReport.Get<UInt16>(endianness:Auxiliary.Tools.Endianness.BIG);
        }

        protected override string GetTimestampNoneComment()
        {
            return string.Empty;
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
    }
}