﻿using Base.OpCodes;
using MicroLogAPI.Devices;
using MicroXBase.OpCodes.Management;
using System;
using System.Threading.Tasks;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x1C };

        #endregion
        #region Enums
        public enum MethodEnum { Save = 0x00, Restore = 0x01, Get = 0x02, Set = 0x03 };

        #endregion
        #region Constructors
        public DefaultCalibration(GenericMicroLogLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        protected MethodEnum Method { get; set; }

        #endregion
        #region Invoke Methods
        public Task<Result> Save()
        {
            return setMethod(MethodEnum.Save);
        }

        public Task<Result> Restore()
        {
            return setMethod(MethodEnum.Restore);
        }

        private Task<Result> setMethod(MethodEnum method)
        {
            Method = method;
            return Invoke();
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateMethod();
        }

        private void PopulateMethod()
        {
            OutReport.Next = (byte)Method;
        }

        #endregion
    }
}
