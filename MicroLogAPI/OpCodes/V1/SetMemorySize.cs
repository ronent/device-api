﻿using MicroLogAPI.OpCodes.Management;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class SetMemorySize : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x42 };

        #endregion
        #region Constructors
        public SetMemorySize(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        
        }

        new protected MicroLogOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroLogOpCodeManager; }
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            base.Populate();
            populateMemorySize();
        }

        private void populateMemorySize()
        {
            byte[] memorySize = BitConverter.GetBytes((ushort)ParentOpCodeManager.Setup.Configuration.MemorySize);
            OutReport.Insert(memorySize);
        }

        #endregion
    }
}
