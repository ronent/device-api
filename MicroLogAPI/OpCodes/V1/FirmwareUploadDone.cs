﻿using Base.Devices;
using Base.OpCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroXBase.OpCodes.Management;
using MicroXBase.Devices.Types;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    public class FirmwareUploadDone : MicroXBase.OpCodes.Common.FirmwareUploadDone
    {
        #region Constructors
        public FirmwareUploadDone(MicroXDevice device)
            : base(device)
        {
        }
        #endregion
    }
}
