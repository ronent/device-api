﻿using Base.Sensors.Management;
using MicroLogAPI.Devices;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroXBase.OpCodes.Common.SetCalibration
    {
        #region Constructors
        public SetCalibration(GenericMicroLogLogger device)
            : base(device)
        { 

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateGainOffset();
        }

        protected override void PopulateGainOffset()
        {
            PopulateGainOffsetAux(Calibration[eSensorIndex.Temperature]);
            PopulateGainOffsetAux(Calibration[eSensorIndex.External1]);
            PopulateGainOffsetAux(Calibration[eSensorIndex.Humidity]);
        }

        #endregion
    }
}
