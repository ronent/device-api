﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroXBase.OpCodes.Common.SetSN
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x16 };

        #endregion
        #region Constructors
        public SetSN(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
    }
}
