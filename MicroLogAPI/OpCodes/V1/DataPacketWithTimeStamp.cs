﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : DataPacket
    {
        #region Members
        private static readonly byte[] receiveOpCode = new byte[] { 0x25 };

        #endregion
        #region Constructors
        public DataPacketWithTimeStamp(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            WaitingForInReport = true;
        }

        #endregion
        #region Properties
        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            bool first = true;

            while (!ParentOpCodeManager.Download.CancelationPending
                && ReportHasAnyDataLeft()
                && ReportHasMeaningfulData())
            {
                if (!tryParseTimeStamp())
                {
                    if (first)
                    {
                        first = false;
                        assignTimeOnFirstRun();
                    }

                    parseDataEntries();
                }
            }

            ReportDoneIfLastPacket();
        }

        #endregion
    }
}
