﻿using MicroLogAPI.DataStructures;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal class TimerRun : OpCodeNoParse
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x19 };

        #endregion
        #region Constructors
        public TimerRun(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateTime();
        }

        protected void PopulateTime()
        {
            var time = new MicroLogTime();
            time.Set(ParentOpCodeManager.Setup.Configuration.TimerStart);
            var block = time.GetBytes();
            OutReport.Insert(block);
        }

        #endregion
    }
}
