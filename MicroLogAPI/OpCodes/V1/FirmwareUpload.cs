﻿using MicroXBase.Devices.Types;
using System;

namespace MicroLogAPI.OpCodes.V1
{
    [Serializable]
    internal abstract class FirmwareUpload : MicroXBase.OpCodes.Common.FirmwareUpload
    {
        #region Constructors
        public FirmwareUpload(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override TimeSpan InvokeTimeOutDuration
        {
            get { return new TimeSpan(0, 3, 30); }
        }

        #endregion
    }
}
