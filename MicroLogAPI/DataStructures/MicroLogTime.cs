﻿using Base.DataStructures;
using System;

namespace MicroLogAPI.DataStructures
{
    internal class MicroLogTime : Time
    {
        #region Static
        public static Time Now 
        { 
            get 
            { 
                var now = new MicroLogTime(); 
                now.Set(DateTime.Now); 
                return now; 
            } 
        }
        public static MicroLogTime Parse(byte[] block)
        {
            MicroLogTime time = new MicroLogTime();
            time.Year = (byte)((block[0] & 0xFC) >> 2);
            time.Month = (byte)((block[0] & 0x03) << 2);
            time.ParseAux(block);
            return time;
        }
        
        public static MicroLogTime ParseDataPacketTime(byte[] block, DateTime startingTime)
        {
            MicroLogTime time = new MicroLogTime();
            time.Set(startingTime);
            time.Month = (byte)((block[0] & 0x03) << 2);
            time.ParseAux(block);

            increaseYearIfBeforeFirstPacketTime(time, startingTime);
            return time;
        }

        private static void increaseYearIfBeforeFirstPacketTime(MicroLogTime time, DateTime startingTime)
        {
            if (time.ToDateTime() < startingTime)
                time.Year++;
        }

        #endregion
        #region Methods
        protected void ParseAux(byte[] block)
        {
            Month += (byte)((block[1] & 0xC0) >> 6);
            Day = (byte)((block[1] & 0x3E) >> 1);
            Hour = (byte)((block[1] & 0x01) << 4);
            Hour += (byte)((block[2] & 0xF0) >> 4);
            Minute = (byte)((block[2] & 0x0F) << 2);
            Minute += (byte)((block[3] & 0xC0) >> 6);
            Second = (byte)(block[3] & 0x3F);
        }
        public override byte[] GetBytes()
        {
            byte[] bytes = new byte[4];
            int index = 0;
            bytes[index++] = (byte)(Year << 2 | ((Month & 0x0C) >> 2));
            bytes[index++] = (byte)(((Month & 0x03) << 6) | ((Day & 0x1F) << 1) | ((Hour & 0x10) >> 4));
            bytes[index++] = (byte)(((Hour & 0x0F) << 4) | ((Minute & 0x3C) >> 2));
            bytes[index++] = (byte)(((Minute & 0x03) << 6) | (Second & 0x3F));

            return bytes;
        }

        public void AdjustYearAccordingTo(DateTime dateTime)
        {
            try
            {
                SetYear(dateTime.Year);
                if (ToDateTime() < dateTime)
                    Year++;
            }
            catch { Console.Beep(); }
        }
        #endregion

    }
}
