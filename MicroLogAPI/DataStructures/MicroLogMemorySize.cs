﻿using Base.DataStructures;
using MicroLogAPI.Devices;
using System;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// Memory size
    /// </summary>
    public enum MemorySizeEnum : ushort { K8 = 8000, K16 = 16000, K32 = 32000, K52 = 52000 }

    /// <summary>
    /// MicroLog memory size
    /// </summary>
    [Serializable]
    public class MicroLogMemorySize : GenericMemorySize
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent device.
        /// </summary>
        /// <value>
        /// The parent device.
        /// </value>
        new internal GenericMicroLogLogger ParentDevice
        {
            get { return base.ParentDevice as GenericMicroLogLogger; }
            set { base.ParentDevice = value; }
        }

        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        internal override Enum Type
        {
            get { return (Enum)ParentDevice.Status.MemorySize; }
        }

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name
        {
            get { return Enum.GetName(typeof(MemorySizeEnum), ParentDevice.Status.MemorySize); }
        }

        /// <summary>
        /// Gets the size.
        /// </summary>
        /// <value>
        /// The size.
        /// </value>
        public override int Size
        {
            get { return (int)ParentDevice.Status.MemorySize; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogMemorySize"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal MicroLogMemorySize(GenericMicroLogLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
