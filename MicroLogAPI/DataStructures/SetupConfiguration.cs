﻿
using Base.OpCodes;
using Base.Sensors.Management;
using Maintenance;
using MicroLogAPI.Devices;
using MicroLogAPI.Sensors.V1;
using MicroLogAPI.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using MicroXBase.DataStructures;

namespace MicroLogAPI.DataStructures
{
    public enum LCDConfigurationEnum : byte
    {
        AlwaysOn = 0x01,
        OnFor30Sec = 0x02,
        OnFor60Sec = 0x04,
        OnFor120Sec = 0x06,
    }
    
    public enum ConfigurationSensorEnum : byte 
    { 
        NONE = 0, 
        TEMPERATURE = 1, 
        HUMIDITY = 2, 
        DEWPOINT = 4,
        EXTERNAL = 8
    }

    [Serializable]
    public class SetupConfiguration : MicroXSetupConfiguration
    {
        public SetupConfiguration()
        {
            Initialize();
            Boomerang = new BoomerangConfiguration();
        }

        #region Properties
        /// <summary>
        /// Determines device display sleep time 
        /// </summary>
        public LCDConfigurationEnum LCDConfiguration { get; set; }
        /// <summary>
        /// Sets the memory size. Not illegal values will fail pre=setup validation
        /// </summary>
        public MemorySizeEnum MemorySize { get; set; }
        /// <summary>
        /// Enable/Disable the option to stop logger on key press
        /// </summary>
        public bool StopOnKeyPress { get; set; }
        /// <summary>
        /// Shows min/max sample on logger screen
        /// </summary>
        public bool ShowMinMax { get; set; }
        public bool ShowPast24HMinMax { get; set; }
        public bool EnableLEDOnAlarm { get; set; }
        public byte AveragePoints { get; set; }
        public override bool TimerRunEnabled
        {
            get { return TimerStart != default(DateTime); }
            set { throw new NotSupportedException(); }
        }
        public override DateTime DeviceTime 
        {
            get { return DateTime.UtcNow.AddHours(UtcOffset); }
            set { throw new NotSupportedException(); }
        }

        #endregion
        #region Validation
        public override void ThrowIfInvalidFor(MicroXDevice dummyDevice)
        {
            base.ThrowIfInvalidFor(dummyDevice);
            checkAveragePoints(dummyDevice);
            checkMemorySize(dummyDevice as GenericMicroLogDevice);
        }
        private void checkAveragePoints(MicroXDevice dummyDevice)
        {
            ThrowIfFalse(AveragePoints <= dummyDevice.AllowedAveragePoints,
                "Average points exceed device limit");
        }
        private void checkMemorySize(GenericMicroLogDevice dummyDevice)
        {
            ThrowIfFalse(dummyDevice.AvailableMemorySizes.Any(x => x == MemorySize), "Illegal memory size");
        }

        #endregion
    }
}