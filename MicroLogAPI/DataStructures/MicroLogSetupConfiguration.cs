﻿using MicroLogAPI.Devices;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Linq;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// MicroLog LCD configuration.
    /// </summary>
    public enum LCDConfigurationEnum : byte
    {
        /// <summary>
        /// Always on.
        /// </summary>
        AlwaysOn = 0x01,
        /// <summary>
        /// On for 30 seconds.
        /// </summary>
        OnFor30Sec = 0x02,
        /// <summary>
        /// On for 60 seconds.
        /// </summary>
        OnFor60Sec = 0x04,
        /// <summary>
        /// On for 120 seconds.
        /// </summary>
        OnFor120Sec = 0x06,
    }

    /// <summary>
    /// MicroLog sensor configuration.
    /// </summary>
    public enum ConfigurationSensorEnum : byte 
    {
        /// <summary>
        /// None
        /// </summary>
        NONE = 0,
        /// <summary>
        /// Temperature.
        /// </summary>
        TEMPERATURE = 1,
        /// <summary>
        /// Humidity.
        /// </summary>
        HUMIDITY = 2,
        /// <summary>
        /// Dew point.
        /// </summary>
        DEWPOINT = 4,
        /// <summary>
        /// External.
        /// </summary>
        EXTERNAL = 8
    }

    /// <summary>
    /// Configuration settings for MicroLog devices.
    /// </summary>
    [Serializable]
    public class MicroLogSetupConfiguration : MicroXSetupConfiguration, IMicroLogConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogSetupConfiguration"/> class.
        /// </summary>
        internal MicroLogSetupConfiguration()
            :base()
        {
            Initialize();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the LCD configuration.
        /// </summary>
        /// <value>
        /// The LCD configuration.
        /// </value>
        public virtual LCDConfigurationEnum LCDConfiguration { get; set; }

        /// <summary>
        /// Gets or sets the size of the memory.
        /// </summary>
        /// <value>
        /// The size of the memory.
        /// </value>
        public virtual MemorySizeEnum MemorySize { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool StopOnKeyPress { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show minimum/maximum].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show minimum maximum]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool ShowMinMax { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [show past 24h minimum/maximum].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show past 24h minimum/maximum]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool ShowPast24HMinMax { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [enable led on alarm].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable led on alarm]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool EnableLEDOnAlarm { get; set; }

        /// <summary>
        /// Gets or sets the average points.
        /// </summary>
        /// <value>
        /// The average points.
        /// </value>
        public virtual byte AveragePoints { get; set; }

        /// <summary>
        /// Gets the device time.
        /// </summary>
        /// <value>
        /// The device time.
        /// </value>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override DateTime DeviceTime 
        {
            get { return DateTime.UtcNow.AddHours(UtcOffset); }
            set { throw new NotSupportedException(); }
        }

        #endregion
        #region Validation
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            base.ThrowIfInvalidFor(device);

            checkAveragePoints(device as GenericMicroLogLogger);
            checkMemorySize(device as GenericMicroLogLogger);
        }

        /// <summary>
        /// Checks the average points.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        private void checkAveragePoints(GenericMicroLogLogger device)
        {
            ThrowIfFalse(AveragePoints <= device.AllowedAveragePoints,
                "Average points exceed device limit");
        }

        /// <summary>
        /// Checks the size of the memory.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        private void checkMemorySize(GenericMicroLogLogger device)
        {
            ThrowIfFalse(device.AvailableMemorySizes.Any(x => x == MemorySize), "Illegal memory size");
        }

        internal override void checkSensors(MicroXLogger device)
        {

        }

        #endregion
    }
}