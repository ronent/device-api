﻿using MicroXBase.DataStructures;
using System;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// Status configuration for MicroLog devices.
    /// </summary>
    [Serializable]
    public class MicroLogStatusConfiguration : MicroXStatusConfiguration, IMicroLogConfiguration, ITSensorSetup, IESensorSetup, ITHDSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogStatusConfiguration"/> class.
        /// </summary>
        internal MicroLogStatusConfiguration()
            : base()
        {
            ExternalSensor = new ESensorSetup();
            FixedSensors = new THDSensorSetup();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the external sensor.
        /// </summary>
        /// <value>
        /// The external sensor.
        /// </value>
        public ESensorSetup ExternalSensor { get; internal set; }

        /// <summary>
        /// Gets the fixed sensors.
        /// </summary>
        /// <value>
        /// The fixed sensors.
        /// </value>
        public THDSensorSetup FixedSensors { get; internal set; }

        /// <summary>
        /// Gets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        public TSensorSetup TemperatureSensor { get { return FixedSensors.GetTemperatureSensor(); } }

        /// <summary>
        /// Gets a value indicating whether [deep sleep mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [deep sleep mode]; otherwise, <c>false</c>.
        /// </value>
        public bool DeepSleepMode { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [stop on disconnect].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on disconnect]; otherwise, <c>false</c>.
        /// </value>
        public bool StopOnDisconnect { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [unit requires setup].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [unit requires setup]; otherwise, <c>false</c>.
        /// </value>
        public bool UnitRequiresSetup { get; internal set; }

        /// <summary>
        /// Gets the minimum required software version.
        /// </summary>
        /// <value>
        /// The minimum required software version.
        /// </value>
        public UInt32 MinimumRequiredSWVersion { get; internal set; }

        /// <summary>
        /// Gets the first packet time.
        /// </summary>
        /// <value>
        /// The first packet time.
        /// </value>
        internal DateTime FirstPacketTime { get; set; }

        /// <summary>
        /// Gets the LCD configuration.
        /// </summary>
        /// <value>
        /// The LCD configuration.
        /// </value>
        public LCDConfigurationEnum LCDConfiguration { get; internal set; }

        /// <summary>
        /// Gets the size of the memory.
        /// </summary>
        /// <value>
        /// The size of the memory.
        /// </value>
        public MemorySizeEnum MemorySize { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        public bool StopOnKeyPress { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [show minimum/maximum].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show minimum/maximum]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowMinMax { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [show past 24h minimum/maximum].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show past24 h minimum maximum]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowPast24HMinMax { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether [enable led on alarm].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable led on alarm]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableLEDOnAlarm { get; internal set; }

        /// <summary>
        /// Gets the average points.
        /// </summary>
        /// <value>
        /// The average points.
        /// </value>
        public byte AveragePoints { get; internal set; }

        #endregion
    }
}
