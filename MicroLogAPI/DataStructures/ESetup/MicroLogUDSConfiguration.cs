﻿using Auxiliary.Tools;
using MicroXBase.DataStructures;
using System;
using System.Runtime.Serialization;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// MicroLog user define sensor class
    /// </summary>
    [Serializable]
    public class MicroLogUDSConfiguration : Base.DataStructures.UDSConfiguration
    {
        /// <summary>
        /// The unit
        /// </summary>
        private ExternalSensorUnitEnum unit;

        /// <summary>
        /// Gets the length of the allowed name.
        /// </summary>
        /// <value>
        /// The length of the allowed name.
        /// </value>
        public override int AllowedNameLength { get { return 10; } }

        /// <summary>
        /// Gets the length of the allowed custom unit.
        /// </summary>
        /// <value>
        /// The length of the allowed custom unit.
        /// </value>
        public override int AllowedCustomUnitLength { get { return 4; } }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public ExternalSensorUnitEnum Unit
        {
            get { return unit; }
            set
            {
                unit = value; 

                if (value != ExternalSensorUnitEnum.Custom) 
                    CustomUnit = FixedUnitTypes.Get(value);
            }
        }

        /// <summary>
        /// Gets or sets the custom unit.
        /// </summary>
        /// <value>
        /// The custom unit.
        /// </value>
        public string CustomUnit { get; set; }

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogUDSConfiguration"/> class.
        /// </summary>
        public MicroLogUDSConfiguration()
        {
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogUDSConfiguration"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLogUDSConfiguration(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            Unit = IO.GetSerializationValue<ExternalSensorUnitEnum>(ref info, "Unit");
            CustomUnit = IO.GetSerializationValue<string>(ref info, "CustomUnit"); 
        }
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
            info.AddValue("Unit", Unit, typeof(ExternalSensorUnitEnum));
            info.AddValue("CustomUnit", CustomUnit, typeof(string));
        }
        #endregion
        #region Object Overrides
        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return base.ToString()
                + Environment.NewLine
                + "Unit: " + CustomUnit;
        }
        #endregion
    }
}

