﻿
namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// External sensor setup.
    /// </summary>
    public interface IESensorSetup
    {
        /// <summary>
        /// Gets or sets the external sensor.
        /// </summary>
        /// <value>
        /// The external sensor.
        /// </value>
        ESensorSetup ExternalSensor { get; }
    }
}
