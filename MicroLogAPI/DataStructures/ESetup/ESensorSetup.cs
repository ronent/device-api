﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroLogAPI.Sensors.Management;
using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Text;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// External sensor setup class
    /// </summary>
    [Serializable]
    public class ESensorSetup : ISensorSetup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ESensorSetup"/> class.
        /// </summary>
        public ESensorSetup()
        {
            Alarm = new Alarm();
            UserDefinedSensor = new MicroLogUDSConfiguration();
        }

        /// <summary>
        /// Gets or sets the alarm.
        /// </summary>
        /// <value>
        /// The alarm.
        /// </value>
        public Alarm Alarm { get; set; }

        /// <summary>
        /// Gets or sets the user defined sensor.
        /// </summary>
        /// <value>
        /// The user defined sensor.
        /// </value>
        public MicroLogUDSConfiguration UserDefinedSensor { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public eSensorType Type { get; set; }

        /// <summary>
        /// Gets a value indicating whether the sensor is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get { return Type != eSensorType.None; } }

        /// <summary>
        /// Gets a value indicating whether the sensor is user defined.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [user defined]; otherwise, <c>false</c>.
        /// </value>
        public bool UserDefined { get { return Type == eSensorType.UserDefined; } }

        #region Methods
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        public void ThrowIfInvalidFor(MicroXLogger dummyDevice)
        {
            ThrowIfInvalidDevice(dummyDevice);
            if (Enabled)
            {
                throwIfInvalidType();
                throwIfInvalidUserDefinedSensor();
                ThrowIfInvalidAlarms(dummyDevice);
            }
        }

        /// <summary>
        /// Throws if invalid device.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        /// <exception cref="System.Exception">Device  + dummyDevice.GetType().Name +  doesn't support external sensor setup</exception>
        public void ThrowIfInvalidDevice(MicroXLogger dummyDevice)
        {
            if (!(dummyDevice.Status is IESensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support external sensor setup");
        }

        /// <summary>
        /// Throws the type of if invalid.
        /// </summary>
        /// <exception cref="System.Exception">Type must be an external type or user-defined</exception>
        private void throwIfInvalidType()
        {
            switch (Type)
            {
                case eSensorType.Current4_20mA:
                case eSensorType.Voltage0_10V:
                case eSensorType.ExternalNTC:
                case eSensorType.PT100:
                case eSensorType.UserDefined:
                    break;
                default:
                    throw new Exception("Type must be an external type or user-defined");
            }
        }

        /// <summary>
        /// Throws if invalid user defined sensor.
        /// </summary>
        /// <exception cref="System.Exception">Invalid user defined sensor</exception>
        private void throwIfInvalidUserDefinedSensor()
        {
            if (UserDefined && !UserDefinedSensor.Valid)
                throw new Exception("Invalid user defined sensor");
        }
        /// <summary>
        /// Throws if invalid alarms.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        public void ThrowIfInvalidAlarms(MicroXLogger dummyDevice)
        {
            if (Enabled && Alarm.Enabled)
                Alarm.ThrowIfNotValid(dummyDevice.Sensors.GetDetachableByType(Type));
        }

        internal static void adjustExternalSensor(MicroXLogger dummyDevice, ESensorSetup externalSensor)
        {
            if (externalSensor.UserDefined)
            {
                if (!externalSensor.UserDefinedSensor.Valid)
                    throw new Exception("User define sensor parameters are not valid.");

                var sensor = dummyDevice.Sensors.GetDetachableByType(externalSensor.UserDefinedSensor.BaseType);
                (sensor.ParentSensorManager as MicroLogSensorManager).SetUserDefined(externalSensor.UserDefinedSensor);
            }
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("External Sensor");
            str.AppendLine("\tType: " + Type);
            if (UserDefined)
                str.AppendLine("\tUser Defined: " + UserDefinedSensor.ToString());
            str.AppendLine("\tAlarm: " + Alarm.ToString());

            return str.ToString();
        }
        #endregion
        #region Equals
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            ESensorSetup other = obj as ESensorSetup;
            if (other == null) return false;

            return Alarm.Equals(other.Alarm)
                && isUserDefinedEqualTo(other)
                && Type == other.Type
                && Enabled == other.Enabled;
        }

        /// <summary>
        /// Determines whether [is user defined equal to] [the specified other].
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        private bool isUserDefinedEqualTo(ESensorSetup other)
        {
            return (!UserDefined && !other.UserDefined)
                || UserDefinedSensor.Equals(other.UserDefinedSensor);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
