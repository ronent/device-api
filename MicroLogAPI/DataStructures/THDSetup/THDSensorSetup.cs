﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Text;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// Fixed sensor combination.
    /// </summary>
    public enum FixedSensorCombinationEnum : byte
    {
        /// <summary>
        /// None
        /// </summary>.
        NONE = 0,
        /// <summary>
        /// The temperature.
        /// </summary>
        TEMPERATURE = 1,
        /// <summary>
        /// The temperature and humidity.
        /// </summary>
        TEMPERATURE_HUMIDITY = 3,
        /// <summary>
        /// The temperature, humidity and dew point.
        /// </summary>
        TEMPERATURE_HUMDIDITY_DEWPOINT = 7
    }

    /// <summary>
    /// Temperature and Humidity class.
    /// </summary>
    [Serializable]
    public class THDSensorSetup : TSensorSetup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="THDSensorSetup"/> class.
        /// </summary>
        public THDSensorSetup()
            : base()
        {
            HumidityAlarm = new Alarm();
            DewPointAlarm = new Alarm();
        }

        /// <summary>
        /// Gets or sets the fixed sensor combination.
        /// </summary>
        /// <value>
        /// The fixed sensor combination.
        /// </value>
        internal FixedSensorCombinationEnum FixedSensorCombination { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether temperature sensor is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [temperature enabled]; otherwise, <c>false</c>.
        /// </value>
        public override bool TemperatureEnabled
        {
            get { return ((byte)FixedSensorCombination & (byte)ConfigurationSensorEnum.TEMPERATURE) != 0; }
            set
            {
                if (value)
                {
                    if (FixedSensorCombination == FixedSensorCombinationEnum.NONE)
                        FixedSensorCombination = FixedSensorCombinationEnum.TEMPERATURE;
                }
                else
                    FixedSensorCombination = FixedSensorCombinationEnum.NONE;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether humidity sensor enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [humidity enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool HumidityEnabled
        {
            get { return ((byte)FixedSensorCombination & (byte)ConfigurationSensorEnum.HUMIDITY) != 0; }
            set
            {
                if (value)
                {
                    if (FixedSensorCombination < FixedSensorCombinationEnum.TEMPERATURE_HUMIDITY)
                        FixedSensorCombination = FixedSensorCombinationEnum.TEMPERATURE_HUMIDITY;
                }
                else
                    FixedSensorCombination = (FixedSensorCombinationEnum)((byte)FixedSensorCombination & (byte)FixedSensorCombinationEnum.TEMPERATURE);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether dew point sensor enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [dew point enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool DewPointEnabled
        {
            get { return ((byte)FixedSensorCombination & (byte)ConfigurationSensorEnum.DEWPOINT) != 0; }
            set
            {
                if (value)
                {
                    if (FixedSensorCombination < FixedSensorCombinationEnum.TEMPERATURE_HUMDIDITY_DEWPOINT)
                        FixedSensorCombination = FixedSensorCombinationEnum.TEMPERATURE_HUMDIDITY_DEWPOINT;
                }
                else
                    FixedSensorCombination = (FixedSensorCombinationEnum)((byte)FixedSensorCombination & (byte)FixedSensorCombinationEnum.TEMPERATURE_HUMIDITY);
            }
        }

        /// <summary>
        /// Gets or sets the humidity alarm.
        /// </summary>
        /// <value>
        /// The humidity alarm.
        /// </value>
        public Alarm HumidityAlarm { get; set; }

        /// <summary>
        /// Gets or sets the dew point alarm.
        /// </summary>
        /// <value>
        /// The dew point alarm.
        /// </value>
        public Alarm DewPointAlarm { get; set; }

        #region Methods
        /// <summary>
        /// Gets the temperature sensor.
        /// </summary>
        /// <returns></returns>
        public TSensorSetup GetTemperatureSensor()
        {
            TSensorSetup temperatureSensor = new TSensorSetup();
            temperatureSensor.TemperatureEnabled = TemperatureEnabled;
            temperatureSensor.TemperatureAlarm = TemperatureAlarm;

            return temperatureSensor;
        }

        /// <summary>
        /// Throws if invalid device.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        /// <exception cref="System.Exception">Device  + dummyDevice.GetType().Name +  doesn't support humidity sensor setup</exception>
        public override void ThrowIfInvalidDevice(MicroXLogger dummyDevice)
        {
            if (!(dummyDevice.Status is ITHDSensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support humidity sensor setup");
        }

        /// <summary>
        /// Throws if invalid alarms.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        public override void ThrowIfInvalidAlarms(MicroXLogger dummyDevice)
        {
            if (HumidityEnabled)
                HumidityAlarm.ThrowIfNotValid(dummyDevice.Sensors.GetFixedByIndex(eSensorIndex.Humidity));

            if (DewPointEnabled)
                DewPointAlarm.ThrowIfNotValid(dummyDevice.Sensors.GetFixedByIndex(eSensorIndex.DewPoint));
        
            base.ThrowIfInvalidAlarms(dummyDevice);
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine("Fixed Sensors");
            str.AppendLine("\tTypes: " + FixedSensorCombination);
            str.AppendLine("\tTemp Alarm: " + TemperatureAlarm.ToString());
            str.AppendLine("\tHumidity Alarm: " + HumidityAlarm.ToString());
            str.AppendLine("\tDew Point Alarm: " + DewPointAlarm.ToString());

            return str.ToString();
        }

        #endregion
        #region Equals
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            THDSensorSetup other = obj as THDSensorSetup;
            if (other == null) return false;

            return base.Equals(obj) 
                && HumidityAlarm.Equals(other.HumidityAlarm)
                && DewPointAlarm.Equals(other.DewPointAlarm)
                && FixedSensorCombination == other.FixedSensorCombination;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}