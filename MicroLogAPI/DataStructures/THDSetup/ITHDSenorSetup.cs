﻿
namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// Temperature and Humidity sensor setup interface
    /// </summary>
    public interface ITHDSensorSetup
    {
        /// <summary>
        /// Gets the fixed sensors.
        /// </summary>
        /// <value>
        /// The fixed sensors.
        /// </value>
        THDSensorSetup FixedSensors { get; }
    }
}
