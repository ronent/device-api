﻿using System;

namespace MicroLogAPI.DataStructures
{
    /// <summary>
    /// MicroLog configuration interface
    /// </summary>
    interface IMicroLogConfiguration
    {
        /// <summary>
        /// Gets the average points.
        /// </summary>
        /// <value>
        /// The average points.
        /// </value>
        byte AveragePoints { get; }

        /// <summary>
        /// Gets a value indicating whether [enable led on alarm].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enable led on alarm]; otherwise, <c>false</c>.
        /// </value>
        bool EnableLEDOnAlarm { get; }

        /// <summary>
        /// Gets the LCD configuration.
        /// </summary>
        /// <value>
        /// The LCD configuration.
        /// </value>
        LCDConfigurationEnum LCDConfiguration { get; }

        /// <summary>
        /// Gets the size of the memory.
        /// </summary>
        /// <value>
        /// The size of the memory.
        /// </value>
        MemorySizeEnum MemorySize { get; }

        /// <summary>
        /// Gets a value indicating whether [show minimum maximum].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show minimum maximum]; otherwise, <c>false</c>.
        /// </value>
        bool ShowMinMax { get; }

        /// <summary>
        /// Gets a value indicating whether [show past24 h minimum maximum].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show past24 h minimum maximum]; otherwise, <c>false</c>.
        /// </value>
        bool ShowPast24HMinMax { get; }

        /// <summary>
        /// Gets a value indicating whether [stop on key press].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on key press]; otherwise, <c>false</c>.
        /// </value>
        bool StopOnKeyPress { get; }

        /// <summary>
        /// Gets a value indicating whether [timer run enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [timer run enabled]; otherwise, <c>false</c>.
        /// </value>
        bool TimerRunEnabled { get; }
    }
}
