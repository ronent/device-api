﻿using MicroLogAPI.DataStructures;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MicroLogAPI.DataStructures
{
    [Serializable]
    public class StatusConfiguration : SetupConfiguration, IMicroXStatusConfiguration, ITSensorSetup, IESensorSetup, ITHDSensorSetup, ICloneable
    {
        public ESensorSetup ExternalSensor { get; set; }
        public THDSensorSetup FixedSensors { get; set; }
        public TSensorSetup TemperatureSensor { get { return FixedSensors.GetTemperatureSensor(); } set { } }

        public StatusConfiguration()
            : base()
        {
            ExternalSensor = new ESensorSetup();
            FixedSensors = new THDSensorSetup();
        }

        public bool DeepSleepMode { get; set; }
        public bool StopOnDisconnect { get; set; }
        public bool UnitRequiresSetup { get; set; }
        public byte PCBVersion { get; set; }
        public byte FWRevision { get; set; }
        public UInt32 MinimumRequiredSWVersion { get; set; }

        public float FirmwareVersion { get; set; }
        public byte BatteryLevel { get; set; }
        public bool Running { get; set; }
        public byte DeviceType { get; set; }
        public byte PCBAssembly { get; set; }
        public DateTime FirstPacketTime { get; set; }

        public object Clone()
        {
            throw new NotImplementedException();
        }
    }
}
