﻿using Base.DataStructures;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Log4Tech;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;
using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Sensors.Management;
using System;
using System.Diagnostics;
using System.Linq;

namespace MicroLogAPI.Sensors.Management
{
    /// <summary>
    /// MicroLog Sensor Manager.
    /// </summary>
    [Serializable]
    public class MicroLogSensorManager : MicroXSensorManager
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent logger.
        /// </summary>
        /// <value>
        /// The parent logger.
        /// </value>
        new internal GenericMicroLogLogger ParentLogger
        {
            get {return base.ParentLogger as GenericMicroLogLogger;}
            set {base.ParentLogger = value;}
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLogSensorManager"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal MicroLogSensorManager(GenericMicroLogLogger parent)
            :base(parent)
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Assigns the configuration.
        /// </summary>
        internal override void AssignConfiguration()
        {
            if (this.IsEmpty)
                return;

            assignTemperatureSensor();
            assignHumidityAndDewPointSensor();
            assignExternalSensor();
        }

        /// <summary>
        /// Assigns the temperature sensor.
        /// </summary>
        private void assignTemperatureSensor()
        {
            ITSensorSetup configT = ParentLogger.Status as ITSensorSetup;
            if (configT != null)
                AssignFixedSensor(eSensorIndex.Temperature, configT.TemperatureSensor.TemperatureEnabled, configT.TemperatureSensor.TemperatureAlarm);
            else
            {
                ITHDSensorSetup configTHD = ParentLogger.Status as ITHDSensorSetup;
                if (configTHD != null)
                    AssignFixedSensor(eSensorIndex.Temperature, configTHD.FixedSensors.TemperatureEnabled, configTHD.FixedSensors.TemperatureAlarm);
            }
        }

        /// <summary>
        /// Assigns the humidity and dew point sensor.
        /// </summary>
        private void assignHumidityAndDewPointSensor()
        {
            var config = ParentLogger.Status as ITHDSensorSetup;
            if (config != null)
            {
                AssignFixedSensor(eSensorIndex.Humidity, config.FixedSensors.HumidityEnabled, config.FixedSensors.HumidityAlarm);
                AssignFixedSensor(eSensorIndex.DewPoint, config.FixedSensors.DewPointEnabled, config.FixedSensors.DewPointAlarm);
            }
        }

        /// <summary>
        /// Assigns the external sensor.
        /// </summary>
        private void assignExternalSensor()
        {
            IESensorSetup config;
            var sensor = resolveUserDefinedSensor(out config);
            if (sensor != null)
                try
                {
                    var alarm = config.ExternalSensor.Alarm;

                    if (config.ExternalSensor.Enabled)
                        EnableDetachable(sensor.Type);
                    else
                        DisableAllDetachables();

                    if (sensor.Enabled)
                        sensor.Alarm.Assign(alarm);
                }
                catch (Exception ex) { Log.Instance.WriteError("Fail in assignExternalSensor",this,ex); }
        }

        /// <summary>
        /// Resolves the user defined sensor.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <returns></returns>
        private GenericSensor resolveUserDefinedSensor(out IESensorSetup config)
        {
            config = ParentLogger.Status as IESensorSetup;
            //if (config.ExternalSensor.UserDefinedSensor.Name == null)
            //{
            //    Debugger.Break();
           // }

            if (config != null)
                if (config.ExternalSensor.UserDefined)
                {
                    SetUserDefined(config.ExternalSensor.UserDefinedSensor);
                    return GetDetachableByType(eSensorType.UserDefined);
                }
                else
                {
                    return GetDetachableByType(config.ExternalSensor.Type);
                }

            return null;
        }

        /// <summary>
        /// Creates the user defined sensor.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        internal IDefinedSensor CreateUserDefinedSensor(UDSConfiguration configuration)
        {
            return new UserDefinedSensor(this, configuration as MicroLogUDSConfiguration);            
        }

        /// <summary>
        /// Sets the user defined.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        internal void SetUserDefined(UDSConfiguration configuration)
        {
            IDefinedSensor sensor = CreateUserDefinedSensor(configuration);
            Items.RemoveAll(x => x.Type == eSensorType.UserDefined);
            Add(sensor as GenericSensor);
        }

        /// <summary>
        /// Gets the user defined.
        /// </summary>
        /// <returns></returns>
        public GenericSensor GetUserDefined()
        {
            try
            {
                return Items.SingleOrDefault(x => x.Type == eSensorType.UserDefined);
            }
            catch { return null; }
        }
        #endregion
    }
}
