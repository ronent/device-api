﻿using Auxiliary.Tools;
using Base.Sensors.Management;
using Base.Sensors.Types;
using Base.Sensors.Units;
using MicroLogAPI.DataStructures;
using MicroXBase.DataStructures;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;

namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// User Defined Sensor.
    /// </summary>
    [Serializable]
    public class UserDefinedSensor : TwoPointUserDefinedSensor
    {
        #region Properties
        /// <summary>
        /// Gets or sets the user defined sensor configuration.
        /// </summary>
        /// <value>
        /// The user defined sensor configuration.
        /// </value>
        new public MicroLogUDSConfiguration UDS { get { return base.UDS as MicroLogUDSConfiguration; } protected set { base.UDS = value; } }

        /// <summary>
        /// Gets or sets the unit.
        /// </summary>
        /// <value>
        /// The unit.
        /// </value>
        public override IUnit Unit
        {
            get 
            {
                return new UserDefinedUnit { Type = UDS.Unit, Name = UDS.CustomUnit };
            }
            internal set 
            {
                UDS.CustomUnit = value.Name;
                if(value is UserDefinedSensor)
                    UDS.Unit = (value as UserDefinedUnit).Type;
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedSensor"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal UserDefinedSensor(SensorManager parent)
            :base(parent)
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedSensor"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="configuration">The configuration.</param>
        internal UserDefinedSensor(SensorManager parent, MicroLogUDSConfiguration configuration)
            : this(parent)
        {
            this.UDS = configuration.Clone();
        }

        /// <summary>
        /// Initializes the specified parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        protected override void Initialize(SensorManager parent)
        {
            base.Initialize(parent);
            UDS = new MicroLogUDSConfiguration();
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedSensor"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        //protected UserDefinedSensor(SerializationInfo info, StreamingContext ctxt) 
        //    : base(info, ctxt)
        //{
        //}

        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        //public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        //{
        //    base.GetObjectData(info, ctxt);
        //}

        #endregion
        #region Object Overrides
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode() + SignificantFigures.GetHashCode();
        }
        #endregion
    }
}