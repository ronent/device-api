﻿using Auxiliary.MathLib;
using Base.Sensors.Management;
using System;

namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// External NTC Sensor.
    /// </summary>
    [Serializable]
    public abstract class ExternalNTC : Base.Sensors.Types.Temperature.ExternalNTC
    {
        #region Properties
        /// <summary>
        /// Gets the sensor maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(150); }
        }

        /// <summary>
        /// Gets the sensor minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit (- 50); }
        }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal ExternalNTC(SensorManager parent)
            : base(parent)
        {
        }
        #endregion
        #region Implemented Methods
        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            decimal value = IntegerFraction.ConvertFromFixedPoint(digitalValue) - Common.TEMPERATURE_DELTA;
            return ConvertToCorrectUnit(value);
        }

        #endregion
        #region Not Supported
        /// <summary>
        /// Not Supported.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
