﻿
namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// Common Sensor Settings.
    /// </summary>
    internal class Common
    {
        /// <summary>
        /// The temperature delta
        /// </summary>
        public const decimal TEMPERATURE_DELTA = 50m;
    }
}
