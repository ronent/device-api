﻿using Auxiliary.MathLib;
using Base.Sensors.Management;
using System;

namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// Current 4-20mA Sensor.
    /// </summary>
    [Serializable]
    public abstract class Current4_20mA : Base.Sensors.Types.Current4_20mA
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Current4_20mA"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Current4_20mA(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
        #region Implemented Methods
        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            return IntegerFraction.ConvertFromFixedPoint(digitalValue);
        }

        #endregion
        #region Not Supported
        /// <summary>
        /// Not Supported.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
