﻿using Auxiliary.MathLib;
using Base.Sensors.Management;
using System;

namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// Digital Temperature Sensor.
    /// </summary>
    [Serializable]    
    public abstract class DigitalTemperature : Base.Sensors.Types.Temperature.DigitalTemperature
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalTemperature"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DigitalTemperature(SensorManager parent)
            : base(parent)
        {
        }

        #endregion
        #region Implemented Methods
        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            decimal value = IntegerFraction.ConvertFromFixedPoint(digitalValue) - Common.TEMPERATURE_DELTA;
            return ConvertToCorrectUnit(value);
        }

        #endregion
        #region Not Supported
        /// <summary>
        /// Not Supported.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
