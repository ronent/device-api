﻿using Base.Sensors.Management;
using System;

namespace MicroLogAPI.Sensors.V1
{
    /// <summary>
    /// Dew point Sensor.
    /// </summary>
    [Serializable]    
    public abstract class DewPoint : MicroXBase.Sensors.Generic.DewPoint
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DewPoint"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DewPoint(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
