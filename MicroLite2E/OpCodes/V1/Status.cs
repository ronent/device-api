﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroLogAPI.OpCodes.V1.Status
    {
        #region Constructors
        public Status(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
