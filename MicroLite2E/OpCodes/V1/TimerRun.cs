﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class TimerRun : MicroLogAPI.OpCodes.V1.TimerRun
    {
        #region Constructors
        public TimerRun(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
