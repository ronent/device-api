﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : MicroLogAPI.OpCodes.V1.GetCalibration
    {
        #region Constructors
        public GetCalibration(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
