﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : MicroLogAPI.OpCodes.V1.DefaultCalibration
    {
        #region Constructors
        public DefaultCalibration(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
