﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroLite2E.DataStructures;
using MicroLite2E.Devices;
using MicroLogAPI.DataStructures;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroLogAPI.OpCodes.V1.Setup
    {
        #region Properties
        new public MicroLite2ESetupConfiguration Configuration
        {
            get { return base.Configuration as MicroLite2ESetupConfiguration; }
            set { base.Configuration = value; }
        }

        #endregion
        #region Configuration Properties
        protected override bool ExternalEnabled { get { return Configuration.ExternalSensor.Enabled; } }
        protected override Alarm ExternalAlarm { get { return Configuration.ExternalSensor.Alarm; } }
        protected override bool StopOnDisconnect { get { return Configuration.StopOnDisconnect; } }
        protected override eSensorType ExternalType { get { return Configuration.ExternalSensor.Type; } }
        protected override bool UserDefined { get { return Configuration.ExternalSensor.UserDefined; } }
        protected override MicroLogUDSConfiguration UserDefinedSensor { get { return Configuration.ExternalSensor.UserDefinedSensor; } }

        #endregion
        #region Constructors
        public Setup(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
