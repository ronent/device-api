﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class SetMemorySize : MicroLogAPI.OpCodes.V1.SetMemorySize
    {
        #region Constructors
        public SetMemorySize(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
