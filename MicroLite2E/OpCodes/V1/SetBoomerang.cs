﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroLogAPI.OpCodes.V1.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
