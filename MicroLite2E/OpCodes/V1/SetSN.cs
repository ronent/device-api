﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroLogAPI.OpCodes.V1.SetSN
    {
        #region Constructors
        public SetSN(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
