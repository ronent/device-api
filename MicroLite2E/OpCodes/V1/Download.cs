﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroLogAPI.OpCodes.V1.Download
    {
        #region Constructors
        public Download(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
