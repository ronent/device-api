﻿using System;
using MicroLite2E.Devices;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    public class FirmwareUploadDone : MicroLogAPI.OpCodes.V1.FirmwareUploadDone
    {
        #region Constructors
        public FirmwareUploadDone(MicroLite2EDevice device)
            : base(device)
        {
        }
        #endregion
    }
}
