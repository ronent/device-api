﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : MicroLogAPI.OpCodes.V1.OnlineTimeStamp
    {
        #region Constructors
        public OnlineTimeStamp(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
