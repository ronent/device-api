﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : MicroLogAPI.OpCodes.V1.DataPacketWithTimeStamp
    {
        #region Constructors
        public DataPacketWithTimeStamp(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
