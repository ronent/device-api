﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class SetDefinedSensor : MicroLogAPI.OpCodes.V1.SetDefinedSensor
    {
        #region Constructors
        public SetDefinedSensor(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
