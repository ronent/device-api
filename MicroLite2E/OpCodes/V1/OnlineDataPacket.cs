﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : MicroLogAPI.OpCodes.V1.OnlineDataPacket
    {
        #region Constructors
        public OnlineDataPacket(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
