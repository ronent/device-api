﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroLogAPI.OpCodes.V1.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }

}
