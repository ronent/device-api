﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class DataPacket : MicroLogAPI.OpCodes.V1.DataPacket
    {
        #region Constructors
        public DataPacket(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
