﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUpload : MicroLogAPI.OpCodes.V1.FirmwareUpload
    {
        #region Constructors
        public FirmwareUpload(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
