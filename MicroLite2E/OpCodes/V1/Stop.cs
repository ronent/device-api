﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class Stop : MicroLogAPI.OpCodes.V1.Stop
    {
        #region Constructors
        public Stop(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
