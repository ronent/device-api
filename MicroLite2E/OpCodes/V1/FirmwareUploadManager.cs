﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroLogAPI.OpCodes.V1.FirmwareUploadManager
    {
        #region Constructors
        public FirmwareUploadManager(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
