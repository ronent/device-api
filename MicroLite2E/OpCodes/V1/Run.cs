﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class Run : MicroLogAPI.OpCodes.V1.Run
    {
        #region Constructors
        public Run(MicroLite2ELogger device)
            : base(device)
        {
        }
        #endregion
    }
}
