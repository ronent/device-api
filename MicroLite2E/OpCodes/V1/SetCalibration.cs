﻿using MicroLite2E.Devices;
using System;

namespace MicroLite2E.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroLogAPI.OpCodes.V1.SetCalibration
    {
        #region Constructors
        public SetCalibration(MicroLite2ELogger device)
            : base(device)
        { }
        #endregion
    }
}
