﻿using System;
using System.Runtime.Serialization;

namespace MicroLite2E.Maintenance
{
    /// <summary>
    /// MicroLite II External Sensor Device Firmware.
    /// </summary>
    [Serializable]
    internal class MicroLite2EFirmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return "MicroLite II"; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EFirmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public MicroLite2EFirmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EFirmware"/> class.
        /// </summary>
        public MicroLite2EFirmware()
        {

        }
    }
}
