﻿using System;
using System.Runtime.Serialization;

namespace MicroLite2E.Sensors.V1.Calibrations
{
    /// <summary>
    /// External NTC Calibration.
    /// </summary>
    [Serializable]
    sealed class ExternalNTC : Base.Sensors.Calibrations.GenericNTC
    {
        internal ExternalNTC()
        {

        }

        #region Fields
        public override decimal DefaultCoeffA { get { return 0.00113277715659262m; } }

        public override decimal DefaultCoeffB { get { return 0.000233509622607238m; } }

        public override decimal DefaultCoeffC { get { return 0.000000090261418093m; } }

        #endregion
        #region Not Supported
        /// <summary>
        /// Calibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Calibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Decalibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Decalibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Serialization & Deserialization
        protected ExternalNTC(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
