﻿using Base.Sensors.Management;
using System;

namespace MicroLite2E.Sensors.V1
{
    /// <summary>
    /// Voltage 0-10V Sensor.
    /// </summary>
    [Serializable]
    public sealed class Voltage0_10V : MicroLogAPI.Sensors.V1.Voltage0_10V
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return false; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Voltage0_10V"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Voltage0_10V(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
