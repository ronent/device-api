﻿using Base.Sensors.Management;
using System;

namespace MicroLite2E.Sensors.V1
{
    /// <summary>
    /// Current 4-20mA Sensor.
    /// </summary>
    [Serializable]
    public sealed class Current4_20mA : MicroLogAPI.Sensors.V1.Current4_20mA
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return false; }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Current4_20mA"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Current4_20mA(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
