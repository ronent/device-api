﻿using Base.Sensors.Management;
using System;

namespace MicroLite2E.Sensors.V1
{
    /// <summary>
    /// External NTC Sensor.
    /// </summary>
    [Serializable]
    public sealed class ExternalNTC : MicroLogAPI.Sensors.V1.ExternalNTC
    {
        #region Properties
        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return false; }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal ExternalNTC(SensorManager parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Initializes the calibration.
        /// </summary>
        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.ExternalNTC();
        }

        #endregion
    }
}
