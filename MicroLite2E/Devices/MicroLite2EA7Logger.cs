﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace MicroLite2E.Devices
{
    /// <summary>
    /// MicroLite II Current 4-20mA Device.
    /// </summary>
    [Serializable]
    public abstract class MicroLite2EA7Logger : MicroLite2ELogger
    {
        #region Fields
        private static readonly string deviceTypeName = "MicroLite II Current 4-20 mA";
        private static readonly byte SUBTYPE = (byte)DeviceTypeEnum.Current_4to20mA;

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return SUBTYPE; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User)
                    return new eSensorType[] 
                    {
                        eSensorType.Current4_20mA,
                    };
                else
                    return base.AvailableDetachableSensors;
            }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EA7Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLite2EA7Logger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EA7Logger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLite2EA7Logger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
