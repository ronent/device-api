﻿using Base.Devices.Management;
using MicroLogAPI.Modules;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using System;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLite2E.Devices
{
    public enum DeviceTypeEnum : byte
    {
        NTC = 0xA5,
        PT100 = 0xA6,
        Current_4to20mA = 0xA7,
        Voltage_0to10V = 0xA8,
    }

    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "MicroLite II External")]
    internal class MicroLite2EInterfacer : GenericSubDeviceInterfacer<MicroLite2ELogger>
    {
        public const byte TYPE = 0x00;
        public const byte BITS = 0x00;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }


        public MicroLite2EInterfacer()
            : base()
        {

        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            return Enum.IsDefined(typeof(DeviceTypeEnum), (device as MicroXLogger).Status.DeviceType);
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x10C4, 0xEA70), new DeviceID(MicroXLoggerManager.VID, 0x0004) }; } }

        protected override Type GetSubType(byte subtype_byte)
        {
            DeviceTypeEnum subtype = (DeviceTypeEnum)subtype_byte;
            switch (subtype)
            {
                case DeviceTypeEnum.NTC:
                    return typeof(MicroLite2EA5Logger);
                case DeviceTypeEnum.PT100:
                    return typeof(MicroLite2EA6Logger);
                case DeviceTypeEnum.Current_4to20mA:
                    return typeof(MicroLite2EA7Logger);
                case DeviceTypeEnum.Voltage_0to10V:
                    return typeof(MicroLite2EA8Logger);
                default:
                    return null;
            }
        }

        protected override GenericDevice CreateNewDevice()
        {
            throw new NotSupportedException();
        }
    }
}