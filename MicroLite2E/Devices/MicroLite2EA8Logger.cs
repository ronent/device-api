﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using System;
using System.Runtime.Serialization;

namespace MicroLite2E.Devices
{
    /// <summary>
    /// MicroLite II Voltage 0-10V Device.
    /// </summary>
    [Serializable]
    public abstract class MicroLite2EA8Logger : MicroLite2ELogger
    {
        #region Fields
        private static readonly string deviceTypeName = "MicroLite II Voltage 0-10 V";
        private static readonly byte SUBTYPE = (byte)DeviceTypeEnum.Voltage_0to10V;

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return SUBTYPE; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User)
                    return new eSensorType[] 
                    {
                        eSensorType.Voltage0_10V,
                    };
                else
                    return base.AvailableDetachableSensors;
            }
        }
        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EA8Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLite2EA8Logger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2EA8Logger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLite2EA8Logger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
