﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Features;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace MicroLite2E.Devices
{
    /// <summary>
    /// MicroLite II External Device.
    /// </summary>
    [Serializable]
    public abstract class MicroLite2ELogger : MicroLiteLogger
    {
        #region Members
        private static readonly string deviceTypeName = "MicroLite II External";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return MicroLite2EInterfacer.TYPE; } }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return MicroLite2EInterfacer.BITS; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }
        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[]
                {
                    MicroXFeature.SHOW_MINMAX,
                    MicroXFeature.SET_MEMORY_SIZE,
                    MicroXFeature.LED_ON_ALARM,                    
                }).ToArray();
            }
        }
        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] 
                {
                    eSensorType.Current4_20mA,
                    eSensorType.Voltage0_10V,
                    eSensorType.ExternalNTC,
                    //eSensorType.PT100, Commented out pending further notice
                };
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2ELogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLite2ELogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2ELogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLite2ELogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
