﻿using Base.Devices;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Sensors.Management;
using MicroXBase.Devices.Types;
using System;

namespace MicroLite2E.DataStructures
{
    /// <summary>
    /// Setup configuration for MicroLite II external sensor.
    /// </summary>
    [Serializable]
    public sealed class MicroLite2ESetupConfiguration : MicroLogSetupConfiguration, IESensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2ESetupConfiguration"/> class.
        /// </summary>
        public MicroLite2ESetupConfiguration()
            : base()
        {
            ExternalSensor = new ESensorSetup();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the external sensor.
        /// </summary>
        /// <value>
        /// The external sensor.
        /// </value>
        public ESensorSetup ExternalSensor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether device stops on cap disconnection.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [stop on disconnect]; otherwise, <c>false</c>.
        /// </value>
        public bool StopOnDisconnect { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Validates the configuration and throws exception for error.
        /// </summary>
        /// <param name="device">The device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void adjustDummyStatus(MicroXLogger dummy)
        {
            ESensorSetup.adjustExternalSensor(dummy, ExternalSensor);

            base.adjustDummyStatus(dummy);
        }

        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(ExternalSensor.Enabled, "External sensor must be enabled");
            ExternalSensor.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}