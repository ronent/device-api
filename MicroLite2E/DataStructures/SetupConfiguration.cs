﻿using Base.OpCodes;
using Base.Sensors.Management;
using Maintenance;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;

using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MicroLite2E.DataStructures
{
    [Serializable]
    public class MicroLite2ESetupConfiguration : SetupConfiguration, IESensorSetup
    {
        public ESensorSetup ExternalSensor { get; set; }
        public bool StopOnDisconnect { get; set; }

        public MicroLite2ESetupConfiguration()
            :base()
        {
            ExternalSensor = new ESensorSetup();
        }
    }
}