﻿using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet920.OpCodes.V1
{
    [Serializable]
    internal class GetSensorStatus : DataNetLoggersAPI.OpCodes.V1.GetSensorStatus
    {
        #region Constructor
        public GetSensorStatus(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
