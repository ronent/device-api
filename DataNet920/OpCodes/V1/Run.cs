﻿using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet920.OpCodes.V1
{
    [Serializable]
    internal class Run : DataNetLoggersAPI.OpCodes.V1.Run
    {
        #region Constructor
        public Run(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
