﻿using DataNet920.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet920.OpCodes.V1
{
    [Serializable]
    internal class DeviceTypeQuery : DataNetLoggersAPI.OpCodes.V1.DeviceTypeQuery
    {
        #region Constructor
        public DeviceTypeQuery(DataNet920Logger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
