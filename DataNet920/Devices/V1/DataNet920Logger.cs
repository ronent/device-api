﻿using Base.Devices.Management.DeviceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataNet920.Devices.V1
{
    /// <summary>
    /// DataNet 920 Logger.
    /// </summary>
    [Serializable]
    public class DataNet920Logger : Devices.DataNet920Logger
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(3, 49);

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet920Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public DataNet920Logger(IDeviceManager parent)
            : base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
    }
}
