﻿using Base.Devices.Management.DeviceManager;
using DataNet9x0.Devices.V1;
using DataNetLoggersAPI.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataNet920.Devices
{
    /// <summary>
    /// DataNet 920 Logger.
    /// </summary>
    [Serializable]
    public class DataNet920Logger : DataNet9x0Logger
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);
        private static readonly string deviceTypeName = "DNL920";

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet920Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public DataNet920Logger(IDeviceManager parent)
            : base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName { get { return deviceTypeName; } }

        #endregion
    }
}
