﻿using Base.Modules.Interfacer;
using DataNetBase.Modules.Interfacer;
using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;

namespace DataNet920.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "DataNet DNL920")]
    internal class DataNet920LoggerInterfacer : GenericSubDeviceInterfacer<DataNet920Logger>
    {
        #region Constructor
        public DataNet920LoggerInterfacer()
            : base()
        {

        }

        #endregion
        #region GenericSubDeviceInterfacer
        public override bool CanCreate(ConnectionEventArgs e)
        {
            return false;
        }

        protected override Base.Devices.GenericDevice CreateNewDevice()
        {
            return new V1.DataNet920Logger(ParentDeviceManager);
        }

        protected override byte Type
        {
            get { return (byte)eDataNetDevicesType.DNL920; }
        }

        #endregion
    }
}
