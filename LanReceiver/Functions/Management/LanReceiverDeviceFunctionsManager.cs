﻿using Base.DataStructures.Device;
using Base.Functions.Management;
using Base.OpCodes;
using LanReceiver.Devices.Types;
using LanReceiver.Modules;
using LanReceiver.OpCodes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.Functions.Management
{
    [Serializable]
    public class LanReceiverDeviceFunctionsManager : DeviceFunctionsManager
    {
        #region Constructor
        public LanReceiverDeviceFunctionsManager(LanReceiverDevice parent)
            :base(parent)
        {
       
        }

        protected override void SubscribeOpCodesEvents()
        {

        }

        #endregion
        #region Properties
        new protected internal LanReceiverDevice Device { get { return base.Device as LanReceiverDevice; } set { base.Device = value; } }

        protected override string BusyErrorMessage
        {
            get { return "Busy"; }
        }

        public override bool IsUpdatingFirmware
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
        #region Override Functions
        public override Task<Result> SendSetup(BaseDeviceSetupConfiguration configuration)
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        public override Result GetStatus()
        {
            if (IsBusy)
                return (new Result(eResult.BUSY) { MessageLog = BusyErrorMessage });

            //return Task.Factory.StartNew<Result>(() =>
            {
                return GetStatusNoBusy();
            }//);
        }

        public override Task<Result> UploadFirmware()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        internal override Result GetStatusNoBusy()
        {
            //return Task.Factory.StartNew<Result>(() =>
            {
                var result = Device.OpCodes.DeviceTypeQuery.Invoke();
                if (!result.IsOK)
                    return result;

                result.SynthesizeResultAndThrowOnError(Device.OpCodes.AcceptanceInfo.Invoke());

                if (result.IsOK)
                    InvokeOnStatus();

                return result;
            }//);
        }

        internal override Result GetDeviceType()
        {
            //return Task.Factory.StartNew<Result>(() =>
            {
                return Device.OpCodes.DeviceTypeQuery.Invoke();
            }//);
        }

        #endregion
        #region New Functions
        internal void SendDataToDevice(DeviceIO deviceIO, byte[] data)
        {
            ForwardMessageWrapper properties = new ForwardMessageWrapper()
            {
                EUI = deviceIO.Id,
                ID = Convert.ToUInt16(deviceIO.ShortId),
                Data = data
            };

            Device.OpCodes.MessageToZBDevice.Invoke(properties);
        }

        #endregion
        #region IDisposable
        public override void Dispose()
        {
            if (!IsDisposed)
            {

                base.Dispose();
            }
        }

        #endregion
        #region Serialization & Deserialization
        public LanReceiverDeviceFunctionsManager(SerializationInfo info, StreamingContext context)
            :base(info, context)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
