﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.Helpers
{
    /// <summary>
    /// Wrapper struct for Message forwarding between LanR and DataNet.
    /// </summary>
    internal struct ForwardMessageWrapper
    {
        /// <summary>
        /// Gets or sets the EUI.
        /// </summary>
        /// <value>
        /// The eui.
        /// </value>
        public string EUI { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public UInt16 ID { get; set; }

        /// <summary>
        /// Gets or sets the data.
        /// </summary>
        /// <value>
        /// The data.
        /// </value>
        public byte[] Data { get; set; }

        /// <summary>
        /// Gets or sets the hops.
        /// </summary>
        /// <value>
        /// The hops.
        /// </value>
        public List<UInt16> Hops { get; set; }
    }
}
