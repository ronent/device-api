﻿using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    [Serializable]
    internal class RefreshNetwork : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x09 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x09 };

        #endregion
        #region Constructor
        public RefreshNetwork(LanReceiverDevice device)
            :base(device)
        {

        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override void Parse()
        {

        }

        protected override void Populate()
        {
            PopulateSendOpCode();

        }

        #endregion
        #region Parse Methods

        #endregion
        #region Populate Methods
 
        #endregion
    }
}
