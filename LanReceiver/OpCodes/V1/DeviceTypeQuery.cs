﻿using Auxiliary.MathLib;
using LanReceiver.DataStructures;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Auxiliary.Tools;

namespace LanReceiver.OpCodes.V1
{
    [Serializable]
    internal class DeviceTypeQuery : LanReceiverOpCode
    {
        #region Members
        public byte DeviceType { get; private set; }

        private static readonly byte[] sendOpCode = new byte[] { 0x01 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x01 };

        #endregion
        #region Constructor
        public DeviceTypeQuery(LanReceiverDevice device)
            :base(device)
        {

        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override void Parse()
        {
            parseDeviceType();
            parseFirmwareVersion();
            parseSN();
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        protected override void DoBeforePopulate()
        {
            ParentDevice.InitializeStatus();
        }

        #endregion
        #region Parse Methods
        private void parseSN()
        {
            byte[] sn = InReport.GetBlock(LanReceiverSetupConfiguration.AllowedSNLength);
            ParentDevice.Status.SerialNumber = BitConverter.ToInt32(sn, 0).ToString();
        }

        private void parseFirmwareVersion()
        {
            byte integer = InReport.Next;
            byte fraction = InReport.Next;

            IntegerFraction intFrac = new IntegerFraction(integer, fraction);
            ParentDevice.Status.FirmwareVersion = new Version(intFrac.ToString());
        }

        private void parseDeviceType()
        {
            DeviceType = (byte)InReport.Next;
        }

        #endregion
    }
}
