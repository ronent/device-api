﻿using Auxiliary.Tools;
using LanReceiver.Devices.Types;
using LanReceiver.Modules;
using LanReceiver.OpCodes.Helpers;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    enum eJoinedLeft : byte
    {
        Join = 0x00,
        Left = 0x01
    }

    [Serializable]
    internal class DeviceJoinedOrLeft : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x06 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x06 };

        #endregion
        #region Constructor
        public DeviceJoinedOrLeft(LanReceiverDevice device)
            :base(device)
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        public override bool WriteToLog { get { return false; } }

        #endregion
        #region Parse
        protected override void Parse()
        {
            string eui = parseEUI();
            UInt16 id = parseID();

            byte state = InReport.Next;
            if (Enum.IsDefined(typeof(eJoinedLeft), state))
                invokeEvent((eJoinedLeft)state, eui, id);
        }

        private void invokeEvent(eJoinedLeft state, string mac, UInt16 id)
        {
            switch (state)
            {
                case eJoinedLeft.Join:
                    ParentDevice.DevicesManager.InvokeOnDeviceConnected(mac, id);
                    break;
                case eJoinedLeft.Left:
                    ParentDevice.DevicesManager.DeviceLeft(mac);
                    break;
                default:
                    break;
            }
        }

        #endregion
    }
}
