﻿using Auxiliary.Tools;
using Base.OpCodes;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Helpers;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    [Serializable]
    internal class MessageToZBDevice : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x0C };
        private static readonly byte[] receiveOpCode = new byte[] { 0x0C };

        #endregion
        #region Fields
        private ForwardMessageWrapper Properties;
        private BlockingCollection<ForwardMessageWrapper> queue = new BlockingCollection<ForwardMessageWrapper>();

        #endregion
        #region Constructor
        public MessageToZBDevice(LanReceiverDevice device)
            :base(device)
        {

        }

        #endregion
        #region Override Methods
        protected override void initialize()
        {
            base.initialize();
            startQueueListener();
        }

        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        public override bool WriteToLog { get { return false; } }

        public override bool SupportsRetry { get { return false; } }

        internal void Invoke(ForwardMessageWrapper properties)
        {
            queue.Add(properties);
        }

        protected override void Parse()
        {
            throw new NotImplementedException();
        }

        protected override void DoAfterPopulate()
        {
            Finish(Result.OK, false);
        }

        #endregion
        #region Methods
        private void startQueueListener()
        {
            Task.Factory.StartNew(() =>
                {
                    while (!IsDisposed)
                    {
                        try
                        {
                            Properties = queue.Take();
                            Result result = Invoke();
                        }
                        catch (Exception ex)
                        {
                            Log4Tech.Log.Instance.WriteError("On startQueueListener()", this, ex);
                        }
                    }
                });
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            base.Populate();
           
            populateEUI();
            populateID();
            populateData();
        }

        private void populateEUI()
        {
            OutReport.InsertStringToHexArray(Properties.EUI, Endianness.BIG);
        }

        private void populateID()
        {
            OutReport.Insert(Properties.ID);
        }

        private void populateData()
        {
            OutReport.Insert(Properties.Data);
        }

        #endregion
    }
}
