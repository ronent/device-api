﻿using Base.OpCodes;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    enum eResponse : byte
    {
        Get = 0x00,
        Reject = 0x01,
        Accepted = 0x02,
        TimeOut = 0x03,
    }

    [Serializable]
    internal class AcceptanceInfo : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x02 };
        private static readonly byte[] receiveOpCode = new byte[] { 0x02 };

        private eResponse response;

        #endregion
        #region Constructor
        public AcceptanceInfo(LanReceiverDevice device)
            :base(device)
        {

        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        protected override void Parse()
        {
            switch (response)
            {
                case eResponse.Get:
                    parseUsername();
                    parsePassword();

                    response = eResponse.Accepted;
                    break;
                case eResponse.Reject:
                    break;
                case eResponse.Accepted:
                    break;
                case eResponse.TimeOut:
                    break;
                default:
                    break;
            }
        }

        protected override void DoBeforePopulate()
        {
            response = eResponse.Get;
        }

        protected override void Populate()
        {
            switch (response)
            {
                case eResponse.Get:
                    PopulateSendOpCode();
                    populateHandShakeResponse();
                    break;
                case eResponse.Reject:
                case eResponse.Accepted:
                case eResponse.TimeOut:
                    PopulateSendOpCode(true);
                    populateHandShakeResponse();
                    populateTimeOut();
                    break;
                default:
                    break;
            }
        }

        protected override void AckReceived()
        {
            Finish(Result.OK);
        }

        protected override void DoAfterParse()
        {
            populateAndSend();
        }

        private void populateAndSend()
        {
            OutReport.Initialize();
            Populate();
            Send();
        }

        #endregion
        #region Parse Methods
        private void parsePassword()
        {
            
        }

        private void parseUsername()
        {
            
        }

        #endregion
        #region Populate Methods
        private void populateHandShakeResponse()
        {
            OutReport.Insert((byte)response);
        }

        private void populateTimeOut()
        {
          
        }

        #endregion
    }
}
