﻿using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    [Serializable]
    internal class EchoMode : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x0F };
        private static readonly byte[] receiveOpCode = new byte[] { 0x0F };

        #endregion
        #region Constructor
        public EchoMode(LanReceiverDevice device)
            :base(device)
        {

        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }





        #endregion
        #region Parse Methods
        protected override void Parse()
        {

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
    }
}
