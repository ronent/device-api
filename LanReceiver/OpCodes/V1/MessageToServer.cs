﻿using Auxiliary.Tools;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Helpers;
using LanReceiver.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.V1
{
    [Serializable]
    internal class MessageToServer : LanReceiverOpCode
    {
        #region Members
        private static readonly byte[] sendOpCode = new byte[] { 0x0D };
        private static readonly byte[] receiveOpCode = new byte[] { 0x0D };

        #endregion
        #region Constructor
        public MessageToServer(LanReceiverDevice device)
            :base(device)
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Override Methods
        public override byte[] SendOpCode
        {
            get { return sendOpCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return receiveOpCode; }
        }

        public override bool WriteToLog { get { return false; } }

        public override bool SupportsRetry { get { return false; } }

        protected override void DoAfterFinished()
        {
            WaitingForInReport = true;
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            ForwardMessageWrapper properties = new ForwardMessageWrapper()
            {
                Hops = new List<ushort>()
            };

            try
            {
                properties.EUI = InReport.GetHexString(Consts.Status.EUI_LENGTH, Endianness.BIG);
                properties.ID = InReport.Get<UInt16>();

                byte numberOfHops = InReport.Next;

                for (int i = 0; i < numberOfHops; i++)
                {
                    properties.Hops.Add(InReport.Get<UInt16>());
                }

                properties.Data = InReport.GetRest();

                ParentDevice.DevicesManager.Receive(properties);
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On Parse()", this, ex);
            }
        }

        #endregion
    }
}