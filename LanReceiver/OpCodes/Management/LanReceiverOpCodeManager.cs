﻿using Auxiliary.Tools;
using Base.OpCodes;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.Management
{
    internal class LanReceiverOpCodeManager : OpCodeManager, IDisposable
    {
        #region Consts
        public const int GENERIC_REPORT_SIZE = 416;

        #endregion
        #region Fields
        public static readonly byte[] STRT = new byte[] { 0x53, 0x54, 0x52, 0xEC };
        private byte sequence = 0; 

        #endregion
        #region Constructor
        public LanReceiverOpCodeManager(LanReceiverDevice device)
            :base(device)
        {
            
        }

        #endregion
        #region Override Methods
        public override int OpCodeStartByte
        {
            get { return 0; }
        }

        public override int MaximumArraySize
        {
            get { return GENERIC_REPORT_SIZE - 12; }
        }

        protected override void DataReceived(byte[] packet, bool propogateAck)
        {
            try
            {
                byte[] data = parseMessageHeader(packet);
                base.DataReceived(data, propogateAck);
            }
            catch { }
        }

        public override void SendData(byte[] data)
        {
            try
            {
                byte[] packet = populateMessageHeader(data);
                base.SendData(packet);
            }
            catch { }
        }

        #endregion
        #region Private Methods
            #region Parse
            private byte[] parseMessageHeader(byte[] packet)
            {
                checkSTRT(packet);
                int dataLength = trim(ref packet);
                if (checkCRC(packet, dataLength))
                    return removeMessageHeaders(packet, dataLength);
                else
                    throw new Exception("checksum error.");
            }

            private int trim(ref byte[] packet)
            {
                int length = (packet[4] << 8) + packet[5];
                packet = Utilities.SubArray(packet, 0, length + 13);

                return length;
            }

            private byte[] removeMessageHeaders(byte[] packet, int length)
            {
                return Utilities.SubArray(packet, 7, length + 1);
            }

            private bool checkCRC(byte[] data, int dataLength)
            {
                int received = BitConverter.ToInt32(Utilities.SubArray(data, dataLength + 8, 4), 0);
                int computed = Auxiliary.Tools.CRC32.Compute(Utilities.SubArray(data, 0, dataLength + 8));

                return received == computed;
            }

            private void checkSTRT(byte[] data)
            {
                try
                {
                    for (int i = 0; i < STRT.Length; i++)
                    {
                        if (data[i] != STRT[i])
                            throw new Exception("Illegal message, STRT error.");
                    }
                }
                catch
                {
                    throw new Exception("Illegal message, STRT error.");
                }
            }

            #endregion
            #region Populate
            private byte[] populateMessageHeader(byte[] data)
            {
                byte[] arr = new byte[LanReceiverOpCodeManager.GENERIC_REPORT_SIZE];

                if (data[0] == 0x00)
                    data = Utilities.SubArray(data, 1, data.Length - 1);

                addSTRT(ref arr);
                addLength(ref arr, data.Length - 1); // excluding the opcode from the data
                addSequence(ref arr);
                addData(ref arr, data);
                addCRC(ref arr, data.Length + 7);
                trim(ref arr);

                return arr;
            }

            private void addCRC(ref byte[] arr, int index)
            {
                int crc = Auxiliary.Tools.CRC32.Compute(Utilities.SubArray(arr, 0, index));
                byte[] crcArr = BitConverter.GetBytes(crc);

                for (int i = 0; i < crcArr.Length; i++)
                {
                    arr[index + i] = crcArr[i];
                }
            }

            private void addData(ref byte[] arr, byte[] data)
            {
                for (int index = 0; index < data.Length; index++)
                {
                    arr[7 + index] = data[index];
                }
            }

            private void addSequence(ref byte[] arr)
            {
                arr[6] = (byte)sequence++;
            }

            private void addLength(ref byte[] arr, int length)
            {
                byte[] len = BitConverter.GetBytes((Int16)length);
                Array.Reverse(len);

                for (int i = 0; i < len.Length; i++)
                {
                    arr[4 + i] = len[i];
                }
            }

            private void addSTRT(ref byte[] arr)
            {
                for (int i = 0; i < STRT.Length; i++)
                {
                    arr[i] = STRT[i];
                }
            }

            #endregion
        #endregion
        #region OpCodes
        internal DeviceTypeQuery DeviceTypeQuery;
        internal AcceptanceInfo AcceptanceInfo;

        internal DeviceJoinedOrLeft DeviceJoinedOrLeft;
        internal DeviceIOState DeviceIOState;

        internal MessageToZBDevice MessageToZBDevice;
        internal MessageToServer MessageToServer;

        #endregion
    }
}
