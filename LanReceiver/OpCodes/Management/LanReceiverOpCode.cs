﻿using Auxiliary.Tools;
using Base.OpCodes;
using Base.OpCodes.Types;
using LanReceiver.Devices.Types;
using LanReceiver.OpCodes.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.OpCodes.Management
{
    [Serializable]
    internal abstract class LanReceiverOpCode : OpCode
    {
        #region Consts
        private static readonly byte[] ackHeader = new byte[] { 0x7F };

        #endregion
        #region Constructor
        public LanReceiverOpCode(LanReceiverDevice device)
            :base(device)
        {
            
        }

        #endregion
        #region Override
        public override TimeSpan InvokeTimeOutDuration
        {
            get
            {
                return new TimeSpan(0, 0, 30);
            }
        }

        public override byte[] AckHeader
        {
            get { return ackHeader; }
        }

        new protected LanReceiverDevice ParentDevice
        {
            get { return base.ParentDevice as LanReceiverDevice; }
            set { base.ParentDevice = value; }
        }

        new protected LanReceiverOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as LanReceiverOpCodeManager; }
        }

        #endregion
        #region Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        protected override bool IsAckHeader(Base.OpCodes.IncomingReport report)
        {
            return report.SkipIfContainsBlock(AckHeader);
        }

        protected void PopulateSendOpCode(bool isAckNeeded = false)
        {
            byte b = SendOpCode[0];

            if (isAckNeeded)
                b += 128;

            OutReport.Insert(b);
        }

        protected override byte[] GetOutReportContent()
        {
            return OutReport.GetTrimmedContent();
        }

        protected string parseEUI()
        {
            return InReport.GetHexString(Consts.Status.EUI_LENGTH, Endianness.BIG);
        }

        protected UInt16 parseID()
        {
            return InReport.Get<UInt16>();
        }

        #endregion
    }
}
