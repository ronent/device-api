﻿using Base.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.DataStructures
{
    /// <summary>
    /// Status Configuration Settings For Lan Receiver device.
    /// </summary>
    [Serializable]
    public class LanReceiverStatusConfiguration : BaseDeviceStatusConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LanReceiverStatusConfiguration"/> class.
        /// </summary>
        internal LanReceiverStatusConfiguration()
            :base()
        {

        }
    }
}
