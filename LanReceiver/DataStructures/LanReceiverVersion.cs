﻿using Base.DataStructures;
using LanReceiver.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.DataStructures
{
    /// <summary>
    /// Lan Receiver Version.
    /// </summary>
    [Serializable]
    public class LanReceiverVersion : GenericVersion
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXVersion"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal LanReceiverVersion(LanReceiverDevice device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the parent device.
        /// </summary>
        /// <value>
        /// The parent device.
        /// </value>
        new internal LanReceiverDevice ParentDevice
        {
            get { return base.ParentDevice as LanReceiverDevice; }
            set { base.ParentDevice = value; }
        }

        /// <summary>
        /// Gets the PCB assembly version.
        /// </summary>
        /// <value>
        /// The PCB assembly version.
        /// </value>
        public override byte PCBAssembly
        {
            get { return ParentDevice.Status.PCBAssembly; }
        }

        /// <summary>
        /// Gets the PCB version.
        /// </summary>
        /// <value>
        /// The PCB version.
        /// </value>
        public override byte PCBVersion
        {
            get { return ParentDevice.Status.PCBVersion; }
        }

        /// <summary>
        /// Gets the firmware revision.
        /// </summary>
        /// <value>
        /// The firmware revision.
        /// </value>
        public override byte FirmwareRevision
        {
            get { return ParentDevice.Status.FWRevision; }
        }

        /// <summary>
        /// Gets the firmware version.
        /// </summary>
        /// <value>
        /// The firmware version.
        /// </value>
        public override Version Firmware
        {
            //get { return ParentDevice.Status.FirmwareVersion; }
            get { return new Version(1, 0); }
        }

        #endregion
    }
}
