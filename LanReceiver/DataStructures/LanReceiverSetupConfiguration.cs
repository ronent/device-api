﻿using Base.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.DataStructures
{
    /// <summary>
    /// Setup Configuration Settings For Lan Receiver device.
    /// </summary>
    [Serializable]
    public class LanReceiverSetupConfiguration : BaseDeviceSetupConfiguration
    {
        #region Fields
        public static readonly int AllowedSNLength = 4;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="LanReceiverSetupConfiguration"/> class.
        /// </summary>
        public LanReceiverSetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
