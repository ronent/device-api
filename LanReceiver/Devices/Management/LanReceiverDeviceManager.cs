﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using Base.Devices.Management.DeviceManager;
using System.Runtime.Serialization;
using Base.Devices;
using System.Net;
using LanReceiver.Devices.Types;
using Infrastructure.Communication;

namespace ModuleLanReceiver.Devices.Management
{
    /// <summary>
    /// Lan Receiver Device Manager.
    /// </summary>
    [Export(typeof(IDeviceManager))]
    [ExportMetadata("Name", "Lan Receiver Devices Manager")]
    [Serializable]
    public class LanReceiverDeviceManager : GenericDeviceManager<GenericDevice>, ISerializable, IDeviceManager
    {
        #region Fields
        private LanReceiverInterfacer<LanReceiverDevice> module;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="LanReceiverDeviceManager"/> class.
        /// </summary>
        internal LanReceiverDeviceManager()
            :base()
        {
            module = new LanReceiverInterfacer<LanReceiverDevice>(this);
        }

        #endregion
        #region IDeviceManager Methods
        /// <summary>
        /// Called when device found.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs"/> instance containing the event data.</param>
        /// <returns></returns>
        public override bool OnDeviceFound(object sender, ConnectionEventArgs e)
        {
            bool toReturn = false;

            if (module.CanCreate(e))
            {
                module.CreateFromIdAndReport(e);
                toReturn = true;
            }

            return toReturn;
        }

        /// <summary>
        /// Ons the device remove.
        /// </summary>
        /// <param name="device">The device.</param>
        protected override void onDeviceRemove(GenericDevice device)
        {
            TurnOffline(device);
        }

        /// <summary>
        /// Subscribes the specified report method.
        /// </summary>
        /// <typeparam name="E"></typeparam>
        /// <param name="reportMethod">The report method.</param>
        /// <returns></returns>
        public override bool Subscribe<E>(ReportDeviceDelegate<E> reportMethod)
        {
            return false;
        }

        #endregion
    }
}
