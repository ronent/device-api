﻿using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication.Modules;
using LanReceiver.Devices.Types;
using LanReceiver.Modules;
using LanReceiver.OpCodes.Helpers;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.Devices.Management
{
    /// <summary>
    /// Manager for Lan Receiver child devices and loggers
    /// </summary>
    internal class DevicesManager
    {
        #region Fields
        private static CommunicationManager manager = (CommunicationManager)SuperDeviceManager.Get(typeof(CommunicationManager));

        private ConcurrentDictionary<string, DeviceIO> connectedDevicesIO;
        private ConcurrentBag<GenericDevice> devices;
        internal LanReceiverDevice parentDevice;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DevicesManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public DevicesManager(LanReceiverDevice device)
        {
            IsDisposed = false;
            connectedDevicesIO = new ConcurrentDictionary<string, DeviceIO>();
            devices = new ConcurrentBag<GenericDevice>();
            parentDevice = device;

            parentDevice.OnOffline += parent_OnOffline;
            SuperDeviceManager.OnDeviceConnected += SuperDeviceManager_OnDeviceConnected;
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets the devices/loggers.
        /// </summary>
        /// <value>
        /// The devices.
        /// </value>
        public IEnumerable<GenericDevice> Devices { get { return devices; } }

        #endregion
        #region Methods
        /// <summary>
        /// Invokes device connected event.
        /// </summary>
        /// <param name="mac">The mac.</param>
        /// <param name="id">The identifier.</param>
        internal void InvokeOnDeviceConnected(string mac, UInt16 id)
        {
            DeviceIO deviceIO = new DeviceIO(this, mac, id);

            if (connectedDevicesIO.TryAdd(deviceIO.Id, deviceIO))
                manager.InvokeOnDeviceConnected(deviceIO);
        }

        /// <summary>
        /// Invokes device disconnected event.
        /// </summary>
        /// <param name="mac">The mac.</param>
        internal void InvokeOnDeviceDisconnected(string mac)
        {
            DeviceIO deviceIO;

            if (connectedDevicesIO.TryRemove(mac, out deviceIO))
                manager.InvokeOnDeviceDisconnected(deviceIO);
        }

        /// <summary>
        /// Device left the network.
        /// </summary>
        /// <param name="mac">The mac.</param>
        internal void DeviceLeft(string mac)
        {
            GenericDevice toRemove = Devices.FirstOrDefault((device) => device.OpCodes.DeviceIO.Id.Equals(mac));

            GenericDevice removedDevice;
            if (devices.TryTake(out removedDevice))
            {
                InvokeOnDeviceDisconnected(mac);
            }
        }

        /// <summary>
        /// Send data from device or logger to Lan Receiver.
        /// </summary>
        /// <param name="mac">The mac.</param>
        /// <param name="data">The data.</param>
        internal void Send(string mac, byte[] data)
        {
            try
            {
                DeviceIO device;
                if (connectedDevicesIO.TryGetValue(mac, out device))
                {
                    parentDevice.Functions.SendDataToDevice(device, data);
                }
            }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On Send()", this, ex);
            }
        }

        /// <summary>
        /// Receives data from Lan Receiver to device or logger.
        /// </summary>
        /// <param name="properties">The properties.</param>
        internal void Receive(ForwardMessageWrapper properties)
        {
            try
            { 
                DeviceIO device;
                if (connectedDevicesIO.TryGetValue(properties.EUI, out device))
                {
                    device.UpdateID(properties.ID);
                    device.InvokeOnDataReceived(properties.Data);
                }
            }
            catch (ObjectDisposedException) { }
            catch (Exception ex)
            {
                Log4Tech.Log.Instance.WriteError("On Receive()", this, ex);
            }
        }

        /// <summary>
        /// Handles the OnOffline event of the parent control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs{GenericDevice}"/> instance containing the event data.</param>
        void parent_OnOffline(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            TurnOffAllDevices();
        }

        /// <summary>
        /// Turns the off all devices.
        /// </summary>
        internal void TurnOffAllDevices()
        {
            foreach (var device in connectedDevicesIO)
            {
                device.Value.Dispose();
            }
        }

        /// <summary>
        /// Handles the OnDeviceConnected event of the SuperDeviceManager control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="ConnectionEventArgs{GenericDevice}"/> instance containing the event data.</param>
        void SuperDeviceManager_OnDeviceConnected(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            if (e.Device.OpCodes.DeviceIO is ILanReceiverDeviceIO)
            {
                if ((e.Device.OpCodes.DeviceIO as ILanReceiverDeviceIO).IsParent(this.parentDevice.Id) && !devices.Any(x => x.Status.SerialNumber == e.SerialNumber))
                    devices.Add(e.Device);
            }
        }

        /// <summary>
        /// Reconnects all Lan Receiver devices and loggers.
        /// </summary>
        internal void LanReceiverReconnect()
        {
            Parallel.ForEach(Devices, (device) =>
                {
                    InvokeOnDeviceConnected(device.Id, default(ushort));
                });
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                TurnOffAllDevices();
                connectedDevicesIO.Clear();

                devices = new ConcurrentBag<GenericDevice>();

                SuperDeviceManager.OnDeviceConnected -= SuperDeviceManager_OnDeviceConnected;
                parentDevice.OnOffline -= parent_OnOffline;
            }
        }

        #endregion
    }
}