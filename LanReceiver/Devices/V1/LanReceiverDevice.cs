﻿using Base.Devices.Management.DeviceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.Devices.V1
{
    /// <summary>
    /// Lan Receiver Device.
    /// </summary>
    [Serializable]
    public sealed class LanReceiverDevice : LanReceiver.Devices.Types.LanReceiverDevice
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

        #endregion
        /// <summary>
        /// Initializes a new instance of the <see cref="LanReceiverDevice"/> class.
        /// </summary>
        /// <param name="deviceManager">The device manager.</param>
        internal LanReceiverDevice(IDeviceManager deviceManager)
            :base(deviceManager)
        {

        }
    }
}
