﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Modules.Interfacer;
using Base.OpCodes;
using System.Threading.Tasks;
using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;

namespace LanReceiver.Devices.Types
{
    internal class LanReceiverInterfacer<T> : GenericDeviceInterfacer<LanReceiverDevice>, IDeviceInterfacer
    {
        #region Fields
        public const byte TYPE = 0x00;

        private IDeviceManager parentDeviceManager;

        #endregion
        #region Constructor
        public LanReceiverInterfacer(IDeviceManager parent)
            :base()
        {
            parentDeviceManager = parent;
        }

        #endregion
        #region Override Methods
        protected override IDeviceManager ParentDeviceManager
        {
            get { return parentDeviceManager; }
        }

        protected override byte Type { get { return TYPE; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.LanReceiverDevice(ParentDeviceManager);
        }

        public override bool CanCreate(ConnectionEventArgs e)
        {
            if (e is HIDConnectionEventArgs || e.DeviceIO is ILanReceiverDeviceIO)
                return false;

            return e.DeviceIO.ShortId.IsIPAddress();
        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            return device is LanReceiverDevice;
        }

        #endregion
    }
}
