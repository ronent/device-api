﻿using Base.DataStructures.Device;
using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.OpCodes;
using Infrastructure.Communication.Modules;
using LanReceiver.DataStructures;
using LanReceiver.Devices.Management;
using LanReceiver.Functions.Management;
using LanReceiver.OpCodes.Management;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.Devices.Types
{
    /// <summary>
    /// Lan Receiver Device.
    /// </summary>
    [Serializable]
    public class LanReceiverDevice : GenericDevice
    {
        #region Members
        private static readonly string deviceTypeName = "Lan Receiver";
        private static readonly string firmwareDeviceName = "Lan Receiver";
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXDevice"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal LanReceiverDevice(IDeviceManager parent)
            :base(parent)
        {

        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            try
            {
                if (DevicesManager == null)
                    DevicesManager = new DevicesManager(this);

                if(Version == null)
                    Version = new LanReceiverVersion(this);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Initialize LanRcv", this, ex);
            }
        }

        /// <summary>
        /// Initializes the OpCodes.
        /// </summary>
        internal override void InitializeOpCodesManager()
        {
            OpCodes = new LanReceiverOpCodeManager(this);
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal override void InitializeStatus()
        {
            Status = new LanReceiverStatusConfiguration();
        }

        /// <summary>
        /// Initializes the functions.
        /// </summary>
        internal override void InitializeFunctionManager()
        {
            Functions = new LanReceiverDeviceFunctionsManager(this);
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal LanReceiverOpCodeManager OpCodes
        {
            get { return base.OpCodes as LanReceiverOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public LanReceiverStatusConfiguration Status
        {
            get { return base.Status as LanReceiverStatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public LanReceiverDeviceFunctionsManager Functions
        {
            get { return base.Functions as LanReceiverDeviceFunctionsManager; }
            set { base.Functions = value; }
        }

        /// <summary>
        /// Gets the devices manager.
        /// </summary>
        /// <value>
        /// The devices manager.
        /// </value>
        internal DevicesManager DevicesManager { get; private set; }

        /// <summary>
        /// Gets the DataNet devices.
        /// </summary>
        /// <value>
        /// The devices.
        /// </value>
        public IEnumerable<GenericDevice> Devices { get { return DevicesManager.Devices; } }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get { return deviceTypeName; }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Called when device turns offline.
        /// </summary>
        protected override void OnDeviceOffline()
        {
            DevicesManager.TurnOffAllDevices();
        }

        /// <summary>
        /// Called when device reconnected.
        /// </summary>
        protected override void OnDeviceReconnected()
        {
            DevicesManager.LanReceiverReconnect();
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            DevicesManager.Dispose();
        }

        #endregion
    }
}