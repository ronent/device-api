﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using LanReceiver.Devices.Management;
using LanReceiver.Devices.Types;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace LanReceiver.Modules
{
    /// <summary>
    /// Lan Receiver DeviceIO
    /// </summary>
    internal class DeviceIO : ILanReceiverDeviceIO
    {
        #region Fields
        private DevicesManager parent;
        private bool writeToLog = false;

        private UInt16 id;
        private string eui;

        #endregion
        #region Events
        /// <summary>
        /// Occurs when device received data.
        /// </summary>
        public event DataReceivedEventHandler OnDataReceived;

        /// <summary>
        /// Occurs when device sends data.
        /// </summary>
        public event DataSentEventHandler OnDataSent;

        /// <summary>
        /// Gets or sets the on offline.
        /// </summary>
        /// <value>
        /// The on offline.
        /// </value>
        public Action OnOffline { get; set; }

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceIO"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="mac">The mac.</param>
        /// <param name="id">The identifier.</param>
        public DeviceIO(DevicesManager parent, string mac, UInt16 id)
        {
            IsDisposed = false;

            this.parent = parent;
            this.eui = mac;
            this.id = id;
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        #endregion
        #region IDeviceIO
        /// <summary>
        /// Gets the device unique identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get { return eui; } }

        /// <summary>
        /// Gets the device short identifier.
        /// </summary>
        /// <value>
        /// The short identifier.
        /// </value>
        public string ShortId { get { return id.ToString(); } }

        /// <summary>
        /// Sends the data.
        /// </summary>
        /// <param name="data">The data.</param>
        public void SendData(byte[] data)
        {
            throwIfDisposed();
            
            parent.Send(Id, data);
            Utilities.InvokeEvent(OnDataSent, this, new DataTransmittedEventArgs(data));
        }

        /// <summary>
        /// Invokes the on data received.
        /// </summary>
        /// <param name="data">The data.</param>
        internal void InvokeOnDataReceived(byte[] data)
        {
            throwIfDisposed();

            Utilities.InvokeEvent(OnDataReceived, this, new DataTransmittedEventArgs(data));            
        }

        /// <summary>
        /// Updates the identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        internal void UpdateID(ushort id)
        {
            if (!Id.Equals(id))
                this.id = id;
        }

        #endregion
        #region ILanReceiverDeviceIO
        /// <summary>
        /// Determines whether the specified parent identifier is parent.
        /// </summary>
        /// <param name="parentID">The parent identifier.</param>
        /// <returns></returns>
        public bool IsParent(string parentID)
        {
            return parent.parentDevice.Id == parentID;
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                parent.InvokeOnDeviceDisconnected(Id);

                if (OnOffline != null)
                    OnOffline();

                OnDataReceived = null;
                OnDataSent = null;
                parent = null;
            }
        }

        /// <summary>
        /// Throws exception if disposed.
        /// </summary>
        /// <exception cref="System.ObjectDisposedException"></exception>
        private void throwIfDisposed()
        {
            if (IsDisposed)
                throw new ObjectDisposedException(string.Empty);
        }

        #endregion
    }
}