﻿using Auxiliary.Tools;
using Infrastructure.Communication;
using Infrastructure.Communication.Modules;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LanReceiver.Modules
{
    /// <summary>
    /// Manages all Lan Receiver Communication IO.
    /// </summary>
    [Export(typeof(ICommunicationManager))]
    [ExportMetadata("Name", "LanReceiver Communication")]
    public class CommunicationManager : ICommunicationManager
    {
        #region Fields

        #endregion
        #region Events
        /// <summary>
        /// Occurs when device connected.
        /// </summary>
        public event DeviceConnectedDelegate OnDeviceConnected;

        /// <summary>
        /// Occurs when device removed.
        /// </summary>
        public event DeviceRemovedDelegate OnDeviceRemoved;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="CommunicationManager"/> class.
        /// </summary>
        public CommunicationManager()
        {
            IsDisposed = false;
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        #endregion
        #region Methods
        /// <summary>
        /// Scans for devices.
        /// </summary>
        public void ScanForDevices()
        {
            
        }

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        public void Shutdown()
        {
            Dispose();
        }

        /// <summary>
        /// Invokes device connected event.
        /// </summary>
        /// <param name="deviceIO">The device io.</param>
        internal void InvokeOnDeviceConnected(DeviceIO deviceIO)
        {
            ConnectionEventArgs args = new ConnectionEventArgs()
            {
                UID = deviceIO.Id,
                DeviceIO = deviceIO,
            };

            Log.Instance.Write("[LanReceiver Communication] ==> Device connected: '{0}'", this, Log.LogSeverity.DEBUG, args.DeviceIO.ShortId);

            Utilities.InvokeEvent(OnDeviceConnected, this, args);
        }

        /// <summary>
        /// Invokes device disconnected event.
        /// </summary>
        /// <param name="e">The <see cref="ConnectionEventArgs"/> instance containing the event data.</param>
        internal void InvokeOnDeviceDisconnected(ConnectionEventArgs e)
        {
            Log.Instance.Write("[LanReceiver Communication] ==> Device disconnected: '{0}'", this, Log.LogSeverity.DEBUG, e.DeviceIO.ShortId);

            Utilities.InvokeEvent(OnDeviceRemoved, this, e);
        }

        /// <summary>
        /// Invokes device disconnected event.
        /// </summary>
        /// <param name="deviceIO">The device io.</param>
        internal void InvokeOnDeviceDisconnected(DeviceIO deviceIO)
        {
            InvokeOnDeviceDisconnected(new ConnectionEventArgs() {DeviceIO = deviceIO, UID = deviceIO.Id });
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
            }
        }

        #endregion
    }
}