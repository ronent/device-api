﻿using Base.DataStructures.Device;
using Base.OpCodes;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using DataNetBase.Functions.Management;
using DataNetLoggersAPI.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.Functions.Management
{
    /// <summary>
    /// Manager for DataNet loggers functions.
    /// </summary>
    [Serializable]
    public class GenericDataNetLoggerFunctionsManager : DataNetLoggerFunctionsManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLoggerFunctionsManager"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public GenericDataNetLoggerFunctionsManager(GenericDataNetLogger logger)
            :base(logger)
        {

        }

        /// <summary>
        /// Subscribes the op codes events.
        /// </summary>
        protected override void SubscribeOpCodesEvents()
        {
            
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        new protected internal GenericDataNetLogger Device { get { return base.Device as GenericDataNetLogger; } set { base.Device = value; } }

        /// <summary>
        /// Gets a value indicating whether this instance is downloading.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is downloading; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsDownloading
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is running.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is running; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsRunning
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether [push to run mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [push to run mode]; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool PushToRunMode
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether [timer run enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [timer run enabled]; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool TimerRunEnabled
        {
            get { return false; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is updating firmware.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is updating firmware; otherwise, <c>false</c>.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override bool IsUpdatingFirmware
        {
            get { return false; }
        }

        #endregion
        #region DataNetLoggerFunctionsManager
        /// <summary>
        /// Downloads this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> Download()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        /// <summary>
        /// Runs this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> Run()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        /// <summary>
        /// Stops this instance.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> Stop()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        /// <summary>
        /// Gets the status.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> GetStatus()
        {
            var result = new Result(eResult.BUSY) {MessageLog = BusyErrorMessage};
            if (!IsBusy)
                result = GetStatusNoBusy().Result;

            return Task.FromResult(result);
        }

        /// <summary>
        /// Gets the status no busy.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        internal override Task<Result> GetStatusNoBusy()
        {
            var result = Device.OpCodes.DeviceTypeQuery.Invoke().Result;

            result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetLoggerStatus.Invoke().Result);

            result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetSensorStatus.Invoke(eSensorIndex.External1));
            result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetSensorStatus.Invoke(eSensorIndex.External2));
            result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetSensorStatus.Invoke(eSensorIndex.External3));
            result.SynthesizeResultAndThrowOnError(Device.OpCodes.GetSensorStatus.Invoke(eSensorIndex.External4));

            if (result.IsOK)
                InvokeOnStatus();

            return Task.FromResult(result);
        }

        /// <summary>
        /// Sends the setup.
        /// </summary>
        /// <param name="configuration">The configuration.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> SendSetup(BaseDeviceSetupConfiguration configuration)
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        /// <summary>
        /// Uploads the firmware.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Task<Result> UploadFirmware()
        {
            return Task.FromResult(Result.UNSUPPORTED);           
        }

        public override Task<Result> ResetCalibration()
        {
            return Task.FromResult(Result.UNSUPPORTED); 
        }

          public override Task<Result> SendCalibration(CalibrationConfiguration calibration)
        {
            return Task.FromResult(Result.UNSUPPORTED);          
        }

        #endregion
    }
}
