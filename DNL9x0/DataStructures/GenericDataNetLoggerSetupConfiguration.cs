﻿using DataNetBase.DataStructures.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.DataStructures
{
    /// <summary>
    /// Setup Configuration Settings For DataNet 9x0 logger.
    /// </summary>
    [Serializable]
    public class GenericDataNetLoggerSetupConfiguration : DataNetLoggerSetupConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLoggerSetupConfiguration"/> class.
        /// </summary>
        public GenericDataNetLoggerSetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
