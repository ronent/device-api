﻿using DataNetBase.DataStructures.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.DataStructures
{
    /// <summary>
    /// Status Configuration Settings For DataNet 9x0 logger.
    /// </summary>
    [Serializable]
    public class GenericDataNetLoggerStatusConfiguration : DataNetLoggerStatusConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLoggerStatusConfiguration"/> class.
        /// </summary>
        internal GenericDataNetLoggerStatusConfiguration()
            :base()
        {

        }

        #endregion
    }
}
