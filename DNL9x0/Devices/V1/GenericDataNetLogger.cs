﻿using Base.Devices.Management.DeviceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.Devices.V1
{
    [Serializable]
    public class GenericDataNetLogger : Devices.GenericDataNetLogger
    {
        #region Constructor
        public GenericDataNetLogger(IDeviceManager parent)
            :base(parent)
        {

        }

        #endregion
    }
}
