﻿using Base.Devices.Management.DeviceManager;
using DataNetBase.Devices.Types;
using DataNetLoggersAPI.DataStructures;
using DataNetLoggersAPI.Functions.Management;
using DataNetLoggersAPI.OpCodes.Managment;
using DataNetLoggersAPI.Sensors.Managment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.Devices
{
    /// <summary>
    /// Generic Data Net Logger.
    /// </summary>
    [Serializable]
    public abstract class GenericDataNetLogger : DataNetLogger
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public GenericDataNetLogger(IDeviceManager parent)
            :base(parent)
        {

        }

        /// <summary>
        /// Initializes the sensor manager.
        /// </summary>
        internal override void InitializeSensorManager()
        {
            Sensors = new GenericDataNetLoggerSensorManager(this);
        }

        /// <summary>
        /// Initializes the function manager.
        /// </summary>
        internal override void InitializeFunctionManager()
        {
            Functions = new GenericDataNetLoggerFunctionsManager(this);
        }

        /// <summary>
        /// Initializes the op codes manager.
        /// </summary>
        internal override void InitializeOpCodesManager()
        {
            OpCodes = new GenericDataNetLoggerOpCodeManager(this);
        }

        /// <summary>
        /// Initializes the status.
        /// </summary>
        internal override void InitializeStatus()
        {
            Status = new GenericDataNetLoggerStatusConfiguration();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the sensors.
        /// </summary>
        /// <value>
        /// The sensors.
        /// </value>
        new public GenericDataNetLoggerSensorManager Sensors
        {
            get { return base.Sensors as GenericDataNetLoggerSensorManager; }
            protected set { base.Sensors = value; }
        }

        /// <summary>
        /// Gets or sets the op codes.
        /// </summary>
        /// <value>
        /// The op codes.
        /// </value>
        new internal GenericDataNetLoggerOpCodeManager OpCodes
        {
            get { return base.OpCodes as GenericDataNetLoggerOpCodeManager; }
            set { base.OpCodes = value; }
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        /// <value>
        /// The status.
        /// </value>
        new public GenericDataNetLoggerStatusConfiguration Status
        {
            get { return base.Status as GenericDataNetLoggerStatusConfiguration; }
            protected set { base.Status = value; }
        }

        /// <summary>
        /// Gets or sets the functions.
        /// </summary>
        /// <value>
        /// The functions.
        /// </value>
        new public GenericDataNetLoggerFunctionsManager Functions
        {
            get { return base.Functions as GenericDataNetLoggerFunctionsManager; }
            protected set { base.Functions = value; }
        }

        #endregion
        #region DataNetLogger
        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Base.Sensors.Management.eSensorType[] AvailableDetachableSensors
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Base.Sensors.Management.eSensorType[] AvailableFixedSensors
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get { return "Generic DNL9x0 Logger"; }
        }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        internal override string FirmwareDeviceName
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets the minimum required firmware version.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { throw new NotImplementedException(); }
        }

        #endregion
    }
}