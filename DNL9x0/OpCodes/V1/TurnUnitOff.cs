﻿using DataNetLoggersAPI.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.V1
{
    [Serializable]
    internal class TurnUnitOff : DataNetBase.OpCodes.Common.TurnUnitOff
    {
        #region Constructor
        public TurnUnitOff(GenericDataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
