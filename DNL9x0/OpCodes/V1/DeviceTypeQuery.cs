﻿using DataNetBase.Devices.Types;
using DataNetLoggersAPI.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.V1
{
    [Serializable]
    internal class DeviceTypeQuery : DataNetBase.OpCodes.Common.DeviceTypeQuery
    {
        #region Constructor
        public DeviceTypeQuery(GenericDataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
