﻿using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.V1
{
    [Serializable]
    internal class Run : DataNetOpCode
    {
        #region Fields
        private static readonly byte[] opCode = new byte[] { 0x11 };

        #endregion
        #region Constructor
        public Run(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return opCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return opCode; }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            ParentLogger.InitializeStatus();
        }

        protected override void Parse()
        {
            base.Parse();
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Populate Methods

        #endregion
        #region Parse Methods

        #endregion
    }
}
