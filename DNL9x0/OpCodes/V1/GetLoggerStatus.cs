﻿using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.V1
{
    [Serializable]
    internal class GetLoggerStatus : DataNetOpCode
    {
        #region Fields
        private static readonly byte[] opCode = new byte[] { 0x0B };

        #endregion
        #region Constructor
        public GetLoggerStatus(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return opCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return opCode; }
        }

        #endregion
        #region Methods
        protected override void Parse()
        {
            base.Parse();

            parseCurrentTime();
            parseZBMode();
            parseLoggerMode();
            parseInterval();
            parseTransmissionsInterval();
            parseAveraging();
            parseInternalSensorSetupBooleans();
            parseInternalSensorAlarmInfo();
            parseAlarmInfo();
            parseAttachedSensorsAlarmInfo();
            parseSetupSignutare();
            parseComment();
            parseBatterylevel();
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Parse Methods
        private void parseCurrentTime()
        {

        }

        private void parseZBMode()
        {

        }

        private void parseLoggerMode()
        {

        }

        private void parseInterval()
        {

        }

        private void parseTransmissionsInterval()
        {

        }

        private void parseAveraging()
        {
           
        }

        private void parseInternalSensorSetupBooleans()
        {

        }

        private void parseInternalSensorAlarmInfo()
        {
 
        }

        private void parseAlarmInfo()
        {
            parseAlarmDelay();
            parsePreAlarmDelay();
            parseAlarmDuration();
        }

        private void parseAlarmDelay()
        {
            
        }

        private void parsePreAlarmDelay()
        {
            
        }

        private void parseAlarmDuration()
        {

        }

        private void parseAttachedSensorsAlarmInfo()
        {
            
        }

        private void parseSetupSignutare()
        {

        }

        private void parseComment()
        {

        }

        private void parseBatterylevel()
        {
   
        }

        #endregion
    }
}
