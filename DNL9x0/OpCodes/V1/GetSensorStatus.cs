﻿using Base.OpCodes;
using Base.Sensors.Management;
using DataNetBase.Devices.Types;
using DataNetBase.OpCodes.Management;
using Log4Tech;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.V1
{
    [Serializable]
    internal class GetSensorStatus : DataNetOpCode
    {
        #region Fields
        private static readonly byte[] opCode = new byte[] { 0x0C };
        private eSensorIndex sensorIndex;

        #endregion
        #region Constructor
        public GetSensorStatus(DataNetLogger logger)
            : base(logger)
        {

        }

        #endregion
        #region Properties
        private byte SensorIndex
        {
            get
            {
                byte index = (byte)sensorIndex;
                index -= 3;
                return index;
            }
        }

        public override byte[] SendOpCode
        {
            get { return opCode; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return opCode; }
        }

        #endregion
        #region Methods
        internal Result Invoke(eSensorIndex sensorIndex)
        {
            this.sensorIndex = sensorIndex;

            return Invoke().Result;
        }

        protected override void Parse()
        {
            base.Parse();

            parseSensorNumberAndType();
            parseName();
            parseUnit();
            parseStartValue();
            parseStepValue();
            parseMode();
            parseSensorDefinition();
            parseAlarms();
            parsePCStartValue();
            parsePCStepValue();
        }

        protected override void Populate()
        {
            PopulateSendOpCode();
            OutReport.Insert(SensorIndex);
        }

        protected override string NameString
        {
            get
            {
                return base.NameString + " " + sensorIndex;
            }
        }

        #endregion
        #region Populate Methods

        #endregion
        #region Parse Methods
        private void parseSensorNumberAndType()
        {

        }

        private void parseName()
        {

        }

        private void parseUnit()
        {
            
        }

        private void parseStartValue()
        {

        }

        private void parseStepValue()
        {

        }

        private void parseMode()
        {

        }

        private void parseSensorDefinition()
        {
           
        }

        private void parseAlarms()
        {
          
        }

        private void parsePCStartValue()
        {
   
        }

        private void parsePCStepValue()
        {
         
        }

        #endregion
    }
}