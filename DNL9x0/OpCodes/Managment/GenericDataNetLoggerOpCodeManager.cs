﻿using DataNetBase.OpCodes.Management;
using DataNetLoggersAPI.Devices;
using DataNetLoggersAPI.OpCodes.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.OpCodes.Managment
{
    /// <summary>
    /// Generic DataNet Logger Op Code Manager.
    /// </summary>
    [Serializable]
    internal class GenericDataNetLoggerOpCodeManager : DataNetOpCodeManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLoggerOpCodeManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        public GenericDataNetLoggerOpCodeManager(GenericDataNetLogger device)
            :base(device)
        {

        }

        #endregion
        #region OpCodes
        internal GetLoggerStatus GetLoggerStatus;
        internal GetSensorStatus GetSensorStatus;
        internal Run Run;
        internal Stop Stop;
        internal SetBuzzer SetBuzzer;

        #endregion
    }
}
