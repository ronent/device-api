﻿using DataNetBase.Sensors.Management;
using DataNetLoggersAPI.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetLoggersAPI.Sensors.Managment
{
    /// <summary>
    /// DataNet 9x0 Sensor Manger.
    /// </summary>
    [Serializable]
    public class GenericDataNetLoggerSensorManager : DataNetSensorManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericDataNetLoggerSensorManager"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public GenericDataNetLoggerSensorManager(GenericDataNetLogger logger)
            :base(logger)
        {

        }

        #endregion
    }
}
