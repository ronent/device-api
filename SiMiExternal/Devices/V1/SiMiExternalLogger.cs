﻿using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace SiMiExternal.Devices.V1
{
    [Serializable]
    public sealed class SiMiExternalLogger : Devices.SiMiExternalLogger
    {
        #region Members
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

        internal static readonly string firmwareDeviceName = Devices.SiMiExternalLogger.firmwareDeviceName + " V1";
        #endregion
        #region Properties
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        internal SiMiExternalLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiExternalLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

       
        #endregion
    }
}
