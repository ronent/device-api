﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using SiMiAPI.Modules;
using SiMiBase.Devices.Management;

namespace SiMiExternal.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "SiMi External")]
    internal class SiMiExternalInterfacer : GenericSubDeviceInterfacer<SiMiExternalLogger>
    {
        public const byte TYPE = 0x03;//0xA0
        public const byte BITS = 0x02;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public SiMiExternalInterfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(SiMiBaseLoggerManager.VID, 0x000A) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.SiMiExternalLogger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
