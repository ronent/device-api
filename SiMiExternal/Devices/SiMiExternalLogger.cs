﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using SiMiAPI.Devices;
using SiMiBase.Devices.Features;

namespace SiMiExternal.Devices
{
    [Serializable]
    public class SiMiExternalLogger : SiMiLogger
    {
        #region Members
        private static readonly string deviceTypeName = "SiMi External Logger";
        internal static readonly string firmwareDeviceName = "SiMi External Logger";
        #endregion
        #region Properties
        protected override byte Type { get { return SiMiExternalInterfacer.TYPE; } }

        protected override byte Bits { get { return SiMiExternalInterfacer.BITS; } }

        public override string DeviceTypeName
        {
            get { return deviceTypeName; }
        }

        public override SiMiBaseFeature[] Features
        {
            get
            {
                return base.Features.Concat(new[]
                {
                    SiMiBaseFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }

        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new[] { eSensorType.InternalNTC };
            }
        }

        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        #endregion
        #region Constructors
        internal SiMiExternalLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiExternalLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

      
        #endregion
    }
}
