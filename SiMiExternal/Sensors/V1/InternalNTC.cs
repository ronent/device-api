﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;

namespace SiMiExternal.Sensors.V1
{
    [Serializable]
    public sealed class InternalNTC : SiMiAPI.Sensors.V1.PT100
    {
        #region Properties
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(80); }
        }

        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-40); }
        }

        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        internal InternalNTC(SensorManagerV2 parent)
            : base(parent)
        {
        }

        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.InternalNTC();
        }
        #endregion
    }
}
