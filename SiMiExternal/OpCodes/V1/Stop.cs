﻿using System;
using SiMiExternal.Devices;


namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class Stop:SiMiAPI.OpCodes.V1.Stop
    {
        public Stop(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
