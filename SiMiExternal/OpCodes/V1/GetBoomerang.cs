﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : SiMiAPI.OpCodes.V1.GetBoomerang
    {
        public GetBoomerang(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
