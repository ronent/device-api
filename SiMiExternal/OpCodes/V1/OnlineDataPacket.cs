﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : SiMiAPI.OpCodes.V1.OnlineDataPacket
    {
        public OnlineDataPacket(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
