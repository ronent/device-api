﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class DynamicStatus : SiMiAPI.OpCodes.V1.DynamicStatus
    {
        public DynamicStatus(SiMiExternalLogger device)
            : base(device)
        {

        }

        
    }
}
