﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class StaticStatus: SiMiAPI.OpCodes.V1.StaticStatus
    {
        public StaticStatus(SiMiExternalLogger device)
            : base(device)
        {

        }

        #region Overrides of OpCode

        protected override void DoAfterFinished()
        {
            base.DoAfterFinished();
            ParentDevice.Status.FixedSensors.HumidityEnabled = true;
        }

        #endregion
    }
}
