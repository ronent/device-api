﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : SiMiAPI.OpCodes.V1.DefaultCalibration
    {
        public DefaultCalibration(SiMiExternalLogger device) : base(device)

        {

        }
    }
}
