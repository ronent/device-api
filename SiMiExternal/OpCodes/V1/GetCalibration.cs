﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : SiMiAPI.OpCodes.V1.GetCalibration
    {
        public GetCalibration(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
