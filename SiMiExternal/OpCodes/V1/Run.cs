﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class Run : SiMiAPI.OpCodes.V1.Run
    {
        public Run(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
