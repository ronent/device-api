﻿using System;
using SiMiExternal.Devices;

namespace SiMiExternal.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : SiMiAPI.OpCodes.V1.SetBoomerang
    {
        public SetBoomerang(SiMiExternalLogger device)
            : base(device)
        {

        }
    }
}
