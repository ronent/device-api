﻿using System;
using System.Runtime.Serialization;

namespace SiMiExternal.Maintenance
{
    [Serializable]
    internal class SiMiExternalFirmware : SiMiBase.Maintenance.SiMiBaseFirmware
    {
        public override string Name { get { return Devices.V1.SiMiExternalLogger.firmwareDeviceName; } }

        public SiMiExternalFirmware(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public SiMiExternalFirmware()
        {

        }
    }
}
