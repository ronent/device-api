﻿using DataNetBase.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.DataStructures
{
    /// <summary>
    /// DNR900 Setup Configuration.
    /// </summary>
    public class DNR900SetupConfiguration : DataNetDeviceSetupConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DNR900SetupConfiguration"/> class.
        /// </summary>
        public DNR900SetupConfiguration()
            :base()
        {

        }

        #endregion
    }
}
