﻿using DataNetBase.DataStructures.Device;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.DataStructures
{
    /// <summary>
    /// DNR900 Status Configuration
    /// </summary>
    public class DNR900StatusConfiguration : DataNetDeviceStatusConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DNR900StatusConfiguration"/> class.
        /// </summary>
        internal DNR900StatusConfiguration()
            :base()
        {

        }

        #endregion
    }
}
