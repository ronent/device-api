﻿using Base.DataStructures.Device;
using Base.OpCodes;
using DataNetBase.Functions.Management;
using DataNetRepeater.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.Functions.Management
{
    /// <summary>
    /// DNR900 Functions Manager.
    /// </summary>
    [Serializable]
    public class DNR900FunctionsManager : DataNetDeviceFunctionsManager
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNetDeviceFunctionsManager"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal DNR900FunctionsManager(DNR900Device device)
            :base(device)
        {

        }

        protected override void SubscribeOpCodesEvents()
        {
            
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the device.
        /// </summary>
        /// <value>
        /// The device.
        /// </value>
        new protected internal DNR900Device Device { get { return base.Device as DNR900Device; } set { base.Device = value; } }

        protected override string BusyErrorMessage
        {
            get { throw new NotImplementedException(); }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is updating firmware.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is updating firmware; otherwise, <c>false</c>.
        /// </value>
        public override bool IsUpdatingFirmware
        {
            get {return false; }
        }

        #endregion
        #region DataNetDeviceFunctionsManager

        internal override Task<Result> GetDeviceType()
        {
            return Device.OpCodes.DeviceTypeQuery.Invoke();
        }

        public override Task<Result> GetStatus()
        {
            var result = new Result(eResult.BUSY) {MessageLog = BusyErrorMessage};
            if (!IsBusy)
                result= GetStatusNoBusy().Result;

            return Task.FromResult(result);
        }

        internal override Task<Result> GetStatusNoBusy()
        {
            var result = Device.OpCodes.DeviceTypeQuery.Invoke().Result;

            if (result.IsOK)
                InvokeOnStatus();

            return Task.FromResult(result);
        }

        public override Task<Result> SendSetup(BaseDeviceSetupConfiguration configuration)
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        public override Task<Result> UploadFirmware()
        {
            return Task.FromResult(Result.UNSUPPORTED);
        }

        #endregion
    }
}
