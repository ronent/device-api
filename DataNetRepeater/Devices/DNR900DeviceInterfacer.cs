﻿using Base.Modules.Interfacer;
using DataNetBase.Modules.Interfacer;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "DataNet Repeater")]
    internal class DNR900DeviceInterfacer : GenericSubDeviceInterfacer<DNR900Device>
    {
        #region Constructor
        public DNR900DeviceInterfacer()
            :base()
        {

        }

        #endregion
        #region DataNetDevicesInterfacer
        public override bool CanCreate(Infrastructure.Communication.ConnectionEventArgs e)
        {
            return false;
        }

        protected override Base.Devices.GenericDevice CreateNewDevice()
        {
            return new V1.DNR900Device(ParentDeviceManager);
        }

        protected override byte Type
        {
            get { return (byte)eDataNetDevicesType.DNR900; }
        }

        #endregion
    }
}
