﻿using Base.Devices.Management.DeviceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.Devices.V1
{
    /// <summary>
    /// DNR900 Device.
    /// </summary>
    [Serializable]
    public class DNR900Device : Devices.DNR900Device
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(3, 1, 3);

        #endregion
        #region Constructor
        internal DNR900Device(IDeviceManager parent)
            :base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
    }
}
