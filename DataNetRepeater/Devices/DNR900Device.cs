﻿using Base.Devices.Management.DeviceManager;
using DataNetBase.Devices.Types;
using DataNetRepeater.DataStructures;
using DataNetRepeater.Functions.Management;
using DataNetRepeater.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.Devices
{
    /// <summary>
    /// DNR900 Device.
    /// </summary>
    [Serializable]
    public abstract class DNR900Device : DataNetDevice
    {
        #region Fields
        internal static readonly string firmwareDeviceName = "DNR900";

        private static readonly string deviceTypeName = "DataNet Repeater";
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DNR900Device"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DNR900Device(IDeviceManager parent)
            :base(parent)
        {

        }

        internal override void InitializeFunctionManager()
        {
            Functions = new DNR900FunctionsManager(this);
        }

        internal override void InitializeOpCodesManager()
        {
            OpCodes = new DNR900OpCodeManager(this);
        }

        internal override void InitializeStatus()
        {
            Status = new DNR900StatusConfiguration();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override string DeviceTypeName
        {
            get { return deviceTypeName; }
        }

        /// <summary>
        /// Gets the name of the firmware device.
        /// </summary>
        /// <value>
        /// The name of the firmware device.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        internal override string FirmwareDeviceName
        {
            get { return firmwareDeviceName; }
        }

        /// <summary>
        /// Gets the minimum required firmware version.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        /// <exception cref="System.NotImplementedException"></exception>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
    }
}
