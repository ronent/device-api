﻿using DataNetBase.OpCodes.Management;
using DataNetRepeater.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.OpCodes.Management
{
    internal class DNR900OpCodeManager : DataNetOpCodeManager
    {
        #region Constructor
        public DNR900OpCodeManager(DNR900Device device)
            :base(device)
        {

        }

        #endregion
    }
}
