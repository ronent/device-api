﻿using DataNetRepeater.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.OpCodes.V1
{
    [Serializable]
    internal class DeviceTypeQuery : DataNetBase.OpCodes.Common.DeviceTypeQuery
    {
        #region Constructor
        public DeviceTypeQuery(DNR900Device logger)
            : base(logger)
        {

        }

        #endregion
    }
}
