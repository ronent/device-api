﻿using DataNetRepeater.Devices.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNetRepeater.OpCodes.V1
{
    [Serializable]
    internal class TurnUnitOff : DataNetBase.OpCodes.Common.TurnUnitOff
    {
        #region Constructor
        public TurnUnitOff(DNR900Device device)
            : base(device)
        {

        }

        #endregion
    }
}
