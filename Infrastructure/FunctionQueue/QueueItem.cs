﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Infrastructure.FunctionQueue
{
    /// <summary>
    /// Function Type
    /// </summary>
    public enum eFunctionType
    {
        Download,
        FirmwareUpdate,
    }

    /// <summary>
    /// Queue Item.
    /// </summary>
    internal class QueueItem : IDisposable
    {
        #region Fields
        private ManualResetEventSlim mre = new ManualResetEventSlim(false);

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="QueueItem"/> class.
        /// </summary>
        /// <param name="functionType">Type of the function.</param>
        /// <param name="sn">The sn.</param>
        public QueueItem(eFunctionType functionType, string sn)
        {
            IsDisposed = false;
            SerialNumber = sn;
            FunctionType = functionType;
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Gets the serial number.
        /// </summary>
        /// <value>
        /// The serial number.
        /// </value>
        public string SerialNumber { get; private set; }

        /// <summary>
        /// Gets the type of the function.
        /// </summary>
        /// <value>
        /// The type of the function.
        /// </value>
        public eFunctionType FunctionType { get; private set; }

        #endregion
        #region Methods
        /// <summary>
        /// Waits this instance.
        /// </summary>
        public void Wait()
        {
            mre.Wait();
        }

        /// <summary>
        /// Sets this instance.
        /// </summary>
        public void Set()
        {
            mre.Set();
        }

        #endregion
        #region Object Override
        public override bool Equals(object obj)
        {
            if (obj is QueueItem)
            {
                QueueItem other = obj as QueueItem;
                if (other.SerialNumber == SerialNumber && other.FunctionType == FunctionType)
                    return true;
                else
                    return false;
            }
            else
                return base.Equals(obj);
        }

        #endregion
        #region IDisposable
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;

                if (mre != null)
                    mre.Dispose();
            }
        }

        #endregion
    }
}
