﻿using Auxiliary.Tools;
using Infrastructure.FunctionQueue.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.FunctionQueue
{
    public class FunctionQueue : IDisposable
    {
        #region Fields
        private const int MAX = 3;
        private ConcurrentQueue<QueueItem> queue = new ConcurrentQueue<QueueItem>();
        private ConcurrentDictionary<string, QueueItem> workers = new ConcurrentDictionary<string, QueueItem>();

        #endregion
        #region Events
        public event FunctionAddedToQueueDelegate FunctionAddedToQueue;

        #endregion
        #region Constructor
        public FunctionQueue()
        {
            IsDisposed = false;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }

        private int workersCount { get { return workers.Count; } }
        private int queueCount { get { return queue.Count; } }

        #endregion
        #region Methods
        public void Add(eFunctionType function, string serialNumber)
        {
            QueueItem newItem = new QueueItem(function, serialNumber);
            throwOnExisting(newItem);

            if (workersCount < MAX)
            {
                workers.TryAdd(serialNumber, newItem);
            }
            else
            {
                queue.Enqueue(newItem);
                Utilities.InvokeEvent(FunctionAddedToQueue, this, new FunctionAddedToQueueEventArgs(function, serialNumber));
                newItem.Wait();
            }
        }

        private void throwOnExisting(QueueItem newItem)
        {
            if (workers.Values.Any(item1 => item1.Equals(newItem) || queue.Any(item2 => item2.Equals(newItem))))
                throw new AddException(eAddExceptionType.Busy);
        }

        public void Remove(string serialNumber)
        {
           QueueItem workerItem;
           if (workers.TryRemove(serialNumber, out workerItem))
           {
               workerItem.Dispose();
           }

            QueueItem newItem;
            if (queue.TryDequeue(out newItem))
            {
                newItem.Set();
                workers.TryAdd(newItem.SerialNumber ,newItem);
            }
        }

        public bool IsBusy(string serialNumber)
        {
            return queue.Any(item => item.SerialNumber == serialNumber) || workers.Values.Any(item => item.SerialNumber == serialNumber);
        }

        #endregion
        #region Object overrides
        public override string ToString()
        {
            return string.Format("Workers Count: {0}, Queue Count: {1}", workersCount, queueCount);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
            }
        }

        #endregion
    }
}