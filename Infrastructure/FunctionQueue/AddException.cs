﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.FunctionQueue
{
    public enum eAddExceptionType
    {
        Busy,
    }

    public class AddException : Exception
    {
        public AddException(eAddExceptionType type)
        {
            Type = type;
        }

        public eAddExceptionType Type { get; private set; }
    }
}
