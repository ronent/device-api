﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.FunctionQueue.Events
{
    public delegate void FunctionAddedToQueueDelegate(object sender, FunctionAddedToQueueEventArgs e);

    public class FunctionAddedToQueueEventArgs : EventArgs
    {
        #region Constructor
        public FunctionAddedToQueueEventArgs(eFunctionType functionType, string serialNumber)
            :base()
        {
            SerialNumber = serialNumber;
            FunctionType = functionType;
        }

        #endregion
        #region Properties
        public string SerialNumber { get; private set; }
        public eFunctionType FunctionType { get; private set; }

        #endregion
    }
}
