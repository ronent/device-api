﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.Communication
{
    public class DataTransmittedEventArgs : EventArgs
    {
        public byte[] Data 
        { 
            get 
            { 
                return Packets.Count() > 0 ? Packets.ToArray()[0] : null; 
            } 
        }

        public IEnumerable<byte[]> Packets { get; private set; }
        
        public DataTransmittedEventArgs(byte[] data)
        {
            Packets = new List<byte[]>{data}.AsEnumerable();
        }

        public DataTransmittedEventArgs(IEnumerable<byte[]> data)
        {
            Packets = data;
        }
    }
}
