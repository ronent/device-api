﻿using Infrastructure.Communication.DeviceIO;
using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Infrastructure.Communication
{
    [DataContract]
    [KnownType(typeof(HIDConnectionEventArgs))]
    public class ConnectionEventArgs : EventArgs
    {
        [DataMember]
        public string UID { get; set; }
        public IDeviceIO DeviceIO { get; set; }
        [DataMember]
        public int UserId { get; set; }

        public override string ToString()
        {
            return UID;
        }
    }
}
