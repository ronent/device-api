﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Communication.DeviceIO
{
    public interface IBundle
    {
        /// <summary>
        /// Occurs when device receives bundle.
        /// </summary>
        event DataReceivedEventHandler OnBundleReceived;

        /// <summary>
        /// Invokes the loop through as long mechanism (bundle).
        /// </summary>
        /// <param name="args">The arguments.</param>
        void InvokeLoopThroughAsLong(LoopArgs args);

        /// <summary>
        /// Gets a value indicating whether is bundle supported.
        /// Sending a bunch of packets to the server at once (to speed up server client communication).
        /// </summary>
        /// <value>
        ///   <c>true</c> if [is bundle supported]; otherwise, <c>false</c>.
        /// </value>
        bool IsBundleSupported { get; } 
    }
}
