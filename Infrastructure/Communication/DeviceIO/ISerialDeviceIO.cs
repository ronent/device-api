﻿using System;
using System.Text;

namespace Infrastructure.Communication.DeviceIO
{
    public interface ISerialDeviceIO : IDeviceIO
    {
        void ApplyChanges(int baudRate, Encoding encoding, string newLine);

        int BaudRate { get; }
        Encoding Encoding { get; }
        string NewLine { get; }
    }
}
