﻿using Infrastructure.Communication.Modules;
using Infrastructure.Communication.Modules.HID;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Communication.DeviceIO
{
    public interface IHIDDeviceIO : IDeviceIO
    {
        FunctionQueue.FunctionQueue FunctionQueue { get; }
    }
}
