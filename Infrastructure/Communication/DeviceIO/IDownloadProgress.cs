﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Communication.DeviceIO
{
    public interface IDownloadProgress
    {
        /// <summary>
        /// Occurs when downloading data.
        /// </summary>
        void InvokeDownloadProgress(int percents);
    }
}
