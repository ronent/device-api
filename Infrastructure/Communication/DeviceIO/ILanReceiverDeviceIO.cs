﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infrastructure.Communication.DeviceIO
{
    /// <summary>
    /// Lan Receiver DeviceIO
    /// </summary>
    public interface ILanReceiverDeviceIO : IDeviceIO
    {
        /// <summary>
        /// Determines whether the specified parent identifier is parent.
        /// </summary>
        /// <param name="parentID">The parent identifier.</param>
        /// <returns></returns>
        bool IsParent(string parentID);
    }
}
