﻿using System;
using System.ServiceModel;

namespace Infrastructure.Communication.DeviceIO
{
    public delegate void DataReceivedEventHandler(object sender, DataTransmittedEventArgs e);
    public delegate void DataSentEventHandler(object sender, DataTransmittedEventArgs e);

    /// <summary>
    /// Device Input/Output.
    /// </summary>
    [ServiceContract]
    public interface IDeviceIO : IDisposable
    {
        #region Events
        /// <summary>
        /// Occurs when device received data.
        /// </summary>
        event DataReceivedEventHandler OnDataReceived;

        /// <summary>
        /// Occurs when device sends data.
        /// </summary>
        event DataSentEventHandler OnDataSent;

        /// <summary>
        /// Gets or sets the on offline.
        /// </summary>
        /// <value>
        /// The on offline.
        /// </value>
        Action OnOffline { get; set; }

        #endregion
        #region Properties
        /// <summary>
        /// Gets a value indicating whether this instance is disposed.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is disposed; otherwise, <c>false</c>.
        /// </value>
        bool IsDisposed { get; }

        /// <summary>
        /// Gets the device unique identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>      
        string Id { get; }

        /// <summary>
        /// Gets the device short identifier.
        /// </summary>
        /// <value>
        /// The short identifier.
        /// </value>     
        string ShortId { get; }

        #endregion
        #region Methods
        /// <summary>
        /// Sends the data.
        /// </summary>
        /// <param name="data">The data.</param>      
        void SendData(byte[] data);

        #endregion
    }
}
