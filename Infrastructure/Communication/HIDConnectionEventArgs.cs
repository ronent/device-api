﻿using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Communication
{
    [DataContract]
    public class HIDConnectionEventArgs : ConnectionEventArgs
    {
        [DataMember]
        public DeviceID DeviceID { get; set; }
      
    }
}
