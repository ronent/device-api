﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Communication.Modules
{
    public delegate void DeviceConnectedDelegate(object sender, ConnectionEventArgs e);
    public delegate void DeviceRemovedDelegate(object sender, ConnectionEventArgs e);

    /// <summary>
    /// Manages IO communication channels
    /// </summary>
    public interface ICommunicationManager : IDisposable
    {
        #region Events
        /// <summary>
        /// Occurs when device connected.
        /// </summary>
        event DeviceConnectedDelegate OnDeviceConnected;

        /// <summary>
        /// Occurs when device removed.
        /// </summary>
        event DeviceRemovedDelegate OnDeviceRemoved;

        #endregion
        #region Methods
        /// <summary>
        /// Scans for devices.
        /// </summary>
        void ScanForDevices();

        /// <summary>
        /// Shutdowns this instance.
        /// </summary>
        void Shutdown();

        #endregion
    }
}
