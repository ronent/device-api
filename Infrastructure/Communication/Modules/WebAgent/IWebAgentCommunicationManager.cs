﻿using Infrastructure.Communication.Modules.HID;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Communication.Modules.WebAgent
{
    public delegate UserLoginResult UserLoginDelegate(string username, string password);

    public interface IWebAgentCommunicationManager : IHIDCommunicationManager
    {
        event UserLoginDelegate OnUserLogin;

        int GetCustomerId(string deviceSN);
    }

    public class UserLoginResult
    {
        public enum eResult
        {
            Ok,
            Error,
        }

        public eResult Result { get; private set; }
        public int CustomerId { get; private set; }

        public UserLoginResult(int customerId)
        {
            Result = eResult.Ok;
            CustomerId = customerId;
        }

        public UserLoginResult()
        {
            Result = eResult.Error;
        }
    }
}
