﻿using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Communication.Modules.HID
{
    public interface IHIDCommunicationManager : ICommunicationManager
    {
        #region Properties
        /// <summary>
        /// Gets the HID ids.
        /// </summary>
        /// <value>
        /// The ids.
        /// </value>
        List<DeviceID> Ids { get; }

        #endregion
        #region Methods
        /// <summary>
        /// Registers the HID device ids.
        /// </summary>
        /// <param name="deviceIDs">The device ids.</param>
        void RegisterDeviceIds(List<DeviceID> deviceIDs);

        /// <summary>
        /// Unregisters the HID device ids.
        /// </summary>
        void UnregisterDeviceIds();

        #endregion
    }
}
