﻿namespace Infrastructure.Communication.Modules
{
    public interface ICommunicationMetadata
    {
        string Name { get; }
    }
}
