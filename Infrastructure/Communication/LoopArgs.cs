﻿using System;
using System.Collections.Generic;

namespace Infrastructure.Communication
{
    /// <summary>
    /// Sends the OpCode to the logger as long as return:
    ///     OpCode == "ReturnOpCodeAcks" || "ReturnOpCodesToIgnore"
    ///     Number of times doesn't reach LimitCount
    ///     
    /// If no op code is returned, repeat "Retries" times within "RetryIntervalms"
    /// </summary>
    [Serializable]
    public class LoopArgs
    {
        public byte[] OpCode;
        /// <summary>
        /// Opcodes that are counted as valid result and are eventually sent to the server
        /// </summary>
        public IEnumerable<byte[]> ReturnOpCodesToProcess;

        /// <summary>
        /// Opcodes that don't stop the loop process, but don't count as valid result that are send to server
        /// </summary>
        public IEnumerable<byte[]> ReturnOpCodesToIgnore;

        /// <summary>
        /// Opcodes that don't stop the loop process, but immediate sent to server
        /// </summary>
        public IEnumerable<byte[]> ReturnOpCodesToImmediateSend;

        /// <summary>
        /// Opcodes that stops the loop process.
        /// </summary>
        public IEnumerable<byte[]> ReturnOpCodesToFinish; 

        public int OpCodeStartIndex;
        /// <summary>
        /// Number of expected packets, 
        /// If null, No limit is set
        /// </summary>
        public uint? ExpectedCount; 
        public int Retries;
        public int RetryIntervalms;
    }
}
