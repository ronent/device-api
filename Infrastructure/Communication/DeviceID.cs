﻿using System;
using System.Runtime.Serialization;

namespace Infrastructure.Communication
{
    [DataContract]
    [Serializable]
    public class DeviceID
    {
        [DataMember]
        public uint VID
        {
            get;
            private set;
        }

        [DataMember]
        public uint PID
        {
            get;
            private set;
        }

        public DeviceID(uint vid, uint pid)
        {
            VID = vid;
            PID = pid;
        }

        public override bool Equals(object obj)
        {
            try
            {
                return (obj as DeviceID).VID == VID
                    && (obj as DeviceID).PID == PID;
            }
            catch{return false;}
        }

        public override int GetHashCode()
        {
            return (Int16)VID + (Int16)PID;
        }

        public override string ToString()
        {
            return "[ VID: " + VID.ToString("X") + ", PID: " + PID.ToString("X") + " ]";
        }
    }
}
