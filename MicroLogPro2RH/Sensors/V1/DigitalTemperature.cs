﻿using Base.Sensors.Management;
using System;

namespace MicroLogPro2TH.Sensors.V1
{
    /// <summary>
    /// Digital Temperature sensor.
    /// </summary>
    [Serializable]
    public sealed class DigitalTemperature : MicroLogAPI.Sensors.V1.DigitalTemperature
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalTemperature"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DigitalTemperature(SensorManager parent)
            : base(parent)
        {
        }
        #endregion
    }
}
