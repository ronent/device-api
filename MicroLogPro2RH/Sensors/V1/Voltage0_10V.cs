﻿using Base.Sensors.Management;
using System;

namespace MicroLogPro2TH.Sensors.V1
{
    /// <summary>
    /// Voltage 0-10V sensor.
    /// </summary>
    [Serializable]
    public sealed class Voltage0_10V : MicroLogAPI.Sensors.V1.Voltage0_10V
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Voltage0_10V"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Voltage0_10V(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
