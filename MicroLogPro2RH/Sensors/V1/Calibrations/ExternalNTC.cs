﻿using System;
using System.Runtime.Serialization;

namespace MicroLogPro2TH.Sensors.V1.Calibrations
{
    /// <summary>
    /// External NTC calibration.
    /// </summary>
    [Serializable]
    sealed class ExternalNTC : Base.Sensors.Calibrations.GenericNTC, ISerializable
    {
        internal ExternalNTC()
        {

        }

        #region Fields
        public override decimal DefaultCoeffA { get { return 0.001163228932885m; } }

        public override decimal DefaultCoeffB { get { return 0.000228425655975m; } }

        public override decimal DefaultCoeffC { get { return 0.000000106047548m; } }

        #endregion
        #region Not Supported
        /// <summary>
        /// Calibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Calibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Decalibrates the specified value.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override decimal Decalibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Serialization & Deserialization
        protected ExternalNTC(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }

        #endregion
    }
}
