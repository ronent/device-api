﻿using Base.Sensors.Management;
using System;

namespace MicroLogPro2TH.Sensors.V1
{
    /// <summary>
    /// Current 4-20mA sensor.
    /// </summary>
    [Serializable]
    public sealed class Current4_20mA : MicroLogAPI.Sensors.V1.Current4_20mA
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Current4_20mA"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Current4_20mA(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
