﻿using Base.Sensors.Management;
using System;

namespace MicroLogPro2TH.Sensors.V1
{
    /// <summary>
    /// External NTC sensor.
    /// </summary>
    [Serializable]
    public sealed class ExternalNTC : MicroLogAPI.Sensors.V1.ExternalNTC
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="ExternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal ExternalNTC(SensorManager parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Initializes the calibration.
        /// </summary>
        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.ExternalNTC();
        }

        #endregion
    }
}
