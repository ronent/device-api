﻿using System;
using System.Runtime.Serialization;

namespace MicroLogPro2TH.Maintenance
{
    /// <summary>
    /// MicroLogPro II 16Bit Device firmware.
    /// </summary>
    [Serializable]
    internal class EC850B16Firmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return "EC8xx 16Bit"; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Firmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public EC850B16Firmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Firmware"/> class.
        /// </summary>
        public EC850B16Firmware()
        {

        }
    }
}
