﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class DataPacket : MicroLogAPI.OpCodes.V1.DataPacket
    {
        #region Constructors
        public DataPacket(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
