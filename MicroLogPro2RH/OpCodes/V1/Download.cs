﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroLogAPI.OpCodes.V1.Download
    {
        #region Constructors
        public Download(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
