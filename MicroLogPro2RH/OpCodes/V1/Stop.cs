﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class Stop : MicroLogAPI.OpCodes.V1.Stop
    {
        #region Constructors
        public Stop(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
