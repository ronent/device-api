﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroLogAPI.OpCodes.V1.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
