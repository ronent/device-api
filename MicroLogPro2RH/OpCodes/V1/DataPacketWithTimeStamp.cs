﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : MicroLogAPI.OpCodes.V1.DataPacketWithTimeStamp
    {
        #region Constructors
        public DataPacketWithTimeStamp(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
