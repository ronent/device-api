﻿using System;
using EC850_16Bit.Devices.V1;

namespace EC850_16Bit.OpCodes.V1
{
    [Serializable]
    public class FirmwareUploadDone : MicroLogAPI.OpCodes.V1.FirmwareUploadDone
    {
        #region Constructors
        public FirmwareUploadDone(EC850B16Device device)
            : base(device)
        {
        }
        #endregion
    }
}
