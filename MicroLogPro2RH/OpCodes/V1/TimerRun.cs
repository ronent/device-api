﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class TimerRun : MicroLogAPI.OpCodes.V1.TimerRun
    {
        #region Constructors
        public TimerRun(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
