﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : MicroLogAPI.OpCodes.V1.OnlineDataPacket
    {
        #region Constructors
        public OnlineDataPacket(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
