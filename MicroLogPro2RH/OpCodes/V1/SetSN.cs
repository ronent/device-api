﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroLogAPI.OpCodes.V1.SetSN
    {
        #region Constructors
        public SetSN(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
