﻿using MicroLogPro2TH.Devices;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroLogAPI.OpCodes.V1.Status
    {
        #region Constructors
        public Status(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
