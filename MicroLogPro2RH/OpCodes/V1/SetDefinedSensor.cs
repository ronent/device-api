﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class SetDefinedSensor : MicroLogAPI.OpCodes.V1.SetDefinedSensor
    {
        #region Constructors
        public SetDefinedSensor(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
