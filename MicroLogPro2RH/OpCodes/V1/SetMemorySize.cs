﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class SetMemorySize : MicroLogAPI.OpCodes.V1.SetMemorySize
    {
        #region Constructors
        public SetMemorySize(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
