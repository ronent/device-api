﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUpload : MicroLogAPI.OpCodes.V1.FirmwareUpload
    {
        #region Constructors
        public FirmwareUpload(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
