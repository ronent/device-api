﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroLogPro2TH.DataStructures;
using MicroLogPro2TH.Devices;
using MicroLogAPI.DataStructures;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroLogAPI.OpCodes.V1.Setup
    {
        #region Properties
        new public EC850B16SetupConfiguration Configuration
        {
            get { return base.Configuration as EC850B16SetupConfiguration; }
            set { base.Configuration = value; }
        }
        #endregion
        #region Configuration Properties
        protected override bool TemperatureEnabled { get { return Configuration.FixedSensors.TemperatureEnabled; } }
        protected override bool HumidityEnabled { get { return Configuration.FixedSensors.HumidityEnabled; } }
        protected override bool DewPointEnabled { get { return Configuration.FixedSensors.DewPointEnabled; } }
        protected override bool ExternalEnabled { get { return Configuration.ExternalSensor.Enabled; } }
        protected override Alarm TemperatureAlarm { get { return Configuration.FixedSensors.TemperatureAlarm; } }
        protected override Alarm HumidityAlarm { get { return Configuration.FixedSensors.HumidityAlarm; } }
        protected override Alarm DewPointAlarm { get { return Configuration.FixedSensors.DewPointAlarm; } }
        protected override Alarm ExternalAlarm { get { return Configuration.ExternalSensor.Alarm; } }
        protected override bool DeepSleepMode { get { return Configuration.DeepSleepMode; } }
        protected override eSensorType ExternalType { get { return Configuration.ExternalSensor.Type; } }
        protected override bool UserDefined { get { return Configuration.ExternalSensor.UserDefined; } }
        protected override MicroLogUDSConfiguration UserDefinedSensor { get { return Configuration.ExternalSensor.UserDefinedSensor; } }

        #endregion
        #region Constructors
        public Setup(EC850B16Logger device)
            : base(device)
        {
        }

        #endregion
    }
}