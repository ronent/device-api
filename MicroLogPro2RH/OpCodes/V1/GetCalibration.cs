﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : MicroLogAPI.OpCodes.V1.GetCalibration
    {
        #region Constructors
        public GetCalibration(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
