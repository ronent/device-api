﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroLogAPI.OpCodes.V1.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
