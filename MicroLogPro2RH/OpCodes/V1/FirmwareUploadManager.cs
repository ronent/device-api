﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroLogAPI.OpCodes.V1.FirmwareUploadManager
    {
        #region Constructors
        public FirmwareUploadManager(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
