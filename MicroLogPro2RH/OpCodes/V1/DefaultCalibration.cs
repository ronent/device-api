﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : MicroLogAPI.OpCodes.V1.DefaultCalibration
    {
        #region Constructors
        public DefaultCalibration(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
