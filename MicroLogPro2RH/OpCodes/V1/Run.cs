﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class Run : MicroLogAPI.OpCodes.V1.Run
    {
        #region Constructors
        public Run(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
