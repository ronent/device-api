﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroLogAPI.OpCodes.V1.SetCalibration
    {
        #region Constructors
        public SetCalibration(EC850B16Logger device)
            : base(device)
        { }
        #endregion
    }
}
