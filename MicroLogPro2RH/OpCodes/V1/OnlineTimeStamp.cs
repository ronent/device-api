﻿using MicroLogPro2TH.Devices.V1;
using System;

namespace MicroLogPro2TH.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : MicroLogAPI.OpCodes.V1.OnlineTimeStamp
    {
        #region Constructors
        public OnlineTimeStamp(EC850B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
