﻿using Base.Devices;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Sensors.Management;
using MicroXBase.Devices.Types;
using System;

namespace MicroLogPro2TH.DataStructures
{
    /// <summary>
    /// Setup configuration for MicroLogPro II 16 Bits Temperature & Humidity.
    /// </summary>
    [Serializable]
    public sealed class EC850B16SetupConfiguration : MicroLogSetupConfiguration, IESensorSetup, ITHDSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16SetupConfiguration"/> class.
        /// </summary>
        public EC850B16SetupConfiguration()
            : base()
        {
            ExternalSensor = new ESensorSetup();
            FixedSensors = new THDSensorSetup();
        }


        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the external sensor.
        /// </summary>
        /// <value>
        /// The external sensor.
        /// </value>
        public ESensorSetup ExternalSensor { get; set; }

        /// <summary>
        /// Gets the fixed (Temperature, Humidity, Dew Point ) sensors.
        /// </summary>
        /// <value>
        /// The fixed sensors.
        /// </value>
        public THDSensorSetup FixedSensors { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether device should enter deep sleep mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [deep sleep mode]; otherwise, <c>false</c>.
        /// </value>
        public bool DeepSleepMode { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Validates the configuration and throws exception for error.
        /// </summary>
        /// <param name="device">The device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void adjustDummyStatus(MicroXLogger dummy)
        {
            ESensorSetup.adjustExternalSensor(dummy, ExternalSensor);
            base.adjustDummyStatus(dummy);
        }

        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(FixedSensors.TemperatureEnabled || ExternalSensor.Enabled, "At least one sensor must be enabled");
            ExternalSensor.ThrowIfInvalidFor(device);
            FixedSensors.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}