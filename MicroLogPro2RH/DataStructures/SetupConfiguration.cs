﻿using Base.OpCodes;
using Base.Sensors.Management;
using Maintenance;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;

using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace EC850_16Bit.DataStructures
{
    [Serializable]
    public class EC850B16SetupConfiguration : SetupConfiguration, IESensorSetup, ITHDSensorSetup
    {
        public ESensorSetup ExternalSensor { get; set; }
        public THDSensorSetup FixedSensors { get; set; }
        public bool DeepSleepMode { get; set; }

        public EC850B16SetupConfiguration()
            :base()
        {
            ExternalSensor = new ESensorSetup();
            FixedSensors = new THDSensorSetup();
        }
    }
}