﻿using Base.Devices.Management;
using MicroLogAPI.Modules;
using MicroXBase.Devices.Types;
using MicroXBase.Modules;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using System;
using Base.Devices.Management.DeviceManager.HID;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLogPro2TH.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "EC850 16Bit")]
    internal class EC850B16Interfacer : GenericSubDeviceInterfacer<EC850B16Logger>
    {
        public const byte TYPE = 0x15;
        public const byte BITS = 0x02;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public EC850B16Interfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x10C4, 0x1F0C), new DeviceID(MicroXLoggerManager.VID, 0x0006) }; } }

        public override bool CanCreateFrom(GenericDevice device)
        {
            // PCBAssembly = for EC means '1' = 12 bit, '2' = 16 bit
            return device.Version.PCBAssembly == Bits
                && base.CanCreateFrom(device);
        }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.EC850B16Logger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
