﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Features;
using System;
using System.Linq;
using System.Runtime.Serialization;

namespace MicroLogPro2TH.Devices
{
    /// <summary>
    /// MicroLogPro II 16Bit Temperature & Humidity Device.
    /// </summary>
    [Serializable]
    public class EC850B16Logger : MicroLog16BitLogger
    {
        #region Members
        private static readonly string deviceTypeName = "EC850 Temperature & RH";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return EC850B16Interfacer.TYPE; } }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return EC850B16Interfacer.BITS; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[]
                {
                    MicroXFeature.DEEP_SLEEP,
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new eSensorType[]
                    {eSensorType.DigitalTemperature, 
                     eSensorType.Humidity,
                     eSensorType.DewPoint};
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[]
                    {eSensorType.Current4_20mA, 
                     eSensorType.Voltage0_10V,
                     eSensorType.ExternalNTC};
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Device"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal EC850B16Logger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Device"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected EC850B16Logger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
