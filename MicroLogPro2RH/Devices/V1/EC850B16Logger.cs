﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace MicroLogPro2TH.Devices.V1
{
    /// <summary>
    /// MicroLogPro II 16Bit Temperature & Humidity Device.
    /// </summary>
    [Serializable]
    public sealed class EC850B16Logger : MicroLogPro2TH.Devices.EC850B16Logger
    {
        #region Fields
        private static readonly Version MINIMUM_FW_VERSION = new Version(2, 0);

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal EC850B16Logger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Logger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected EC850B16Logger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}
