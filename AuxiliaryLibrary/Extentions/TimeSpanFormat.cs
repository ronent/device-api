﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxiliary.Extentions
{
    public static class TimeSpanFormat
    {
        public static string ToFormatedString(this TimeSpan timeSpan)
        {
            return string.Format(@"{0:hh\:mm\:ss\.ff}", timeSpan);
        }
    }
}
