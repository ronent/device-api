﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Auxiliary")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("fourtec")]
[assembly: AssemblyProduct("Auxiliary")]
[assembly: AssemblyCopyright("Copyright ©  2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d1b8b6f8-c7be-48cf-9a5e-308d40ad025e")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: InternalsVisibleTo("DataNetAPI, PublicKey=0024000004800000940000000602000000240000525341310004000001000100f50592fac98329" +
                                                    "8a1e4de45ae938da5f9b202c0038f673d21bba403f5dc30529b64a5ef091083d6e792f646b331d" +
                                                    "37854c902deac606a256027e28b093a8ddd6082342b4e81b01d1499006550c290b8931759d499b" +
                                                    "dbf0c3f9065c3a1319d94d31e3c4aecb441af475796410ec827875e11ee112c574864f2e2337aa" +
                                                    "14086db3")]
[assembly: InternalsVisibleTo("MicroLabAPI, PublicKey=002400000480000094000000060200000024000052534131000400000100010079af5327a3aa1a" +
                                                    "c80082bbf5bdd069c4bb09f9d74c55109534b2e1765168f088241f4cf17cd7c964fbfc3e9aa9b6" +
                                                    "c1c6a37ba8b154430a16178e357f7c79dce918685d9def3db621ce94443577e825223658f7034a" +
                                                    "2705511a148e6030d65b4670529008706a12572297b90ab71d924eb216a6d028eed4f49a537e88" +
                                                    "297fe68c")]
[assembly: InternalsVisibleTo("DataSuite, PublicKey=00240000048000009400000006020000002400005253413100040000010001001d1348ff84f1d7" +
                                                    "88edb5593526045b18bc2de7b83d379b40f4139640f83f5ec9d9d49d23e033e49a35bb8c94b656" +
                                                    "833bd69734e8a04f839384086c302b8a2fff1e250320980c986c2ce45b7f5415fd6e998437380b" +
                                                    "ff0e92773a3475a0ca5150140ea77993cd3fa120d0ef674818107991a7f5b57b4c4b3dd2c6c272" +
                                                    "eb488cb3")]

[assembly: InternalsVisibleTo("DataNet, PublicKey=002400000480000094000000060200000024000052534131000400000100010009086000533cd2" +
                                                    "81ba3e15a0748af688aa1a71696d45e3df1a9e0d921cea8e5e86a32351f1cb457cd754f670f539" +
                                                    "708a0247b9f2f5ac3db3fa62a4390ab9c63ceb5a6dce6eb0305c0997fe5be8524365b2e23756a2" +
                                                    "61a540e5328bec14e9c6aaa6fde9f3969a3a012849f89ac7e88f446cdbc3749852cad20d352dc7" +
                                                    "3684f2af")]

[assembly: InternalsVisibleTo("Base, PublicKey=002400000480000094000000060200000024000052534131000400000100010073cd380f739a55" +
                                                    "9f38f6b62c0437f4771effbaec493184242a3256510dcc8d2aa40c3ab91d99973eb883e98d8407" +
                                                    "71bb8de360633ef97ec286775a0e4d4e3b14db859ac23009b12674dc6019575b37cbbd0e73542a" +
                                                    "03cdb91e8aa65533b5a6abf23c516a6322f76b2473e7d26635e10438487ced15d0072bc86d447c" +
                                                    "63f4c8bf")]

[assembly: InternalsVisibleTo("Validata, PublicKey=00240000048000009400000006020000002400005253413100040000010001005d8339741d0ebb" +
                                                    "459157c2339b2040548f174b2fee43171e6d5c83c1fb69005ec58a3bd1fa62d18dfa8a9860c48e" +
                                                    "4c25a7a8504819bba9540094febf6a6466f3ddeedc66bdb50dbfa355e114dd54be2805e775af2e" +
                                                    "a20ca8916580d8e830487a2fd7ab6b28e7c794a1400ac34bf229e776dd3b2454763aceb4f9c82a" +
                                                    "104db39e")]
[assembly: InternalsVisibleTo("UptodataHeader, PublicKey=0024000004800000940000000602000000240000525341310004000001000100b5bde51b1c64ba" +
                                                    "10afa8d61c2faf045ced3dd7a0d8f6c4a8c9d299fbf7c272fea5e1092fc627de475b61bfdd93e4" +
                                                    "451d8f70a50a1d4e961840ce55cd373e1fd8e0a4c1bce0ef814c55809f4d806273b7ab7bbdc95e" +
                                                    "6643267f71a73b3db3103206949dc8b5fc0230d416394f960a9bcbd3f5c6d01ee52f3eb0003c59" +
                                                    "3f0a05a5")]
[assembly: InternalsVisibleTo("CmdUtility, PublicKey=0024000004800000940000000602000000240000525341310004000001000100ff4a9626ccbd58" +
                                                    "df36a4cb30492ed4711ea2905520c3589c3bd2c38763323290a10ec685b9fc6d12f8d9888cb254" +
                                                    "247ee7cef51eb908a4c7d70bf8a0b540384387b6a8be57873951516e479b72e92b0a0833cbc999" +
                                                    "8c135e52aeb065d85bcd2342953d1eaa152f07ce70208eeb913e83cec2ecd803bd1485fbf86410" +
                                                    "64bd3cd2")]
[assembly: InternalsVisibleTo("DatPass, PublicKey=002400000480000094000000060200000024000052534131000400000100010059a8813405e5cc" +
                                                    "5e1d3cf120010d376be7f1608c9ad792f17cd4bdc45a9423f57ec2b1f4dd6f64833a4aea7737a6" +
                                                    "1359173cfc3959d08d25e761b7f4dd2a7293452e44ae44e572798d99e74bb225fc27ad3af1ae2b" +
                                                    "03c5fa896bb71075bd56e8be5f74929fe66209b768e121e510b01ded7e86ff9a32fba44b29a0f8" +
                                                    "488b7af4")]
[assembly: InternalsVisibleTo("CFR, PublicKey=00240000048000009400000006020000002400005253413100040000010001002f17310f357018" +
                                                    "013536ae444431e2e4bc6f039330ebe518876986f76020ef386346c79c2f5ea104012331ac492f" +
                                                    "8143b1f71cac2d779dba5d73756812d0ef41174d0f1b67702d964fe933d32976ae623fdc9da36d" +
                                                    "fcdec39c39cb97635b16e3cbdce403ef2732b5b270cda59d1f47a2fb20fd2758948dbe2e5a0cb7" +
                                                    "3fa5fbce")]