﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace Auxiliary.Tools
{
    public class IO : IDisposable
    {
        #region Fields
        private readonly static byte[] IV = Convert.FromBase64String("U125M2zSfPuMT5qs1sTYYA==");
        private readonly static byte[] KEY = Convert.FromBase64String("syuOBuP8NqCqzvSZ10YZxMLwf5ZpgyF2prv2VgKdyck=");
        private object ReadWriteShare = new object();

        private static SymmetricAlgorithm key = SymmetricAlgorithm.Create();
        private FileStream WritingStream = null, ReadingStream = null;
        private string Name;
        private bool Lock = false;
        #endregion
        #region Properties
        public string FileName
        {
            get 
            {
                return Name;
            }
            set
            {
                lock(ReadWriteShare)
                if (value != FileName)
                {
                    try
                    {
                        if (WritingStream != null)
                        {
                            WritingStream.Flush();
                            WritingStream.Dispose();
                        }
                    }
                    catch (Exception) { }

                    CreateDirectoryStructure(value);
                    if(Lock)
                        ReopenForReading(value);

                    Name = value;
                }
            }
        }

        public bool Valid
        {
            get { return FileName != string.Empty; }
        }

        #endregion
        #region Constructors
        static IO()
        {
            key.IV = IV;
            key.Key = KEY;
        }
        public IO(string FileName)
            : this(FileName, true)
        {}

        public IO(string FileName, bool Lock)
        {
            this.FileName = FileName;
            this.Lock = Lock;
        }

        public IO()
        {
        }
        #endregion
        #region Methods
        public void EncryptAndSerialize<T>(T obj)// where T : ISerializable
        {
            CryptoStream cs = null;
            lock (ReadWriteShare)
                try
                {
                    if (ReadingStream != null)
                        ReadingStream.Close();

                    //An underscore is added to the name so that old files will not be overriden till the writing process is not finished.
                    using (WritingStream = File.Open(FileName + "_", FileMode.Create, FileAccess.Write, FileShare.Read))
                        using (cs = new CryptoStream(WritingStream, key.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            BinaryFormatter binFormat = new BinaryFormatter();
                            binFormat.Serialize(cs, obj);
                        }
                    FileInfo f = new FileInfo(FileName + "_");
                    f.CopyTo(FileName, true);
                    f.Delete();
                }
                catch (System.Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (Lock && WritingStream != null)
                    {
                        ReopenForReading(FileName);
                    }
                }
        }

        public T DecryptAndDeserialize<T>()
        {
            if (!Valid)
                throw new Exception("IO object not initialized");

            CryptoStream cs = null;
            lock (ReadWriteShare)
                try
                {
                    ReopenForReading(FileName);
                    using ( cs = new CryptoStream(ReadingStream, key.CreateDecryptor(), CryptoStreamMode.Read))
                    {
                        BinaryFormatter binFormat = new BinaryFormatter();
                        return (T)binFormat.Deserialize(cs);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (Lock && ReadingStream != null)
                        ReopenForReading(FileName);
                }
        }

        private static void CreateDirectoryStructure(string FileName)
        {
            Directory.CreateDirectory(Path.GetDirectoryName(FileName));
        }

        private void ReopenForReading(string FileName)
        {
            try
            {
                Dispose();
                ReadingStream = File.Open(FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                //ReadingStream.Lock(0, ReadingStream.Length);
            }
            catch
            {
                ReadingStream = null;
            }
        }

        public void Dispose()
        {
            try
            {
                if (ReadingStream != null)
                {
                    ReadingStream.Close();
                    ReadingStream.Dispose();
                }

                if (WritingStream != null)
                {
                    WritingStream.Dispose();                    
                }
            }
            catch (Exception) { }
        }
        #endregion
        #region Get Directory/File Size
        public static double GetFileSize(string FilePath)
        {
            try
            {
                FileInfo file = new FileInfo(FilePath);
                return file.Length;
            }
            catch (System.Exception)
            {
                return 0;
            }
        }
        public static double GetDirectorySize(string directory)
        {
            double size = 0;

            foreach (string dir in Directory.GetDirectories(directory))
            {
                size += GetDirectorySize(dir);
            }

            foreach (FileInfo file in new DirectoryInfo(directory).GetFiles())
            {
                size += file.Length;
            }

            return size;
        }
        #endregion
        #region Copy whole directory +subdirs
        public static void DirectoryDeepCopy(string Source, string Destination, bool Overwrite)
        {
            //Now Create all of the directories 
            foreach (string dirPath in Directory.GetDirectories(Source, "*", SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(Source, Destination));
            //Copy all the files 
            foreach (string newPath in Directory.GetFiles(Source, "*.*", SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(Source, Destination), Overwrite);
        }
        #endregion
        #region Removable USB Drive Methods
        const int NA = -1;
        public static DriveInfo[] GetUSBDrives()
        {
            return GetUSBDrives(NA);
        }
        public static DriveInfo[] GetUSBDrives(double requiredSpace)
        {
            DriveInfo[] AllDrives = DriveInfo.GetDrives();
            List<DriveInfo> USBDrives = new List<DriveInfo>();
            foreach (DriveInfo drive in AllDrives)
            {
                if (drive.IsReady
                    && drive.DriveType == DriveType.Removable
                    && (requiredSpace == NA || drive.AvailableFreeSpace > requiredSpace))
                    USBDrives.Add(drive);
            }
            return USBDrives.ToArray();
        }
        #endregion
        #region Deserialization Helper
        public static T GetSerializationValue<T>(ref SerializationInfo info, string Name)
        {
            try
            {
                return (T)info.GetValue(Name, typeof(T));
            }
            catch (System.Exception)
            {
                return default(T);
            }
        }
        #endregion

    }
}
