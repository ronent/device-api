﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Diagnostics;

namespace Auxiliary.Tools
{
    public enum Endianness { BIG, LITTLE };
    public static class Utilities
    {
        #region Get AllUsers Path
        [DllImport("shell32.dll")]
        static extern bool SHGetSpecialFolderPath(IntPtr hwndOwner, [Out] StringBuilder lpszPath, int nFolder, bool fCreate);

        public static string AllUsersPath
        {
            get
            {
                string Path = (System.Environment.OSVersion.Version.Major >= 6 ? "public" : "allusersprofile");
                return System.Environment.GetEnvironmentVariable(Path);
            }
        }
        public static string UserPath
        {
            get
            {
                return System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
        }
        #endregion
        #region Extract numbers from string
        public static string ExtractNumbers(string expr)
        {
            return string.Join(null, System.Text.RegularExpressions.Regex.Split(expr, "[^\\d]"));
        }
        #endregion
        #region Type check
        public static bool OfType(Type sourceType, Type destinationType)
        {
            if (sourceType == destinationType)
                return true;
            else if (sourceType == typeof(object))
                return false;
            else
                return OfType(sourceType.BaseType, destinationType);
        }
        #endregion
        #region Random String Generation
        public enum StringTypeEnum { IOValid, Any };
        public static string GenerateString(int Length, StringTypeEnum StringType)
        {
            Random r = new Random();
            string result =string.Empty;
            for (int i = 0; i < Length; i++)
                result += Convert.ToChar(r.Next(256));
            switch (StringType)
            {
                case StringTypeEnum.IOValid:
                    return Convert.ToBase64String((from char c in result
                                                   select Convert.ToByte(c)).ToArray()).Replace('/', '~').Replace("=", "");
                default:
                    return result;
            }
        }
        #endregion
        #region Set Control Values
        public static void InitializeDateTimePicker(ref DateTimePicker ReceptionAlarmDelayDateTimePicker, short Value)
        {
            DateTime CurrentValue = new DateTime(2007, 01, 01, Value / 60, Value % 60, 0);
            ReceptionAlarmDelayDateTimePicker.Value = CurrentValue;
        }

        #endregion
        #region Linq Methods
        public static IEnumerable<double> CumulativeSum(this IEnumerable<double> sequence)
        {
            double sum = 0;
            foreach (var item in sequence)
            {
                sum += item;
                yield return sum;
            }
        }
        #endregion
        #region Arrays (copy/initialize/etc.)
        public static byte[] InitializeArray(byte[] arr)
        {
            return Enumerable.Repeat<byte>(0, arr.Length).ToArray();
        }

        public static T[] CloneArray<T>(this T[] data)
        {
            return data.SubArray(0, data.Length);
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            if (data.Length == length)
                return data;

            T[] result = new T[length];
            short size = Convert.ToInt16(Math.Min(length, data.Length));
            Array.Copy(data, index, result, 0, size);
            return result;
        }

        #endregion
        #region Endianness
        public static byte[] GetBytes(byte[] source, int startIndex, int length, Endianness endianness = Endianness.LITTLE)
        {
            bool littleEndian = endianness == Endianness.LITTLE;
            byte[] data = source.SubArray(startIndex, length);
            if (littleEndian ^ BitConverter.IsLittleEndian)
                Array.Reverse(data);
                
            return data;
        }

        public static byte[] AdjustToEndianness(this byte[] data, Endianness endianness)
        {
            return GetBytes(data, 0, data.Length, endianness);
        }

        #endregion
        #region Validation
        public static bool IsEmailValid(this string emailaddress)
        {
            try
            {
                MailAddress m = new MailAddress(emailaddress);
                // TODO - add regex validation
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsIPAddress(this string ip)
        {
            IPAddress address;
            return IPAddress.TryParse(ip, out address);
        }

        public static bool IsComPort(this string uid)
        {
            return uid.Contains("COM");
        }

        #endregion
        #region APM for Tasks
        public static Task<TResult> ToApm<TResult>(this Task<TResult> task, AsyncCallback callback, object state)
        {
            if (state != null && task.AsyncState == state)
            {
                if (callback != null)
                {
                    task.ContinueWith(delegate { callback(task); },
                        CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Default);
                }
                return task;
            }

            var tcs = new TaskCompletionSource<TResult>(state);
            task.ContinueWith(delegate
            {
                if (task.IsFaulted) tcs.TrySetException(task.Exception.InnerExceptions);
                else if (task.IsCanceled) tcs.TrySetCanceled();
                else tcs.TrySetResult(task.Result);

                if (callback != null) callback(tcs.Task);

            }, CancellationToken.None, TaskContinuationOptions.None, TaskScheduler.Default);
            return tcs.Task;
        }
        #endregion
        #region Event invocation
        public delegate void ErrorHandlerDelegate(Exception ex);
        [DebuggerStepThrough]
        public static object[] InvokeEvent(Delegate eventDelegate, object sender, EventArgs e, ErrorHandlerDelegate errorHandlingMethod = null)
        {
            var list = new List<object>();
            if (eventDelegate == null) return list.ToArray();
            foreach (var handler in eventDelegate.GetInvocationList())
            {
                Exception ex;
                var result = TryCatchArgs(handler, errorHandlingMethod, out ex, sender, e);

                if (result != null)
                    list.Add(result);
            }

            return list.ToArray();
        }

        [DebuggerStepThrough]        
        public static object[] InvokeEvent(Delegate eventDelegate, object sender, Object parameter, ErrorHandlerDelegate errorHandlingMethod = null)
        {
            List<object> list = new List<object>();
            if (eventDelegate != null)
                foreach (var handler in eventDelegate.GetInvocationList())
                {
                    Exception ex;
                    object result = TryCatchArgs(handler, errorHandlingMethod, out ex, sender, parameter);

                    if (result != null)
                        list.Add(result);
                }

            return list.ToArray();
        }

        public static object TryCatch(Delegate method, ErrorHandlerDelegate errorHandlingMethod)
        {
            Exception exception;
            return TryCatchArgs(method, errorHandlingMethod, out exception);
        }

        public static object TryCatch(Action method, ErrorHandlerDelegate errorHandlingMethod)
        {
            Exception exception;
            return TryCatchArgs(method, errorHandlingMethod, out exception);
        }

        public static object TryCatch(Delegate method, ErrorHandlerDelegate errorHandlingMethod, out Exception exception)
        {
            return TryCatchArgs(method, errorHandlingMethod, out exception);
        }

        public static object TryCatch(Action method, ErrorHandlerDelegate errorHandlingMethod, out Exception exception)
        {
            return TryCatchArgs(method, errorHandlingMethod, out exception);
        }

        public static object TryCatchArgs(Delegate method, ErrorHandlerDelegate errorHandlingMethod, out Exception exception, params object[] args)
        {
            exception = null;
            try { return method.DynamicInvoke(args); }
            catch (Exception ex)
            {
                if (errorHandlingMethod != null)
                    errorHandlingMethod(ex);
                exception = ex;
                return null;
            }
        }

        #endregion
        #region Icon to ImageSource
        [DllImport("gdi32.dll", SetLastError = true)]
        private static extern bool DeleteObject(IntPtr hObject);

        public static ImageSource ToImageSource(this Icon icon)
        {
            Bitmap bitmap = icon.ToBitmap();
            IntPtr hBitmap = bitmap.GetHbitmap();

            ImageSource wpfBitmap = Imaging.CreateBitmapSourceFromHBitmap(
                hBitmap,
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions());

            if (!DeleteObject(hBitmap))
            {
                throw new Win32Exception();
            }

            return wpfBitmap;
        }
        #endregion
        #region Get bounded value
        public static T GetBoundedValue<T>(T lower, T value, T upper) where T : IComparable
        {
            if (value.CompareTo(lower) < 0)
                return lower;
            else if (value.CompareTo(upper) > 0)
                return upper;
            else return value;
        }
        #endregion
    }
}
