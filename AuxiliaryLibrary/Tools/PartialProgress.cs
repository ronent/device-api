﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Auxiliary.Tools
{
    public class PartialProgress
    {
        public int Start { get; set; }
        public int End { get; set; }
        public float Proportion { get { return (End - Start) / 100f; } }

        public PartialProgress(int start, int end)
        {
            this.Start = start;
            this.End = end;
        }

        public int GetPartialPercentFrom(int percent)
        {
            return Math.Min(Start + Convert.ToInt16(Proportion * percent), End);
        }
    }
}
