﻿using System;
using System.Windows.Forms;

namespace Auxiliary.Tools
{
    public static class Validation
    {
        #region Validation Methods
        public static bool ValidateKey(byte AsciiChar)
        {
            return (((AsciiChar >= 32) && (AsciiChar <= 127) && (AsciiChar != 34) && (AsciiChar != 38) && (AsciiChar != 39) && (AsciiChar != 92) && (AsciiChar != 126)) ||
                    (AsciiChar == (int)Keys.Escape) || (AsciiChar == (int)Keys.Enter) || (AsciiChar == 3) || (AsciiChar == 8) || (AsciiChar == 22) || (AsciiChar == 24) || (AsciiChar == 0xB0));
        }

        public static bool ValidateString(String Str)
        {
            int StrIndex = 0;
            while (StrIndex < Str.Length)
            {
                if (!ValidateKey((byte)(Str[StrIndex])))
                {
                    return false;
                }
                StrIndex++;
            }
            return true;
        }
        #endregion
    }
}
