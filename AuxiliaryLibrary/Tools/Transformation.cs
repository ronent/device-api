﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Auxiliary.Tools
{
    public static class Transformation
    {
        #region Parsing Methods
        public static T Parse<T>(string StringToConvert) where T : IConvertible
        {
            try
            {
                T TValue = default(T);
                MethodInfo Method = typeof(T).GetMethod("Parse", new Type[] { typeof(string) });
                if (Method != null)
                {
                    CultureInfo CurrentCulture = System.Threading.Thread.CurrentThread.CurrentCulture;
                    System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                    TValue = (T)Method.Invoke(null, new object[] { StringToConvert });
                    System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCulture;
                }
                return TValue;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Cloning Methods
        public static T Clone<T>(this T source) where T : ISerializable
        {
            try
            {
                // Don't serialize a null object, simply return the default for that object
                if (Object.ReferenceEquals(source, null))
                { return default(T); }
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new MemoryStream();
                using (stream)
                {
                    formatter.Serialize(stream, source);
                    stream.Seek(0, SeekOrigin.Begin);
                    T t = (T)formatter.Deserialize(stream);
                    return t;
                }
            }
            catch (System.Exception ex)
            {
                Console.Write(ex);
                return default(T);
            }
        }

        public static TTarget Merge<TTarget>(this object copyFrom) where TTarget : new()
        {
            var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var FieldsDic = typeof(TTarget).GetFields(flags).ToDictionary(f => f.Name);
            var PropertiesDic = typeof(TTarget).GetProperties(flags).ToDictionary(f => f.Name);
            var ret = new TTarget();
            var Fields = copyFrom.GetType().GetFields(flags);
            var Properties = copyFrom.GetType().GetProperties(flags);

            foreach (var f in Fields)
            {
                if (FieldsDic.ContainsKey(f.Name))
                    FieldsDic[f.Name].SetValue(ret, f.GetValue(copyFrom));
                //                else
                //throw new InvalidOperationException(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName)); 
                //                    Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
            }

            //This works only for non-indexed properties
            foreach (var f in Properties)
            {
                if (PropertiesDic.ContainsKey(f.Name) && PropertiesDic[f.Name].CanWrite)
                    PropertiesDic[f.Name].SetValue(ret, f.GetValue(copyFrom, null), null);
                //                else
                //throw new InvalidOperationException(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName)); 
                //                    Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
            }
            return ret;
        }
        public static void Merge<TTarget>(this object copyFrom, TTarget copyTo)
        {
            var flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            var FieldsDic = typeof(TTarget).GetFields(flags).ToDictionary(f => f.Name);
            var PropertiesDic = typeof(TTarget).GetProperties(flags).ToDictionary(f => f.Name);
            var Fields = copyFrom.GetType().GetFields(flags);
            var Properties = copyFrom.GetType().GetProperties(flags);

            foreach (var f in Fields)
            {
                if (FieldsDic.ContainsKey(f.Name))
                    try
                    {
                        FieldsDic[f.Name].SetValue(copyTo, f.GetValue(copyFrom));
                    }
                    catch
                    {
                        Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
                    }
                //                else
                //throw new InvalidOperationException(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName)); 
                //                    Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
            }

            //This works only for non-indexed properties
            foreach (var f in Properties)
            {
                if (PropertiesDic.ContainsKey(f.Name) && PropertiesDic[f.Name].CanWrite)
                    try
                    {
                        PropertiesDic[f.Name].SetValue(copyTo, f.GetValue(copyFrom, null), null);
                    }
                    catch
                    {
                        Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
                    }
                //                else
                //throw new InvalidOperationException(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName)); 
                //                    Console.WriteLine(string.Format("The field “{0}” has no corresponding field in the type “{1}”.", f.Name, typeof(TTarget).FullName));
            }
        }
        #endregion
        #region Hex <-> Byte Array
        public static byte[] HexStringToByteArray(String hex)
        {
            try
            {
                int NumberChars = hex.Length;
                byte[] bytes = new byte[NumberChars / 2];
                for (int i = 0; i < NumberChars; i += 2)
                    bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
                return bytes;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static string ByteArrayToHexString(byte[] ba)
        {
            try
            {
                string hex = BitConverter.ToString(ba);
                return hex.Replace("-", "");
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        #region BCD <-> Int/Byte
        public static byte IntToBCD(int integer)
        {
            if (integer < 0 || integer > 256) throw new ArgumentException();
            int bcd = 0;
            for (int digit = 0; digit < 4; ++digit)
            {
                int nibble = integer % 10;
                bcd |= nibble << (digit * 4);
                integer /= 10;
            }
            return (byte)(bcd & 0xff);
        }
        public static byte[] Int32ToBCD(Int32 integer)
        {
            int bcd = 0;

            for (int digit = 0; digit < 4; ++digit)
            {
                int nibble = integer % 10;
                bcd |= nibble << (digit * 4);
                integer /= 10;
            }

            byte[] Value = new byte[sizeof(Int32)];
            for (int i = 0; i < sizeof(Int32); i++)
                Value[i] = (byte)((bcd >> 8 * i) & 0xff);
            return Value.ToArray();
        }

        public static Int32 BCD2Int(byte[] bcdNumber)
        {
            bcdNumber = bcdNumber.Reverse().ToArray();
            Int32 result = 0;
            foreach (byte b in bcdNumber)
            {
                int digit1 = b >> 4;
                int digit2 = b & 0x0f;
                result = (result * 100) + digit1 * 10 + digit2;
            }
 
            return result;
        }

        public static byte BCD2Byte(byte b)
        {
            return (byte)BCD2Int(new byte[] {b});
        }

        /// <summary> 
        /// Convert two PLC words in BCD format (forming 8 digit number) into single binary integer. 
        /// e.g. If Lower = 0x5678 and Upper = 0x1234, then Return is 12345678 decimal, or 0xbc614e. 
        /// </summary> 
        /// <param name="lower">Least significant 16 bits.</param> 
        /// <param name="upper">Most significant 16 bits.</param> 
        /// <returns>32 bit unsigned integer.</returns> 
        /// <remarks>If the parameters supplied are invalid, returns zero.</remarks> 
        private static UInt32 BCD2ToUInt32(uint lower, uint upper)
        {
            uint binVal = 0;

            if ((lower | upper) != 0)
            {
                int shift = 0;
                uint multiplier = 1;
                uint bcdVal = (upper << 16) | lower;

                for (int i = 0; i < 8; i++)
                {
                    uint digit = (bcdVal >> shift) & 0xf;

                    if (digit > 9)
                    {
                        binVal = 0;
                        break;
                    }
                    else
                    {
                        binVal += digit * multiplier;
                        shift += 4;
                        multiplier *= 10;
                    }
                }
            }

            return binVal;
        }
        #endregion
        #region Version <-> HashCode
        const int REVISION_MAX = 128;
        const int BUILD_MAX = 128;
        const int MINOR_MAX = 32;
        const int MAJOR_MAX = 32;

        const int REVISION_SHIFT = 0;
        const int BUILD_SHIFT = 7;
        const int MINOR_SHIFT = BUILD_SHIFT + 7;
        const int MAJOR_SHIFT = MINOR_SHIFT + 5;

        public static string GetVersionHashCode(Version version)
        {
            return GetVersionHashCode(version.ToString());
        }
        public static string GetVersionHashCode(string version)
        {
            string[] parts = version.Split('.');
            UInt16[] versions = (from part in parts
                                 select UInt16.Parse(part)).ToArray();
            return Convert.ToUInt16((((versions[0] * 101 + versions[1]) * 103 + versions[2]) * 107 + versions[3]) % UInt16.MaxValue).ToString("X");
        }

        public static long GetSoftwareVersionCode(string strVersion)
        {
            string NA;
            return GetSoftwareVersionCode(strVersion, out NA);
        }
        public static long GetSoftwareVersionCode(string strVersion, out string codedVersion)
        {
            string[] parts = strVersion.Split('.');
            UInt16[] versions = (from part in parts
                                 select UInt16.Parse(part)).ToArray();
            int major = versions[0];
            int minor = versions[1];
            int build = versions[2];
            int rev = versions[3];

            Debug.Assert(major < MAJOR_MAX);
            Debug.Assert(minor < MINOR_MAX);
            Debug.Assert(build < BUILD_MAX);
            Debug.Assert(rev < REVISION_MAX);

            long version = (major << MAJOR_SHIFT) + (minor << MINOR_SHIFT) + (build << BUILD_SHIFT) + (rev << REVISION_SHIFT);
            codedVersion = version.ToString("X");

            return version;
        }
        public static string GetDataSuiteVersion(long version)
        {
            int major = Convert.ToInt16(version >> MAJOR_SHIFT);
            int minor = Convert.ToInt16((version >> MINOR_SHIFT) & 0x1F);
            int build = Convert.ToInt16((version >> BUILD_SHIFT) & 0x7F);
            int rev = Convert.ToInt16((version >> REVISION_SHIFT) & 0x7F);

            return new Version(major, minor, build, rev).ToString();
        }
        #endregion
        #region Single -> ByteArray
        #endregion
        #region Modify by small degree
        public static decimal ModifyBySmallDegree(decimal value)
        {
            Random r = new Random();
            double factor = r.NextDouble();
            return value * ((decimal)factor / 20m + 1);
        }
        #endregion
        #region Celsius/Fahrenheit
        public static decimal ConvertToFahrenheit(decimal celsiusTemperature)
        {
            return celsiusTemperature * 9 / 5 + 32;
        }

        public static decimal ConvertToCelsius(decimal fahrenheitValue)
        {
            return (fahrenheitValue - 32)* 5 / 9;
        }
        #endregion
    }
}
