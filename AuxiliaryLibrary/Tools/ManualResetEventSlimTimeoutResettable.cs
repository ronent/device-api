﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace Auxiliary.Tools
{
    public class ManualResetEventSlimTimeoutResettable : IDisposable
    {
        #region Fields
        private volatile ManualResetEventSlim mre;
        private volatile CancellationTokenSource cancel;
        private volatile bool resetTimeout = true;

        private object locker = new object();

        #endregion
        #region Constructor
        public ManualResetEventSlimTimeoutResettable(TimeSpan timeout)
        {
            IsDisposed = false;
            Timeout = timeout;
        }

        #endregion
        #region Properties
        public bool IsDisposed { get; private set; }
        public TimeSpan Timeout { get; private set; }

        public bool IsSet
        {
            get
            {
                if (mre == null)
                    return false;

                return mre.IsSet;
            }
        }

        #endregion
        #region Methods
        public bool Wait()
        {
            bool toReturn = false;

            while (resetTimeout)
            {
                resetTimeout = false;

                if (cancel != null)
                    cancel.Dispose();

                cancel = new CancellationTokenSource();

                if (mre != null)
                    mre.Dispose();

                mre = new ManualResetEventSlim(false);

                try
                {
                    toReturn = mre.Wait(Timeout, cancel.Token);
                }
                catch (OperationCanceledException) { }
            }

            return toReturn || mre.IsSet;
        }

        public void Set()
        {
            try
            {
                Monitor.Enter(locker);
                //Log4Tech.Log.Instance.Write("Setting MRE: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, this);

                if (mre != null && !mre.IsSet)
                {
                    resetTimeout = false;
                    mre.Set();
                }
            }
            catch { }
            finally
            {
                Monitor.Exit(locker);
            }
        }

        public void ResetTimeout()
        {
            try
            {
                Monitor.Enter(locker);
                //Log4Tech.Log.Instance.Write("Resetting MRE: {0}", this, Log4Tech.Log.LogSeverity.DEBUG, this);

                if (mre != null && !mre.IsSet)
                {
                    resetTimeout = true;
                    cancel.Cancel();
                }
            }
            catch { }
            finally
            {
                Monitor.Exit(locker);
            }
        }

        #endregion
        #region Object Overrides
        public override string ToString()
        {
            int inner = mre != null ? mre.GetHashCode() : 0;
            return string.Format("MRESTR: {0}, Inner: {1}", this.GetHashCode(), inner);
        }

        #endregion
        #region IDisposable
        public void Dispose()
        {
            if (!IsDisposed)
            {
                IsDisposed = true;
                mre.Dispose();
            }
        }

        #endregion
    }

}
