﻿using System;
using System.Collections.Generic;

namespace Auxiliary.MathLib
{
    public class Vector : Matrix
    {
        public Vector(double[] data)
            : base(Create2ArrayMatrix(data))
        {

        }

        public Vector(float[] data)
            : this(System.Array.ConvertAll(data, new Converter<float, double>(x => Convert.ToDouble(x))))
        {

        }

        private static double[][] Create2ArrayMatrix(double[] data)
        {
            List<double[]> matrix = new List<double[]>();
            foreach (var d in data)
            {
                matrix.Add(new double[] { d });
            }

            return matrix.ToArray();
        }
    }
}
