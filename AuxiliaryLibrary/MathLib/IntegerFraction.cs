﻿using System;
using System.Globalization;

namespace Auxiliary.MathLib
{
    [Serializable]
    public class IntegerFraction
    {
        public Int32 Integer { get; private set; }
        public UInt32 Fraction { get; private set; }

        public decimal Value { get; private set; }

        #region Properties

        #endregion
        #region Constructors
        public IntegerFraction(Int32 integer, UInt32 fraction)
        {
            Integer = integer;
            Fraction = fraction;

            double pow = 0;
            if (fraction.ToString().Length == 1)
                pow++;

            decimal frac = (Fraction / Convert.ToDecimal(Math.Pow(10, Fraction.ToString().Length + pow)));

            if (Integer >= 0)
                Value = Integer + frac;
            else
                Value = Integer - frac;
        }

        public IntegerFraction(decimal value)
        {
            Value = value;
            string[] splitParts = value.ToString().Split('.');

            Int32 integer;
            Int32.TryParse(splitParts[0], out integer);
            Integer = integer;

            if (splitParts.Length > 1)
            {
                uint fraction;
                UInt32.TryParse(splitParts[1], out fraction);

                if (splitParts[1].Length == 1)
                    fraction = fraction * 10;
                
               Fraction = fraction;
            }
        }

        #endregion
        #region Methods
        public byte FractionAsByte
        {
            get
            {
                byte b;
                byte.TryParse(Fraction.ToString(), out b);

                return b;
            }
        }

        #endregion
        #region Static Conversion Methods
        public static decimal ConvertFromFixedPoint(long value)
        {
            return Convert.ToDecimal(value) / 256;
        }

        public static bool IsReasonablyCloseTo(decimal first, decimal other)
        {
            decimal Epsilon = Math.Max(1, 3 / 100 * first);
            return Math.Abs(first - other) < Epsilon;
        }

        #endregion
        #region Override Methods
        public override string ToString()
        {
            return Value.ToString("00.00");
        }

        #endregion
    }
}
