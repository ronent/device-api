﻿using System;

namespace Auxiliary.MathLib
{
    public class MathEx
    {
        public static decimal Pow(decimal b, decimal p)
        {
            return Convert.ToDecimal(System.Math.Pow(Convert.ToDouble(b), Convert.ToDouble(p)));
        }

        public static decimal Sqrt(decimal b)
        {
            return Convert.ToDecimal(System.Math.Sqrt(Convert.ToDouble(b)));
        }

        public static decimal Exp(decimal p)
        {
            return Convert.ToDecimal(System.Math.Exp(Convert.ToDouble(p)));
        }
    }
}
