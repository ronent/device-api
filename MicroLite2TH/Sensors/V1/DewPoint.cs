﻿using Base.Sensors.Management;
using System;

namespace MicroLite2TH.Sensors.V1
{
    /// <summary>
    /// Dew Point Sensor.
    /// </summary>
    [Serializable]    
    public sealed class DewPoint : MicroLogAPI.Sensors.V1.DewPoint
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DewPoint"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DewPoint(SensorManager parent)
            :base(parent)
        {
        }

        #endregion
    }
}
