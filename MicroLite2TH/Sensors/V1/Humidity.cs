﻿using Base.Sensors.Management;
using System;

namespace MicroLite2TH.Sensors.V1
{
    /// <summary>
    /// Humidity Sensor.
    /// </summary>
    [Serializable]    
    public sealed class Humidity : MicroLogAPI.Sensors.V1.Humidity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Humidity"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Humidity(SensorManager parent)
            : base(parent)
        {
        }

        #endregion
    }
}
