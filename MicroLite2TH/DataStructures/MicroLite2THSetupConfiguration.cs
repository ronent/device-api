﻿using Base.Devices;
using MicroLogAPI.DataStructures;
using MicroXBase.Devices.Types;
using System;

namespace MicroLite2TH.DataStructures
{
    /// <summary>
    /// Setup configuration for MicroLite II Temperature % Humidity.
    /// </summary>
    [Serializable]
    public sealed class MicroLite2THSetupConfiguration : MicroLogSetupConfiguration, ITHDSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2THSetupConfiguration"/> class.
        /// </summary>
        public MicroLite2THSetupConfiguration()
            : base()
        {
            FixedSensors = new THDSensorSetup();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the fixed (Temperature, Humidity, Dew Point) sensors.
        /// </summary>
        /// <value>
        /// The fixed sensors.
        /// </value>
        public THDSensorSetup FixedSensors { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="device">The dummy device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(FixedSensors.TemperatureEnabled, "Temperature sensor must be enabled");
            FixedSensors.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}