﻿using Base.OpCodes;
using Base.Sensors.Management;
using Maintenance;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Devices;

using MicroLogAPI.Sensors.V1;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace MicroLite2TH.DataStructures
{
    [Serializable]
    public class MicroLite2THSetupConfiguration : SetupConfiguration, ITHDSensorSetup
    {
        public THDSensorSetup FixedSensors { get; set; }

        public MicroLite2THSetupConfiguration()
            :base()
        {
            FixedSensors = new THDSensorSetup();
        }
    }
}