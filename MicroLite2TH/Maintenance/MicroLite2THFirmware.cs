﻿using System;
using System.Runtime.Serialization;

namespace MicroLite2TH.Log
{
    /// <summary>
    /// MicroLite II Temperature & Humidity Device Firmware.
    /// </summary>
    [Serializable]
    internal class MicroLite2THFirmware : MicroXBase.Maintenance.MicroXFirmware
    {
        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public override string Name { get { return "MicroLite II"; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2THFirmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public MicroLite2THFirmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2THFirmware"/> class.
        /// </summary>
        public MicroLite2THFirmware()
        {

        }
    }
}
