﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class DefaultCalibration : MicroLogAPI.OpCodes.V1.DefaultCalibration
    {
        #region Constructors
        public DefaultCalibration(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
