﻿using MicroLite2TH.Devices;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class Status : MicroLogAPI.OpCodes.V1.Status
    {
        #region Constructors
        public Status(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
