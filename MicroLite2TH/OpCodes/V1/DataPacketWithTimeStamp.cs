﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : MicroLogAPI.OpCodes.V1.DataPacketWithTimeStamp
    {
        #region Constructors
        public DataPacketWithTimeStamp(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
