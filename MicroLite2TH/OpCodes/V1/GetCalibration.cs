﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class GetCalibration : MicroLogAPI.OpCodes.V1.GetCalibration
    {
        #region Constructors
        public GetCalibration(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
