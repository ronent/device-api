﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class Stop : MicroLogAPI.OpCodes.V1.Stop
    {
        #region Constructors
        public Stop(MicroLite2THLogger device)
            : base(device)
        {

        }

        #endregion
    }
}
