﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class TimerRun : MicroLogAPI.OpCodes.V1.TimerRun
    {
        #region Constructors
        public TimerRun(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
