﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUploadManager : MicroLogAPI.OpCodes.V1.FirmwareUploadManager
    {
        #region Constructors
        public FirmwareUploadManager(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
