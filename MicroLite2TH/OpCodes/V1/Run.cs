﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class Run : MicroLogAPI.OpCodes.V1.Run
    {
        #region Constructors
        public Run(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
