﻿using Base.OpCodes;
using MicroLite2TH.DataStructures;
using MicroLite2TH.Devices;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroLogAPI.OpCodes.V1.Setup
    {
        #region Properties
        new public MicroLite2THSetupConfiguration Configuration
        {
            get { return base.Configuration as MicroLite2THSetupConfiguration; }
            set { base.Configuration = value; }
        }
        #endregion
        #region Configuration Properties
        protected override bool TemperatureEnabled { get { return Configuration.FixedSensors.TemperatureEnabled; } }
        protected override bool HumidityEnabled { get { return Configuration.FixedSensors.HumidityEnabled; } }
        protected override bool DewPointEnabled { get { return Configuration.FixedSensors.DewPointEnabled; } }
        protected override Alarm TemperatureAlarm { get { return Configuration.FixedSensors.TemperatureAlarm; } }
        protected override Alarm HumidityAlarm { get { return Configuration.FixedSensors.HumidityAlarm; } }
        protected override Alarm DewPointAlarm { get { return Configuration.FixedSensors.DewPointAlarm; } }
        protected override bool StopOnDisconnect { get { return false; } }
        #endregion
        #region Constructors
        public Setup(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
