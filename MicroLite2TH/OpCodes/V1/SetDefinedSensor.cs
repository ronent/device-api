﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class SetDefinedSensor : MicroLogAPI.OpCodes.V1.SetDefinedSensor
    {
        #region Constructors
        public SetDefinedSensor(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
