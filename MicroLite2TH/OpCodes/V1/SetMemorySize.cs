﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class SetMemorySize : MicroLogAPI.OpCodes.V1.SetMemorySize
    {
        #region Constructors
        public SetMemorySize(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
