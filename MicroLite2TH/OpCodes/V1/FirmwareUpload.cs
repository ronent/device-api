﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class FirmwareUpload : MicroLogAPI.OpCodes.V1.FirmwareUpload
    {
        #region Constructors
        public FirmwareUpload(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
