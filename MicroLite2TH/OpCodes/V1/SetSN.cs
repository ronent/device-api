﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroLogAPI.OpCodes.V1.SetSN
    {
        #region Constructors
        public SetSN(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
