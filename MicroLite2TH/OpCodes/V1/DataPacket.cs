﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class DataPacket : MicroLogAPI.OpCodes.V1.DataPacket
    {
        #region Constructors
        public DataPacket(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
