﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class OnlineTimeStamp : MicroLogAPI.OpCodes.V1.OnlineTimeStamp
    {
        #region Constructors
        public OnlineTimeStamp(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
