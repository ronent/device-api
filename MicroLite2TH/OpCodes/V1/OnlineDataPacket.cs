﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : MicroLogAPI.OpCodes.V1.OnlineDataPacket
    {
        #region Constructors
        public OnlineDataPacket(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
