﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : MicroLogAPI.OpCodes.V1.GetBoomerang
    {
        #region Constructors
        public GetBoomerang(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }

}
