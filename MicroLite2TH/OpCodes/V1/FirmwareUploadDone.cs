﻿using System;
using MicroLite2TH.Devices.V1;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    public class FirmwareUploadDone : MicroLogAPI.OpCodes.V1.FirmwareUploadDone
    {
        #region Constructors
        public FirmwareUploadDone(MicroLite2THDevice device)
            : base(device)
        {
        }
        #endregion
    }
}
