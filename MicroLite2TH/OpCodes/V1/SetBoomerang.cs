﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : MicroLogAPI.OpCodes.V1.SetBoomerang
    {
        #region Constructors
        public SetBoomerang(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
