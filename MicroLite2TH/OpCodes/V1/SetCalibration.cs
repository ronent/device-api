﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class SetCalibration : MicroLogAPI.OpCodes.V1.SetCalibration
    {
        #region Constructors
        public SetCalibration(MicroLite2THLogger device)
            : base(device)
        { }
        #endregion
    }
}
