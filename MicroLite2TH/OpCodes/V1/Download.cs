﻿using MicroLite2TH.Devices.V1;
using System;

namespace MicroLite2TH.OpCodes.V1
{
    [Serializable]
    internal class Download : MicroLogAPI.OpCodes.V1.Download
    {
        #region Constructors
        public Download(MicroLite2THLogger device)
            : base(device)
        {
        }
        #endregion
    }
}
