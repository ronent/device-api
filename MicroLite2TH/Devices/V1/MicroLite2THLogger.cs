﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace MicroLite2TH.Devices.V1
{
    /// <summary>
    /// MicroLite II Temperature & Humidity Device.
    /// </summary>
    [Serializable]
    public sealed class MicroLite2THLogger : Devices.MicroLite2THLogger, ISerializable
    {
        #region Members
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 1);

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version supported by the software.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2THLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroLite2THLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }

        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroLite2THLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroLite2THLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
