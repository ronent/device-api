﻿using Base.Devices.Management;
using MicroLogAPI.Modules;
using MicroXBase.Modules;
using System.ComponentModel.Composition;
using System.Reflection;
using System.Collections.Generic;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using System;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLite2TH.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "MicroLite II Temperature & Humidity")]
    internal class MicroLite2THInterfacer : GenericSubDeviceInterfacer<MicroLite2THLogger>
    {
        public const byte TYPE = 0xA2;
        public const byte BITS = 0x02;
                         
        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public MicroLite2THInterfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x10C4, 0xEA70), new DeviceID(MicroXLoggerManager.VID, 0x0003) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.MicroLite2THLogger(ParentDeviceManager as IHIDDeviceManager);
        }

    }
}
