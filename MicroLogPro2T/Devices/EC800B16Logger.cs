﻿using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using MicroLogAPI.Devices;
using MicroXBase.Devices.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.Devices
{
   [Serializable]
    public class EC800B16Logger : MicroLog16BitLogger
    {
        #region Members
        private static readonly string deviceTypeName = "EC800 Temperature";

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected override byte Type { get { return EC800B16Interfacer.TYPE; } }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected override byte Bits { get { return EC800B16Interfacer.BITS; } }

        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName
        {
            get
            {
                return deviceTypeName;
            }
        }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public override MicroXFeature[] Features
        {
            get
            {
                return base.Features.Concat(new MicroXFeature[]
                {
                    MicroXFeature.DEEP_SLEEP,
                }).ToArray();
            }
        }

        /// <summary>
        /// Gets the available fixed sensors.
        /// </summary>
        /// <value>
        /// The available fixed sensors.
        /// </value>
        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new eSensorType[] { eSensorType.InternalNTC };
            }
        }

        /// <summary>
        /// Gets the available detachable sensors.
        /// </summary>
        /// <value>
        /// The available detachable sensors.
        /// </value>
        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[]
                    {eSensorType.Current4_20mA, 
                     eSensorType.Voltage0_10V,
                     eSensorType.ExternalNTC};
            }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Device"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal EC800B16Logger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        //Deserialization constructor.
        /// <summary>
        /// Initializes a new instance of the <see cref="EC850B16Device"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected EC800B16Logger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

        //Serialization function.
        /// <summary>
        /// Gets the object data.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }
        #endregion
    }
}
