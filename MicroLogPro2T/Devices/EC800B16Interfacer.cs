﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;
using MicroXBase.Modules;
using MicroXBase.Devices.Types;
using MicroLogAPI.Modules;
using MicroXBase.Modules.Interfacer;
using Base.Modules.Interfacer;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using MicroXBase.Devices.Management;
using Infrastructure.Communication;

namespace MicroLogPro2T.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "EC800 16Bit")]
    internal class EC800B16Interfacer : GenericSubDeviceInterfacer<EC800B16Logger>
    {
        public const byte TYPE = 0x10;
        public const byte BITS = 0x02;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public EC800B16Interfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() { new DeviceID(0x10C4, 0x1F0C), new DeviceID(MicroXLoggerManager.VID, 0x0005) }; } }

        public override bool CanCreateFrom(GenericDevice device)
        {
            // PCBAssembly = for EC means '1' = 12 bit, '2' = 16 bit
            return device.Version.PCBAssembly == Bits
                && base.CanCreateFrom(device);
        }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.EC800B16Logger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
