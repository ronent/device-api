﻿using MicroLogPro2T.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.OpCodes.V1
{
    [Serializable]
    internal class DataPacketWithTimeStamp : MicroLogAPI.OpCodes.V1.DataPacketWithTimeStamp
    {
        #region Constructors
        public DataPacketWithTimeStamp(EC800B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
