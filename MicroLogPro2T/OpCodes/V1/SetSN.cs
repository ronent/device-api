﻿using MicroLogPro2T.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.OpCodes.V1
{
    [Serializable]
    internal class SetSN : MicroLogAPI.OpCodes.V1.SetSN
    {
        #region Constructors
        public SetSN(EC800B16Logger device)
            : base(device)
        {
        }
        #endregion
    }
}
