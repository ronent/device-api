﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroLogAPI.DataStructures;
using MicroLogPro2T.DataStructures;
using MicroLogPro2T.Devices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.OpCodes.V1
{
    [Serializable]
    internal class Setup : MicroLogAPI.OpCodes.V1.Setup
    {
        #region Properties
        new public EC800B16SetupConfiguration Configuration
        {
            get { return base.Configuration as EC800B16SetupConfiguration; }
            set { base.Configuration = value; }
        }

        #endregion
        #region Configuration Properties
        protected override bool TemperatureEnabled { get { return Configuration.TemperatureSensor.TemperatureEnabled; } }
        protected override bool ExternalEnabled { get { return Configuration.ExternalSensor.Enabled; } }
        protected override Alarm TemperatureAlarm { get { return Configuration.TemperatureSensor.TemperatureAlarm; } }
        protected override Alarm ExternalAlarm { get { return Configuration.ExternalSensor.Alarm; } }
        protected override bool DeepSleepMode { get { return Configuration.DeepSleepMode; } }
        protected override eSensorType ExternalType { get { return Configuration.ExternalSensor.Type; } }
        protected override bool UserDefined { get { return Configuration.ExternalSensor.UserDefined; } }
        protected override MicroLogUDSConfiguration UserDefinedSensor { get { return Configuration.ExternalSensor.UserDefinedSensor; } }

        #endregion
        #region Constructors
        public Setup(EC800B16Logger device)
            : base(device)
        {
        }

        #endregion
    }
}
