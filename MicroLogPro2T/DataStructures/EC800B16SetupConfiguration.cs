﻿using Base.Devices;
using MicroLogAPI.DataStructures;
using MicroLogAPI.Sensors.Management;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.DataStructures
{
    /// <summary>
    /// Setup configuration for MicroLogPro II 16 Bits Temperature.
    /// </summary>
    public sealed class EC800B16SetupConfiguration : MicroLogSetupConfiguration, IESensorSetup, ITSensorSetup
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="EC800B16SetupConfiguration"/> class.
        /// </summary>
        public EC800B16SetupConfiguration()
            : base()
        {
            ExternalSensor = new ESensorSetup();
            TemperatureSensor = new TSensorSetup();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets the external sensor.
        /// </summary>
        /// <value>
        /// The external sensor.
        /// </value>
        public ESensorSetup ExternalSensor { get; set; }

        /// <summary>
        /// Gets the fixed (Temperature, Humidity, Dew Point ) sensors.
        /// </summary>
        /// <value>
        /// The fixed sensors.
        /// </value>
        public TSensorSetup TemperatureSensor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether device should enter deep sleep mode.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [deep sleep mode]; otherwise, <c>false</c>.
        /// </value>
        public bool DeepSleepMode { get; set; }

        #endregion
        #region Validation
        /// <summary>
        /// Validates the configuration and throws exception for error.
        /// </summary>
        /// <param name="device">The device.</param>
        internal override void ThrowIfInvalidFor(MicroXLogger device)
        {
            var dummy = GetDummyDevice(device);
            base.ThrowIfInvalidFor(dummy);
        }

        internal override void adjustDummyStatus(MicroXLogger dummy)
        {
            ESensorSetup.adjustExternalSensor(dummy, ExternalSensor);
            base.adjustDummyStatus(dummy);
        }

        internal override void checkSensors(MicroXLogger device)
        {
            ThrowIfFalse(TemperatureSensor.TemperatureEnabled || ExternalSensor.Enabled, "At least one sensor must be enabled");
            ExternalSensor.ThrowIfInvalidFor(device);
            TemperatureSensor.ThrowIfInvalidFor(device);
        }

        #endregion
    }
}
