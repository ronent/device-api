﻿using Base.Sensors.Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroLogPro2T.Sensors.V1
{
    /// <summary>
    /// Internal NTC Sensor.
    /// </summary>
    [Serializable]
    public sealed class InternalNTC : MicroLogAPI.Sensors.V1.InternalNTC
    {
        #region Properties
        /// <summary>
        /// Gets the maximum.
        /// </summary>
        /// <value>
        /// The maximum.
        /// </value>
        public override decimal Maximum
        {
            get { return ConvertToCorrectUnit(80); }
        }

        /// <summary>
        /// Gets the minimum.
        /// </summary>
        /// <value>
        /// The minimum.
        /// </value>
        public override decimal Minimum
        {
            get { return ConvertToCorrectUnit(-40); }
        }

        /// <summary>
        /// Gets a value indicating whether [USB runnable].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="InternalNTC"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal InternalNTC(SensorManager parent)
            : base(parent)
        {
        }

        /// <summary>
        /// Initializes the calibration.
        /// </summary>
        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.InternalNTC();
        }
        #endregion
    }
}
