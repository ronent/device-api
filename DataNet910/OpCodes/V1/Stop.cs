﻿using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.OpCodes.V1
{
    [Serializable]
    internal class Stop : DataNetLoggersAPI.OpCodes.V1.Stop
    {
        #region Constructor
        public Stop(DataNetLogger logger)
            :base(logger)
        {

        }

        #endregion
    }
}
