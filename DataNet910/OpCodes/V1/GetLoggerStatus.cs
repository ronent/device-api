﻿using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.OpCodes.V1
{
    [Serializable]
    internal class GetLoggerStatus : DataNetLoggersAPI.OpCodes.V1.GetLoggerStatus
    {
        #region Constructor
        public GetLoggerStatus(DataNetLogger logger)
            :base(logger)
        {

        }

        #endregion
    }
}
