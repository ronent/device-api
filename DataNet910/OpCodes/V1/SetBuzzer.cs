﻿using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.OpCodes.V1
{
    [Serializable]
    internal class SetBuzzer : DataNetLoggersAPI.OpCodes.V1.SetBuzzer
    {
        #region Constructor
        public SetBuzzer(DataNetLogger logger)
            :base(logger)
        {

        }

        #endregion
    }
}
