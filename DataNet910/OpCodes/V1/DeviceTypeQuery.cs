﻿using DataNet910.Devices;
using DataNetBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.OpCodes.V1
{
    [Serializable]
    internal class DeviceTypeQuery : DataNetLoggersAPI.OpCodes.V1.DeviceTypeQuery
    {
        #region Constructor
        public DeviceTypeQuery(DataNet910Logger logger)
            : base(logger)
        {

        }

        #endregion
    }
}
