﻿using Base.Devices.Management.DeviceManager;
using DataNet9x0.Devices.V1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.Devices
{
    /// <summary>
    /// DataNet 910 Logger.
    /// </summary>
    [Serializable]
    public abstract class DataNet910Logger : DataNet9x0Logger
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);
        private static readonly string deviceTypeName = "DNL910";

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet910Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        public DataNet910Logger(IDeviceManager parent)
            :base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the name of the device type.
        /// </summary>
        /// <value>
        /// The name of the device type.
        /// </value>
        public override string DeviceTypeName { get { return deviceTypeName; } }

        #endregion
    }
}
