﻿using Base.Modules.Interfacer;
using DataNetBase.Modules.Interfacer;
using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "DataNet DNL910")]
    internal class DataNet910LoggerInterfacer : GenericSubDeviceInterfacer<DataNet910Logger>
    {
        #region Constructor
        public DataNet910LoggerInterfacer()
            :base()
        {

        }

        #endregion
        #region GenericSubDeviceInterfacer
        public override bool CanCreate(ConnectionEventArgs e)
        {
            return false;
        }

        protected override Base.Devices.GenericDevice CreateNewDevice()
        {
            return new V1.DataNet910Logger(ParentDeviceManager);
        }

        protected override byte Type
        {
            get { return (byte)eDataNetDevicesType.DNL910; }
        }

        #endregion
    }
}