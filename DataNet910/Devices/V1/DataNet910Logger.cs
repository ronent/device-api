﻿using Base.Devices.Management.DeviceManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataNet910.Devices.V1
{
    /// <summary>
    /// DataNet 910 Logger.
    /// </summary>
    [Serializable]
    public class DataNet910Logger : Devices.DataNet910Logger
    {
        #region Fields
        /// <summary>
        /// The minimum firmware version.
        /// </summary>
        private static readonly Version MINIMUM_FW_VERSION = new Version(3, 1, 3);

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="DataNet910Logger"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DataNet910Logger(IDeviceManager parent)
            : base(parent)
        {

        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the minimum required firmware version.
        /// </summary>
        /// <value>
        /// The minimum required firmware version.
        /// </value>
        public override Version MinimumRequiredFirmwareVersion
        {
            get { return MINIMUM_FW_VERSION; }
        }

        #endregion
    }
}
