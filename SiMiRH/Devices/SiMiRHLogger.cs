﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using Base.Devices.Management.DeviceManager.HID;
using Base.Sensors.Management;
using SiMiAPI.Devices;
using SiMiBase.Devices.Features;

namespace SiMiRH.Devices
{
    [Serializable]
    public class SiMiRHLogger : SiMiLogger
    {
        #region Members
        private static readonly string deviceTypeName = "SiMi RH Logger";
        //internal static readonly string firmwareDeviceName = "SiMi RH Logger";
        #endregion
        #region Properties
        protected override byte Type { get { return SiMiRHInterfacer.TYPE; } }

        protected override byte Bits { get { return SiMiRHInterfacer.BITS; } }

        public override string DeviceTypeName
        {
            get { return deviceTypeName; }
        }

        public override SiMiBaseFeature[] Features
        {
            get
            {
                return base.Features.Concat(new[]
                {
                    SiMiBaseFeature.STOP_ON_KEY_PRESS,
                }).ToArray();
            }
        }

        public override eSensorType[] AvailableFixedSensors
        {
            get
            {
                return new[] { eSensorType.PT100, eSensorType.Humidity };
            }
        }

        public override eSensorType[] AvailableDetachableSensors
        {
            get
            {
                return new eSensorType[] { };
            }
        }

        #endregion
        #region Constructors
        internal SiMiRHLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiRHLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

      
        #endregion
    }
}
