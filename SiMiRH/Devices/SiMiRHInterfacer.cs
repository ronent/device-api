﻿using System.Collections.Generic;
using System.ComponentModel.Composition;
using Base.Devices;
using Base.Devices.Management.DeviceManager.HID;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using SiMiAPI.Modules;
using SiMiBase.Devices.Management;

namespace SiMiRH.Devices
{
    [Export(typeof(IDeviceInterfacer))]
    [ExportMetadata("Name", "SiMi RH")]
    internal class SiMiRHInterfacer : GenericSubDeviceInterfacer<SiMiRHLogger>
    {
        public const byte TYPE = 0x02;//0xA0
        public const byte BITS = 0x02;

        protected override byte Type { get { return TYPE; } }
        protected override byte Bits { get { return BITS; } }

        public SiMiRHInterfacer()
            : base()
        {
        }

        public override List<DeviceID> DeviceIDs { get { return new List<DeviceID>() {  new DeviceID(SiMiBaseLoggerManager.VID, 0x0007) }; } }

        protected override GenericDevice CreateNewDevice()
        {
            return new V1.SiMiRHLogger(ParentDeviceManager as IHIDDeviceManager);
        }
    }
}
