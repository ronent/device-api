﻿using Base.Devices.Management.DeviceManager.HID;
using System;
using System.Runtime.Serialization;

namespace SiMiRH.Devices.V1
{
    [Serializable]
    public sealed class SiMiRHLogger : Devices.SiMiRHLogger
    {
        #region Members
        private static readonly Version MINIMUM_FW_VERSION = new Version(1, 0);

       // internal static readonly string firmwareDeviceName = Devices.SiMiRHLogger.firmwareDeviceName + " V1";
        #endregion
        #region Properties
        public override Version MinimumRequiredFirmwareVersion { get { return MINIMUM_FW_VERSION; } }

        #endregion
        #region Constructors
        internal SiMiRHLogger(IHIDDeviceManager parent)
            : base(parent)
        {

        }
        #endregion
        #region Serialization & Deserialization
        protected SiMiRHLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
        }

       
        #endregion
    }
}
