﻿using System;
using System.Runtime.Serialization;

namespace SiMiRH.Maintenance
{
    [Serializable]
    internal class SiMiRHFirmware : SiMiBase.Maintenance.SiMiBaseFirmware
    {
        public override string Name { get { return "SiMi RH"; } }

        public SiMiRHFirmware(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        public SiMiRHFirmware()
        {

        }
    }
}
