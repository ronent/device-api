﻿using System;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class GetBoomerang : SiMiAPI.OpCodes.V1.GetBoomerang
    {
        public GetBoomerang(SiMiRHLogger device)
            : base(device)
        {

        }
    }
}
