﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class OnlineDataPacket : SiMiAPI.OpCodes.V1.OnlineDataPacket
    {
        public OnlineDataPacket(SiMiRHLogger device)
            : base(device)
        {

        }
    }
}
