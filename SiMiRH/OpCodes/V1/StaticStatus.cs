﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class StaticStatus: SiMiAPI.OpCodes.V1.StaticStatus
    {
        public StaticStatus(SiMiRHLogger device)
            : base(device)
        {

        }

        #region Overrides of OpCode

        protected override void DoAfterFinished()
        {
            base.DoAfterFinished();
            ParentDevice.Status.FixedSensors.HumidityEnabled = true;
        }

        #endregion
    }
}
