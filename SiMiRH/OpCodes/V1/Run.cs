﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class Run : SiMiAPI.OpCodes.V1.Run
    {
        public Run(SiMiRHLogger device)
            : base(device)
        {

        }
    }
}
