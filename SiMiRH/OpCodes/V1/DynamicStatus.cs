﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class DynamicStatus : SiMiAPI.OpCodes.V1.DynamicStatus
    {
        public DynamicStatus(SiMiRHLogger device)
            : base(device)
        {

        }

        
    }
}
