﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class SetBoomerang : SiMiAPI.OpCodes.V1.SetBoomerang
    {
        public SetBoomerang(SiMiRHLogger device)
            : base(device)
        {

        }
    }
}
