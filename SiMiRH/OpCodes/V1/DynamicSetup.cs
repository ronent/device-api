﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SiMiRH.Devices;

namespace SiMiRH.OpCodes.V1
{
    [Serializable]
    internal class DynamicSetup : SiMiAPI.OpCodes.V1.DynamicSetup
    {
        public DynamicSetup(SiMiRHLogger device)
            : base(device)
        {

        }

        
    }
}
