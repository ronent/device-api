﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace SiMiRH.Sensors.V1.Calibrations
{
    [Serializable]
    sealed class PT100 : Base.Sensors.Calibrations.GenericNTC
    {
        internal PT100()
        {

        }

        #region Fields
        public override decimal DefaultCoeffA { get { return 0.0008832357m; } }

        public override decimal DefaultCoeffB { get { return 0.0002524509m; } }
        
        public override decimal DefaultCoeffC { get { return 0.0000001867763m; } }

        #endregion
        #region Not Supported
        internal override decimal Calibrate(decimal value)
        {
            throw new NotSupportedException();
        }

        internal override decimal Decalibrate(decimal value)
        {
            throw new NotSupportedException();
        }
        #endregion
        #region Serialization & Deserialization
        protected PT100(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {

        }

        #endregion
    }
}
