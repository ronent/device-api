﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Base.Sensors.Management;
using Base.Sensors.Units;

namespace SiMiRH.Sensors.V1
{
    [Serializable]
    public sealed class Humidity : SiMiAPI.Sensors.V1.Humidity
    {
        #region Properties
        
        public override bool USBRunnable
        {
            get { return true; }
        }

        #region Overrides of GenericSensorV2

        public override IUnit Unit { get; internal set; }

        #endregion

        #endregion
        #region Constructors
        internal Humidity(SensorManagerV2 parent)
            : base(parent)
        {
        }

        protected override void InitializeCalibration()
        {
            Calibration = new Calibrations.PT100();
        }
        #endregion
    }
}
