﻿using Base.Devices.Features;
using System;

namespace MicroXBase.Devices.Features
{
    /// <summary>
    /// MicroX Device Feature.
    /// </summary>
    public enum MicroXDeviceFeatureEnum : byte
    {
        /// <summary>
        /// Cyclic.
        /// </summary>
        CYCLIC = 0x00,
        /// <summary>
        /// Push to run.
        /// </summary>
        PUSH_TO_RUN,
        /// <summary>
        /// Time run.
        /// </summary>
        TIMER_RUN,
        /// <summary>
        /// Stop on key press.
        /// </summary>
        STOP_ON_KEY_PRESS,
        /// <summary>
        /// Stop on disconnect.
        /// </summary>
        STOP_ON_DISCONNECT,
        /// <summary>
        /// Test mode.
        /// </summary>
        TEST_MODE,
        /// <summary>
        /// Set memory size.
        /// </summary>
        SET_MEMORY_SIZE,
        /// <summary>
        /// Show min/max.
        /// </summary>
        SHOW_MINMAX,
        /// <summary>
        /// Deep sleep mode.
        /// </summary>
        DEEP_SLEEP,
        /// <summary>
        /// Led on alarm.
        /// </summary>
        LED_ON_ALARM,
    }

    /// <summary>
    /// MicroX Device Features.
    /// </summary>
    [Serializable]
    public sealed class MicroXFeature : DeviceFeature
    {
        #region Ready-To-Use DeviceFeatures
        /// <summary>
        /// Gets the cyclic.
        /// </summary>
        /// <value>
        /// The cyclic.
        /// </value>
        public static MicroXFeature CYCLIC { get { return new MicroXFeature(MicroXDeviceFeatureEnum.CYCLIC); } }

        /// <summary>
        /// Gets the push to run.
        /// </summary>
        /// <value>
        /// The push to run.
        /// </value>
        public static MicroXFeature PUSH_TO_RUN { get { return new MicroXFeature(MicroXDeviceFeatureEnum.PUSH_TO_RUN); } }

        /// <summary>
        /// Gets the timer run
        /// </summary>
        /// <value>
        /// The timer run.
        /// </value>
        public static MicroXFeature TIMER_RUN { get { return new MicroXFeature(MicroXDeviceFeatureEnum.TIMER_RUN); } }

        /// <summary>
        /// Gets the stop on key press.
        /// </summary>
        /// <value>
        /// The stop on key press.
        /// </value>
        public static MicroXFeature STOP_ON_KEY_PRESS { get { return new MicroXFeature(MicroXDeviceFeatureEnum.STOP_ON_KEY_PRESS); } }

        /// <summary>
        /// Gets the stop on disconnect.
        /// </summary>
        /// <value>
        /// The sto stop on disconnect.
        /// </value>
        public static MicroXFeature STOP_ON_DISCONNECT { get { return new MicroXFeature(MicroXDeviceFeatureEnum.STOP_ON_DISCONNECT); } }

        /// <summary>
        /// Gets the test mode.
        /// </summary>
        /// <value>
        /// The test mode.
        /// </value>
        public static MicroXFeature TEST_MODE { get { return new MicroXFeature(MicroXDeviceFeatureEnum.TEST_MODE); } }

        /// <summary>
        /// Gets the set memory size.
        /// </summary>
        /// <value>
        /// The set memory size.
        /// </value>
        public static MicroXFeature SET_MEMORY_SIZE { get { return new MicroXFeature(MicroXDeviceFeatureEnum.SET_MEMORY_SIZE); } }

        /// <summary>
        /// Gets the show min/max.
        /// </summary>
        /// <value>
        /// The show min/max.
        /// </value>
        public static MicroXFeature SHOW_MINMAX { get { return new MicroXFeature(MicroXDeviceFeatureEnum.SHOW_MINMAX); } }

        /// <summary>
        /// Gets the deep sleep.
        /// </summary>
        /// <value>
        /// The deep sleep.
        /// </value>
        public static MicroXFeature DEEP_SLEEP { get { return new MicroXFeature(MicroXDeviceFeatureEnum.DEEP_SLEEP); } }

        /// <summary>
        /// Gets the led on alarm.
        /// </summary>
        /// <value>
        /// The led on alarm.
        /// </value>
        public static MicroXFeature LED_ON_ALARM { get { return new MicroXFeature(MicroXDeviceFeatureEnum.LED_ON_ALARM); } }

        #endregion
        /// <summary>
        /// Gets or sets the feature.
        /// </summary>
        /// <value>
        /// The feature.
        /// </value>
        public MicroXDeviceFeatureEnum Feature
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the code.
        /// </summary>
        /// <value>
        /// The code.
        /// </value>
        public override byte Code
        {
            get { return (byte)Feature; }
        }

        /// <summary>
        /// Gets the text.
        /// </summary>
        /// <value>
        /// The text.
        /// </value>
        public override string Text
        {
            get { return Feature.ToString(); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXFeature"/> class.
        /// </summary>
        /// <param name="code">The code.</param>
        internal MicroXFeature(MicroXDeviceFeatureEnum code)
        {
            this.Feature = code;
        }

    }
}
