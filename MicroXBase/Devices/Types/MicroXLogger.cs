﻿using Base.DataStructures;
using Base.DataStructures.Device;
using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.OpCodes;
using Base.OpCodes.Helpers;
using Infrastructure.Communication.Modules.HID;
using Log4Tech;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Features;
using MicroXBase.Functions.Management;
using MicroXBase.OpCodes.Management;
using MicroXBase.Sensors.Management;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MicroXBase.Devices.Types
{
    [Serializable]
    public abstract class MicroXLogger : GenericLogger, ISerializable
    {
        #region Properties
        /// <summary>
        /// Gets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        protected abstract byte Type { get; }

        /// <summary>
        /// Gets the bits.
        /// </summary>
        /// <value>
        /// The bits.
        /// </value>
        protected abstract byte Bits { get; }

        /// <summary>
        /// Gets the device features.
        /// </summary>
        /// <value>
        /// The features.
        /// </value>
        public virtual MicroXFeature[] Features
        {
            get
            {
                return new MicroXFeature[]
                { 
                    MicroXFeature.CYCLIC,
                    MicroXFeature.PUSH_TO_RUN,
                    MicroXFeature.TIMER_RUN,
                };
            }
        }

        new public MicroXSensorManager Sensors
        {
            get { return base.Sensors as MicroXSensorManager; }
            protected set { base.Sensors = value; }
        }

        new internal MicroXOpCodeManager OpCodes
        {
            get { return base.OpCodes as MicroXOpCodeManager; }
            set { base.OpCodes = value; }
        }

        new public MicroXStatusConfiguration Status
        {
            get { return base.Status as MicroXStatusConfiguration; }
            protected set { base.Status = value; }
        }

        new public MicroXLoggerFunctionsManager Functions
        {
            get { return base.Functions as MicroXLoggerFunctionsManager; }
            set { base.Functions = value; }
        }

        #endregion
        #region Constructors & Cleanup
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXLogger"/> class.
        /// </summary>
        /// <param name="parent">The parent device manager.</param>
        internal MicroXLogger(IHIDDeviceManager parent)
            :base(parent)
        {

        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        internal override void Initialize()
        {
            base.Initialize();

            try
            {
                Version = new MicroXVersion(this);
                Battery = new MicroXBattery(this);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in Initialize MicroXDevice", this, ex);
            }
        }

        #endregion
        #region Properties
        
        #endregion
        #region GenericDevice Implementation
        /// <summary>
        /// Determines whether the device is of the same type as the other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>
        /// True if the devices type are equals
        /// </returns>
        public override bool IsOfSameType(GenericDevice other)
        {
            return base.IsOfSameType(other)
                     && this.Memory == (other as MicroXLogger).Memory;
        }

        #endregion
        #region Serialization & Deserialization
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXLogger"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        protected MicroXLogger(SerializationInfo info, StreamingContext ctxt)
            : base(info, ctxt)
        {
            
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            base.GetObjectData(info, ctxt);
        }

        #endregion
    }
}