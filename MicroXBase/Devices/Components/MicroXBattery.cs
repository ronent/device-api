﻿using Base.DataStructures;
using MicroXBase.Devices.Types;
using System;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// MicroX Battery
    /// </summary>
    [Serializable]
    public class MicroXBattery : GenericBattery
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent device.
        /// </summary>
        /// <value>
        /// The parent device.
        /// </value>
        new internal MicroXLogger ParentDevice
        {
            get { return base.ParentDevice as MicroXLogger; }
            set { base.ParentDevice = value; }
        }

        /// <summary>
        /// Gets the battery level.
        /// </summary>
        /// <value>
        /// The battery level.
        /// </value>
        public override byte BatteryLevel
        {
            get { return ParentDevice.Status.BatteryLevel; }
        }

        /// <summary>
        /// Gets a value indicating whether the battery has alarm.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [has alarm]; otherwise, <c>false</c>.
        /// </value>
        public override bool HasAlarm
        {
            get { return false; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXBattery"/> class.
        /// </summary>
        /// <param name="device">The device.</param>
        internal MicroXBattery(MicroXLogger device)
            :base(device)
        {

        }
        #endregion
    }
}
