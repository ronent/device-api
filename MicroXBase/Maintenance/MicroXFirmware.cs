﻿using Base.Firmware;
using System;
using System.Runtime.Serialization;

namespace MicroXBase.Maintenance
{
    /// <summary>
    /// MicroX Device Firmware
    /// </summary>
    [Serializable]
    public abstract class MicroXFirmware : DeviceFirmware
    {
        /// <summary>
        /// Gets the full start address.
        /// </summary>
        /// <value>
        /// The full start address.
        /// </value>
        internal override UInt32 FullStartAddress { get { return Convert.ToUInt32("100800", 16); } }

        /// <summary>
        /// Gets the start address.
        /// </summary>
        /// <value>
        /// The start address.
        /// </value>
        internal override UInt16 StartAddress { get { return Convert.ToUInt16("800", 16); } }

        /// <summary>
        /// Gets the size of the block.
        /// </summary>
        /// <value>
        /// The size of the block.
        /// </value>
        internal override int BlockSize { get { return BLOCK_LENGTH; } }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXFirmware"/> class.
        /// </summary>
        /// <param name="info">The information.</param>
        /// <param name="ctxt">The CTXT.</param>
        public MicroXFirmware(SerializationInfo info, StreamingContext ctxt)
            :base(info, ctxt)
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXFirmware"/> class.
        /// </summary>
        internal MicroXFirmware()
        {

        }

    }
}
