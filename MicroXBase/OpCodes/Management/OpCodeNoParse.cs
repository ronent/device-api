﻿
using Base.OpCodes;
using MicroXBase.Devices.Types;
using System;

namespace MicroXBase.OpCodes.Management
{
    /// <summary>
    /// Use for OpCodes which don't require response from the device
    /// </summary>
    [Serializable]
    internal abstract class OpCodeNoParse : MicroXOpCode
    {
        #region Properties
        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        #endregion
        #region Constructors

        protected OpCodeNoParse(MicroXLogger device)
            : base(device)
        {

        }
        #endregion
        #region Methods
        protected override void DoAfterPopulate()
        {
            Finish(Result.OK);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        protected override void DoBeforeParse()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
