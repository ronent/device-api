﻿using Base.OpCodes;
using Infrastructure.Communication.DeviceIO;
using Infrastructure.Communication.Modules.HID;
using Infrastructure.FunctionQueue;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Common;
using System;
using System.Collections.Generic;

namespace MicroXBase.OpCodes.Management
{
    [Serializable]
    internal abstract class MicroXOpCodeManager : OpCodeManager, IDisposable
    {
        #region Fields
        public const int GENERIC_REPORT_SIZE = 65;

        private FunctionQueue functionQueue;
        private object sendSyncLock = new object();

        #endregion
        #region Properties
        public IEnumerable<byte[]> OpCodesToIgnoreForWebAgent
        {
            get
            {
                return new List<byte[]>() { OnlineDataPacket.ReceiveOpCode, OnlineTimeStamp.ReceiveOpCode };
            }
        }

        new public IHIDDeviceIO DeviceIO
        {
            get
            {
                return base.DeviceIO as IHIDDeviceIO;
            }
        }

        public FunctionQueue FunctionQueue
        {
            get
            {
                if (DeviceIO != null)
                    return DeviceIO.FunctionQueue;

                return null;
            }
        }

        public override int MaximumArraySize
        {
            get { return GENERIC_REPORT_SIZE; }
        }

        public override int OpCodeStartByte
        {
            get { return 1; }
        }

        #endregion
        #region Constructors
        public MicroXOpCodeManager(MicroXLogger parent)
            : base(parent)
        {

        }

        #endregion
        #region OpCodes
        internal OpCodeNoParseAck Run;
        internal OpCodeNoParseAck Stop;
        internal Setup Setup;
        internal Status Status;
        internal SetSN SetSN;
        internal DataPacket DataPacket;
        internal BaseDataPacket OnlineDataPacket;
        internal BaseDataPacket OnlineTimeStamp;
        internal FirmwareUpload FirmwareUpload;
        internal FirmwareUploadManager FirmwareUploadManager;
        internal Boomerang GetBoomerang;
        internal Boomerang SetBoomerang;
        internal Download Download;
        internal SetCalibration SetCalibration;

        #endregion
    }
}
