﻿using Base.OpCodes;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Management
{
    [Serializable]
    internal abstract class OpCodeNoParseAck : MicroXOpCode
    {
        #region Properties
        public override byte[] ReceiveOpCode
        {
            get { return null; }
        }

        #endregion
        #region Constructors
        public OpCodeNoParseAck(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Methods
        protected override void AckReceived()
        {
            Finish(Result.OK);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        protected override void DoBeforeParse()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
