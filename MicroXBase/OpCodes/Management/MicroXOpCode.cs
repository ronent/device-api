﻿using Base.OpCodes;
using Base.OpCodes.Types;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroXBase.OpCodes.Management
{
    [Serializable]
    internal abstract class MicroXOpCode : OpCode
    {
        #region Fields
        private static readonly byte[] ackHeader = { 0x28 };

        #endregion
        #region Constructor

        protected MicroXOpCode(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] AckHeader
        {
            get { return ackHeader; }
        }

        public override byte[] OpCodeSpecificAckHeader
        {
            get
            {
                return SendOpCode != null
                    ? AckHeader.Concat(SendOpCode).ToArray()
                    : null;
            }
        }

        #endregion
        #region Override
        new protected MicroXLogger ParentDevice
        {
            get { return base.ParentDevice as MicroXLogger; }
            set { base.ParentDevice = value; }
        }

        new protected MicroXOpCodeManager ParentOpCodeManager
        {
            get { return base.ParentOpCodeManager as MicroXOpCodeManager; }
        }

        protected override byte[] GetOutReportContent()
        {
            return OutReport.GetContent();
        }

        #endregion
        #region Virtual
        /// <summary>
        /// Valid opcodes that are expected to be return by the device from loopthrowprocess
        /// </summary>
        /// <returns></returns> 
        public virtual List<byte[]> GetReturnOpCodesToProcess() { return null; }

        /// <summary>
        /// Valid opcodes that wont send to server
        /// </summary>
        /// <returns></returns>
        protected List<byte[]> GetReturnOpCodesToIgnore()
        {
            return new List<byte[]>() { AckHeader };
        }

        #endregion
        #region Methods
        protected void PopulateSendOpCode()
        {
            OutReport.Insert(SendOpCode);
        }

        #endregion
    }
}
