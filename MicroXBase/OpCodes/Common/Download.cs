﻿using Base.Devices;
using Base.OpCodes;
using Log4Tech;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Infrastructure.Communication;
using Infrastructure.Communication.DeviceIO;
using Base.Sensors.Samples;
using System.Text.RegularExpressions;
using Auxiliary.Extentions;
using System.Threading;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class Download : MicroXOpCode
    {
        #region Enums
        public enum MessageTypeEnum { PacketCount = 0x00, MemoryFull = 0x01, EndOfPackets = 0x0F, EndOfCycleReached };
        public enum DownloadCommandEnum : byte { Next = 0x00, Anew = 0x01, Cancel = 0x02, Info = 0x03};

        #endregion
        #region Fields
        protected bool firstNextCommand;
        protected SampleList Samples;
       // private Stopwatch stopper;

        #endregion
        #region Constructors
        public Download(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();
            SubscribeToOnDataPacketFinished();
        }

        #endregion
        #region Properties
        public override bool SupportsRetry
        {
            get
            {
                return false;
            }
        }

        public virtual UInt32 PacketIndex
        {
            get;
            protected set;
        }

        public virtual UInt32 PacketCount
        {
            get;
            protected set;
        }

        public MessageTypeEnum MessageType
        {
            get;
            set;
        }

        protected DownloadCommandEnum Command
        {
            get;
            set;
        }

        public bool CancelationPending
        {
            get;
            protected set;
        }

        private bool parsingBundleInProcess;

        public bool IsBundle
        {
            get
            {
                if (ParentOpCodeManager.DeviceIO is IBundle)
                    if ((ParentOpCodeManager.DeviceIO as IBundle).IsBundleSupported)
                        return true;

                return false;
            }
        }

        public override TimeSpan InvokeTimeOutDuration
        {
            get
            {
                if (IsBundle)
                    return new TimeSpan(0, 0, (int)(base.InvokeTimeOutDuration.TotalSeconds * 5));
                else
                    return base.InvokeTimeOutDuration;
            }
        }

        #endregion
        #region Methods
        protected virtual void SubscribeToOnDataPacketFinished()
        {
            try
            {
                ParentOpCodeManager.DataPacket.OnFinished += DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On SubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        protected virtual void UnsubscribeToOnDataPacketFinished()
        {
            try
            {
                ParentOpCodeManager.DataPacket.OnFinished -= DataPacket_OnFinished;
            }
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                Log.Instance.WriteError("On UnsubscribeToOnDataPacketFinished()", this, ex);
            }
        }

        public override void Dispose()
        {
            UnsubscribeToOnDataPacketFinished();
            base.Dispose();
        }

        protected override void DoAfterParse()
        {
            RunNextCommand();
        }

        protected void DataPacket_OnFinished(object sender, Result result)
        {
            if (IsComplete)
                return;

            if (result.IsOK)
            {
                PacketIndex++;
                RunNextCommand();
            }
            else
            {
                Command = DownloadCommandEnum.Cancel;
                Finish(result);
            }
        }

        protected virtual void ResetDownloadTimeout()
        {
            ParentOpCodeManager.Download.ResetProcessTimer();
        }

        public Result Cancel()
        {
            if (IsRunning)
            {
                CancelationPending = true;
                return Result.OK;
            }
            else
                return Result.ILLEGAL_CALL;
        }

        protected override void DoBeforePopulate()
        {
            parsingBundleInProcess = false;
            firstNextCommand = false;
            CancelationPending = false;

           // stopper = new Stopwatch();
           // stopper.Start();
            Command = DownloadCommandEnum.Anew;

            PacketIndex = 0;
            PacketCount = 0;
            ParentOpCodeManager.DataPacket.Initialize();
        }

        protected void RunNextCommand()
        {
            if (CancelationPending)
            {
                Finish(Result.CANCELED);
                return;
            }

            if (!ReachedEndOfData())
            {
                ResetDownloadTimeout();

                if (parsingBundleInProcess)
                    return;

                setNextCommand();

                if (ShouldAskForBundle() && GetExpectedPacketsCount() > 0)
                {
                    parsingBundleInProcess = true;
                    invokeLoopThruMechanism();
                }
                else
                {
                    populateAndSend();
                }
            }
            else
            {
            //    stopper.Stop();
                Finish(Result.OK);
            }
        }

        protected override void WriteFinishToLog(Result result)
        {
            if (result.IsOK)
            {
                Log.Instance.Write("[API] ==> Device: '{0}' finished Opcode: '{1}'  with result: {2}", this, Log.LogSeverity.DEBUG, ParentDevice,
                     NameString,  result);
            }
            else
                base.WriteFinishToLog(result);
        }

        private void populateAndSend()
        {
            OutReport.Initialize();
            Populate();
            Send();
        }

        protected bool ShouldAskForBundle()
        {
            if (IsBundle && checkCommandForBundle())
                return true;

            return false;
        }

        private bool checkCommandForBundle()
        {
            if (firstNextCommand)
            {
                WaitingForInReport = true;

                return true;
            }

            return false;
        }

        private void invokeLoopThruMechanism()
        {
            OutReport.Initialize();
            Populate();

            var args = new LoopArgs()
            {
                OpCode = OutReport.GetContent(),
                ReturnOpCodesToProcess = GetReturnOpCodesToProcess(),
                ReturnOpCodesToFinish = GetReturnOpCodesToFinish(),
                ReturnOpCodesToIgnore = GetReturnOpCodesToIgnore(),
                ReturnOpCodesToImmediateSend = GetReturnOpCodesToImmediateSend(),
                ExpectedCount = GetExpectedPacketsCount(),
                OpCodeStartIndex = ParentOpCodeManager.OpCodeStartByte,
                Retries = 3,
                RetryIntervalms = 1000
            };

            Log.Instance.Write("[API] ==> Device: '{0}' invoked LoopThru with {1} packets expected", this, Log.LogSeverity.DEBUG, ParentDevice, args.ExpectedCount);

            (ParentOpCodeManager.DeviceIO as IBundle).InvokeLoopThroughAsLong(args);
            ResetDownloadTimeout();
        }

        protected virtual List<byte[]> GetReturnOpCodesToFinish()
        {
            return null;
        }

        private List<byte[]> GetReturnOpCodesToImmediateSend()
        {
            return new List<byte[]>() { ParentOpCodeManager.OnlineDataPacket.ReceiveOpCode, ParentOpCodeManager.OnlineTimeStamp.ReceiveOpCode };
        }
            
        public override List<byte[]> GetReturnOpCodesToProcess()
        {
            return new List<byte[]>() { ParentOpCodeManager.DataPacket.ReceiveOpCode };
        }

        protected abstract uint? GetExpectedPacketsCount();

        protected abstract bool ReachedEndOfData();

        private void setNextCommand()
        {
            switch (Command)
            {
                case DownloadCommandEnum.Next:
                case DownloadCommandEnum.Anew:
                    Command = DownloadCommandEnum.Next;
                    if (!firstNextCommand)
                        firstNextCommand = true;
                    break;
                case DownloadCommandEnum.Cancel:
                    break;
                case DownloadCommandEnum.Info:
                    Command = DownloadCommandEnum.Anew;
                    break;
            }
        }

        #endregion
        #region Populate Methods
        protected abstract void PopulateCommand();

        #endregion
    }
}