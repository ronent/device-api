﻿using Base.Devices;
using Base.OpCodes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MicroXBase.OpCodes.Management;
using MicroXBase.Devices.Types;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    public abstract class FirmwareUploadDone : OpCodeNoParse
    {
        #region Properties
        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x33 }; }
        }
        public override byte[] AckOpCode
        {
            get { return null; }
        }
        #endregion
        #region Constructors
        public FirmwareUploadDone(MicroXDevice device)
            : base(device)
        {
        }
        #endregion
        #region Methods
        public virtual Result Start()
        {
            return Result.OK;
        }

        public virtual Result Finish()
        {
            return Invoke();
        }
        #endregion
        #region Populate Methods
        protected override void PopulateReport()
        {
            PopulateOpCodeSend();
        }
        #endregion
        #region Not Supported
        protected override void ParseReport()
        {
            throw new NotSupportedException();
        }
        #endregion
    }
}
