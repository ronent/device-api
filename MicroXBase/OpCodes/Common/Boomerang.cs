﻿using Base.OpCodes;
using Log4Tech;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Diagnostics;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class Boomerang : MicroXOpCode
    {
        #region Consts & Enums
        protected ActionTypeEnum LAST_ACTION { get { return ActionTypeEnum.Bank1; } }
        protected enum ActionTypeEnum { Bank0 = 0x00, Bank1 = 0x01, Info = 0x02 };

        #endregion
        #region Constructors
        public Boomerang(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        protected abstract byte Method { get; }
        protected ActionTypeEnum ActionType { get; set; }

        #endregion
        #region Methods
        protected void NextStep()
        {
            if (ActionType != LAST_ACTION)
            {
                SetNextActionType();
                populateAndSend();

                return;
            }

            Finish(Result.OK);
        }

        private void populateAndSend()
        {
            OutReport.Initialize();
            Populate();
            Send();
        }

        protected override void DoBeforePopulate()
        {
            ResetActionType();
        }

        protected virtual void SetNextActionType()
        {
            //Log.Log.WriteLine("GetBoomerang current action: " + ActionType);
            switch (ActionType)
            {
                case ActionTypeEnum.Info:
                    ActionType = ActionTypeEnum.Bank0;
                    break;
                case ActionTypeEnum.Bank0:
                    ActionType = ActionTypeEnum.Bank1;
                    break;
            }
        }

        protected void ResetActionType()
        {
            ActionType = ActionTypeEnum.Info;
        }

        #endregion
    }
}
