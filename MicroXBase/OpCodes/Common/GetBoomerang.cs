﻿using Base.Misc;
using Base.OpCodes;
using Log4Tech;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;


namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class GetBoomerang : Boomerang
    {
        #region Consts
        const byte GET_METHOD = 0x01;
        #endregion
        #region Constructors
        public GetBoomerang(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        private string contactListString;
        protected override byte Method
        {
            get { return GET_METHOD; }
        }

        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x43 }; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return new byte[] { 0x43 }; }
        }

        public BoomerangConfiguration Configuration
        {
            get { return ParentDevice.Status.Boomerang; }
        }

        #endregion
        #region Parse Methods
        protected override void DoAfterParse()
        {
            NextStep();
        }

        protected override void Parse()
        {
            if (fixFor0xFFBug())
            {
                ParentDevice.Status.BoomerangEnabled = false;
                throw new Exception("Boomerang is disabled.");
            }

            ActionType = parseActionType();

            switch (ActionType)
            {
                case ActionTypeEnum.Info:
                    parseInfo();
                    break;
                case ActionTypeEnum.Bank0:
                    clearContacts();
                    parseContactString();
                    break;
                case ActionTypeEnum.Bank1:
                    parseContactString();
                    parseContacts();
                    //This marks the end of GetBoomerang process
                    break;
                default:
                    throw new Exception("Invalid boomerang configuration on device");
            }
        }

        /// <summary>
        /// Checks if boomerang info packet contains only FF bytes.
        /// </summary>
        /// <returns>true if all byte equals 0xFF.</returns>
        private bool fixFor0xFFBug()
        {
            byte[] arr = InReport.GetContent();

            for (int i = 2; i < arr.Length; i++)
            {
                if (arr[i] != 0xFF)
                    return false;
            }

            return true;
        }

        private void clearContacts()
        {
            Configuration.Contacts.Clear();
            contactListString = string.Empty;
        }

        private void parseContactString()
        {
            contactListString += InReport.GetString();
        }

        private void parseContacts()
        {
            if (contactListString.Length > 0)
            {
                removeLastChar();
                string[] contacts = contactListString.Split(BoomerangConfiguration.CONTACT_BREAK_CHAR);
                foreach (var contact in contacts)
                    Configuration.Contacts.Add(new BasicContact { EMail = contact });
            }
        }

        private void removeLastChar()
        {
            contactListString = contactListString.Substring(0, contactListString.Length - 1);
        }

        private void parseInfo()
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            string[] details;
            string fullInfo = InReport.GetString();
            details = fullInfo.Split(BoomerangConfiguration.INFO_BREAK_CHAR);

            try
            {
                Configuration.Comment = details[0];
                Configuration.Author = details[1];
                byte UTCSetting = encoding.GetBytes(details[2])[0];
                Configuration.CelsiusModeByte = encoding.GetBytes(details[3])[0];
                Configuration.DisplayAlarmLevelsByte = encoding.GetBytes(details[4])[0];
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("GetBoomerang Error",this, ex);
            }
        }

        protected ActionTypeEnum parseActionType()
        {
            return (ActionTypeEnum)(InReport.Next >> 4);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateMethodAndActionCode();
        }

        protected virtual void PopulateMethodAndActionCode()
        {
            OutReport.Next = (byte)(((byte)ActionType) << 4 | (byte)Method);
        }

        #endregion
    }

}