﻿using Auxiliary.MathLib;
using Base.OpCodes;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class Status : MicroXOpCode
    {
        #region Constructors

        protected Status(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Parse Methods
        protected abstract void ParseSetupBooleans();
        protected abstract void ParseTime();
        protected abstract void ParseInterval();
        protected abstract void ParseAlarms();
        protected abstract void ParseDisplayConfiguration();
        protected abstract decimal ParseAlarmEnd();
        protected abstract void ParseFirmwareVersion();
        protected abstract void ParseBatteryLevel();
        protected abstract void ParseTimerStatus();
        protected abstract void ParseAlarmInfo();
        protected abstract void ParseAssembly();
        protected abstract void ParseDeviceType();

        protected virtual void ParseUtcOffset()
        {
            ParentDevice.Status.UtcOffset = (sbyte)InReport.Next;
//            ParentDevice.Status.UtcOffset -= 12; //For legacy purposes with minus
        }

        protected virtual Alarm ParseAlarm()
        {
            var alarm = new Alarm
            {
                High = ParseAlarmEnd(),
                Low = ParseAlarmEnd()
            };

            return alarm;
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Static Methods
        protected static bool IsValidStartTime(byte[] timerRun)
        {
            return BitConverter.ToInt32(timerRun, 0) != 0;
        }

        #endregion
    }
}