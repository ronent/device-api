﻿using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class SetDefinedSensor : OpCodeNoParse
    {
        #region Constructors
        public SetDefinedSensor(MicroXLogger device)
            : base(device)
        { 
        }

        #endregion
        #region Populate Methods
        protected abstract void PopulateName();
        protected abstract void PopulateUnitAndSignificantFigures();
        protected abstract void PopulateCustomUnit();
        protected abstract void PopulateDefinedValues();

        #endregion
    }
}
