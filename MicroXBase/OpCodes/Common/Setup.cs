﻿using Base.DataStructures;
using Base.OpCodes;
using Log4Tech;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class Setup : OpCodeNoParse
    {
        #region Constructors
        public Setup(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public MicroXSetupConfiguration Configuration;

        #endregion
        #region Invoke Methods
        public Task<Result> Invoke(MicroXSetupConfiguration configuration)
        {
            Configuration = configuration;
            return Invoke();
        }

        protected override void DoBeforePopulate()
        {
            checkVersion(ParentDevice);
            Configuration.ThrowIfInvalidFor(ParentDevice);
        }

        #endregion
        #region Private Methods
        private void checkVersion(MicroXLogger ParentDevice)
        {
            string config = Configuration.GetType().Namespace;
            string device = ParentDevice.GetType().Namespace;

            int configVersion;
            if (!int.TryParse(config.Substring(config.LastIndexOf('.') + 2), out configVersion))
                return;

            if (!configVersion.ToString().Equals(device.Substring(device.LastIndexOf('.') + 2)))
                throw new Exception("Setup Configuration version unmatched the device.");
        }

        #endregion
        #region Populate Methods
        protected abstract void PopulateSetupBooleans();
        protected abstract void PopulateTime();
        protected abstract void PopulateInterval();
        protected abstract void PopulateAlarms();
        protected abstract void PopulateDisplayConfiguration();
        protected abstract void PopulateAlarm(Alarm AlarmValue);
        protected abstract void PopulateAlarmInfo();

        protected virtual void PopulateUtcOffset()
        {
//            Configuration.UtcOffset = TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now) ? Convert.ToSByte(TimeZoneInfo.Local.BaseUtcOffset.Hours + 1) : Convert.ToSByte(TimeZoneInfo.Local.BaseUtcOffset.Hours);
//            var utcOffset = (byte)(Configuration.UtcOffset + 12); //For legacy purposes with minus
//            OutReport.Next = utcOffset;
            OutReport.Next = (byte) Configuration.UtcOffset;
        }

        #endregion
        #region Methods
        internal Task<Result> RunSetSN()
        {
            return Configuration.ValidSerialNumber ? ParentOpCodeManager.SetSN.Invoke() : Task.FromResult(Result.ERROR);
        }

        internal Task<Result> RunSetBoomerang()
        {
            return Configuration.BoomerangEnabled ? ParentOpCodeManager.SetBoomerang.Invoke() : Task.FromResult(Result.OK);
        }

        internal Task<Result> CheckAgainstStatus()
        {
            //ParentDevice.Functions.NotifyNextStatus = false;
            var result = ParentDevice.Functions.GetStatusNoBusy().Result;

            if (!result.IsOK) return Task.FromResult(result);
            try
            {
                Configuration.ThrowIfNotEquals(ParentDevice.Status);
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Status FAILED to check out against sent setup. \n", this, ex);
                return Task.FromResult(new Result(ex));
            }

            return Task.FromResult(result);
        }

        #endregion
    }
}