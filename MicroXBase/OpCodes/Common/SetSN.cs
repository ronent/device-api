﻿using Base.OpCodes;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class SetSN : OpCodeNoParse
    {
        #region Constructors
        public SetSN(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateSN();
        }

        protected virtual void PopulateSN()
        {
            string sn;

            if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User)
                sn = ParentDevice.Status.SerialNumber;
            else
                sn = ParentOpCodeManager.Setup.Configuration.SerialNumber;

            OutReport.Insert(sn);
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
