﻿using Auxiliary.Tools;
using Base.Devices;
using Base.OpCodes;
using Base.Sensors.Types;
using Infrastructure.Communication.DeviceIO;
using Log4Tech;
using MicroXBase.Devices.Types;
using System;
using System.Text.RegularExpressions;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class DataPacket : BaseDataPacket
    {
        #region Events
        public event Base.OpCodes.ReportProgressDelegate OnProgressReported;

        #endregion
        #region Constructors
        public DataPacket(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            if (OnProgressReported == null)
                OnProgressReported += ParentDevice.Functions.InvokeOnDownloadProgressReported;

            WaitingForInReport = true;
            PacketTime = null;
            InitializeLastPercent();
        }

        #endregion
        #region Properties
        protected int lastPercent { get; set; }

        public virtual UInt16 CurrentAddress { get; set; }

        protected Download DownloadInfo
        {
            get
            {
                if (ParentOpCodeManager != null)
                    return ParentOpCodeManager.Download;

                return null;
            }
        }

        public virtual UInt32 PacketIndex
        {
            get
            {
                if (DownloadInfo != null)
                    return DownloadInfo.PacketIndex;

                return 0;
            }
        }

        protected virtual UInt32 TotalPackets
        {
            get
            {
                if (DownloadInfo != null)
                    return DownloadInfo.PacketCount;

                return 0;
            }
        }

        protected virtual bool CancelationPending { get { return ParentOpCodeManager.Download.CancelationPending; } }

        protected abstract ushort MaximumDataBlockSize { get; }

        #endregion
        #region Methods
        protected void ReportProgress(int percent)
        {
            if (percent > lastPercent)
            {
                //Log.Instance.Write("Percents: {0}", this, Log.LogSeverity.INFO, percent);
                lastPercent = percent;

                if (DownloadInfo.CancelationPending)
                    return;

                Utilities.InvokeEvent(OnProgressReported, this, new ProgressReportEventArgs(ParentDevice.Status.SerialNumber, percent));
            }
        }

        protected void InitializeLastPercent()
        {
            lastPercent = 0;
        }

        protected bool ReportHasAnyDataLeft()
        {
            return InReport.Has(MaximumDataBlockSize);
        }

        protected bool ReportHasMeaningfulData()
        {
            return !InReport.Is0xFF(MaximumDataBlockSize);
        }

        #endregion
        #region Parse Methods
        protected override void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime)
        {
            sensor.samples.AddOffline(value, dateTime);
        }

        protected override void AddTimeStampToSensor(GenericSensor sensor, long value, DateTime dateTime, string comment)
        {
            sensor.samples.AddOffline(value, dateTime, comment);
        }

        protected virtual void ReportDoneIfLastPacket()
        {
            if (PacketIndex + 1 == TotalPackets)
                ReportProgress(100);
        }

        protected void IncreasePacketTime()
        {
            PacketTime.Increase((long)ParentDevice.Status.Interval.TotalSeconds);
        }

        protected override void WriteFinishToLog(Result result)
        {
            if (result.IsOK && !NameString.Contains("Online"))
            {
                Log4Tech.Log.Instance.Write("[API] ==> Device: '{0}' finished Opcode: '{1}' with result: {2} {3}/{4}", this, Log.LogSeverity.DEBUG, ParentDevice,
                      NameString, result, ParentOpCodeManager.Download.PacketIndex + 1, ParentOpCodeManager.Download.PacketCount);
            }
            else
                base.WriteFinishToLog(result);
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
