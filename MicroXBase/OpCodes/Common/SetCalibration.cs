﻿using Base.OpCodes;
using Base.Sensors.Calibrations;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class SetCalibration : OpCodeNoParseAck
    {
        #region Fields
        public CalibrationConfiguration Calibration { get; set; }

        #endregion
        #region Constructors
        public SetCalibration(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x17 }; }
        }

        #endregion
        #region Methods
        protected override void DoBeforePopulate()
        {
            if (Calibration == null)
                throw new Exception("Calibration configuration is null");
        }

        public Result Invoke(CalibrationConfiguration calibration)
        {
            Calibration = calibration;
            return Invoke().Result;
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateGainOffset();
        }

        protected abstract void PopulateGainOffset();

        protected virtual void PopulateGainOffsetAux(CalibrationCoefficients coefficients)
        {
            ValidateCoefficients(coefficients);

            PopulateGainOffsetAux(coefficients.Af);
            PopulateGainOffsetAux(coefficients.Bf);
            PopulateGainOffsetAux(coefficients.Cf);
        }

        protected static void ValidateCoefficients(CalibrationCoefficients coefficients)
        {
            if (coefficients.Af == 0)
                coefficients.Af = 1;
        }

        protected virtual void PopulateGainOffsetAux(float val)
        {
            byte[] floatBytes = BitConverter.GetBytes(val);
            OutReport.Insert(floatBytes);
        }
        #endregion
    }
}
