﻿using Auxiliary.Tools;
using Base.Devices;
using Base.Firmware;
using Base.OpCodes;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class FirmwareUpload : MicroXOpCode
    {
        #region Events
        public event ReportProgressDelegate OnFirmwareUpdateProgressReported;

        #endregion
        #region Fields
        protected DeviceFirmware DeviceFirmware;
        protected FWIterator Iterator;
        private int lastProgress;

        #endregion
        #region Constructors
        public FirmwareUpload(MicroXLogger device)
            : base(device)
        {

        }

        protected override void initialize()
        {
            base.initialize();

            InitializeDeviceFirmware();
        }

        protected void InitializeDeviceFirmware()
        {
            try
            {
                if (DeviceFirmware == null && ParentDevice.Version != null)
                    DeviceFirmware = FirmwareFactory.GetInstance().Get(ParentDevice.FirmwareDeviceName, ParentDevice.Version.PCBAssembly);

            }
            catch { }
        }

        #endregion
        #region Properties
        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x31 }; }
        }

        public override byte[] ReceiveOpCode
        {
            get { return new byte[] { 0x32 }; }
        }

        protected int ProgressPercent
        {
            get { return Iterator.PercentProgress; }
        }

        public bool CancelationPending { get; protected set; }

        public bool IsFirmwareFileExists
        {
            get
            {
                return FirmwareFactory.GetInstance().Get(ParentDevice.FirmwareDeviceName, ParentDevice.Version.PCBAssembly) != null;
            }
        }

        #endregion
        #region Methods
        protected override void DoAfterParse()
        {
            if (CancelationPending)
                Finish(Result.CANCELED);

            SendNextPacket();
            //packet is no longer checked so PacketSent takes care of sending next packet            
        }

        protected override void DoBeforePopulate()
        {
            InitializeDeviceFirmware();

            if (DeviceFirmware == null)
                throw new Exception("Firmware file is missing");

            lastProgress = 0;
            Iterator = DeviceFirmware.GetIterator();
            CancelationPending = false;
        }

        protected virtual void SendNextPacket()
        {
            if (Iterator.HasNext)
            {
                InvokeReportProgress();
                Iterator.MoveNext();
                populateAndSend();
            }
            else
                Finish(ParentOpCodeManager.FirmwareUploadManager.Finish());
        }

        private void populateAndSend()
        {
            OutReport.Initialize();
            Populate();
            Send();
        }

        private void InvokeReportProgress()
        {
            if (ProgressPercent > lastProgress)
            {
                lastProgress = ProgressPercent;
                var current = Iterator.Current;

                InvokeFWUpdateProgressReport(new ProgressReportEventArgs(ParentDevice.Status.SerialNumber, ProgressPercent));
            }
        }

        protected void InvokeFWUpdateProgressReport(ProgressReportEventArgs e)
        {
            Utilities.InvokeEvent(OnFirmwareUpdateProgressReported, this, e);
            //if (OnFirmwareUpdateProgressReported != null)
            //    OnFirmwareUpdateProgressReported.Invoke(this, e);
        }

        #endregion
        #region Parse Methods
        protected override void Parse()
        {
            parseAndCheckAddress();
            //parseAndCheckData(); Not implemented in device pending further notice
        }

        private void parseAndCheckData()
        {
            byte[] data = InReport.GetBlock(DeviceFirmware.BlockSize);
            if(!data.SequenceEqual(Iterator.Current.Data))
                throw new Exception("Failed to verify address");
        }

        private void parseAndCheckAddress()
        {
            byte[] address = new byte[2];
            populateAddressAccordingToArchitecture(address);
            if( !address.SequenceEqual(Iterator.Current.AddressBytes))
                throw new Exception("Failed to verify data");
        }

        private void populateAddressAccordingToArchitecture(byte[] address)
        {
            address[0] = InReport.Next;
            address[1] = InReport.Next;

            if (BitConverter.IsLittleEndian)
            {
                int i = 0;
                foreach (var b in address.Reverse())
                    address[i++] = b;
            }
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
            PopulateAddress();
            PopulateData();
        }

        protected void PopulateData()
        {
            OutReport.Insert(Iterator.Current.Data);
        }

        protected void PopulateAddress()
        {
            OutReport.Insert(Iterator.Current.Address, Endianness.BIG);
        }

        #endregion
    }
}
