﻿using Base.OpCodes;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;
using System.Threading.Tasks;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class FirmwareUploadManager : OpCodeNoParse
    {
        #region Properties
        public override byte[] SendOpCode
        {
            get { return new byte[] { 0x33 }; }
        }

        public override bool SupportsRetry
        {
            get
            {
                return false;
            }
        }

        #endregion
        #region Constructors
        public FirmwareUploadManager(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Methods
        public virtual Result Start()
        {
            return ParentOpCodeManager.FirmwareUpload.Invoke().Result;
        }

        public virtual Result Cancel()
        {
            //return ParentOpCodeManager.FirmwareUpload.EndInvoke();
            return null;
        }

        public virtual Result Finish()
        {
            return Invoke().Result;
        }

        #endregion
        #region Populate Methods
        protected override void Populate()
        {
            PopulateSendOpCode();
        }

        #endregion
        #region Not Supported
        protected override void Parse()
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}
