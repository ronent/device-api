﻿using Base.DataStructures;
using Base.Sensors.Samples;
using Base.Sensors.Types;
using Log4Tech;
using MicroXBase.Devices.Types;
using MicroXBase.OpCodes.Management;
using System;

namespace MicroXBase.OpCodes.Common
{
    [Serializable]
    internal abstract class BaseDataPacket : MicroXOpCode
    {
        #region Properties
        protected virtual Time PacketTime { get; set; }
        protected MicroXLogger ParentLogger { get { return ParentDevice as MicroXLogger; } }

        #endregion
        #region Constructors
        public BaseDataPacket(MicroXLogger device)
            : base(device)
        {

        }

        #endregion
        #region Parse Methods
        protected abstract void AddSampleToSensor(GenericSensor sensor, long value, DateTime dateTime);
        protected abstract void AddTimeStampToSensor(GenericSensor sensor, long value, DateTime dateTime, string comment);

        /// <summary>
        /// Loops throw the enabled sensors and extracts data from report for each sensor
        /// </summary>
        protected void ExtractAndAdd()
        {
            try
            {
                foreach (GenericSensor sensor in ParentLogger.Sensors.EnabledSensors)
                {
                    var rawValue  = GetRawValueFor(sensor);
                    var packetTime = GetPacketTime();

                    AddSampleToSensor(sensor,rawValue, packetTime);
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in ExtractAndAdd",this,ex);
            }
        }

        protected void ExtractAndAdd(DateTime dateTime)
        {
            try
            {
                foreach (GenericSensor sensor in ParentLogger.Sensors.EnabledSensors)
                {
                    AddTimeStampToSensor(sensor, GetRawValueFor(sensor), dateTime, GetTimestampNoneComment());
                }
            }
            catch (Exception ex)
            {
                Log.Instance.WriteError("Fail in ExtractAndAdd", this, ex);
            }
        }

        protected abstract string GetTimestampNoneComment();

        protected virtual DateTime GetPacketTime()
        {
            return PacketTime.ToDateTime();
        }

        protected abstract long GetRawValue();

        protected long GetRawValueFor(GenericSensor sensor)
        {
            return sensor.SWGenerated ? 0 : GetRawValue();
        }

        #endregion
    }
}