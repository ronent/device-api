﻿using Base.DataStructures;
using Base.DataStructures.Logger;
using Base.Devices;
using Base.Devices.Management;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Configuration Settings For MicroX Devices.
    /// </summary>
    [Serializable]
    public abstract class MicroXSetupConfiguration : BaseLoggerSetupConfiguration, IMicroXSetupConfiguration, IBoomerangable
    {
        #region Fields
        public static readonly int AllowedSNLength = 8;
        public static readonly int AllowedCommentLength = 15;

        private bool testMode = false;

        #endregion
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXSetupConfiguration"/> class.
        /// </summary>
        public MicroXSetupConfiguration()
            :base()
        {
            Initialize();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        protected virtual void Initialize() 
        {
            Boomerang = new BoomerangConfiguration();        
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether boomerang is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [boomerang enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool BoomerangEnabled { get; set; }

        /// <summary>
        /// Gets or sets the boomerang configuration.
        /// </summary>
        /// <value>
        /// The boomerang.
        /// </value>
        public BoomerangConfiguration Boomerang { get; set; }

        /// <summary>
        /// Gets or sets the time of the timer start.
        /// </summary>
        /// <value>
        /// The timer start.
        /// </value>
        public DateTime TimerStart { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether timer run enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [timer run enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool TimerRunEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether cyclic mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cyclic mode]; otherwise, <c>false</c>.
        /// </value>
        public bool CyclicMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether push to run mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [push to run mode]; otherwise, <c>false</c>.
        /// </value>
        public bool PushToRunMode { get; set; }

        /// <summary>
        /// Gets or sets the UTC offset.
        /// </summary>
        /// <value>
        /// The UTC offset.
        /// </value>
        public sbyte UtcOffset { get; set; }

        /// <summary>
        /// Gets the device time.
        /// </summary>
        /// <value>
        /// The device time.
        /// </value>
        internal virtual DateTime DeviceTime { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [test mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
        /// </value>
        public bool TestMode 
        {
            get { return testMode; }
            set 
            {
                if (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.Producation)
                {
                    testMode = value;                     
                }
            }
        }

        #endregion
        #region Validation
        /// <summary>
        /// Validates the configuration and throws exception for error.
        /// </summary>
        /// <param name="device">The device.</param>
        internal virtual void ThrowIfInvalidFor(MicroXLogger device)
        {
            checkCommentLength(device);
            checkComment();
            checkSN(device);
            checkTimerRun();
            checkInterval();
            checkSensors(device);
            if (BoomerangEnabled)
                Boomerang.ThrowIfInvalid();

        }

        internal abstract void checkSensors(MicroXLogger device);

        internal MicroXLogger GetDummyDevice(MicroXLogger device)
        {
            var dummy = GenericDevice.DeepCopy<MicroXLogger>(device);
            adjustDummyStatus(dummy);

            return dummy;
        }

        internal virtual void adjustDummyStatus(MicroXLogger device)
        {
            device.Status.FahrenheitMode = FahrenheitMode;
        }

        /// <summary>
        /// Checks the interval.
        /// </summary>
        protected virtual void checkInterval()
        {
            ThrowIfFalse(Interval.TotalSeconds > 0, "Interval must by greater than 1");
            ThrowIfFalse(Interval.TotalSeconds < ushort.MaxValue, "Interval must by smaller than " + ushort.MaxValue);
        }

        /// <summary>
        /// Checks the length of the comment.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        private void checkCommentLength(MicroXLogger dummyDevice)
        {
            Comment = Comment.TrimStart(' ').TrimEnd(' ');
            ThrowIfFalse(Comment.Length <= MicroXSetupConfiguration.AllowedCommentLength && Comment.Length > 0, "Comment length is empty or exceeds device limit");
        }

        /// <summary>
        /// Validates the comment characters
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        private void checkComment()
        {
            ThrowIfFalse(validateComment(Comment), "Invalid ASCII character");
        }

        private bool validateComment(string comment)
        {
            foreach (char character in comment)
            {
                if (!(character >= 32 && character <= 125)
                    || character == 34 || character == 38 || character == 39 || character == 92)
                    return false;
            }

            return true;
        }

        /// <summary>
        /// Checks the length of the serial number.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        private void checkSN(MicroXLogger dummyDevice)
        {
            ThrowIfFalse(ValidSerialNumber, "Serial Number must contain only digits");
            ThrowIfFalse(SerialNumber.Length <= MicroXSetupConfiguration.AllowedSNLength, "Serial number length exceeds device limit");
        }

        /// <summary>
        /// Gets a value indicating whether [valid serial number].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid serial number]; otherwise, <c>false</c>.
        /// </value>
        internal bool ValidSerialNumber 
        { 
            get 
            {
                Int64 sn;
                return SerialNumber != null && SerialNumber != string.Empty && Int64.TryParse(SerialNumber, out sn); 
            } 
        }

        /// <summary>
        /// Checks the timer run.
        /// </summary>
        private void checkTimerRun()
        {
            ThrowIfFalse(!(TimerRunEnabled && PushToRunMode), "Can't set 'timer run' and 'push to run' together.");
            //checks if the timer start is not lower than the current time with the UTC offset
//            var dayLightSaving = 0;
//           if (TimeZoneInfo.Local.IsDaylightSavingTime(DateTime.Now))
            var datetimeOfUser = DateTime.UtcNow.AddHours(UtcOffset);
            ThrowIfFalse(!(TimerRunEnabled && TimerStart < datetimeOfUser), "Timer run can't be lower than now.");
//            ThrowIfFalse(!(TimerRunEnabled && TimerStart < DateTime.Now),"Timer run has invalid DateTime.");

        }

        /// <summary>
        /// Throws if false.
        /// </summary>
        /// <param name="result">if set to <c>true</c> [result].</param>
        /// <param name="msg">The MSG.</param>
        /// <exception cref="System.Exception"></exception>
        internal void ThrowIfFalse(bool result, string msg)
        {
            if (!result)
                throw new Exception(msg);
        }

        #endregion
        #region Equals
        /// <summary>
        /// Throws if not equals.
        /// </summary>
        /// <param name="statusConfiguration">The other.</param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException"></exception>
        internal void ThrowIfNotEquals(GenericConfiguration statusConfiguration)
        {
            var statusMembers = statusConfiguration.GetMembers();

            foreach (var setup in GetMembers())
            {
                var match = statusMembers.First(status => status.Item1 == setup.Item1);

                if (match.Item2 == null)
                {
                    if (match.Item2 != null)
                        throw new KeyNotFoundException(setup.Item1);
                }
                else if (match.Item2 is BoomerangConfiguration && !BoomerangEnabled)
                {
                    continue;
                }
                else if (match.Item2 is DateTime)
                {
                    if (match.Item1 == "TimerStart" && !TimerRunEnabled
                        || match.Item1 == "DeviceTime")
                        continue;
                    if (!areDatesSimilar(setup, match))
                        throwNotEqualException(setup, match);
                }
                else if (match.Item1 == "BatteryLevel" || (Base.Settings.Settings.Current.Name == Base.Settings.eEnvironment.User && match.Item1 == "SerialNumber"))
                    continue;
                else if (!match.Item2.Equals(setup.Item2))
                    throwNotEqualException(setup, match);
            }
        }

        /// <summary>
        /// Throws the not equal exception.
        /// </summary>
        /// <param name="setup">The member.</param>
        /// <param name="status">The match.</param>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">new Exception(Item1:  + match.Item2 + , Item2:  + member.Item2)</exception>
        /// <exception cref="System.Exception">Item1:  + match.Item2 + , Item2:  + member.Item2</exception>
        private static void throwNotEqualException(Tuple<string, object> setup, Tuple<string, object> status)
        {
            throw new KeyNotFoundException(setup.Item1, new Exception(string.Format("Setup: {0}{1}Status: {2}", setup.Item2, Environment.NewLine, status.Item2)));
        }

        /// <summary>
        /// Ares the dates similar.
        /// </summary>
        /// <param name="member">The member.</param>
        /// <param name="match">The match.</param>
        /// <returns></returns>
        private static bool areDatesSimilar(Tuple<string, object> member, Tuple<string, object> match)
        {
            DateTime status = (DateTime)match.Item2;
            DateTime setup = (DateTime)member.Item2;

            return (setup - status).TotalSeconds <= 1;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            try
            {
                ThrowIfNotEquals(obj as MicroXStatusConfiguration);
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}