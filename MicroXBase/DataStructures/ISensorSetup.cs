﻿using MicroXBase.Devices.Types;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Sensor Setup.
    /// </summary>
    public interface ISensorSetup
    {
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidFor(MicroXLogger dummyDevice);

        /// <summary>
        /// Throws if invalid device.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidDevice(MicroXLogger dummyDevice);

        /// <summary>
        /// Throws if invalid alarms.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        void ThrowIfInvalidAlarms(MicroXLogger dummyDevice);
    }
}
