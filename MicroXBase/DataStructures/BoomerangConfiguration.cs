﻿using Base.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using Auxiliary.Tools;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Boomerang Configuration.
    /// </summary>
    [Serializable]
    public sealed class BoomerangConfiguration: ICloneable
    {
        #region Consts
        /// <summary>
        /// The contact break byte.
        /// </summary>
        internal const byte CONTACT_BREAK_BYTE = 0xff;

        /// <summary>
        /// The contact break character.
        /// </summary>
        internal const char CONTACT_BREAK_CHAR = '?';

        /// <summary>
        /// The info break byte.
        /// </summary>
        internal const byte INFO_BREAK_BYTE = (byte)INFO_BREAK_CHAR;

        /// <summary>
        /// The info break character.
        /// </summary>
        internal const char INFO_BREAK_CHAR = ';';

        /// <summary>
        /// The no timezone.
        /// </summary>
        internal const byte NO_TIMEZONE = 0x1A;

        /// <summary>
        /// The max banK0 size.
        /// </summary>
        internal const int MAX_BANK0_SIZE = 61;

        /// <summary>
        /// The Celsius.
        /// </summary>
        const byte CELSIUS = 0x10;

        /// <summary>
        /// The Fahrenheit.
        /// </summary>
        const byte FAHRENHEIT = 0x11;


        /// <summary>
        /// The opcode size.
        /// </summary>
        const int OPCODE_SIZE = 1;

        /// <summary>
        /// The getset size.
        /// </summary>
        const int GETSET_SIZE = 1;

        /// <summary>
        /// The break size.
        /// </summary>
        const int BREAK_SIZE = 1;

        /// <summary>
        /// The bank count.
        /// </summary>
        const int BANK_COUNT = 2;

        /// <summary>
        /// The info byte field count.
        /// </summary>
        const int INFO_BYTE_FIELD_COUNT = 3;

        /// <summary>
        /// The default author
        /// </summary>
        const string DEFAULT_AUTHOR = "USER";

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="BoomerangConfiguration"/> class.
        /// </summary>
        internal BoomerangConfiguration()
        {
            initialize();
        }

        /// <summary>
        /// Initializes this instance.
        /// </summary>
        private void initialize()
        {
            Contacts = new List<BasicContact>();
            Comment = string.Empty;
            CelsiusMode = true;
        
        }
        #endregion
        #region Properties
        /// <summary>
        /// Gets the length of the allowed comment author.
        /// </summary>
        /// <value>
        /// The length of the allowed comment author.
        /// </value>
        public static int AllowedCommentAuthorLength
        {
            get
            {
                return MicroXBase.OpCodes.Management.MicroXOpCodeManager.GENERIC_REPORT_SIZE - 1 -
                    OPCODE_SIZE - GETSET_SIZE - 4 * BREAK_SIZE - INFO_BYTE_FIELD_COUNT;
            }
        }

        /// <summary>
        /// Gets the length of the allowed contacts.
        /// </summary>
        /// <value>
        /// The length of the allowed contacts.
        /// </value>
        public static int AllowedContactLength
        {
            //2 Banks contain up to 62 bytes
            get { return AllowedCommentAuthorLength * BANK_COUNT; }
        }

        /// <summary>
        /// Gets or sets the contacts.
        /// </summary>
        /// <value>
        /// The contacts.
        /// </value>
        public List<BasicContact> Contacts { get; set; }

        /// <summary>
        /// Gets or sets the comment.
        /// </summary>
        /// <value>
        /// The comment.
        /// </value>
        public string Comment { get; set; }

        /// <summary>
        /// Gets or sets the author.
        /// </summary>
        /// <value>
        /// The author.
        /// </value>
        public string Author { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [Celsius mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [Celsius mode]; otherwise, <c>false</c>.
        /// </value>
        public bool CelsiusMode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [display alarm levels].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [display alarm levels]; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayAlarmLevels { get; set; }

        /// <summary>
        /// Gets or sets the Celsius mode byte.
        /// </summary>
        /// <value>
        /// The Celsius mode byte.
        /// </value>
        internal byte CelsiusModeByte
        {
            get { return Convert.ToByte(CelsiusMode ? CELSIUS : FAHRENHEIT); }
            set { CelsiusMode = (value == CELSIUS); }
        }

        /// <summary>
        /// Gets or sets the display alarm levels byte.
        /// </summary>
        /// <value>
        /// The display alarm levels byte.
        /// </value>
        internal byte DisplayAlarmLevelsByte
        {
            get { return Convert.ToByte(DisplayAlarmLevels ? '1' : '0'); }
            set { DisplayAlarmLevels = (value == '1'); }
        }

        /// <summary>
        /// Gets a value indicating whether [valid].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [valid]; otherwise, <c>false</c>.
        /// </value>
        public bool Valid
        {
            get
            {
                try
                {
                    if (Author == string.Empty)
                        Author = DEFAULT_AUTHOR;

                    ThrowIfInvalid();
                    return true;
                }
                catch { return false; }
            }
        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the contact bytes.
        /// </summary>
        /// <returns></returns>
        internal byte[] GetContactBytes()
        {
            List<byte> Bytes = new List<byte>();
            System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
            if (Contacts != null)
                foreach (BasicContact Contact in Contacts)
                {
                    byte[] bytes = enc.GetBytes(Contact.EMail);
                    if (bytes.Length + Bytes.Count > AllowedContactLength)
                        break;
                    else
                    {
                        Bytes.AddRange(bytes);
                        Bytes.Add(CONTACT_BREAK_BYTE);
                    }
                }
            return Bytes.ToArray();
        }
        #endregion
        #region Validation
        /// <summary>
        /// Throws if invalid.
        /// </summary>
        /// <exception cref="System.Exception">
        /// Comment + Author string exceed allowed maximum:  + AllowedCommentAuthorLength
        /// or
        /// Boomerang contacts cannot be empty
        /// </exception>
        internal void ThrowIfInvalid()
        {
            if (Comment.Length + Author.Length > AllowedCommentAuthorLength)
                throw new Exception("Comment + Author string exceed allowed maximum: " + AllowedCommentAuthorLength);
            else if (GetContactBytes().Count() < 1)
                throw new Exception("Boomerang contacts cannot be empty");
            else 
            {
                foreach (var item in Contacts)
                {
                    try
                    {
                        var mail = new MailAddress(item.EMail);
                    }
                    catch (Exception)
                    {
                        throw new Exception("Boomerang email is not valid");
                    }
//                    if (!item.EMail.IsEmailValid())
//                        throw new Exception("Boomerang email is not valid");
                }
            }
        }

        #endregion
        #region Object Overrides
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            BoomerangConfiguration boomerang = obj as BoomerangConfiguration;
            if (boomerang != null)
                return this.GetHashCode() == boomerang.GetHashCode();

            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return (int)(
                GetContactsHashCode() +
                Comment.GetHashCode() +
                Author.GetHashCode() +
                CelsiusMode.GetHashCode() +
                DisplayAlarmLevels.GetHashCode());
        }

        /// <summary>
        /// Gets the contacts hash code.
        /// </summary>
        /// <returns></returns>
        private int GetContactsHashCode()
        {
            return (from contact in Contacts
                    select contact.GetHashCode()).Sum();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            StringBuilder str = new StringBuilder(Environment.NewLine);
            str.AppendLine("\tAuthor: " + Author);
            str.AppendLine("\tComment: " + Comment);
            str.AppendLine("\tCelsius: " + CelsiusMode);
            str.AppendLine("\tDisplay alarm levels: " + DisplayAlarmLevels);
            int index = 1;
            foreach(var contact in Contacts)
                str.AppendLine("\tContact #" + index++ + ": " + contact.EMail);
            
            return str.ToString();
        }

        #endregion
        #region IClonable
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        public object Clone()
        {
            return this.MemberwiseClone();
        }

        #endregion
    }
}
