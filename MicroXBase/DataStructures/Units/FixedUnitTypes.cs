﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Fixed Unit Types Supported By The Device.
    /// </summary>
    public enum ExternalSensorUnitEnum : byte { Custom = 0x00, mS, V, mA, pH, RH, C, F };

    /// <summary>
    /// Fixed Unit Types Class.
    /// </summary>
    public static class FixedUnitTypes
    {
        /// <summary>
        /// The unit names.
        /// </summary>
        private static readonly string[] unitNames = new string[]{"", "mS", "V", "mA", "pH", "RH", "°C", "°F"};

        /// <summary>
        /// Gets the default units.
        /// </summary>
        /// <value>
        /// The default units.
        /// </value>
        public static UserDefinedUnit[] DefaultUnits
        {
            get 
            {
                IEnumerable<ExternalSensorUnitEnum> array = Enum.GetValues(typeof(ExternalSensorUnitEnum)).Cast<ExternalSensorUnitEnum>();
                return (from unitEnum in array
                       select new UserDefinedUnit(unitEnum)).ToArray();
            }
        }

        /// <summary>
        /// Gets the specified type name.
        /// </summary>
        /// <param name="typeName">Name of the type.</param>
        /// <returns>External Sensor Unit</returns>
        public static ExternalSensorUnitEnum Get(string typeName)
        {
            int index = Array.FindIndex(unitNames, x=> x == typeName);
            return (ExternalSensorUnitEnum)index;
        }

        /// <summary>
        /// Gets the specified type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>string</returns>
        public static string Get(ExternalSensorUnitEnum type)
        {
            int index = (int)type;
            return unitNames[index];
        }
    }
}
