﻿using Base.Sensors.Units;
using System;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Custom Unit Type.
    /// </summary>
    [Serializable]
    public class UserDefinedUnit : IUnit
    {
        /// <summary>
        /// The type.
        /// </summary>
        private ExternalSensorUnitEnum type;

        /// <summary>
        /// Gets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the type.
        /// </summary>
        /// <value>
        /// The type.
        /// </value>
        public ExternalSensorUnitEnum Type
        {
            get { return type; }
            set
            {
                type = value;
                if (type != ExternalSensorUnitEnum.Custom)
                    this.Name = FixedUnitTypes.Get(type);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedUnit"/> class.
        /// </summary>
        internal UserDefinedUnit()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedUnit"/> class.
        /// </summary>
        /// <param name="type">The type.</param>
        internal UserDefinedUnit(ExternalSensorUnitEnum type)
        {
            Type = type;
        }

        #region Methods
        /// <summary>
        /// Sets the specified unit byte.
        /// </summary>
        /// <param name="unitByte">The unit byte.</param>
        public void Set(byte unitByte)
        {
            if (Enum.IsDefined(typeof(ExternalSensorUnitEnum), unitByte))
                Type = (ExternalSensorUnitEnum)unitByte;
            else
                Type = ExternalSensorUnitEnum.Custom;
        }

        #endregion
        #region Object Overrides
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            IUnit other = obj as IUnit;

            if (other != null)
                return GetHashCode() == other.GetHashCode();
                
            return false;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() + Type.GetHashCode();
        }

        /// <summary>
        /// Returns a <see cref="System.String" /> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return Type == ExternalSensorUnitEnum.Custom ? Type.ToString() : Name;
        }

        #endregion
    }
}
