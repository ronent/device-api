﻿using Base.DataStructures.Logger;
using System;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Status Configuration For MicroX Devices.
    /// </summary>
    [Serializable]
    public class MicroXStatusConfiguration : BaseLoggerStatusConfiguration, IMicroXSetupConfiguration
    {
        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXStatusConfiguration"/> class.
        /// </summary>
        internal MicroXStatusConfiguration()
            :base()
        {
            Initialize();
        }

        private void Initialize()
        {
            Boomerang = new BoomerangConfiguration();
        }

        #endregion
        #region Properties
        /// <summary>
        /// Gets the type of the device.
        /// </summary>
        /// <value>
        /// The type of the device.
        /// </value>
        public byte DeviceType { get; internal set; }

        #endregion
        #region IMicroXSetupConfiguration
        /// <summary>
        /// Gets the boomerang configuration.
        /// </summary>
        /// <value>
        /// The boomerang.
        /// </value>
        public BoomerangConfiguration Boomerang { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether boomerang is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [boomerang enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool BoomerangEnabled { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether cyclic mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cyclic mode]; otherwise, <c>false</c>.
        /// </value>
        public bool CyclicMode { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether push to run mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [push to run mode]; otherwise, <c>false</c>.
        /// </value>
        public bool PushToRunMode { get; internal set; }

        /// <summary>
        /// Gets a value indicating whether timer run enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [timer run enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool TimerRunEnabled { get; internal set; }

        /// <summary>
        /// Gets the time of the timer start.
        /// </summary>
        /// <value>
        /// The timer start.
        /// </value>
        public DateTime TimerStart { get; internal set; }

        /// <summary>
        /// Gets the UTC offset.
        /// </summary>
        /// <value>
        /// The UTC offset.
        /// </value>
        public sbyte UtcOffset { get; internal set; }

        /// <summary>
        /// Gets or sets a value indicating whether [test mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
        /// </value>
        public bool TestMode { get; internal set; }

        #endregion
    }
}
