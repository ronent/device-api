﻿
namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Boomerang Interface.
    /// </summary>
    public interface IBoomerangable
    {
        /// <summary>
        /// Gets or sets the boomerang.
        /// </summary>
        /// <value>
        /// The boomerang.
        /// </value>
        BoomerangConfiguration Boomerang { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether boomerang is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [boomerang enabled]; otherwise, <c>false</c>.
        /// </value>
        bool BoomerangEnabled { get; set; }
    }
}
