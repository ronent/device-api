﻿using System;
namespace MicroXBase.DataStructures
{
    interface IMicroXSetupConfiguration
    {
        /// <summary>
        /// Gets the boomerang configuration.
        /// </summary>
        /// <value>
        /// The boomerang.
        /// </value>
        BoomerangConfiguration Boomerang { get; }

        /// <summary>
        /// Gets a value indicating whether boomerang is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [boomerang enabled]; otherwise, <c>false</c>.
        /// </value>
        bool BoomerangEnabled { get; }

        /// <summary>
        /// Gets a value indicating whether cyclic mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [cyclic mode]; otherwise, <c>false</c>.
        /// </value>
        bool CyclicMode { get; }

        /// <summary>
        /// Gets a value indicating whether push to run mode is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [push to run mode]; otherwise, <c>false</c>.
        /// </value>
        bool PushToRunMode { get; }

        /// <summary>
        /// Gets a value indicating whether timer run enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [timer run enabled]; otherwise, <c>false</c>.
        /// </value>
        bool TimerRunEnabled { get; }

        /// <summary>
        /// Gets the time of the timer start.
        /// </summary>
        /// <value>
        /// The timer start.
        /// </value>
        DateTime TimerStart { get; }

        /// <summary>
        /// Gets the UTC offset.
        /// </summary>
        /// <value>
        /// The UTC offset.
        /// </value>
        sbyte UtcOffset { get; }

        /// <summary>
        /// Gets or sets a value indicating whether [test mode].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [test mode]; otherwise, <c>false</c>.
        /// </value>
        bool TestMode { get; }
    }
}
