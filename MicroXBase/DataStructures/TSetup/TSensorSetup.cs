﻿using Base.OpCodes;
using Base.Sensors.Management;
using MicroXBase.Devices.Types;
using System;

namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Temperature Sensor Setup.
    /// </summary>
    [Serializable]
    public class TSensorSetup : ISensorSetup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TSensorSetup"/> class.
        /// </summary>
        public TSensorSetup()
        {
            TemperatureAlarm = new Alarm();
            TemperatureEnabled = true;
        }

        #region Properties
        /// <summary>
        /// Gets or sets the alarm.
        /// </summary>
        /// <value>
        /// The temperature alarm.
        /// </value>
        public Alarm TemperatureAlarm { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether temperature sensor is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [temperature enabled]; otherwise, <c>false</c>.
        /// </value>
        public virtual bool TemperatureEnabled { get; set; }

        #endregion
        #region Methods
        /// <summary>
        /// Throws if invalid for.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        public virtual void ThrowIfInvalidFor(MicroXLogger dummyDevice)
        {
            ThrowIfInvalidDevice(dummyDevice);
            ThrowIfInvalidAlarms(dummyDevice);
        }

        /// <summary>
        /// Throws if invalid device.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        /// <exception cref="System.Exception">Device  + dummyDevice.GetType().Name +  doesn't support temperature sensor setup</exception>
        public virtual void ThrowIfInvalidDevice(MicroXLogger dummyDevice)
        {
            if (!(dummyDevice.Status is ITSensorSetup))
                throw new Exception("Device " + dummyDevice.GetType().Name + " doesn't support temperature sensor setup");
        }

        /// <summary>
        /// Throws if invalid alarms.
        /// </summary>
        /// <param name="dummyDevice">The dummy device.</param>
        public virtual void ThrowIfInvalidAlarms(MicroXLogger dummyDevice)
        {
            if (TemperatureEnabled && TemperatureAlarm.Enabled)
                TemperatureAlarm.ThrowIfNotValid(dummyDevice.Sensors.GetFixedByIndex(eSensorIndex.Temperature));
        }

        #endregion
        #region Equals
        /// <summary>
        /// Determines whether the specified <see cref="System.Object" }, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            TSensorSetup other = obj as TSensorSetup;
            if (other == null) return false;

            return TemperatureAlarm.Equals(other.TemperatureAlarm)
                && TemperatureEnabled == other.TemperatureEnabled;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Temperature Enabled: " + TemperatureEnabled + " " + TemperatureAlarm;
        }
        #endregion
    }
}
