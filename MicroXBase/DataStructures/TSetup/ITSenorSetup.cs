﻿
namespace MicroXBase.DataStructures
{
    /// <summary>
    /// Temperature Sensor.
    /// </summary>
    public interface ITSensorSetup
    {
        /// <summary>
        /// Gets the temperature sensor.
        /// </summary>
        /// <value>
        /// The temperature sensor.
        /// </value>
        TSensorSetup TemperatureSensor { get; }
    }
}
