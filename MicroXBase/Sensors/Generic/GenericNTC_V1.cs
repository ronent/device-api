﻿using Base.Sensors.Management;
using System;

namespace MicroXBase.Sensors.Generic
{
    /// <summary>
    /// NTC Sensor.
    /// </summary>
    [Serializable]
    public abstract class GenericNTC_V1 : Base.Sensors.Types.Temperature.ExternalNTC
    {
        #region Fields
        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <value>
        /// The value table.
        /// </value>
        public abstract UInt16[] ValueTable { get; }

        /// <summary>
        /// Gets the multiplication table.
        /// </summary>
        /// <value>
        /// The multiplication table.
        /// </value>
        public abstract double[] MultiplicationTable { get; }

        /// <summary>
        /// Gets the zero.
        /// </summary>
        /// <value>
        /// The zero.
        /// </value>
        public abstract ushort Zero { get; }

        /// <summary>
        /// Gets a value indicating whether sensor is USB runnable.
        /// </summary>
        /// <value>
        ///   <c>true</c> if [USB runnable]; otherwise, <c>false</c>.
        /// </value>
        public override bool USBRunnable
        {
            get { return true; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="GenericNTC_V1"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal GenericNTC_V1(SensorManager parent)
            : base(parent)
        {
        }

        #endregion    
        #region Methods
        /// <summary>
        /// Gets the sensor value using tables.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        protected decimal GetSensorValueUsingTables(long digitalValue)
        {
            int i;
            long diff;
            uint A, B;
            long fraction = 0, integer = 0;

            if (digitalValue < ValueTable[0])
            {
                digitalValue = ValueTable[0];
            }
            else if (digitalValue > ValueTable[ValueTable.Length - 1])
            {
                digitalValue = ValueTable[ValueTable.Length - 1];
            }

            for (i = 0; i < ValueTable.Length; i++)
            {
                B = ValueTable[i];

                if (digitalValue < B)
                {
                    A = ValueTable[i - 1];
                    diff = B - A;

                    if (i <= Math.Abs(Minimum))
                    {
                        integer = Convert.ToInt16(i - Math.Abs(Minimum) - 1);
                        if (diff > B - digitalValue)
                        {
                            A = GetNTCDiffTableValue(diff);
                            diff = B - digitalValue;
                            fraction = Convert.ToInt16(diff * A);
                            fraction /= 10;
                            if (fraction > 999)
                                fraction = 999;
                            fraction = 1000 - fraction;
                        }
                        else
                        {
                            fraction = 0;
                        }
                    }
                    else
                    {
                        integer = Convert.ToInt16(i - Math.Abs(Minimum) - 1);
                        B = GetNTCDiffTableValue(diff);
                        diff = digitalValue - A;
                        fraction = Convert.ToInt16(diff * B);
                        fraction /= 10;
                        if (fraction > 999) fraction = 999;

                    }
                    break;
                }
            }

            decimal fFraction = ((int)(fraction / 10)) / 100m;
            decimal Value = integer;
            Value += fFraction;

            return Value;

        }

        /// <summary>
        /// Gets the digital value using tables.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        protected UInt16 GetDigitalValueUsingTables(decimal sensorValue)
        {
            UInt16 result;
            double processedSensorValue = Convert.ToDouble(sensorValue) * 100;
            short arrayPosition = Convert.ToInt16((processedSensorValue + 4000) / 100f);
            if (processedSensorValue == 0)
                result = 0;
            else if (processedSensorValue < 0)
            {

                double delta = (-processedSensorValue) % 100 * 100 / MultiplicationTable[arrayPosition];
                int shift = delta > 0 ? 1 : 0;
                result = Convert.ToUInt16(ValueTable[arrayPosition + shift] - delta);
            }
            else
            {
                double delta = processedSensorValue % 100;
                delta = delta * (100f / MultiplicationTable[arrayPosition]);
                result = Convert.ToUInt16(ValueTable[arrayPosition] + delta);
            }

            return result;
        }

        /// <summary>
        /// Gets the NTC difference table value.
        /// </summary>
        /// <param name="Diff">The difference.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Diff cannot be 0</exception>
        private uint GetNTCDiffTableValue(long Diff)
        {
            if (Diff < 0)
                throw new Exception("Diff cannot be 0");
            else if (Diff == 0)
                return 0;
            else
                return (uint)((float)(1f / Diff) * 10000);
        }

        #endregion
        #region Implemented Methods
        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            return GetDigitalValueUsingTables(sensorValue);
        }

        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            return GetSensorValueUsingTables(digitalValue);
        }

        #endregion
    }
}
