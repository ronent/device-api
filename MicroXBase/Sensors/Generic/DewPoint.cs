﻿using Base.Sensors.Management;
using Base.Sensors.Samples;
using MicroXBase.Sensors.Management;
using System;

namespace MicroXBase.Sensors.Generic
{
    /// <summary>
    /// Dew Point Sensor.
    /// </summary>
    [Serializable]    
    public abstract class DewPoint : Base.Sensors.Types.Temperature.DewPoint
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent sensor manager.
        /// </summary>
        /// <value>
        /// The parent sensor manager.
        /// </value>
        new public MicroXSensorManager ParentSensorManager
        {
            get { return base.ParentSensorManager as MicroXSensorManager; }
            set { base.ParentSensorManager = value; }
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="DewPoint"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal DewPoint(SensorManager parent)
            : base(parent)
        {
        }

        #endregion
        #region Methods
        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        /// <exception cref="System.NotSupportedException"></exception>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            Sample temperature = ParentSensorManager.GetFixedByIndex(eSensorIndex.Temperature).LastSample;
            if (temperature != null)
            {
                Sample humidity = ParentSensorManager.GetFixedByIndex(eSensorIndex.Humidity).LastSample;

                decimal tempValue = ConvertToCelsiusIfRequired(temperature.Value);
                decimal value = (1.0m / ((1.0m / (tempValue + 273.0m)) - (Convert.ToDecimal(Math.Log(Convert.ToDouble(humidity.Value) / 100d)) / 5420.0m)) - 273.0m);
                return ConvertToCorrectUnit(value);
            }
            return (decimal)0;
        }
        #endregion
    }
}
