﻿using Base.Sensors.Management;
using System;

namespace MicroXBase.Sensors.Generic
{
    /// <summary>
    /// Humidity Sensor.
    /// </summary>
    [Serializable]
    public class Humidity : Base.Sensors.Types.Humidity
    {
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="Humidity"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal Humidity(SensorManager parent)
            : base(parent)
        {
        }

        #endregion
        #region Implemented Methods
        /// <summary>
        /// Gets the digital value.
        /// </summary>
        /// <param name="sensorValue">The sensor value.</param>
        /// <returns></returns>
        internal override long GetDigitalValue(decimal sensorValue)
        {
            long digitalValue;

            if (sensorValue < 57.769532m)
            {
                digitalValue = Convert.ToInt64((sensorValue * 4096 + 8192) / 143);
            }
            else
            {
                digitalValue = Convert.ToInt64((sensorValue * 4096 - 46288) / 111);
            }

            return digitalValue;
        }

        /// <summary>
        /// Gets the sensor value.
        /// </summary>
        /// <param name="digitalValue">The digital value.</param>
        /// <returns></returns>
        internal override decimal GetSensorValue(long digitalValue)
        {
            decimal sensorValue;

            if (digitalValue <= 1712)
            {
                sensorValue = (143 * digitalValue - 8192) / 4096.0m;
            }
            else
            {
                sensorValue = (111 * digitalValue + 46288) / 4096.0m;
            }

            return sensorValue;
        }

        #endregion
    }
}
