﻿using Base.Sensors.Management;

namespace MicroXBase.Sensors.Management
{
    /// <summary>
    /// Sensor Converter.
    /// </summary>
    internal static class Converter
    {
        /// <summary>
        /// To the type of the sensor.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns></returns>
        public static eSensorType[] ToSensorType(eSensorIndex index)
        {
            switch (index)
            {
                case eSensorIndex.Temperature:
                    return new eSensorType[] { eSensorType.DigitalTemperature, eSensorType.InternalNTC };
                case eSensorIndex.Humidity:
                    return new eSensorType[] { eSensorType.Humidity };
                case eSensorIndex.External1:
                    return new eSensorType[] 
                    { 
                        eSensorType.Current4_20mA,
                        eSensorType.Voltage0_10V,
                        eSensorType.ExternalNTC,
                        eSensorType.PT100,
                    };
                default:
                    return new eSensorType[] { };
            }
        }
    }
}
