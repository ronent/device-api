﻿using Base.Devices;
using Base.Devices.Management;
using Base.Devices.Management.EventsAndExceptions;
using Base.OpCodes;
using Base.Sensors.Calibrations;
using Base.Sensors.Management;
using Base.Sensors.Types;
using MicroXBase.DataStructures;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MicroXBase.Sensors.Management
{
    /// <summary>
    /// MicroX Sensor Manager.
    /// </summary>
    [Serializable]
    public abstract class MicroXSensorManager : SensorManager
    {
        #region Properties
        /// <summary>
        /// Gets or sets the parent logger.
        /// </summary>
        /// <value>
        /// The parent logger.
        /// </value>
        new internal MicroXLogger ParentLogger
        {
            get { return base.ParentLogger as MicroXLogger; }
            set { base.ParentLogger = value; }
        }

        /// <summary>
        /// Gets or sets the calibration.
        /// </summary>
        /// <value>
        /// The calibration.
        /// </value>
        public CalibrationConfiguration Calibration
        {
            get;
            protected set;
        }

        #endregion
        #region Constructors
        /// <summary>
        /// Initializes a new instance of the <see cref="MicroXSensorManager"/> class.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal MicroXSensorManager(GenericLogger parent)
            : base(parent)
        {

        }

        /// <summary>
        /// Initializes the specified parent.
        /// </summary>
        /// <param name="parent">The parent.</param>
        internal override void Initialize(GenericLogger parent)
        {
            base.Initialize(parent);

            Calibration = new CalibrationConfiguration(this);
            InitializeSensors();
            AssignCalibration();
        }

        #endregion
        #region Methods
        /// <summary>
        /// On status received.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The e.</param>
        internal override void ParentLogger_OnStatusReceived(object sender, ConnectionEventArgs<GenericDevice> e)
        {
            initializeSensors();
            AssignConfiguration();
        }

        /// <summary>
        /// Enables the detachable sensor.
        /// </summary>
        /// <param name="type">The type.</param>
        internal void EnableDetachable(eSensorType type)
        {
            GenericSensor sensor = GetDetachableByType(type);
            if (sensor != null)
            {
                DisableAllDetachables();
                sensor.Enabled = true;
            }
        }

        /// <summary>
        /// Disables all detachable sensors.
        /// </summary>
        internal void DisableAllDetachables()
        {
            var all = Items.FindAll(x => x.Index == eSensorIndex.External1).ToArray();

            foreach (var sensor in all)
                sensor.Enabled = false;
        }
      
        /// <summary>
        /// Assigns the calibration.
        /// </summary>
        private void AssignCalibration()
        {
            foreach (var entry in Calibration)
                if (Contains(entry.Key))
                    this[entry.Key].Calibration.Set(entry.Value);
                    //foreach (var sensor in this[entry.Key])
                    //    sensor.Calibration.Set(entry.Value);
        }

        /// <summary>
        /// Initializes the sensors.
        /// </summary>
        private void initializeSensors()
        {
            foreach (var sensor in Items)
                sensor.Initialize();
        }

        /// <summary>
        /// Assigns the configuration.
        /// </summary>
        internal abstract void AssignConfiguration();

        /// <summary>
        /// Assigns the fixed sensor.
        /// </summary>
        /// <param name="sensorIndex">Index of the sensor.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="alarm">The alarm.</param>
        internal void AssignFixedSensor(eSensorIndex sensorIndex, bool enabled, Alarm alarm)
        {
            GenericSensor sensor = GetFixedByIndex(sensorIndex);
            AssignFixedSensor(sensor, enabled, alarm);
        }

        /// <summary>
        /// Assigns the fixed sensor.
        /// </summary>
        /// <param name="sensor">The sensor.</param>
        /// <param name="enabled">if set to <c>true</c> [enabled].</param>
        /// <param name="alarm">The alarm.</param>
        internal void AssignFixedSensor(GenericSensor sensor, bool enabled, Alarm alarm)
        {
            if (sensor != null)
            {
                sensor.Enabled = enabled;
                if (sensor.Enabled)
                    sensor.Alarm.Assign(alarm);
            }
        }

        /// <summary>
        /// Determines whether [contains] [the specified sensor index].
        /// </summary>
        /// <param name="sensorIndex">Index of the sensor.</param>
        /// <returns></returns>
        private bool Contains(eSensorIndex sensorIndex)
        {
            return this[sensorIndex] != null;
        }

        /// <summary>
        /// Gets the reset calibration.
        /// </summary>
        /// <returns></returns>
        internal CalibrationConfiguration GetResetCalibration()
        {
            CalibrationConfiguration config = new CalibrationConfiguration(this);
            addResetConfigurationFor(config, eSensorIndex.Temperature);
            addResetConfigurationFor(config, eSensorIndex.Humidity);
            addResetConfigurationFor(config, eSensorIndex.External1);

            return config;
        }

        /// <summary>
        /// Adds the reset configuration for.
        /// </summary>
        /// <param name="config">The configuration.</param>
        /// <param name="index">The index.</param>
        private void addResetConfigurationFor(CalibrationConfiguration config, eSensorIndex index)
        {
            GenericSensor sensor = index < eSensorIndex.External1 ? GetFixedByIndex(index) : GetEnabledDetachable();
            CalibrationCoefficients coeff = sensor == null ? new CalibrationCoefficients() : sensor.GetDefaultCalibration();
            config.Add(index, coeff);
        }

        #endregion
    }
}
