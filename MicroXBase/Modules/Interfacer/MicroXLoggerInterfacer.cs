﻿using Base.Devices;
using Base.Devices.Management.DeviceManager;
using Base.Devices.Management.DeviceManager.HID;
using Base.Devices.Management.EventsAndExceptions;
using Base.Modules.Interfacer;
using Infrastructure.Communication;
using Log4Tech;
using MicroXBase.Devices.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MicroXBase.Modules.Interfacer
{
    internal abstract class MicroXDeviceInterfacer<T> : GenericDeviceInterfacer<T>, IHIDLoggerInterfacer where T : MicroXLogger
    {
        #region Properties
        public abstract List<DeviceID> DeviceIDs { get; }
        protected abstract byte Bits { get; }

        [Import]
        private IHIDDeviceManager parentDeviceManager;

        #endregion
        #region Constructor
        public MicroXDeviceInterfacer()
            :base()
        {

        }

        #endregion
        #region IDeviceInterfacer
        protected override IDeviceManager ParentDeviceManager
        {
            get 
            {
                return parentDeviceManager; 
            }
        }

        public override bool CanCreate(ConnectionEventArgs e)
        {
            if (!(e is HIDConnectionEventArgs))
                return false;

            return DeviceIDs.Any(x => x.Equals((e as HIDConnectionEventArgs).DeviceID));
        }

        public override bool CanCreateFrom(GenericDevice device)
        {
            return checkType(device as MicroXLogger);
        }

        protected override Type[] LookUpCandidateTypes(GenericDevice device)
        {
            Assembly asm = this.GetType().Assembly;
            try
            {
                return asm.GetTypes()
                    .Where((t) => isOfType(t) && isOfSubtype(t, (device as MicroXLogger).Status.DeviceType))
                    .ToArray();
            }
            catch { return null; }
        }

        #endregion
        #region Methods
        // uses only for ml2 with external sensor
        protected virtual Type GetSubType(byte subtype)
        {
            return typeof(T);
        }

        #endregion
        #region Private Methods
        private bool checkPrerequisites(MicroXLogger genericDevice)
        {
            return genericDevice.Status.PCBAssembly == Bits
                && genericDevice.Status.DeviceType == Type;
        }

        private bool checkType(MicroXLogger device)
        {
            return device.Status.DeviceType == Type;
        }

        private bool isOfSubtype(Type type, byte subtype)
        {
            return type.IsSubclassOf(GetSubType(subtype));
        }

        #endregion
    }
}