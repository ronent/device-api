﻿using Base.Modules.Interfacer;
using Infrastructure.Communication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroXBase.Modules.Interfacer
{
    internal interface IHIDLoggerInterfacer : IDeviceInterfacer
    {
        List<DeviceID> DeviceIDs { get; }
    }
}
